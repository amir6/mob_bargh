# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in D:\Android\SDK/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html
# Add any project specific keep options here:
#Sugar ORM
-dontwarn okio.**
-dontwarn com.parse.**
-dontwarn com.instabug.**
-dontwarn app.topak.pecco.com.**
-keep class com.andc.mobilebargh.Models.** { *; }
#Android
-keep class android.support.design.** { *; }
-keep class android.support.v7.** { *; }
-keep class android.support.v7.internal.view.menu.*MenuBuilder*, android.support.v7.** { *; }
#Crash Report
-renamesourcefileattribute SourceFile
-keepattributes *Annotation*
-keepattributes SourceFile,LineNumberTable
#
# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
