package com.andc.mobilebargh

import android.app.Activity
import android.app.Application
import android.content.Context
import android.os.Handler
import android.support.multidex.MultiDex
import android.support.multidex.MultiDexApplication
import android.support.v4.app.Fragment
import com.andc.mobilebargh.Injection.AppInjector
import com.andc.mobilebargh.Utility.SSLHelper
import com.orm.SugarContext
import com.parse.Parse
import com.parse.ParseInstallation
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.support.HasSupportFragmentInjector
import uk.co.chrisjenx.calligraphy.CalligraphyConfig
import javax.inject.Inject

class MobileBargh : Application(),HasActivityInjector {


    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>

   init {
      instance = this

    }

    override fun activityInjector(): AndroidInjector<Activity> {
       return activityInjector
    }

    override fun onCreate() {
        super.onCreate()
        applicationHandler = Handler(this.mainLooper)
        AppInjector.init(this)

        SugarContext.init(this)


        //Fabric.with(this, new Crashlytics());

        CalligraphyConfig.initDefault(CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/IRANSans(FaNum).ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        )

        // [Optional] Power your app with Local Datastore. For more info, go to
        // https://parse.com/docs/android/guide#local-datastore
        Parse.enableLocalDatastore(this)
        //Parse.initialize(this);
        //Parse.initialize(this, "qS11wStzYeo0Mkw6AYd5iKONc0rK2SFBg6UrYYdu", "CCawNDFfbqPqEhMTaJVpMExxoIdGI7kd3OeG8v2L");
        Parse.initialize(
                Parse.Configuration.Builder(this)
                        .server("http://85.17.22.12:1337/parse/")
                        .applicationId("qS11wStzYeo0Mkw6AYd5iKONc0rK2SFBg6UrYYdu")
                        .clientKey("CCawNDFfbqPqEhMTaJVpMExxoIdGI7kd3OeG8v2L")
                        .build()
        )
        ParseInstallation.getCurrentInstallation().saveInBackground()

        //FIXME Change This Function to check SSL Certificate Checking
        //SSLHelper.checkSSLCertificate();
        SSLHelper.verifyNullHostName()
        SSLHelper.disableSSLCertificateChecking()

    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
    companion object {
        private var instance: MobileBargh? = null
        private lateinit var applicationHandler: Handler
        @JvmStatic
        fun getContext(): Context {
            return instance!!.applicationContext
        }
        @JvmStatic
        fun getHandler(): Handler {
            return applicationHandler
        }


    }






}