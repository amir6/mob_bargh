package com.andc.mobilebargh.Services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.andc.mobilebargh.Activities.FragmentActivity;
import com.andc.mobilebargh.Activities.MainActivity;
import com.andc.mobilebargh.Controllers.PreferencesHelper;
import com.andc.mobilebargh.Fragments.SettingFragment;
import com.andc.mobilebargh.MobileBargh;
import com.andc.mobilebargh.Models.BranchInfoRecord;
import com.andc.mobilebargh.R;

import java.util.ArrayList;

import ir.smartlab.persindatepicker.util.PersianCalendar;

/**
 * Created by Tenkei on 6/6/2016.
 */
public class MyNotificationService extends Service {

    private Context mContext;

    // Sets an ID for the notification
    int mDeadlineReachedNotificationId = 001;
    private int msgCount;


    @Override
    public IBinder onBind(Intent arg0)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onCreate()
    {
        // TODO Auto-generated method stub
        super.onCreate();
        mContext = MobileBargh.getContext();
    }

    @SuppressWarnings("static-access")
    @Override
    public void onStart(Intent intent, int startId)
    {
        super.onStart(intent, startId);
        int remindMeBefore = SettingFragment.deadlineDays[PreferencesHelper.loadInt(PreferencesHelper.KEY_REMIND_ME_BEFORE)];

        //If User Selected Don't Notify Me Return
        //TODO Add a Special Notification On Day of Deadline
        if(remindMeBefore == 0)
            return;

        //Get Limit Date
        long limitMillis = System.currentTimeMillis() + remindMeBefore * 60 * 60 * 24 * 1000;
        String limitDate = new PersianCalendar(limitMillis).getPersianShortDate();
        String today = new PersianCalendar().getPersianShortDate();

        //Get Bills Info
        ArrayList<BranchInfoRecord> mRecords = (ArrayList<BranchInfoRecord>)BranchInfoRecord.listAll(BranchInfoRecord.class);
        if(mRecords!= null && mRecords.size()>0)
            for(BranchInfoRecord record:mRecords)
                //TODO If Shown a Special Notification On Day of Deadline Change today '<=' record.LstPayLimitDate to '<'
                if(today.compareTo(record.LstPayLimitDate) <= 0 &&
                        limitDate.compareTo(record.LstPayLimitDate) >= 0 &&
                        Integer.parseInt(record.CrDbTot) > 1000){
                    msgCount++;
                }

        //Return if No there is No Message to Show
        if(msgCount<1)
            return;

        //Create Notification Main Action
        Intent payIntent = new Intent(mContext, MainActivity.class);
        PendingIntent piPay = PendingIntent.getActivity(mContext, 0, payIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        //Create Notification Actions
        Intent settingIntent = new Intent(mContext, FragmentActivity.class);
        settingIntent.putExtra(FragmentActivity.EXTRA_FRAGMENT_TYPE, R.id.setting);
        settingIntent.setAction("Setting");

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(mContext);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addParentStack(FragmentActivity.class);
        stackBuilder.addNextIntent(settingIntent);

        PendingIntent settingPendingIntent = stackBuilder.getPendingIntent(
                0,
                PendingIntent.FLAG_UPDATE_CURRENT);

        //Create Notification
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext)
                .setSmallIcon(R.drawable.ic_credit_card_white_36dp)
                .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), R.mipmap.ic_launcher))
                .setDefaults(Notification.DEFAULT_ALL)
                .setContentTitle("خدمات مشترکین")
                .setContentText("مشترک گرامی مهلت شما جهت پرداخت قبض رو به پایان می باشد. ")
                .setContentIntent(piPay)
                .setStyle(new NotificationCompat.BigTextStyle().bigText("مشترک گرامی مهلت شما جهت پرداخت قبض رو به پایان می باشد. جهت پرداخت بر روی اینجا کلیک نمایید."))
                .addAction(R.drawable.ic_delete_grey600_24dp, "Disable Notification", settingPendingIntent)
                .setAutoCancel(true);

        msgCount++;
        if(msgCount > 1)
            mBuilder.setNumber(msgCount);

        NotificationManager nm = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify(mDeadlineReachedNotificationId, mBuilder.build());

        Log.i("ANDCServices", "Created Notification");
        stopSelf();
    }
}
