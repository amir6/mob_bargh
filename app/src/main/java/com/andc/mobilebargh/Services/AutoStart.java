package com.andc.mobilebargh.Services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by Esbati on 6/7/2016.
 */
public class AutoStart extends BroadcastReceiver {

    public void onReceive(Context context, Intent intent) {

        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            AlarmManagerHelper.registerWithAlarmManager(context);
        }
    }
}
