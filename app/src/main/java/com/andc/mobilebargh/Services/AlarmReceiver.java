package com.andc.mobilebargh.Services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by Esbati on 5/21/2016.
 */
public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        //Get Alarm Time
        //Calendar calendar = Calendar.getInstance();
        //int Hour = calendar.get(Calendar.HOUR_OF_DAY);

        Intent notificationService = new Intent(context, MyNotificationService.class);
        context.startService(notificationService);

        Log.i("ANDCServices", "Starting Notification Service");
    }
}
