package com.andc.mobilebargh.Services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.util.Calendar;

/**
 * Created by Esbati on 8/1/2016.
 */
public class AlarmManagerHelper {

    public static void registerWithAlarmManager(Context context){
        //Set Alarm Time
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 9);

        int interval = 1000 * 60 * 60 * 24;

        //Create Intent
        Intent myIntent = new Intent(context , AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, myIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        //Register Intent with Alarm Manager
        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), interval, pendingIntent);

        Log.i("ANDCServices", "Registered With Alarm Manager");
    }
}
