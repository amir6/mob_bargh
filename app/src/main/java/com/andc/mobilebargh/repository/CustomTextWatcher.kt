package com.andc.mobilebargh.Utility.Util

import android.support.v7.widget.AppCompatEditText
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import com.andc.mobilebargh.R

class CustomTextWatcher: TextWatcher {
    val mEditText: EditText




    constructor(editText: EditText){
        this.mEditText = editText
        mEditText.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
    }
    override fun afterTextChanged(s: Editable?) {
        if(s.isNullOrEmpty()){
            //  mButton.visibility = View.GONE
            mEditText.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)

        }
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {


    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        // mButton.visibility = View.VISIBLE

            if(s.toString().equals("0"))
                mEditText.setCompoundDrawablesWithIntrinsicBounds(0,0, 0,0)
            else
                mEditText.setCompoundDrawablesWithIntrinsicBounds(0,0, R.drawable.ic_action_clear,0)
    }
}