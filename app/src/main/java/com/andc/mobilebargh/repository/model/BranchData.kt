package com.andc.mobilebargh.repository.model

data class BranchData (

        var DistributorName: String = "",
        var RegionName: String= "",
        var BranchTypeCode: String = "",
        var OwnerName: String = "",
        var OwnerFamily: String = "",
        var OwnerTy: String = "",
        var CompanyName:String = "",
        var HomeAdderss: String = "",
        var HomePoNum: String = "",
        var LatitueX:String  = "",
        var LongitudeY:String = "",
        var Phs: String = "",
        var Amp: String = "",
        var PwrCnt: String = "",
        var voltcode:String = "",
        var TrfHCode:String = "",
        var TrfHCodeName:String = "",
        var BranchKindCode:String = "",
        var BranchKindCodeName:String = "",
        var BranchStatCode:String = "",
        var BranchSrl:String = "",
        var BranchCode:String = "",
        var IdentityCode:String = "",
        var CrDbTot:String ="",
        var CrDBBranchSale:String= "",
        var CrDbOtherService:String ="",
        var FabrikNumber : String = "",
        var ErrorCode : String = ""



)
