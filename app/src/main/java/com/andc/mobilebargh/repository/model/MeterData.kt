package com.andc.mobilebargh.repository.model

data class MeterData(

     var activeNormal : String = "0",
     var activePeak : String = "0",
     var activeLow : String = "0",
     var activeWeekend : String = "0",
     var reactiveNormal : String ="0",
     var demandVal : String = "0",
     var dateVal : String = ""

)
