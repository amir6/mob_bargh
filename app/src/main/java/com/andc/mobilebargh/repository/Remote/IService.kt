package com.andc.mobilebargh.repository.Remote


import android.database.DataSetObservable
import com.andc.mobilebargh.repository.model.BranchData
import com.andc.mobilebargh.repository.model.DataObject
import com.andc.mobilebargh.repository.model.JsonData
import com.andc.mobilebargh.repository.model.ResponseData
import io.reactivex.Observable
import okhttp3.FormBody
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface IService {


   @GET("adpmobile/api/CustomService/GetSimpleBranchDataView/{billId}")
   fun getBranchDetail(@Path("billId") billId: String):Observable<DataObject>

   @POST("adpmobile/api/PishkhanApi/AddServiceOrder")
   fun sendSalesService(@Body body: RequestBody):Observable<ResponseData>

   @POST("adpmobile/api/PishkhanApi/AddAdjustPaymentRequest")
   fun sendExamineInvoice(@Body body: RequestBody): Observable<ResponseData>

   @POST("adpmobile/api/PishkhanApi/AddSettlementOrder")
   fun sendsettlementService(@Body body: RequestBody): Observable<ResponseData>

   @POST("adpmobile/api/PishkhanApi/IssueCopyBill")
   fun getBillImageService(@Body body: RequestBody): Observable<String>

   @POST("adpmobile/api/PishkhanApi/GetPaymentReqByCustomerInfo")
   fun sendReadmeterByCustmer(@Body body: RequestBody): Observable<ResponseData>

   @POST("adpmobile/api/PishkhanApi/TariffChange")
   fun sendTarrifChange(@Body body: RequestBody):Observable<ResponseData>

   @POST( "adpmobile/api/PishkhanApi/EnquiryOrderInfo")
   fun getEnquiryOrder(@Body body: RequestBody):Observable<ResponseData>


}