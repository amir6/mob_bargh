package com.andc.mobilebargh.repository

import android.databinding.BindingAdapter
import android.text.TextWatcher
import android.widget.EditText
import com.andc.mobilebargh.R

class BindAttr {

    companion object {
        @JvmStatic
        @BindingAdapter("textChangedListener")
        fun textChange(editText: EditText, textWatcher: TextWatcher) {
           // editText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_action_clear, 0)
            editText.addTextChangedListener(textWatcher)

        }
    }

}