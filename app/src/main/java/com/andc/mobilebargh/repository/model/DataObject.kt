package com.andc.mobilebargh.repository.model

import org.json.JSONArray
import org.json.JSONObject

data class DataObject (

  var data: String,
  var error: Array<ErrorData>

)

data class JsonData(
        var StringData: String
)

data class ErrorData (

    var ErrorCode: Int,
    var ErrorMsg: String


)
data class BillResponse(

        var IMAGE_Format: String,
        var ErrorMsg: String,
        var PDF_Format: String,
        var OUT_PAYMENT_IDENTIFIER: String,
        var OUT_Bill_IDENTIFIER: String

)

data class ResponseData(
     var REF_CODE: String,
     var RefCode: String,
     var AUDIT_DATE: String,
     var ErrorCode : String,
     var Message: String




)