package com.andc.mobilebargh.Injection.Component

import android.app.Application
import com.andc.mobilebargh.Injection.Modules.ActivityBuilder
import com.andc.mobilebargh.Injection.Modules.AppModule
import com.andc.mobilebargh.MobileBargh
import dagger.BindsInstance
import dagger.Component
import dagger.Module
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AndroidInjectionModule::class, AndroidSupportInjectionModule::class, AppModule::class, ActivityBuilder::class))


    interface AppComponent {
        @Component.Builder
        interface Builder {
            @BindsInstance
            fun application(application: Application): Builder


            fun build(): AppComponent
        }

        fun inject(application: MobileBargh)
    }


