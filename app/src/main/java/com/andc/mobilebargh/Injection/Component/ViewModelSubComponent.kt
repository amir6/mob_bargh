package com.andc.mobilebargh.Injection.Component



import com.andc.mobilebargh.ViewModel.EnquiryVm
import com.andc.mobilebargh.ViewModel.InvoiceVm
import com.andc.mobilebargh.ViewModel.ReceiveBranchVm
import com.andc.mobilebargh.ViewModel.TariffVm
import dagger.Subcomponent

@Subcomponent
interface ViewModelSubComponent {

    @Subcomponent.Builder
     interface Builder{
        fun build(): ViewModelSubComponent

    }
    fun receiveBranchVm() : ReceiveBranchVm
    fun invoiceVm(): InvoiceVm
    fun tariffVm(): TariffVm
    fun enquiryVm(): EnquiryVm


}