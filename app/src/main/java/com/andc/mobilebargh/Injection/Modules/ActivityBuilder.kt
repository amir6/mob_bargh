package com.andc.mobilebargh.Injection.Modules

import com.andc.mobilebargh.Activities.*
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {



    @ContributesAndroidInjector(modules = [FragmentBuilderModule::class])
    abstract fun contributRequestActivity(): RequestActivity

    @ContributesAndroidInjector(modules = [FragmentBuilderModule::class])
    abstract fun contributeInvoiceActivity(): InvoiceActivity

     @ContributesAndroidInjector(modules = [FragmentBuilderModule::class])
    abstract fun contributeTariffActivity() : TarrifActivity

    @ContributesAndroidInjector(modules = [FragmentBuilderModule::class])
    abstract fun contributeEnquiryActivity(): EnquiryActivity
}