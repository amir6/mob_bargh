package com.andc.mobilebargh.Injection

import android.os.Bundle
import android.app.Activity
import android.app.Application
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.app.FragmentManager
import com.andc.mobilebargh.Injection.Component.DaggerAppComponent
import com.andc.mobilebargh.MobileBargh
import dagger.android.AndroidInjection
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.DaggerAppCompatActivity



object AppInjector {

      fun init(application: MobileBargh) {

         DaggerAppComponent.builder().application(application)
              //   .sharedPreferencesModule(SharedPreferencesModule(application))
                .build().inject(application)

        application
                .registerActivityLifecycleCallbacks(object : Application.ActivityLifecycleCallbacks {
                    override fun onActivityPaused(p0: Activity?) {

                    }

                    override fun onActivityResumed(p0: Activity?) {
                     }

                    override fun onActivityStarted(p0: Activity?) {
                     }

                    override fun onActivityDestroyed(p0: Activity?) {
                     }

                    override fun onActivitySaveInstanceState(p0: Activity?, p1: Bundle?) {
                     }

                    override fun onActivityStopped(p0: Activity?) {
                     }

                    override fun onActivityCreated(activity: Activity?, p1: Bundle?) {
                        activity?.let {
                            handleActivity(it)
                        }

                     }

                })
    }

    private fun handleActivity(activity: Activity?) {
        if (activity is DaggerAppCompatActivity) {
            AndroidInjection.inject(activity)
         }
        if (activity is FragmentActivity) {
            activity.supportFragmentManager
                    .registerFragmentLifecycleCallbacks(
                            object : FragmentManager.FragmentLifecycleCallbacks() {
                                override fun onFragmentCreated(fm: FragmentManager, fragment: Fragment, savedInstanceState: Bundle?) {
                                    if (fragment is Injectable) {
                                        AndroidSupportInjection.inject(fragment)
                                    }
                                }
                            }, true)
        }
    }
}