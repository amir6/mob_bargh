package com.andc.mobilebargh.Injection

import android.arch.lifecycle.ViewModel
import javax.inject.Inject
import android.arch.lifecycle.ViewModelProvider
import android.support.v4.util.ArrayMap
import com.andc.mobilebargh.Injection.Component.ViewModelSubComponent
import com.andc.mobilebargh.ViewModel.EnquiryVm
import com.andc.mobilebargh.ViewModel.InvoiceVm
import com.andc.mobilebargh.ViewModel.ReceiveBranchVm
import com.andc.mobilebargh.ViewModel.TariffVm
import java.util.concurrent.Callable
import javax.inject.Singleton




@Singleton
class ViewModelFactory @Inject
constructor(viewModelSubComponent: ViewModelSubComponent) : ViewModelProvider.Factory {
    private val creators: ArrayMap<Class<*>, Callable<out ViewModel>> = ArrayMap()

    init {
        // View models cannot be injected directly because they won't be bound to the owner's
        // view model scope.
        creators[ReceiveBranchVm::class.java] = Callable { viewModelSubComponent.receiveBranchVm()}
        creators[InvoiceVm::class.java] = Callable { viewModelSubComponent.invoiceVm()}
        creators[TariffVm::class.java] = Callable { viewModelSubComponent.tariffVm() }
        creators[EnquiryVm::class.java] = Callable { viewModelSubComponent.enquiryVm() }
    }

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        var creator = creators.get(modelClass)
        if (creator == null) {

            creators.forEach { entry ->
                if (modelClass.isAssignableFrom(entry.key)) {
                    creator = entry.value
                }

            }

        }
        if (creator == null) {
            throw IllegalArgumentException("Unknown model class $modelClass") as Throwable
        }
        try {
            return creator?.call() as T
        } catch (e: Exception) {
            throw RuntimeException(e)
        }

    }
}