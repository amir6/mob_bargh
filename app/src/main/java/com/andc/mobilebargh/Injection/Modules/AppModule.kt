package com.andc.mobilebargh.Injection.Modules

import android.app.Application
import android.arch.lifecycle.ViewModelProvider
import com.andc.mobilebargh.Controllers.SecurityHelper
import com.andc.mobilebargh.Injection.Component.ViewModelSubComponent
import com.andc.mobilebargh.Injection.ViewModelFactory
import com.andc.mobilebargh.repository.Remote.IService
import com.andc.mobilebargh.Utility.Util.CurrentBranchDeserilize
import com.andc.mobilebargh.repository.model.BranchData
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton
import okhttp3.OkHttpClient
import okhttp3.Response
import java.io.IOException
import java.util.concurrent.TimeUnit


@Module(subcomponents = arrayOf(ViewModelSubComponent::class))
class AppModule {
    @Provides
    @Singleton
    fun provideOkHttp(application: Application): OkHttpClient {
        val builder = OkHttpClient.Builder()
                .addInterceptor(object : Interceptor {
                    @Throws(IOException::class)
                    override fun intercept(chain: Interceptor.Chain): Response {
                        var request = chain.request()
                        val builder = request.newBuilder()
                                .addHeader("Accept", "application/json")
                                .addHeader("Token", SecurityHelper.getToken())

                        request = builder.build()
                        return chain.proceed(request)
                    }
                })

       /* if (BuildConfig.DEBUG) {
            builder.addNetworkInterceptor(StethoInterceptor())
        }*/

        // build this authenticator in the same way as all the other dependencies shown here

        builder.connectTimeout(60, TimeUnit.SECONDS)
        builder.readTimeout(60, TimeUnit.SECONDS)
        return builder.build()
    }
    fun buildGsonConverterFactory(): GsonConverterFactory {
        val gsonBuilder = GsonBuilder()

        gsonBuilder.registerTypeAdapter(BranchData::class.java, CurrentBranchDeserilize())
        return GsonConverterFactory.create(gsonBuilder.create())
    }

   @Singleton
   @Provides
   fun provideRetrofit(okHttp: OkHttpClient):Retrofit{

       return Retrofit.Builder()
               .baseUrl("https://mobilebargh.pec.ir/")
               .addConverterFactory(GsonConverterFactory.create())
               .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
               .client(okHttp)
               .build()


   }

    @Singleton
    @Provides
    fun provideService(retrofit: Retrofit): IService{

        return retrofit.create(IService::class.java)

    }


    @Singleton
    @Provides
    fun provideViewModelFactory(viewModelSubComponent: ViewModelSubComponent.Builder): ViewModelProvider.Factory {
        return ViewModelFactory(viewModelSubComponent.build())
    }



}


