package com.andc.mobilebargh.Injection.Modules


import com.andc.mobilebargh.Fragments.RequestFragments.EnquiryRequestFragment.EnquiryReceiveInfoFagment
import com.andc.mobilebargh.Fragments.RequestFragments.InvoiceFragment.*
import com.andc.mobilebargh.Fragments.RequestFragments.SupportRequestFragments.RequestReceiveInfoFragment
import com.andc.mobilebargh.Fragments.RequestFragments.SupportRequestFragments.SupportBranchInfoFragment
import com.andc.mobilebargh.Fragments.RequestFragments.SupportRequestFragments.SupportDocumentsFragment
import com.andc.mobilebargh.Fragments.RequestFragments.SupportRequestFragments.SupportTrackingFragment
import com.andc.mobilebargh.Fragments.RequestFragments.TariffChangeFragmemts.*
import com.andc.mobilebargh.Fragments.ServiceFragments.BaseFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
internal abstract class FragmentBuilderModule {


    @ContributesAndroidInjector
    abstract fun contributeBaseFragment(): BaseFragment
    @ContributesAndroidInjector
    abstract fun contributeRequestReceiveFragment(): RequestReceiveInfoFragment
    @ContributesAndroidInjector
    abstract fun contributeSupportBranchInfoFragment(): SupportBranchInfoFragment
    @ContributesAndroidInjector
    abstract fun contributeSupportDocumentsFragment(): SupportDocumentsFragment
    @ContributesAndroidInjector
    abstract fun contributeSupportTrackingFragment(): SupportTrackingFragment
    @ContributesAndroidInjector
    abstract fun contributeInvoiceReceiveInfoFragment(): InvoiceReceiveInfoFragment
    @ContributesAndroidInjector
    abstract fun contributeInvoiceBranchInfoFragment(): InvoiceBranchInfoFragment
    @ContributesAndroidInjector
    abstract fun contributeInvoiceDateFragment(): InvoiceDateFragment
    @ContributesAndroidInjector
    abstract fun contributeInvoiceDocumentFragment(): InvoiceDocumentFragment
    @ContributesAndroidInjector
    abstract fun contributeInvoiceMeterFragment(): InvoiceMeterFragment
    @ContributesAndroidInjector
    abstract fun contributeInvoiceTrackingFragment(): InvoiceTrackingFragment
    @ContributesAndroidInjector
    abstract fun contributeInvoiceIssueBillFragment(): InvoiceIssueBillFragment
    @ContributesAndroidInjector
    abstract fun contributeTarrifReceiveInfoFragment(): TarrifReceiveInfoFragment
    @ContributesAndroidInjector
    abstract fun contributeTariffChangeInfoFragment(): TariffChangeInfoFragment
    @ContributesAndroidInjector
    abstract fun contributeTariffBranchInfoFragments(): TariffBranchInfoFragments
    @ContributesAndroidInjector
    abstract fun contributeTariffDocumentFragment(): TariffDocumentFragment
    @ContributesAndroidInjector
    abstract fun contributeTariffTrackingFragment(): TariffTrackingFragment
    @ContributesAndroidInjector
    abstract fun contributeEnquiryReceiveInfoFagment(): EnquiryReceiveInfoFagment


}
