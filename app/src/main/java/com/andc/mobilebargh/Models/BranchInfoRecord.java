package com.andc.mobilebargh.Models;

import com.orm.SugarRecord;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by win on 11/17/2015.
 */
public class BranchInfoRecord extends SugarRecord {


    public static final HashMap<String, String> mProviders;
    public static final HashMap<String, String> mProvidersWebsite;
    static {
        mProviders = new HashMap<>();
        mProviders.put("10", "برق منطقه ای آذربايجان");
        mProviders.put("11", "توزيع نيروی برق تبريز");
        mProviders.put("12", "توزيع نيروی برق آذربايجان شرقی");
        mProviders.put("13", "توزيع نيروی برق آذربايجان غربی");
        mProviders.put("14", "توزيع نيروی برق اردبيل ");
        mProviders.put("20", "برق منطقه ای اصفهان");
        mProviders.put("21", "توزيع نيروی برق شهرستان اصفهان");
        mProviders.put("22", "توزيع نيروی برق استان اصفهان");
        mProviders.put("23", "توزيع نيروی برق  چهارمحال بختياری");
        mProviders.put("30", "برق منطقه ای باختر");
        mProviders.put("31", "توزيع نيروی برق استان مرکزی");
        mProviders.put("32", "توزيع نيروی برق استان همدان");
        mProviders.put("33", "توزيع نيروی برق استان لرستان");
        mProviders.put("40", "شرکت برق منطقه ای تهران");
        mProviders.put("41", "توزيع نيروی برق تهران بزرگ");
        mProviders.put("42", " توزیع نیروی برق استان تهران");
        mProviders.put("43", "توزيع نيروي برق استان البرز");
        mProviders.put("44", "توزیع نیروی برق استان قم");
        mProviders.put("50", "برق منطقه ای خراسان");
        mProviders.put("51", "توزيع نيروی برق شهرستان مشهد");
        mProviders.put("52", "توزيع نيروي برق استان خراسان رضوي");
        mProviders.put("53", "توزيع نيروي برق استان خراسان شمالي");
        mProviders.put("54", "توزيع نيروي برق خراسان جنوبي");
        mProviders.put("60", "برق منطقه ای خوزستان");
        mProviders.put("61", "توزيع نيروی برق شهرستان اهواز");
        mProviders.put("62", "توزيع نيروی برق استان خوزستان ");
        mProviders.put("63", "توزيع نيروی برق کهگيلويه و بوير احمد");
        mProviders.put("70", "برق منطقه ای زنجان");
        mProviders.put("71", "توزيع نيروی برق استان زنجان");
        mProviders.put("72", "توزيع نيروی برق استان قزوين");
        mProviders.put("80", "برق منطقه ای سمنان");
        mProviders.put("81", "توزيع نيروی برق استان سمنان");
        mProviders.put("90", "برق منطقه ای سيستان");
        mProviders.put("91", "توزيع نيروی برق استان سيستان و بلوچستان");
        mProviders.put("100", "برق منطقه ای غرب");
        mProviders.put("101", "توزيع نيروی برق استان کرمانشاه");
        mProviders.put("102", "توزيع نيروی برق استان کردستان");
        mProviders.put("103", "توزيع نيروی برق استان ايلام");
        mProviders.put("110", "برق منطقه ای فارس");
        mProviders.put("111", "توزيع نيروی برق شهرستان شيراز");
        mProviders.put("112", "توزيع نيروی برق استان فارس");
        mProviders.put("113", "توزيع نيروی برق استان بوشهر");
        mProviders.put("120", "برق منطقه ای کرمان");
        mProviders.put("121", "توزيع نيروی برق شمال استان کرمان");
        mProviders.put("122", "توزيع نيروی برق جنوب استان کرمان ");
        mProviders.put("130", "برق منطقه ای گيلان");
        mProviders.put("131", "توزيع نيروی برق استان گيلان");
        mProviders.put("140", "برق منطقه ای مازندران");
        mProviders.put("141", "توزيع نيروي برق مازندران");
        mProviders.put("142", "توزيع نيروی برق غرب مازندران");
        mProviders.put("143", "توزيع برق گلستان");
        mProviders.put("150", "برق منطقه ای هرمزگان");
        mProviders.put("151", "توزيع نيروی برق استان هرمزگان");
        mProviders.put("160", "برق منطقه ای يزد");
        mProviders.put("161", "توزيع نيروی برق استان يزد");


        mProvidersWebsite = new HashMap<>();
        mProvidersWebsite.put("10", "");
        mProvidersWebsite.put("11", "http://www.toziehtabriz.co.ir/");
        mProvidersWebsite.put("12", "http://www.ezepdico.ir/");
        mProvidersWebsite.put("13", "http://www.waepd.ir/");
        mProvidersWebsite.put("14", "http://www.aped.ir/");
        mProvidersWebsite.put("20", "http://www.erec.co.ir/");
        mProvidersWebsite.put("21", "http://www.eepdc.ir/");
        mProvidersWebsite.put("22", "http://www.eepdc.ir/");
        mProvidersWebsite.put("23", "http://www.chb-edc.ir/");
        mProvidersWebsite.put("30", "http://www.brec.ir/");
        mProvidersWebsite.put("31", "http://www.mpedc.ir/");
        mProvidersWebsite.put("32", "http://www.edch.ir/");
        mProvidersWebsite.put("33", "http://www.barghlorestan.ir/");
        mProvidersWebsite.put("40", "http://www.trec.co.ir/");
        mProvidersWebsite.put("41", "http://www.tbtb.co.ir/");
        mProvidersWebsite.put("42", "http://www.tvedc.ir/");
        mProvidersWebsite.put("43", "http://www.wtppdc.ir/");
        mProvidersWebsite.put("44", "http://www.qepd.co.ir/");
        mProvidersWebsite.put("50", "http://www.krec.ir/");
        mProvidersWebsite.put("51", "http://www.meedc.net/");
        mProvidersWebsite.put("52", "http://www.kedc.ir/");
        mProvidersWebsite.put("53", "http://www.nkedc.ir/");
        mProvidersWebsite.put("54", "http://www.skedc.ir/");
        mProvidersWebsite.put("60", "http://kzrec.co.ir/");
        mProvidersWebsite.put("61", "http://www.aepdco.ir/");
        mProvidersWebsite.put("62", "http://www.kepdc.co.ir/");
        mProvidersWebsite.put("63", "http://www.kbepdco.ir/");
        mProvidersWebsite.put("70", "http://www.zrec.co.ir/");
        mProvidersWebsite.put("71", "http://www.zedc.ir/");
        mProvidersWebsite.put("72", "http://new.qazvin-ed.co.ir/");
        mProvidersWebsite.put("80", "http://semrec.co.ir/");
        mProvidersWebsite.put("81", "http://www.semepd.ir/");
        mProvidersWebsite.put("90", "http://www.sbrec.co.ir/");
        mProvidersWebsite.put("91", "http://www.sbedc.ir/");
        mProvidersWebsite.put("100", "http://www.ghrec.co.ir/default.aspx");
        mProvidersWebsite.put("101", "http://www.kpedc.ir/");
        mProvidersWebsite.put("102", "http://www.kurdelectric.ir/");
        mProvidersWebsite.put("103", "http://www.bargh-ilam.ir/");
        mProvidersWebsite.put("110", "http://www.frec.co.ir/");
        mProvidersWebsite.put("111", "http://www.shirazedc.co.ir/");
        mProvidersWebsite.put("112", "http://www.farsedc.ir/");
        mProvidersWebsite.put("113", "http://www.bedc.ir/");
        mProvidersWebsite.put("120", "http://www.krec.co.ir/");
        mProvidersWebsite.put("121", "http://www.nked.co.ir/");
        mProvidersWebsite.put("122", "http://www.sked.co.ir/");
        mProvidersWebsite.put("130", "http://gilrec.co.ir/");
        mProvidersWebsite.put("131", "http://www.gilanpdc.ir/");
        mProvidersWebsite.put("140", "http://www.mazrec.co.ir/");
        mProvidersWebsite.put("141", "http://www.maztozi.ir/");
        mProvidersWebsite.put("142", "http://www.bargh-gmaz.ir/");
        mProvidersWebsite.put("143", "http://www.golestantb.ir/");
        mProvidersWebsite.put("150", "http://www.hrec.co.ir/");
        mProvidersWebsite.put("151", "http://www.hedc.co.ir/");
        mProvidersWebsite.put("160", "http://www.yrec.co.ir/");
        mProvidersWebsite.put("161", "http://www.yed.co.ir/");
    }


    //Data Base Labels
    public final static String LABEL_BILL_ID = "Bill_id";

    //JSON Tags
    public final static String TAG_Owner_Given_Name = "OwnerName";
    public final static String TAG_Owner_Family_Name = "OwnerFamily";
    public final static String TAG_Phs = "Phs";
    public final static String TAG_Amp = "Amp";
    public final static String TAG_PwrCnt = "PwrCnt";
    public final static String TAG_isDemand = "isDemand";
    public final static String TAG_TrfHCode = "TrfHCode";
    public final static String TAG_FabrikNumber = "FabrikNumber";
    public final static String TAG_Adress = "Adress";
    public final static String TAG_BillId = "BillId";
    public final static String TAG_PaymentId = "PaymentId";
    public final static String TAG_CrDbTot = "CrDbTot";
    public final static String TAG_LstPayLimitDate = "LstPayLimitDate";
    public final static String TAG_BillIdBranchSale = "BillIdBranchSale";
    public final static String TAG_PaymentIdDBranchSale = "PaymentIdDBranchSale";
    public final static String TAG_CrDBBranchSale = "CrDBBranchSale";
    public final static String TAG_PaymentIdOtherService = "PaymentIdOtherService";
    public final static String TAG_BillIdOtherService = "BillIdOtherService";
    public final static String TAG_CrDbOtherService = "CrDbOtherService";

    //Variables
    public String OwnerName;
    public String Phs;
    public String Amp;
    public String PwrCnt;
    public boolean isDemand;
    public String TrfHCode;
    public String FabrikNumber;
    public String Adress;
    public String BillId;
    public String PaymentId;
    public String CrDbTot;
    public String LstPayLimitDate;
    public String BillIdBranchSale;
    public String PaymentIdDBranchSale;
    public String CrDBBranchSale;
    public String PaymentIdOtherService;
    public String BillIdOtherService;
    public String CrDbOtherService;


    public BranchInfoRecord(){
        this.OwnerName = "";
        this.Phs = "";
        this.Amp = "";
        this.PwrCnt = "";
        this.TrfHCode = "";
        this.FabrikNumber = "";
        this.Adress = "";
        this.BillId = "";
        this.PaymentId = "";
        this.CrDbTot = "";
        this.LstPayLimitDate = "";
        this.BillIdBranchSale = "";
        this.PaymentIdDBranchSale = "";
        this.CrDBBranchSale = "";
        this.PaymentIdOtherService = "";
        this.BillIdOtherService = "";
        this.CrDbOtherService = "";
    }

    public boolean isDemand(String TrfCode){
        /*TrfCode = TrfCode.substring(1);
        if(this.Phs=="3" && this.Amp=="50") {
            return true;
        } else {
            if(TrfCode=="1" || Double.parseDouble(this.PwrCnt) <= 30 )
                return false;
            else
                return true;
        }*/
        //ak
        if (Double.parseDouble(this.PwrCnt) >= 30){
            return true;
        }else{
            if (Double.parseDouble(this.Amp) >0){
                if ((Double.parseDouble(this.Phs)*Double.parseDouble(this.Amp)/5) < 30 ){
                    return false ;
                }else{
                    return true ;
                }
            }
            else {
                isDemand(TrfCode);
            }
        }
        return false ;
    }

    public BranchInfoRecord fromJSON(JSONObject branchInfoJSON) throws JSONException {

        this.Phs = branchInfoJSON.getString(BranchInfoRecord.TAG_Phs);
        this.Amp = branchInfoJSON.getString(BranchInfoRecord.TAG_Amp);
        this.PwrCnt = branchInfoJSON.getString(BranchInfoRecord.TAG_PwrCnt);
        this.TrfHCode = branchInfoJSON.getString(BranchInfoRecord.TAG_TrfHCode);
        this.FabrikNumber = branchInfoJSON.getString(BranchInfoRecord.TAG_FabrikNumber);
        this.Adress = branchInfoJSON.getString(BranchInfoRecord.TAG_Adress);
        this.BillId = branchInfoJSON.getString(BranchInfoRecord.TAG_BillId);
        this.PaymentId = branchInfoJSON.getString(BranchInfoRecord.TAG_PaymentId);
        this.CrDbTot = branchInfoJSON.getString(BranchInfoRecord.TAG_CrDbTot);
        this.LstPayLimitDate = branchInfoJSON.getString(BranchInfoRecord.TAG_LstPayLimitDate);
        this.BillIdBranchSale = branchInfoJSON.getString(BranchInfoRecord.TAG_BillIdBranchSale);
        this.PaymentIdDBranchSale = branchInfoJSON.getString(BranchInfoRecord.TAG_PaymentIdDBranchSale);
        this.CrDBBranchSale = branchInfoJSON.getString(BranchInfoRecord.TAG_CrDBBranchSale);
        this.PaymentIdOtherService = branchInfoJSON.getString(BranchInfoRecord.TAG_PaymentIdOtherService);
        this.BillIdOtherService = branchInfoJSON.getString(BranchInfoRecord.TAG_BillIdOtherService);
        this.CrDbOtherService = branchInfoJSON.getString(BranchInfoRecord.TAG_CrDbOtherService);

        //Set Owner Name
        String ownerGivenName = branchInfoJSON.getString(BranchInfoRecord.TAG_Owner_Given_Name);
        String ownerFamilyName = branchInfoJSON.getString(BranchInfoRecord.TAG_Owner_Family_Name);
        if(ownerGivenName==null || ownerGivenName.equalsIgnoreCase("")){
            this.OwnerName = ownerFamilyName;
        } else{
            this.OwnerName = ownerGivenName + " " + ownerFamilyName;
        }

        //Set Branch Type
        String TrfCode = branchInfoJSON.getString("TrfCode");
        this.isDemand = isDemand(TrfCode);

        return this;
    }
}

