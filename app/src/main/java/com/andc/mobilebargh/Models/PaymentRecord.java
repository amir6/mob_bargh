package com.andc.mobilebargh.Models;

import android.util.Log;

import com.andc.mobilebargh.Utility.BillAPI.Shenase;
import com.orm.SugarRecord;

import org.json.JSONObject;

/**
 * Created by Esbati on 1/31/2016.
 */
public class PaymentRecord extends SugarRecord {

    //Static Variables
    public final static String EXTRA_RECEIPT_DATE = "1500/12/30";
    public final static String EXTRA_BANK_CODE = "54";

    //JSON Tags
    public final static String TAG_PAYMENT_Bill_Id = "BillID";
    public final static String TAG_PAYMENT_Payment_Id = "PaymentID";
    public final static String TAG_PAYMENT_Status = "Status";
    public final static String TAG_PAYMENT_Invoice_Number = "InvoiceNumber";
    public final static String TAG_PAYMENT_Trace_Number = "TraceNo";
    public final static String TAG_PAYMENT_Score = "Score";
    public final static String TAG_PAYMENT_Date = "ReceiptDate";

    //DB LABELS
    public final static String LABEL_PAYMENT_Bill_Id = "m_bill_id";


    public String mBillId;
    public String mPaymentId;
    public String mPayDate;
    public byte mStatus;
    public long mInvoiceNumber;
    public int mTraceNo;
    public int mScore;

    //Flag
    public boolean isRegistered;

    public PaymentRecord(){
        isRegistered = false;
    }

    public void setBillInfo(String billId, String paymentId){
        this.mBillId = billId;
        this.mPaymentId = paymentId;
    }

    public void setPaymentInfo(byte status, long invoiceNumber,
                               int traceNo, int score, String receiptDate){
        this.mStatus =  status;
        this.mInvoiceNumber =  invoiceNumber;
        this.mTraceNo = traceNo;
        this.mScore = score;
        this.mPayDate = receiptDate;
    }

    public boolean equals(BillingHistoryRecord billingHR){
        int paidAmount = Shenase.getAmount(mPaymentId);

        //Two Record Are equals if Payment Amount and Bank Code Match
        return paidAmount == billingHR.rcptamt
                && Integer.parseInt(billingHR.bankcode) == 54;
    }

    //Convert Record to Billing History Record
    public BillingHistoryRecord toBillingHistory(){
        BillingHistoryRecord mTemporaryBillingHistory = new BillingHistoryRecord(
                mBillId, mPayDate,
                EXTRA_RECEIPT_DATE, Shenase.getAmount(mPaymentId),
                EXTRA_BANK_CODE, "",
                "" + mTraceNo);
        mTemporaryBillingHistory.isTemporary = true;
        return mTemporaryBillingHistory;
    }

    //Covert Record to JSON
    public JSONObject toJSON(){
        JSONObject paymentJSON = new JSONObject();
        try {
            paymentJSON.put(LABEL_PAYMENT_Bill_Id, mBillId);
            paymentJSON.put(TAG_PAYMENT_Payment_Id, mPaymentId);
            paymentJSON.put(TAG_PAYMENT_Status, mStatus);
            paymentJSON.put(TAG_PAYMENT_Invoice_Number, mInvoiceNumber);
            paymentJSON.put(TAG_PAYMENT_Trace_Number, mTraceNo);
            paymentJSON.put(TAG_PAYMENT_Score, mScore);
            paymentJSON.put(TAG_PAYMENT_Date, mPayDate);

            return paymentJSON;
        } catch (Exception e){
            Log.e("ERROR", e.getMessage(), e);
            return null;
        }
    }
}
