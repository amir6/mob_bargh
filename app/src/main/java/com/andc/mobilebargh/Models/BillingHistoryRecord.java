package com.andc.mobilebargh.Models;

import com.orm.SugarRecord;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by win on 11/17/2015.
 */
public class BillingHistoryRecord extends SugarRecord {

    public static final HashMap<String, String> mBanks;
    static {
        mBanks = new HashMap<>();

        mBanks.put("11","بانک صنعت و معدن");
        mBanks.put("12","بانک ملت");
        mBanks.put("13","بانک رفاه");
        mBanks.put("14","بانک مسکن");
        mBanks.put("15","بانک سپه");
        mBanks.put("16","بانک کشاورزی");
        mBanks.put("17","بانک ملی");
        mBanks.put("18","بانک تجارت");
        mBanks.put("19","بانک صادرات");
        mBanks.put("20","توسعه صادرات");
        mBanks.put("21","پست بانک");
        mBanks.put("22","توسعه تعاون");
        mBanks.put("51","موسسه توسعه اعتباری");
        mBanks.put("53","کار آفرین");
        mBanks.put("54","بانک پارسیان");
        mBanks.put("55","بانک اقتصاد نوین");
        mBanks.put("56","بانک سامان");
        mBanks.put("57","پاسارگاد");
        mBanks.put("58","بانک سرمایه");
        mBanks.put("59","بانک سينا");
        mBanks.put("60","بانک قرض الحسنه مهر");
        mBanks.put("61","بانک شهر");
        mBanks.put("62","بانك تات");
        mBanks.put("63","بانک انصار");
        mBanks.put("101","انصار المجاهدين");
    }

    //Data Base Labels
    public final static String LABEL_BILL_ID = "billid";

    //JSON Tags
    public final static String TAG_billid = "billid";
    public final static String TAG_FPayDate = "FPayDate";
    public final static String TAG_FrcptDate = "FrcptDate";
    public final static String TAG_rcptamt = "rcptamt";
    public final static String TAG_bankcode = "bankcode";
    public final static String TAG_rcptdatetime = "rcptdatetime";
    public final static String TAG_traceNo = "refCode";

    public String billid;
    public String FPayDate;
    public String FrcptDate;
    public int rcptamt;
    public String bankcode;
    public String rcptdatetime;
    public String traceNo;

    //Flag
    public boolean isTemporary;

    public BillingHistoryRecord(){
        this.billid = "";
        this.FPayDate = "";
        this.FrcptDate = "";
        this.rcptamt = -1;
        this.bankcode = "";
        this.rcptdatetime = "";
        this.traceNo = "";
        this.isTemporary = false;
    }

    public BillingHistoryRecord(String billid, String FPayDate,
                                String FrcptDate, int rcptamt,
                                String bankcode, String rcptdatetime,
                                String traceNo){
        this.billid = billid;
        this.FPayDate = FPayDate;
        this.FrcptDate = FrcptDate;
        this.rcptamt = rcptamt;
        this.bankcode = bankcode;
        this.rcptdatetime = rcptdatetime;
        this.traceNo = traceNo;
    }

    public BillingHistoryRecord fromJSON(JSONObject billingHistoryJSON) throws JSONException {

        this.billid = billingHistoryJSON.getString(BillingHistoryRecord.TAG_billid);
        this.FPayDate = billingHistoryJSON.getString(BillingHistoryRecord.TAG_FPayDate);
        this.FrcptDate = billingHistoryJSON.getString(BillingHistoryRecord.TAG_FrcptDate);
        this.rcptamt = billingHistoryJSON.getInt(BillingHistoryRecord.TAG_rcptamt);
        this.bankcode = billingHistoryJSON.getString(BillingHistoryRecord.TAG_bankcode);
        this.rcptdatetime = billingHistoryJSON.getString(BillingHistoryRecord.TAG_rcptdatetime);
        this.traceNo = billingHistoryJSON.getString(BillingHistoryRecord.TAG_traceNo);

        return this;
    }

    public static ArrayList<BillingHistoryRecord> parseJSON(JSONArray billingHistoriesJSON) throws JSONException {
        ArrayList<BillingHistoryRecord> billingHistories = new ArrayList<>();

        for(int i = 0 ; i < billingHistoriesJSON.length() ; i++){
            BillingHistoryRecord billingHistory = new BillingHistoryRecord();
            billingHistory.fromJSON(billingHistoriesJSON.getJSONObject(i));
            billingHistories.add(billingHistory);
        }

        return billingHistories;
    }

}
