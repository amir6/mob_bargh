package com.andc.mobilebargh.Models;

import com.andc.mobilebargh.R;

/**
 * Created by Esbati on 12/23/2015.
 */
public class IntroductionItems {

    public enum Items {

        FEATURE1(R.drawable.ic_feature1, R.string.features_feature1_title, R.string.features_feature1_description),
        FEATURE2(R.drawable.ic_feature2, R.string.features_feature2_title, R.string.features_feature2_description),
        FEATURE3(R.drawable.ic_feature3, R.string.features_feature3_title, R.string.features_feature3_description),
        FEATURE4(R.drawable.ic_feature4, R.string.features_feature4_title, R.string.features_feature4_description);

        private int mTitleResId;
        private int mDescriptionResId;
        private int mImageResId;

        Items(int imageResId, int descriptionResId, int titleResId) {
            mImageResId = imageResId;
            mTitleResId = titleResId;
            mDescriptionResId = descriptionResId;
        }

        public int getTitleResId() {
            return mTitleResId;
        }

        public int getDescriptionResId() {
            return mDescriptionResId;
        }

        public int getImageResId() {
            return mImageResId;
        }
    }

    static public int getSize(){
        return 4;
    }
}
