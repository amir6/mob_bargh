package com.andc.mobilebargh.Models;

import com.orm.SugarRecord;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by win on 11/17/2015.
 */
public class UsageHistoryRecord extends SugarRecord {

    //Data Base Labels
    public final static String LABEL_BILL_ID = "billid";

    //JSON Tags
    public final static String TAG_billid = "billid";
    public final static String TAG_saleyear = "saleyear";
    public final static String TAG_saleprd = "saleprd";
    public final static String TAG_FPrevRdgDate = "FPrevRdgDate";
    public final static String TAG_FCurrRdgDate = "FCurrRdgDate";
    public final static String TAG_CurrRdgDate = "CurrRdgDate";
    public final static String TAG_useact = "useact";
    public final static String TAG_prdamt = "prdamt";
    public final static String TAG_crdbtot = "crdbtot";
    public final static String TAG_bilamt = "bilamt";


    public String billid;
    public int saleyear;
    public int saleprd;
    public String FPrevRdgDate;
    public String FCurrRdgDate;
    public String CurrRdgDate;
    public int useact;
    public int prdamt;
    public int crdbtot;
    public int bilamt;

    public UsageHistoryRecord(){
        this.billid = "";
        this.saleyear = -1;
        this.saleprd = -1;
        this.FPrevRdgDate = "";
        this.FCurrRdgDate = "";
        this.CurrRdgDate = "";
        this.useact = -1;
        this.prdamt = -1;
        this.crdbtot = -1;
        this.bilamt = -1;
    }

    public UsageHistoryRecord fromJSON(JSONObject usageHistoryJSON) throws JSONException {

        this.billid = usageHistoryJSON.getString(UsageHistoryRecord.TAG_billid);
        this.saleyear = usageHistoryJSON.getInt(UsageHistoryRecord.TAG_saleyear);
        this.saleprd = usageHistoryJSON.getInt(UsageHistoryRecord.TAG_saleprd);
        this.FPrevRdgDate = usageHistoryJSON.getString(UsageHistoryRecord.TAG_FPrevRdgDate);
        this.FCurrRdgDate = usageHistoryJSON.getString(UsageHistoryRecord.TAG_FCurrRdgDate);
        this.CurrRdgDate = usageHistoryJSON.getString(UsageHistoryRecord.TAG_CurrRdgDate);
        this.useact = usageHistoryJSON.getInt(UsageHistoryRecord.TAG_useact);
        this.prdamt = usageHistoryJSON.getInt(UsageHistoryRecord.TAG_prdamt);
        this.crdbtot = usageHistoryJSON.getInt(UsageHistoryRecord.TAG_crdbtot);
        this.bilamt = usageHistoryJSON.getInt(UsageHistoryRecord.TAG_bilamt);

        return this;
    }

    public static ArrayList<UsageHistoryRecord> parseJSON(JSONArray usageHistoriesJSON) throws JSONException {
        ArrayList<UsageHistoryRecord> usageHistories = new ArrayList<>();

        for(int i = 0 ; i < usageHistoriesJSON.length() ; i++){
            UsageHistoryRecord usageHistory = new UsageHistoryRecord();
            usageHistory.fromJSON(usageHistoriesJSON.getJSONObject(i));
            usageHistories.add(usageHistory);
        }

        return usageHistories;
    }


}
