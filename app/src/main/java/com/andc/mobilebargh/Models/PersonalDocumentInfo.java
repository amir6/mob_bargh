package com.andc.mobilebargh.Models;

import java.io.File;

/**
 * Created by Ak on 4/9/2018.
 */

public class PersonalDocumentInfo {
    private File nationalCartFront , nationalCartBehind , identityCart , commitment , ownershipDoc1
            ,ownershipDoc2 , ownershipDoc3 , other ;

    public void setNationalCartFront(File nationalCartFront) {
        this.nationalCartFront = nationalCartFront;
    }

    public void setNationalCartBehind(File nationalCartBehind) {
        this.nationalCartBehind = nationalCartBehind;
    }

    public void setIdentityCart(File identityCart) {
        this.identityCart = identityCart;
    }

    public void setCommitment(File commitment) {
        this.commitment = commitment;
    }

    public void setOwnershipDoc1(File ownershipDoc1) {
        this.ownershipDoc1 = ownershipDoc1;
    }

    public void setOwnershipDoc2(File ownershipDoc2) {
        this.ownershipDoc2 = ownershipDoc2;
    }

    public void setOwnershipDoc3(File ownershipDoc3) {
        this.ownershipDoc3 = ownershipDoc3;
    }

    public void setOther(File other) {
        this.other = other;
    }

    public File getCommitment() {
        return commitment;
    }

    public File getOwnershipDoc1() {
        return ownershipDoc1;
    }

    public File getOwnershipDoc2() {
        return ownershipDoc2;
    }

    public File getOwnershipDoc3() {
        return ownershipDoc3;
    }

    public File getOther() {
        return other;
    }

    public PersonalDocumentInfo(File nationalCartFront, File nationalCartBehind,
                                File identityCart, File commitment, File ownershipDoc1, File ownershipDoc2,
                                File ownershipDoc3, File other) {
        this.nationalCartFront = nationalCartFront;
        this.nationalCartBehind = nationalCartBehind;
        this.identityCart = identityCart;
        this.commitment = commitment;
        this.ownershipDoc1 = ownershipDoc1;
        this.ownershipDoc2 = ownershipDoc2;
        this.ownershipDoc3 = ownershipDoc3;
        this.other = other;
    }

    public PersonalDocumentInfo(){}

    public File getNationalCartFront() {
        return nationalCartFront;
    }

    public File getNationalCartBehind() {
        return nationalCartBehind;
    }

    public File getIdentityCart() {
        return identityCart;
    }


}
