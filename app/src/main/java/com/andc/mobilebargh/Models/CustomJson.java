package com.andc.mobilebargh.Models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by AK on 3/11/2018.
 */

public class CustomJson {
    //JSON Tags
    public final static String TAG_DATA = "data";
    public final static String TAG_ERROR = "error";
    public final static String TAG_ERROR_CODE = "ErrorCode";
    public final static String TAG_ERROR_MSG = "ErrorMsg";

    private String data;
    private ArrayList<Error> errorList = new ArrayList<>();
    private JSONObject jsonObject;

    public CustomJson(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    public  ArrayList<Error> GetErrorList() throws JSONException{
        JSONArray jsonArray = jsonObject.optJSONArray(TAG_ERROR);
        for (int i = 0 ; i<jsonArray.length(); i++){
           errorList.add(parseJson(jsonArray.optJSONObject(i)));
        }
        return errorList;
    }

    private Error parseJson(JSONObject object) throws JSONException{
        Error mError = new Error();
        mError.setErrorCode(object.optInt(TAG_ERROR_CODE));
        mError.setErrorMsg(object.optString(TAG_ERROR_MSG));
        return mError;
    }

    public JSONObject getData() throws JSONException{
        if (jsonObject == null)
            return  null ;
        String st = jsonObject.optString(TAG_DATA);
        JSONObject object = new JSONObject(st);
        return  object;
    }

    private static class Error{
         private int errorCode ;
         private String errorMsg ;

        public void setErrorCode(int errorCode) {
            this.errorCode = errorCode;
        }

        public void setErrorMsg(String errorMsg) {
            this.errorMsg = errorMsg;
        }
    }

}
