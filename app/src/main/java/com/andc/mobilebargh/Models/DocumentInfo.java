package com.andc.mobilebargh.Models;

import java.util.ArrayList;

/**
 * Created by AFalah on 3/17/2018.
 */

public class DocumentInfo {
    ArrayList<String> nationalCard ;
    ArrayList<String> identity ;
    ArrayList<String> ownerShipDocument ;
    ArrayList<String> commitmentLettr ;
    ArrayList<String> otherDocument ;

    public DocumentInfo(ArrayList<String> nationalCard, ArrayList<String> identity, ArrayList<String> ownerShipDocument, ArrayList<String> commitmentLettr, ArrayList<String> otherDocument) {
        this.nationalCard = nationalCard;
        this.identity = identity;
        this.ownerShipDocument = ownerShipDocument;
        this.commitmentLettr = commitmentLettr;
        this.otherDocument = otherDocument;
    }
}
