package com.andc.mobilebargh.Models;

import android.util.Log;

import com.orm.SugarRecord;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Esbati on 12/14/2015.
 */
public class BillRecord extends SugarRecord {

    //JSON TAGS
    public final static String TAG_BILL = "ACUserBillTblList";
    public final static String TAG_BILL_ID = "BillID";
    public final static String TAG_BILL_TITLE = "BillTitle";
    public final static String TAG_BILL_SHOULD_PRINT = "ShouldPrint";
    public final static String TAG_BILL_SHOULD_PUSH = "ShouldPush";
    public final static String TAG_BILL_SHOULD_SMS = "ShouldSMS";

    //DB LABELS
    public final static String LABEL_BILL_ID = "m_bill_id";

    public enum State{
        Created,
        Edited,
        Deleted
    }

    //Attributes
    public String mBillId;
    public String mBillTitle;
    public String mImageUri;
    public boolean mShouldPrint;
    public boolean mShouldPush;
    public boolean mShouldSMS;
    //public State mState;

    //Flag
    public boolean isUpdate;

    //Constructors
    public BillRecord(){
        this.mBillId = "";
        this.mBillTitle = "";
        mShouldPrint = true;
        mShouldPush = true;
        mShouldSMS = false;
    }

    public BillRecord(String billId){
        this();
        this.mBillId = billId;
    }

    public BillRecord(String billId, String billTitle){
        this();
        this.mBillId = billId;
        this.mBillTitle = billTitle;
    }

    //JSON Operations
    public BillRecord fromJSON(JSONObject billJSON) throws JSONException{

        mBillId = billJSON.getString(TAG_BILL_ID);
        mBillTitle = billJSON.getString(TAG_BILL_TITLE);
        mShouldPrint = billJSON.optBoolean(TAG_BILL_SHOULD_PRINT, true);
        mShouldPush = billJSON.optBoolean(TAG_BILL_SHOULD_PUSH, true);
        mShouldSMS = billJSON.optBoolean(TAG_BILL_SHOULD_SMS, false);

        return this;
    }

    public JSONObject toJSON(){
        JSONObject billJSON = new JSONObject();
        try{
            billJSON.put(TAG_BILL_ID, mBillId);
            billJSON.put(TAG_BILL_TITLE, mBillTitle);
            billJSON.put(TAG_BILL_SHOULD_PRINT, mShouldPrint);
            billJSON.put(TAG_BILL_SHOULD_PUSH, mShouldPush);
            billJSON.put(TAG_BILL_SHOULD_SMS, mShouldSMS);
        } catch (JSONException e){
            e.printStackTrace();
            Log.e("JSONParser", e.getMessage(), e);
        }

        return billJSON;
    }

    public static BillRecord parseJSON(JSONObject billJSON) throws JSONException{

        BillRecord billRecord = new BillRecord();
        billRecord.fromJSON(billJSON);

        return billRecord;
    }

    public static ArrayList<BillRecord> parseJSON(JSONArray billsJSON){

        ArrayList<BillRecord> mBills = new ArrayList<>();
        for (int i = 0 ; i < billsJSON.length() ; i++)
            try{
                BillRecord billRecord = new BillRecord();
                billRecord.fromJSON(billsJSON.getJSONObject(i));
                mBills.add(billRecord);
            } catch (JSONException e){
                e.printStackTrace();
                Log.e("JSONParser", e.getMessage(), e);
            }

        return mBills;
    }

}
