package com.andc.mobilebargh.Models;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Esbati on 5/29/2016.
 */
public class CustomError {

    //Attributes
    public int mCode;
    public String mMsg;

    //JSON Tags
    public final static String TAG_DATA = "data";
    public final static String TAG_ERROR = "error";
    public final static String TAG_ERROR_CODE = "ErrorCode";
    public final static String TAG_ERROR_MSG = "ErrorMsg";

    public static CustomError parseJSON(JSONObject errorJSON) throws JSONException{
        CustomError error = new CustomError();
        error.mCode = errorJSON.getInt(TAG_ERROR_CODE);
        error.mMsg = errorJSON.getString(TAG_ERROR_MSG);
        return error;
    }

    public static ArrayList<CustomError> parseJSON(JSONArray errorsJSON) throws JSONException{
        ArrayList<CustomError> errors = new ArrayList<>();
        for(int i = 0; i < errorsJSON.length(); i++)
            try{
                errors.add(parseJSON(errorsJSON.getJSONObject(i)));
            } catch (JSONException e){
                e.printStackTrace();
                Log.e("JSONParser", e.getMessage(), e);
            }

        return errors;
    }


}
