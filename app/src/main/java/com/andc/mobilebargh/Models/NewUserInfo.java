package com.andc.mobilebargh.Models;

/**
 * Created by AFalah on 3/17/2018.
 */

public class NewUserInfo {
    public int typeUser,sex;
    public String legalMobile, legalPhone, legalNumberSave, legalCompany, legalNewsPaper, legalEconomyCode, legalPost, legalAddress, legalMoasse, legalActivtyCode, legalPostWriterCode, legalWriterAddress, legalEmail;
    public String realNationalCode,realPhoneNumber,realMobileNo , realFirstName, realLastName,
            realIdentityNo, realPlaceIssuance, realPostCode, realBranchingAddress,
            realWritterPost, realWritterAddress, realMail,realFatehrName , realPrePhone;
    public String legalNewsPaperDate;
    public String realBirthDate;


    public NewUserInfo(String realNationalCode, String realFirstName, String realLastName,
                       String realIdentityNo, String realPlaceIssuance, int sex, String realPhoneNumber,
                       String realMobileNo, String realPostCode, String realBranchingAddress,
                       String realWritterPost, String realWritterAddress, String realMail,
                       String realBirthDate, String fatherName,String prePhone) {
        this.typeUser = 1;//real
        this.realNationalCode = realNationalCode;
        this.realFirstName = realFirstName;
        this.realLastName = realLastName;
        this.realIdentityNo = realIdentityNo;
        this.realPlaceIssuance = realPlaceIssuance;
        this.sex = sex ;
        this.realPhoneNumber = realPhoneNumber;
        this.realMobileNo = realMobileNo;
        this.realPostCode = realPostCode;
        this.realBranchingAddress = realBranchingAddress;
        this.realWritterPost = realWritterPost;
        this.realWritterAddress = realWritterAddress;
        this.realMail = realMail;
        this.realBirthDate = realBirthDate;
        this.realFatehrName = fatherName;
        this.realPrePhone = prePhone;
    }

    public NewUserInfo(String legalMobile, String legalPhone, String legalNumberSave, String legalCompany, String legalNewsPaper, String legalEconomyCode, String legalPost, String legalAddress, String legalMoasse, String legalActivtyCode, String legalPostWriterCode, String legalWriterAddress, String legalEmail, String legalNewsPaperDate) {
        this.typeUser = 2;
        this.legalMobile = legalMobile;
        this.legalPhone = legalPhone;
        this.legalNumberSave = legalNumberSave;
        this.legalCompany = legalCompany;
        this.legalNewsPaper = legalNewsPaper;
        this.legalEconomyCode = legalEconomyCode;
        this.legalPost = legalPost;
        this.legalAddress = legalAddress;
        this.legalMoasse = legalMoasse;
        this.legalActivtyCode = legalActivtyCode;
        this.legalPostWriterCode = legalPostWriterCode;
        this.legalWriterAddress = legalWriterAddress;
        this.legalEmail = legalEmail;
        this.legalNewsPaperDate = legalNewsPaperDate;
    }

    @Override
    public String toString() {
        return "NewUserInfo{" +
                "typeUser = " + String.valueOf(typeUser) + '\'' +
                "legalMobile='" + legalMobile + '\'' +
                ", legalPhone='" + legalPhone + '\'' +
                ", legalNumberSave='" + legalNumberSave + '\'' +
                ", legalCompany='" + legalCompany + '\'' +
                ", legalNewsPaper='" + legalNewsPaper + '\'' +
                ", legalEconomyCode='" + legalEconomyCode + '\'' +
                ", legalPost='" + legalPost + '\'' +
                ", legalAddress='" + legalAddress + '\'' +
                ", legalMoasse='" + legalMoasse + '\'' +
                ", legalActivtyCode='" + legalActivtyCode + '\'' +
                ", legalPostWriterCode='" + legalPostWriterCode + '\'' +
                ", legalWriterAddress='" + legalWriterAddress + '\'' +
                ", legalEmail='" + legalEmail + '\'' +
                ", realNationalCode='" + realNationalCode + '\'' +
                ", realFirstName='" + realFirstName + '\'' +
                ", realLastName='" + realLastName + '\'' +
                ", realIdentityNo='" + realIdentityNo + '\'' +
                ", realPlaceIssuance='" + realPlaceIssuance + '\'' +
                ", realPostCode='" + realPostCode + '\'' +
                ", realBranchingAddress='" + realBranchingAddress + '\'' +
                ", realWritterPost='" + realWritterPost + '\'' +
                ", realWritterAddress='" + realWritterAddress + '\'' +
                ", realMail='" + realMail + '\'' +
                '}';
    }
}
