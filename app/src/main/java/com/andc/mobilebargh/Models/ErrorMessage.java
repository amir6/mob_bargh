package com.andc.mobilebargh.Models;

import android.widget.EditText;

/**
 * Created by AFalah on 4/11/2018.
 */

public class ErrorMessage {
    private String errorMessage ;
    private EditText etError ;

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public EditText getEtError() {
        return etError;
    }

    public void setEtError(EditText etError) {
        this.etError = etError;
    }
}

