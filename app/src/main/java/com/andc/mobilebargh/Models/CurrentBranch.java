package com.andc.mobilebargh.Models;

import org.json.JSONObject;

/**
 * Created by AFalah on 3/12/2018.
 */

public class CurrentBranch {
    private static final String TAG_DistributorName = "_DistributorName";
    private static final String TAG_RegionName = "RegionName";
    private static final String TAG_BranchTypeCode = "BranchTypeCode";
    private static final String TAG_OwnerName = "OwnerName";
    private static final String TAG_OwnerType = "OwnerType";
    private static final String TAG_CompanyName = "CompanyName";
    private static final String TAG_HomeAdderss = "HomeAdderss";
    private static final String TAG_HomePoNum = "HomePoNum";
    private static final String TAG_LatitueX = "LatitueX";
    private static final String TAG_LongitudeY = "LongitudeY";
    private static final String TAG_Phs = "Phs";
    private static final String TAG_Amp = "Amp";
    private static final String TAG_PwrCnt = "PwrCnt";
    private static final String TAG_voltcode = "voltcode";
    private static final String TAG_TrfHCode = "TrfHCode";
    private static final String TAG_TrfHCodeName = "TrfHCodeName";
    private static final String TAG_BranchKindCode = "BranchKindCode";
    private static final String TAG_BranchKindCodeName = "BranchKindCodeName";
    private static final String TAG_BranchStatCode = "BranchStatCode";
    private static final String TAG_BranchSrl = "BranchSrl";
    private static final String TAG_BranchCode = "BranchCode";
    private static final String TAG_IdentityCode = "IdentityCode";
    private static final String TAG_CrDbTot = "CrDbTot";
    private static final String TAG_CrDBBranchSale = "CrDBBranchSale";
    private static final String TAG_CrDbOtherService = "CrDbOtherService";

    public String DistributorName;
    public String RegionName;
    public String BranchTypeCode;
    public String OwnerName;
    public String OwnerType;
    public String CompanyName;
    public String HomeAdderss;
    public String HomePoNum;
    public String LatitueX;
    public String LongitudeY;
    public String Phs;
    public String Amp;
    public String PwrCnt;
    public String voltcode;
    public String TrfHCode;
    public String TrfHCodeName;
    public String BranchKindCode;
    public String BranchKindCodeName;
    public String BranchStatCode;
    public String BranchSrl;
    public String BranchCode;
    public String IdentityCode;
    public String CrDbTot;
    public String CrDBBranchSale;
    public String CrDbOtherService;

    public static CurrentBranch parseJson(JSONObject jsonObject) {
        if (jsonObject == null) return null ;
        CurrentBranch model = new CurrentBranch();
        model.DistributorName = jsonObject.optString(TAG_DistributorName);
        model.RegionName = jsonObject.optString(TAG_RegionName);
        model.BranchTypeCode = jsonObject.optString(TAG_BranchTypeCode);
        model.OwnerName = jsonObject.optString(TAG_OwnerName);
        model.OwnerType = jsonObject.optString(TAG_OwnerType);
        model.CompanyName = jsonObject.optString(TAG_CompanyName);
        model.HomeAdderss = jsonObject.optString(TAG_HomeAdderss);
        model.HomePoNum= jsonObject.optString(TAG_HomePoNum);
        model.LatitueX= jsonObject.optString(TAG_LatitueX);
        model.LongitudeY= jsonObject.optString(TAG_LongitudeY);
        model.Phs= jsonObject.optString(TAG_Phs);
        model.Amp= jsonObject.optString(TAG_Amp);
        model.PwrCnt= jsonObject.optString(TAG_PwrCnt);
        model.voltcode= jsonObject.optString(TAG_voltcode);
        model.TrfHCode= jsonObject.optString(TAG_TrfHCode);
        model.TrfHCodeName= jsonObject.optString(TAG_TrfHCodeName);
        model.BranchKindCode= jsonObject.optString(TAG_BranchKindCode);
        model.BranchKindCodeName= jsonObject.optString(TAG_BranchKindCodeName);
        model.BranchStatCode= jsonObject.optString(TAG_BranchStatCode);
        model.BranchSrl= jsonObject.optString(TAG_BranchSrl);
        model.BranchCode= jsonObject.optString(TAG_BranchCode);
        model.IdentityCode= jsonObject.optString(TAG_IdentityCode);
        model.CrDbTot= jsonObject.optString(TAG_CrDbTot);
        model.CrDBBranchSale= jsonObject.optString(TAG_CrDBBranchSale);
        model.CrDbOtherService= jsonObject.optString(TAG_CrDbOtherService);
        return model;
    }

}
