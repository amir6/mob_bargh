package com.andc.mobilebargh.Models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Tenkei on 6/5/2016.
 */
public class BillData{

    public final static String TAG_BILL_DATA_BRANCH_INFO = "branchData";
    public final static String TAG_BILL_DATA_USAGE_HISTORY = "SaleList";
    public final static String TAG_BILL_DATA_BILLING_HISTORY = "ReceiptList";

    public BranchInfoRecord mBranchInfo;
    public ArrayList<UsageHistoryRecord> mUsageHistory;
    public ArrayList<BillingHistoryRecord> mBillingHistory;

    public BillData(){
        mBranchInfo = new BranchInfoRecord();
        mUsageHistory = new ArrayList<>();
        mBillingHistory = new ArrayList<>();
    }

    //Data Base Operations
    public void save() {
        mBranchInfo.save();

        for(UsageHistoryRecord usageHistory:mUsageHistory)
            usageHistory.save();

        for(BillingHistoryRecord billingHistory:mBillingHistory)
            billingHistory.save();
    }

    public void load(String billId){
        ArrayList<BranchInfoRecord> branchInfo = (ArrayList<BranchInfoRecord>)BranchInfoRecord
                .find(BranchInfoRecord.class, BranchInfoRecord.LABEL_BILL_ID + " = ?", billId);
        if(branchInfo!=null && branchInfo.size()>0)
            mBranchInfo = BranchInfoRecord.find(BranchInfoRecord.class, BranchInfoRecord.LABEL_BILL_ID + " = ?", billId).get(0);

        mUsageHistory = (ArrayList<UsageHistoryRecord>)UsageHistoryRecord
                .find(UsageHistoryRecord.class, UsageHistoryRecord.LABEL_BILL_ID + " = ?", billId);

        mBillingHistory = (ArrayList<BillingHistoryRecord>)BillingHistoryRecord
                .find(BillingHistoryRecord.class, BillingHistoryRecord.LABEL_BILL_ID + " = ?", billId);
    }

    public void delete() {
        if(mBranchInfo!=null)
            mBranchInfo.delete();

        if(mUsageHistory!=null)
            for(UsageHistoryRecord usageHistory:mUsageHistory)
                usageHistory.delete();

        if(mBillingHistory!=null)
            for(BillingHistoryRecord billingHistory:mBillingHistory)
                billingHistory.delete();
    }

    public static void clear(String billId){
        ArrayList<BranchInfoRecord> oldBranchInfoRecords = (ArrayList<BranchInfoRecord>) BranchInfoRecord
                .find(BranchInfoRecord.class, BranchInfoRecord.LABEL_BILL_ID + " = ?", billId);
        if(oldBranchInfoRecords!=null && oldBranchInfoRecords.size()>0){
            for(BranchInfoRecord mBranch : oldBranchInfoRecords)
                mBranch.delete();
        }

        ArrayList<UsageHistoryRecord> oldUsageHistoryRecords = (ArrayList<UsageHistoryRecord>) UsageHistoryRecord
                .find(UsageHistoryRecord.class, UsageHistoryRecord.LABEL_BILL_ID + " = ?", billId);
        if(oldUsageHistoryRecords!=null && oldUsageHistoryRecords.size()>0){
            for(UsageHistoryRecord oldUsageRecord : oldUsageHistoryRecords)
                oldUsageRecord.delete();
        }

        ArrayList<BillingHistoryRecord> oldBillingHistoryRecords = (ArrayList<BillingHistoryRecord>) BillingHistoryRecord
                .find(BillingHistoryRecord.class, BillingHistoryRecord.LABEL_BILL_ID + " = ?", billId);
        if(oldBillingHistoryRecords!=null && oldBillingHistoryRecords.size()>0){
            for(BillingHistoryRecord oldBillingRecord : oldBillingHistoryRecords)
                oldBillingRecord.delete();
        }
    }

    public JSONObject convertStringtoJson(String item) throws JSONException {
         return new JSONObject(item);
    }

    //JSON Operations
    public BillData fromJSON(JSONObject billDataJSON) throws JSONException {
        JSONObject branchJSON = billDataJSON.getJSONObject(TAG_BILL_DATA_BRANCH_INFO);
        JSONArray usageHistoriesJSON = billDataJSON.getJSONArray(TAG_BILL_DATA_USAGE_HISTORY);
        JSONArray billingHistoriesJSON = billDataJSON.getJSONArray(TAG_BILL_DATA_BILLING_HISTORY);

        mBranchInfo.fromJSON(branchJSON);
        mUsageHistory = UsageHistoryRecord.parseJSON(usageHistoriesJSON);
        mBillingHistory = BillingHistoryRecord.parseJSON(billingHistoriesJSON);

        return this;
    }


    public BillData parsCurrentBranch(JSONObject data) throws JSONException {

        mBranchInfo.fromJSON(data);
        return this;

    }
}
