package com.andc.mobilebargh.Models;

/**
 * Created by AFalah on 4/9/2018.
 */

public class CurrentUserInfo {
    public String billID,Name,Address,Region,ramz,bedehiMasraf ,bedehiEnshab,bedehiOrther,
    amper,power,phase,ensheab,tarefe,fabricNo;

    public CurrentUserInfo(String billID, String name, String address, String region, String ramz, String bedehiMasraf, String bedehiEnshab, String bedehiOrther, String amper, String power,String phase, String ensheab, String tarefe,String fabricNo) {
        this.billID = billID;
        this.Name = name;
        this.Address = address;
        this.Region = region;
        this.ramz = ramz;
        this.bedehiMasraf = bedehiMasraf;
        this.bedehiEnshab = bedehiEnshab;
        this.bedehiOrther = bedehiOrther;
        this.amper = amper;
        this.power = power;
        this.phase = phase;
        this.ensheab = ensheab;
        this.tarefe = tarefe;
        this.fabricNo = fabricNo;
    }
}
