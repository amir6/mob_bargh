package com.andc.mobilebargh.Models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Esbati on 5/2/2016.
 */
public class UpdateFile {


    public final static String TAG_UPDATE_VERSION_CODE = "VersionCode";
    public final static String TAG_UPDATE_TYPE = "UpdateType";
    public final static String TAG_UPDATE_MESSAGE = "UpdateMsg";
    public final static String TAG_UPDATE_URL = "UpdateURL";
    public final static String TAG_SERVER_ADDRESS = "ServerAddress";

    public enum UpdateType {
        Optional,
        Mandatory
    }

    public int mVersionCode;
    public UpdateType mUpdateType;
    public String mUpdateMsg;
    public String mUpdateUrl;
    public String mServerAddress;


    public static UpdateFile parseJSON(JSONObject updateJSON){
        UpdateFile newUpdate= new UpdateFile();

        try {
            newUpdate.mVersionCode = updateJSON.getInt(TAG_UPDATE_VERSION_CODE);
            newUpdate.mUpdateType = UpdateType.valueOf(updateJSON.getString(TAG_UPDATE_TYPE));
            newUpdate.mUpdateMsg = updateJSON.getString(TAG_UPDATE_MESSAGE);
            newUpdate.mUpdateUrl = updateJSON.getString(TAG_UPDATE_URL);
            newUpdate.mServerAddress = updateJSON.getString(TAG_SERVER_ADDRESS);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return newUpdate;
    }
}
