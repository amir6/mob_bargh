package com.andc.mobilebargh.Controllers;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.andc.mobilebargh.MobileBargh;

/**
 * Created by Esbati on 4/25/2016.
 */
public class TutorialHelper {

    public final static String TUTORIAL_FIRST_TIME = "tutorial_first_time";
    public final static String TUTORIAL_HOME = "tutorial_home";
    public final static String TUTORIAL_DRAWER = "tutorial_drawer";

    public static boolean shouldTutorialShown(String key){
        SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(MobileBargh.getContext());

        return settings.getBoolean(key, true);
    }

    public static void setTutorialVisibility(String key, boolean isActive){
        SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(MobileBargh.getContext());
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(key, isActive);
        editor.commit();
    }

    public static void tutorialCompleted(String key){
        setTutorialVisibility(key, false);
    }


}
