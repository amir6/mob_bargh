package com.andc.mobilebargh.Controllers;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.wifi.WifiManager;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.andc.mobilebargh.MobileBargh;

import java.net.HttpURLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Esbati on 2/7/2016.
 */
public class SecurityHelper {

    public static final String SECURITY_TOKEN = "Token";

    public static String getToken(){
        SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(MobileBargh.getContext());
        String token = settings.getString(SECURITY_TOKEN, "");

        return token;
    }

    public static void saveToken(String token){
        if(TextUtils.isEmpty(token))
            return;

        SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(MobileBargh.getContext());
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(SECURITY_TOKEN, token);
        editor.commit();
    }

    public static boolean isTokenValid(){
        if(getToken()!=null && !getToken().equals(""))
            return true;
        else
            return false;
    }

    public static void addHTTPAuthorization(HttpURLConnection urlConnection, String billId){
        String token = getMD5Hash(billId);
        String macAddress = ((WifiManager) MobileBargh.getContext().getApplicationContext()
                .getSystemService(Context.WIFI_SERVICE)).getConnectionInfo().getMacAddress();
        urlConnection.setRequestProperty("Token", token);
        urlConnection.setRequestProperty("MacAddress", macAddress);
    }

    public static void addHTTPAuthorization(HttpURLConnection urlConnection){
        urlConnection.setRequestProperty("Token", SecurityHelper.getToken());
    }

    private static String getMD5Hash(String billId){
        String mPackage = MobileBargh.getContext().getPackageName();
        String mMacAddress = ((WifiManager) MobileBargh.getContext().getApplicationContext()
                .getSystemService(Context.WIFI_SERVICE))
                .getConnectionInfo()
                .getMacAddress();
        String mBillId = billId;

        //String mDate = new PersianCalendar().getPersianShortDate();
        SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MM-dd");
        dateFormatGmt.setTimeZone(TimeZone.getTimeZone("GMT"));
        String mDate = dateFormatGmt.format(new Date());

        String md5 = mPackage + mMacAddress + mBillId + mDate;

        return getMD5(md5);
    }

    private static String getMD5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }
}
