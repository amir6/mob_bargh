package com.andc.mobilebargh.Controllers;

import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.preference.PreferenceManager;
import android.widget.Toast;

import com.andc.mobilebargh.MobileBargh;

/**
 * Created by Esbati on 2/13/2016.
 */
public class PreferencesHelper {

    //Setting Attributes
    public final static int APP_CURRENT_VERSION = 105;
    public final static String KEY_LOGIN_ID = "parseLoginObjectId";

    //Setting Toggles Keys
    public final static String KEY_SMS_BILLS = "isSmsActive";
    public final static String KEY_SPLASH_LOADING = "isSplashActive";
    public final static String KEY_AUTO_UPDATE = "isAutoUpdateActive";

    //Setting Values Keys
    public final static String KEY_FIRST_NAME = "firstName";
    public final static String KEY_FAMILY_NAME = "familyName";
    public final static String KEY_NATIONAL_CODE = "nationalCode";
    public final static String KEY_CELLPHONE = "cellphone";
    public final static String KEY_REMIND_ME_BEFORE = "remindMeBefore";

    //Update Check Keys
    public final static String KEY_UPDATE_LATEST_VERSION = "latestUpdateVersion";
    public final static String KEY_UPDATE_LAST_CHECK = "lastUpdateCheck";

    //Data Sync Keys
    public final static String KEY_SYNC_NECESSARY = "shouldSyncData";
    public final static String KEY_SYNC_LAST_DATE = "lastSyncDate";

    public static void initiate(){
        SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(MobileBargh.getContext());
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(KEY_SMS_BILLS, true);
        editor.putBoolean(KEY_SPLASH_LOADING, true);
        editor.putBoolean(KEY_AUTO_UPDATE, true);
        editor.putInt(KEY_REMIND_ME_BEFORE, 3);
        editor.commit();
    }

    public static boolean isOptionActive(String key){
        SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(MobileBargh.getContext());

        return settings.getBoolean(key, true);
    }

    public static void setOption(String key, boolean isActive){
        SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(MobileBargh.getContext());
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(key, isActive);
        editor.commit();
    }

    public static void toggleOption(String key){
        SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(MobileBargh.getContext());
        SharedPreferences.Editor editor = settings.edit();

        boolean value = settings.getBoolean(key, true);
        editor.putBoolean(key, !value);
        editor.commit();
    }

    public static String load(String key){
        SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(MobileBargh.getContext());
        String value = settings.getString(key, ""/*default value*/);

        return value;
    }

    public static void save(String key, String value){
        SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(MobileBargh.getContext());
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static int getCurrentVersion(){
        //return APP_CURRENT_VERSION;

        int versionCode;
        try{
            PackageInfo info = MobileBargh.getContext().getPackageManager().getPackageInfo(
                    MobileBargh.getContext().getPackageName(), 0
            );
            versionCode = info.versionCode;
        } catch (Exception e){
            Toast.makeText(MobileBargh.getContext(), "مشکل در بازیابی شماره نسخه", Toast.LENGTH_SHORT).show();
            versionCode = 105;
        }

        return versionCode;
    }

    public static int loadInt(String key){
        SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(MobileBargh.getContext());
        int intValue = settings.getInt(key, 0);

        return intValue;
    }

    public static void saveInt(String key, int intValue){
        SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(MobileBargh.getContext());
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(key, intValue);
        editor.commit();
    }
}
