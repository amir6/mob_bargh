package com.andc.mobilebargh.Controllers;

import android.content.Context;
import android.text.TextUtils;

import com.andc.mobilebargh.Models.BillRecord;
import com.andc.mobilebargh.Networking.Tasks.SyncBills;

import java.util.ArrayList;

/**
 * Created by Esbati on 12/14/2015.
 */
public class BillCollection {

    private static BillCollection sBillCollection;
    private BillRecord mCurrentBill;
    private ArrayList<BillRecord> mBills;
    private ArrayList<String> mFaceArray;
    private ArrayList<String> mSpinnerArray;

    //Constructor
    private BillCollection(){
        mFaceArray = new ArrayList<>();
        mSpinnerArray = new ArrayList<>();
        mBills = (ArrayList<BillRecord>)BillRecord.listAll(BillRecord.class);
    }

    //Get The Singleton
    public static BillCollection get(Context context){
        if(sBillCollection == null){
            sBillCollection = new BillCollection();
        }
        return sBillCollection;
    }

    public void saveChanges(){
        for(BillRecord bill:mBills)
            bill.save();
    }

    //region Get Data
    public void setCurrentBill(BillRecord currentBill){
        mCurrentBill = currentBill;
    }

    //Get Current Bill
    public BillRecord getCurrentBill(){
        //Return First Bill If there is no Current Set
        if(mCurrentBill==null)
            if(mBills!=null && mBills.size()>0)
                mCurrentBill = mBills.get(0);

        return mCurrentBill;
    }

    //Return an Array of Bills
    public ArrayList<BillRecord> getBills(){
        return mBills;
    }

    //Return an Array of Bill IDs for fast calculations
    public ArrayList<String> getBillIds(){
        ArrayList<String> mBillIds = new ArrayList<>();
        for(BillRecord bill:mBills)
            mBillIds.add(bill.mBillId);

        return mBillIds;
    }

    //Return an Array of Bill Titles If the Bill has no Title it returns Bill ID
    public ArrayList<String> getFace(){
        //Instead of newing the Array we clear it so the reference is not destroyed
        mFaceArray.clear();

        for(BillRecord bill:mBills)
            if (TextUtils.isEmpty(bill.mBillTitle))
                mFaceArray.add(bill.mBillId);
            else
                mFaceArray.add(bill.mBillTitle);

        return mFaceArray;
    }

    public ArrayList<String> getSpinnerArray(){
        //Instead of newing the Array we clear it so the reference is not destroyed
        mSpinnerArray.clear();

        for(BillRecord bill:mBills)
            if (TextUtils.isEmpty(bill.mBillTitle))
                mSpinnerArray.add(bill.mBillId);
            else
                mSpinnerArray.add(bill.mBillId + " (" + bill.mBillTitle + ")");

        return mSpinnerArray;
    }


    //endregion

    //region Bill Operations
    //Notify Class that All Data Inside Collection Has Changed
    public void notifyDataSetChanged(){
        //Update Bill Array
        mBills = new ArrayList<>();
        mBills = (ArrayList<BillRecord>)BillRecord.listAll(BillRecord.class);

        //Update Spinner Array
        mFaceArray.clear();
        for(BillRecord bill:mBills)
            if (bill.mBillTitle == null || bill.mBillTitle.equalsIgnoreCase(""))
                mFaceArray.add(bill.mBillId);
            else
                mFaceArray.add(bill.mBillTitle);
    }

    //Notify Class that Face of Data has Changed
    public void notifyFaceChanged(){
        //Update Spinner Array
        mFaceArray.clear();
        for(BillRecord bill:mBills)
            if (bill.mBillTitle == null || bill.mBillTitle.equalsIgnoreCase(""))
                mFaceArray.add(bill.mBillId);
            else
                mFaceArray.add(bill.mBillTitle);
    }

    //Find a Bill Based on Both Title and ID
    public BillRecord find(String query){
        if(query.charAt(0)<'0' || query.charAt(0)>'9'){
            //If Query don't Start with Int Search Based on Bill Titles
            for(BillRecord bill:mBills){
                if(bill.mBillTitle.equalsIgnoreCase(query))
                    return bill;
            }
        }else{
            //Else Search Based on Bill ID
            for(BillRecord bill:mBills){
                if(bill.mBillId.equalsIgnoreCase(query))
                    return bill;
            }

            //Search in Titles Again if Didn't Find Anything in IDs
            for(BillRecord bill:mBills){
                if(bill.mBillTitle.equalsIgnoreCase(query))
                    return bill;
            }
        }

        return null;
    }

    //Get Bill Based on Its ID
    public BillRecord findBillById(String billId){
        for(BillRecord bill:mBills)
            if (bill.mBillId.equalsIgnoreCase(billId))
                return bill;

        return null;
    }

    //Get Bill Based on Its Title
    public BillRecord findBillByTitle(String billTitle){
        for(BillRecord bill:mBills)
            if (bill.mBillTitle.equalsIgnoreCase(billTitle))
                return bill;

        return null;
    }

    //Create a New Bill
    public void add(BillRecord newBill){
        newBill.save();
        mBills.add(newBill);

        //sBillCollection.notifyFaceChanged();
        PreferencesHelper.setOption(PreferencesHelper.KEY_SYNC_NECESSARY, true);
        new SyncBills().execute();
    }

    //Remove a Bill
    public void remove(BillRecord bill){
        //If Current Item Get Deleted Change it to the Null
        if(mCurrentBill!= null && mCurrentBill.equals(bill))
            //TODO Set a New Current Bill if the Current One Being Removed
            mCurrentBill = null;

        mBills.remove(bill);
        bill.delete();

        //sBillCollection.notifyFaceChanged();
        PreferencesHelper.setOption(PreferencesHelper.KEY_SYNC_NECESSARY, true);
        new SyncBills().execute();
    }
    //endregion
}
