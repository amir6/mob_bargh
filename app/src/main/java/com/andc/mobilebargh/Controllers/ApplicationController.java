package com.andc.mobilebargh.Controllers;

import android.app.Application;
import android.content.Context;
import android.os.Handler;
import android.support.multidex.MultiDexApplication;

import com.andc.mobilebargh.R;
import com.andc.mobilebargh.Utility.SSLHelper;

import com.orm.SugarApp;
import com.orm.SugarContext;
import com.orm.SugarDb;
import com.parse.Parse;
import com.parse.ParseInstallation;


import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by Esbati on 12/22/2015.
 */
public class ApplicationController extends MultiDexApplication {

    private static Context mContext;
    public static volatile Handler applicationHandler;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        //SugarDb db = new SugarDb(this);
      //  db.onCreate(db.getDB());
        SugarContext.init(this);
        applicationHandler = new Handler(mContext.getMainLooper());

        //Fabric.with(this, new Crashlytics());

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                        .setDefaultFontPath("fonts/IRANSans(FaNum).ttf")
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        );

        // [Optional] Power your app with Local Datastore. For more info, go to
        // https://parse.com/docs/android/guide#local-datastore
        Parse.enableLocalDatastore(this);
        //Parse.initialize(this);
        //Parse.initialize(this, "qS11wStzYeo0Mkw6AYd5iKONc0rK2SFBg6UrYYdu", "CCawNDFfbqPqEhMTaJVpMExxoIdGI7kd3OeG8v2L");
        Parse.initialize(
                new Parse.Configuration.Builder(this)
                        .server("http://85.17.22.12:1337/parse/")
                        .applicationId("qS11wStzYeo0Mkw6AYd5iKONc0rK2SFBg6UrYYdu")
                        .clientKey("CCawNDFfbqPqEhMTaJVpMExxoIdGI7kd3OeG8v2L")
                        .build()
        );
        ParseInstallation.getCurrentInstallation().saveInBackground();

        //FIXME Change This Function to check SSL Certificate Checking
        //SSLHelper.checkSSLCertificate();
        SSLHelper.verifyNullHostName();
        SSLHelper.disableSSLCertificateChecking();
    }

    public static Context getContext(){
        return mContext;
    }

    public static Handler getHandler(){
        return applicationHandler;
    }
}
