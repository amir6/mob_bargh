package com.andc.mobilebargh.Fragments.RequestFragments.SupportRequestFragments

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.opengl.Visibility
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.andc.mobilebargh.Activities.IRequest
import com.andc.mobilebargh.Controllers.BillCollection
import com.andc.mobilebargh.Fragments.DialogFramgments.ResponseStatusDialog
import com.andc.mobilebargh.Fragments.DialogFramgments.WaringDialog
import com.andc.mobilebargh.Fragments.ServiceFragments.BaseFragment
import com.andc.mobilebargh.Models.BillData
import com.andc.mobilebargh.R
import com.andc.mobilebargh.Utility.General
import com.andc.mobilebargh.Utility.General.FontSize
import com.andc.mobilebargh.Utility.Util.Constants
import com.andc.mobilebargh.Utility.Util.Constants.COLLECT_BRANCH_PERMANENT_TAG
import com.andc.mobilebargh.Utility.Util.Constants.COLLECT__BRANCH_TEMPORARY_TAG
import com.andc.mobilebargh.Utility.Util.Constants.CORRECT_BARNCH_SERVICE_TAG
import com.andc.mobilebargh.Utility.Util.Constants.DISCONNECT_BRANCH_TEMPORARY_TAG
import com.andc.mobilebargh.Utility.Util.Constants.INTERNAL_MOVE_METER_TAG
import com.andc.mobilebargh.Utility.Util.Constants.METER_TEST_TAG
import com.andc.mobilebargh.Utility.Util.Constants.RECONNECT_BRANCH_TAG
import com.andc.mobilebargh.Utility.Util.Constants.REINSTALL_BARNCH_TAG
import com.andc.mobilebargh.Utility.Util.ServerStatus
import com.andc.mobilebargh.ViewModel.ReceiveBranchVm
import kotlinx.android.synthetic.main.dialog_footer.view.*
import kotlinx.android.synthetic.main.fragment_request_receive_info.*
import kotlinx.android.synthetic.main.fragment_request_receive_info.view.*
import kotlinx.android.synthetic.main.fragment_request_sub_items.*

class RequestReceiveInfoFragment: BaseFragment(), View.OnClickListener {


    lateinit var mListener: IRequest
    var mBills: ArrayList<String> = ArrayList()
    lateinit var mViewModel: ReceiveBranchVm
    lateinit var mBundle: Bundle
    lateinit var mServiceList: ArrayList<String>
    var mCode:String = "0"
    lateinit var mAdapter: ArrayAdapter<String>



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel =activity.run { ViewModelProviders.of(this!!, viewModelFactory).get(ReceiveBranchVm::class.java) }
        mViewModel .cheCkStatus.observe(this, object: Observer<ServerStatus?> {
            override fun onChanged(t: ServerStatus?) {
                if( t == ServerStatus.RECEIVE){
                    progress_receive_info.hide()
                    mListener.OnNextClick()
                }
                if(t == ServerStatus.ERROR){
                    btn_request_receive_info.isEnabled = true
                   var dialog :  ResponseStatusDialog  = ResponseStatusDialog(activity)
                    dialog.showFail()
                    progress_receive_info.hide()

                }

            }
        } )


    }

    private fun initAdapter(v: View) {
        mServiceList = ArrayList()

        if(mBundle.get("type") == 0){
            mServiceList.add(resources.getString(R.string.spinner_select_item))
            mServiceList.add(resources.getString(R.string.disconnect_branch_temporary))
            mServiceList.add(resources.getString(R.string.reconnect_branch))
        }
        if(mBundle.get("type")== 1){
            mServiceList.add(resources.getString(R.string.spinner_select_item))
            mServiceList.add(resources.getString(R.string.collect_branch_temporary))
            mServiceList.add(resources.getString(R.string.reinstall_branch))
        }
        if(mBundle.get("type") == 0 || mBundle.get("type") == 1) {
           mAdapter = ArrayAdapter(activity, android.R.layout.simple_list_item_1, mServiceList)
            v.spn_type_after_sale_request.adapter = mAdapter
            v.tv_title.visibility = View.VISIBLE

        }else{
            v.tv_title.visibility = View.GONE
            v.spn_type_after_sale_request.visibility = View.GONE
            setRequestCode()
        }


    }

    private fun setRequestCode() {
        if(mBundle.get("type") == 2)
            mCode = COLLECT_BRANCH_PERMANENT_TAG
        if(mBundle.get("type") == 3)
            mCode = INTERNAL_MOVE_METER_TAG
        if(mBundle.get("type") == 4)
            mCode = METER_TEST_TAG
        if (mBundle.get("type") == 5)
            mCode = CORRECT_BARNCH_SERVICE_TAG

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.fragment_request_receive_info, container, false)
        mListener = activity as IRequest
        FontSize(activity, view, "IRANSans(FaNum)_Medium.ttf")
        initAdapter(view)
        initBillIds(view)
        displayHelp();
        view.spn_type_after_sale_request.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if(mBundle.get("type") == 0){
                    if(position == 1)
                        mCode = DISCONNECT_BRANCH_TEMPORARY_TAG
                    if(position == 2)
                        mCode = RECONNECT_BRANCH_TAG
                }
                if(mBundle.get("type") == 1){
                    if(position == 1)
                        mCode = COLLECT__BRANCH_TEMPORARY_TAG
                    if(position == 2)
                        mCode = REINSTALL_BARNCH_TAG

                }
            }
        }
        return view
    }

    private fun displayHelp() {
        if (mBundle.get("type") == 0){
            WaringDialog.displayHelpMsg(activity,resources.getString(R.string.disconnect_reconnect_temporary), resources.getString(R.string.disconnect_reconnect_branch_content_info), true)
        }
        if (mBundle.get("type")== 1){
            WaringDialog.displayHelpMsg(activity, resources.getString(R.string.collect_install_branch),resources.getString(R.string.collect_install_branch_content_info), true)
        }
        if(mBundle.get("type")== 2){
            WaringDialog.displayHelpMsg(activity, resources.getString(R.string.collect_branch_permanent),resources.getString(R.string.collect_branch_permanent_content_info), true)

        }
        if (mBundle.get("type")== 3){
            WaringDialog.displayHelpMsg(activity,resources.getString(R.string.move_meter_branch),resources.getString(R.string.move_location_branch_content_info), true)
        }
        if (mBundle.get("type")== 4){
            WaringDialog.displayHelpMsg(activity, resources.getString(R.string.meter_test_branch), resources.getString(R.string.meter_test_branch_content_info), true)
        }
        if (mBundle.get("type")== 5){
            WaringDialog.displayHelpMsg(activity, resources.getString(R.string.correction_service_branch), resources.getString(R.string.correction_service_branch_content_info), true)
        }
    }




    private fun initBillIds(v: View) {
        mBills.addAll(BillCollection.get(activity).billIds)
        var adapter = ArrayAdapter(activity, android.R.layout.simple_spinner_item, mBills)
        v.edt_auto_bill_ids.threshold = 1
        v.edt_auto_bill_ids.setAdapter(adapter)
    }

    override fun onClick(v: View?) {
       if(validate()) {
           progress_receive_info.show()
           btn_request_receive_info.isEnabled = false
           var name: String = edt_auto_bill_ids.text.toString()
           mViewModel.getBranchDetail(name, mCode)
       }


    }

    fun validate(): Boolean{
        var isValid: Boolean = true
        if(edt_auto_bill_ids.text.toString().isEmpty()){
            tv_error_bill_id.visibility = View.VISIBLE
            isValid =false

        }else{
            tv_error_bill_id.visibility = View.GONE
        }
        if(mBundle.get("type") == 0 || mBundle.get("type") == 1)
        if(spn_type_after_sale_request.selectedItemPosition == 0){
            tv_error_request_type.visibility = View.VISIBLE
            tv_title.visibility = View.GONE
            isValid =false
        }else{
            tv_error_request_type.visibility = View.GONE
            tv_title.visibility = View.VISIBLE
        }
        return isValid
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btn_request_receive_info.setOnClickListener(this)

    }



    companion object {

        fun newInstance(type : Int)= RequestReceiveInfoFragment().apply{
            mBundle = Bundle()
            arguments.apply {
                mBundle.putInt("type", type) }

        }

    }


}