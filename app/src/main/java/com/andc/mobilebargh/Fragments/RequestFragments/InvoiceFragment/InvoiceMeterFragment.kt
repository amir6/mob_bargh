package com.andc.mobilebargh.Fragments.RequestFragments.InvoiceFragment

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import com.andc.mobilebargh.Activities.IRequestInvoice
import com.andc.mobilebargh.Fragments.DialogFramgments.WaringDialog
import com.andc.mobilebargh.Fragments.ServiceFragments.BaseFragment
import com.andc.mobilebargh.Models.CustomError
import com.andc.mobilebargh.R
import com.andc.mobilebargh.Utility.General
import com.andc.mobilebargh.Utility.Listeners.DatePicker
import com.andc.mobilebargh.Utility.Util.CustomTextWatcher
import com.andc.mobilebargh.repository.AttrebuteDef

import com.andc.mobilebargh.ViewModel.InvoiceVm
import com.andc.mobilebargh.databinding.FragmentInvoiceMeterUsageBinding
import com.andc.mobilebargh.repository.AttrebuteDef.Companion.clickonDrawableRight
import com.andc.mobilebargh.repository.model.MeterData
import kotlinx.android.synthetic.main.fragment_invoice_meter_usage.*


class InvoiceMeterFragment: BaseFragment(), View.OnTouchListener {


    private lateinit var mViewModel: InvoiceVm
    private lateinit var mBinding: FragmentInvoiceMeterUsageBinding
    private lateinit var mListener: IRequestInvoice


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel = activity.run {ViewModelProviders.of(this!!, viewModelFactory).get(InvoiceVm::class.java)}
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
       mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_invoice_meter_usage, null, false)
        var view = mBinding.root
        mBinding.meterData = MeterData()
        mListener = activity as IRequestInvoice
        General.FontSize(activity, view, "IRANSans(FaNum)_Medium.ttf")
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        back_btn!!.setOnClickListener {
            WaringDialog.exitDialog(activity, resources.getString(R.string.dialog_exit_body))
        }

            next_btn.setOnClickListener{
            it.let {
                if(valiadte()) {
                    mViewModel.setMeterValue(mBinding.meterData!!)
                    mViewModel.setDate(declare_usage_date.text.toString())
                    mListener.OnNextClick()
                }
            }
        }

        initDateDialog()

        declare_usage_date.setOnTouchListener(this)
        declare_usage_date.addTextChangedListener(CustomTextWatcher(declare_usage_date))
        declare_usage_avg.setOnTouchListener(this)
        declare_usage_avg.addTextChangedListener(CustomTextWatcher(declare_usage_avg))
        declare_usage_low.setOnTouchListener(this)
        declare_usage_low.addTextChangedListener(CustomTextWatcher(declare_usage_low))
        declare_usage_high.setOnTouchListener(this)
        declare_usage_high.addTextChangedListener(CustomTextWatcher(declare_usage_high))
        declare_usage_friday.setOnTouchListener(this)
        declare_usage_friday.addTextChangedListener(CustomTextWatcher(declare_usage_friday))
        declare_usage_reactive.setOnTouchListener(this)
        declare_usage_reactive.addTextChangedListener(CustomTextWatcher(declare_usage_reactive))
        declare_usage_dimand.setOnTouchListener(this)
        declare_usage_dimand.addTextChangedListener(CustomTextWatcher(declare_usage_dimand))


    }

    private fun valiadte(): Boolean {
        var isValid = true
        if(declare_usage_date.text.isEmpty()){
            isValid =false
            tv_error_date.visibility = View.VISIBLE

        }else{
            tv_error_date.visibility = View.GONE
        }
        if(declare_usage_avg.text.isEmpty()){
            isValid =false
            tv_error_usage_avr.visibility = View.VISIBLE
        }else{
            tv_error_usage_avr.visibility = View.GONE
        }


    return isValid
    }

    private fun initDateDialog() {
        declare_usage_date_container.setOnClickListener(DatePicker(declare_usage_date))
        declare_usage_date.setOnClickListener(DatePicker(declare_usage_date))
        declare_usage_date.setOnFocusChangeListener { v, hasFocus ->

            if(hasFocus){
                DatePicker(declare_usage_date).pickDate(declare_usage_date)
            }
        }
    }


    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
      when(v?.id){
          R.id.declare_usage_date -> clickonDrawableRight(declare_usage_date, event)
          R.id.declare_usage_avg -> clickonDrawableRight(declare_usage_avg, event)
          R.id.declare_usage_low -> clickonDrawableRight(declare_usage_low, event)
          R.id.declare_usage_high -> clickonDrawableRight(declare_usage_high, event)
          R.id.declare_usage_friday -> clickonDrawableRight(declare_usage_friday, event)
          R.id.declare_usage_reactive -> clickonDrawableRight(declare_usage_reactive, event)
          R.id.declare_usage_dimand -> clickonDrawableRight(declare_usage_dimand, event)

      }
        return false
    }

    companion object {
        fun newInstance() = InvoiceMeterFragment().apply {
            arguments.apply { }
        }
    }


}