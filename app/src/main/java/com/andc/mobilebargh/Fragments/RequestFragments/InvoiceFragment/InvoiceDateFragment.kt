package com.andc.mobilebargh.Fragments.RequestFragments.InvoiceFragment

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.andc.mobilebargh.Activities.IRequestInvoice
import com.andc.mobilebargh.Fragments.DialogFramgments.WaringDialog
import com.andc.mobilebargh.Fragments.ServiceFragments.BaseFragment
import com.andc.mobilebargh.R
import com.andc.mobilebargh.Utility.General
import com.andc.mobilebargh.Utility.Listeners.DatePicker
import com.andc.mobilebargh.ViewModel.InvoiceVm
import kotlinx.android.synthetic.main.fragment_request_date.*

class InvoiceDateFragment: BaseFragment(), View.OnClickListener {


    private lateinit var mViewmodel: InvoiceVm
    private lateinit var mListener:IRequestInvoice

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewmodel = activity.run { ViewModelProviders.of(this!!,viewModelFactory).get(InvoiceVm::class.java)}
}


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.fragment_request_date, container, false)
        mListener = activity as IRequestInvoice
        General.FontSize(activity, view, "IRANSans(FaNum)_Medium.ttf")
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        layout_request_date_container.setOnClickListener(DatePicker(edt_request_date))
        edt_request_date.setOnClickListener(DatePicker(edt_request_date))
        edt_request_date.setOnFocusChangeListener{v, hasFocus ->
            if(hasFocus){
                DatePicker(edt_request_date).pickDate(edt_request_date)
            }
        }
        back_btn.setOnClickListener(this)
        next_btn.setOnClickListener(this)





    }
    override fun onClick(v: View?) {
        when(v!!.id){
            R.id.back_btn -> WaringDialog.exitDialog(activity,resources.getString(R.string.dialog_exit_body))
            R.id.next_btn -> {
                if(validate()){
                    mViewmodel.setDate(edt_request_date.text.toString())
                    mListener.OnNextClick()
                }
            }


        }

    }

    private fun validate(): Boolean{
        var isValid: Boolean = true
        if(edt_request_date.text.toString().isEmpty()){
            isValid = false
            tv_error_date.visibility = View.VISIBLE
        }else{
            tv_error_date.visibility = View.GONE
        }
        return isValid
    }

    companion object {
        fun newInstance() =  InvoiceDateFragment().apply {
            arguments.apply {  }
        }
    }

}