package com.andc.mobilebargh.Fragments.DialogFramgments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.andc.mobilebargh.Activities.MainActivity;
import com.andc.mobilebargh.Controllers.BillCollection;
import com.andc.mobilebargh.Models.BillRecord;
import com.andc.mobilebargh.R;
import com.andc.mobilebargh.Utility.BillAPI.CheckDigitsUtility;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

/**
 * Created by Esbati on 4/24/2016.
 */
public class AddBillDialog extends DialogFragment {

    public final static int RC_BARCODE_CAPTURE = 9001;

    //Variables
    private String statusMessage;
    private String barcodeValue;

    //Global Views
    private View rootView;
    private TextView mBillId;
    private ImageView mScanBarcode;
    private ImageView mAlertIcon;
    private TextView mAlertMessage;
    private Button mAddNewBill;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        rootView = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_add_bill, null);

        setupView(rootView);
        return rootView;
    }

    private void setupView(View rootView){
        mBillId = (EditText)rootView.findViewById(R.id.new_bill_id);
        mAlertIcon = (ImageView)rootView.findViewById(R.id.new_bill_alert);
        mAlertMessage = (TextView)rootView.findViewById(R.id.new_bill_alert_message);
        mScanBarcode = (ImageView)rootView.findViewById(R.id.new_bill_scan);
        mAddNewBill = (Button)rootView.findViewById(R.id.new_bill_add);

        mScanBarcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentIntegrator integrator = IntentIntegrator.forSupportFragment(AddBillDialog.this);
                integrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);
                integrator.setOrientationLocked(false);
                integrator.setPrompt("بارکد روی قبض خود را در این مکان قرار دهید.");
                integrator.setBeepEnabled(false);
                integrator.initiateScan();

                // launch barcode activity.
                //Intent intent = new Intent(getActivity(), BarcodeCaptureActivity.class);
                //intent.putExtra(BarcodeCaptureActivity.AutoFocus, true);
                //intent.putExtra(BarcodeCaptureActivity.UseFlash, false);
                //startActivityForResult(intent, RC_BARCODE_CAPTURE);
            }
        });

        mAddNewBill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addNewBill(mBillId.getText().toString());
            }
        });
    }

    private boolean addNewBill(String newBillId){
        //Return Error if Bill Id is Empty or Is not a Valid Number
        if(TextUtils.isEmpty(newBillId)){
            mAlertIcon.setVisibility(View.VISIBLE);
            mAlertMessage.setVisibility(View.VISIBLE);
            mAlertMessage.setText(R.string.error_add_bill_nothing_received);
            return false;
        }

        //Check to See if the Bill Id is already added
        if(BillCollection.get(getActivity()).getBillIds().contains(newBillId)){
            mAlertIcon.setVisibility(View.VISIBLE);
            mAlertMessage.setVisibility(View.VISIBLE);
            mAlertMessage.setText(R.string.error_add_bill_duplicate);
            return false;
        }

        //Check to See if the Bill Id is Valid
        if(!TextUtils.isDigitsOnly(newBillId) || !CheckDigitsUtility.isDigitValid(newBillId)){
            mAlertIcon.setVisibility(View.VISIBLE);
            mAlertMessage.setVisibility(View.VISIBLE);
            mAlertMessage.setText(R.string.error_add_bill_invalid);
            return false;
        }

        //Save the New Bill Id
        BillRecord mNewBill = new BillRecord(newBillId);
        BillCollection.get(getActivity()).add(mNewBill);

        //TODO Move This Line to MainActivity.onActivityResult()
        BillCollection.get(getActivity()).setCurrentBill(mNewBill);

        //Close Dialog and Refresh Main Activity
        dismiss();
        ((MainActivity)getActivity()).onActivityResult(MainActivity.REQUEST_ADD_BILL, Activity.RESULT_OK, null);
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            if(result.getContents() == null) {
                Toast.makeText(getActivity(), R.string.barcode_failure, Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getActivity(), R.string.barcode_success, Toast.LENGTH_LONG).show();
            }

            barcodeValue = result.getContents();

            String newBillId = barcodeValue;
            if(barcodeValue != null && barcodeValue.length()>14){
                //newBillId = barcodeValue.substring(0, 13);
                //mBillId.setText(newBillId);
            }

            mBillId.setText(barcodeValue);
            addNewBill(newBillId);
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }

        /*
        switch (requestCode){
            case RC_BARCODE_CAPTURE:
                if (resultCode == CommonStatusCodes.SUCCESS) {
                    if (data != null) {
                        Barcode barcode = data.getParcelableExtra(BarcodeCaptureActivity.BarcodeObject);
                        statusMessage = getString(R.string.barcode_success);
                        barcodeValue = barcode.displayValue;
                        Log.d("MyTag", "Barcode read: " + barcode.displayValue);

                        String newBillId = "";
                        if(barcodeValue.length()>14)
                            newBillId = barcodeValue.substring(0, 13);

                        mBillId.setText(newBillId);
                        addNewBill(newBillId);
                    } else {
                        statusMessage = getString(R.string.barcode_failure);
                        Log.d("MyTag", "No barcode captured, intent data is null");

                        //Clear EditText and Show Error Message
                        //mBillId.setText("");
                        addNewBill(null);
                        Toast.makeText(getActivity(), statusMessage, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    statusMessage = String.format(getString(R.string.barcode_error),
                            CommonStatusCodes.getStatusCodeString(resultCode));

                    //Clear EditText and Show Error Message
                    //mBillId.setText("");
                    addNewBill(null);
                    Toast.makeText(getActivity(), statusMessage, Toast.LENGTH_SHORT).show();
                }
                break;

            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
        */
    }

}
