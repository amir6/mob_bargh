package com.andc.mobilebargh.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.BounceInterpolator;
import android.widget.TextView;

import com.andc.mobilebargh.R;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by Tenkei on 5/18/2016.
 */
public class PickLocationFragment extends Fragment {

    public final static String EXTRA_SELECTED_LOCATION = "extra_selected_location";

    //Global Vars
    private Toolbar mToolbar;
    private SupportMapFragment mMapFragment;
    private GoogleMap mMap;
    private BitmapDescriptor mDefaultMarker;
    private LatLng mSelectedLocation;
    private TextView mConfirmLocation;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments()!=null){
            mSelectedLocation = getArguments().getParcelable(EXTRA_SELECTED_LOCATION);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_pick_location, null, false);

        setupControllers(rootView);
        setupMapIfNeeded();
        return rootView;
    }

    public void setupControllers(View rootView){
        mConfirmLocation = (TextView)rootView.findViewById(R.id.confirm_location);
        mConfirmLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSelectedLocation != null) {
                    Intent data = new Intent();
                    data.putExtra(EXTRA_SELECTED_LOCATION, mSelectedLocation);
                    getActivity().setResult(Activity.RESULT_OK, data);
                } else {
                    getActivity().setResult(Activity.RESULT_CANCELED);
                }

                getActivity().finish();
            }
        });
    }

    public void setupMapIfNeeded(){
        if(mMapFragment == null){
            mMapFragment = ((SupportMapFragment)getChildFragmentManager().findFragmentById(R.id.map));

            if(mMapFragment!=null){
                mMapFragment.getMapAsync(new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(GoogleMap googleMap) {
                        mMap = googleMap;
                        loadMap(googleMap);
                    }
                });
            }
        }
    }

    public void loadMap(final GoogleMap map){
        if(map!=null){
            //Map Setup
            map.getUiSettings().setMapToolbarEnabled(false);

            map.setMyLocationEnabled(true);
            CameraUpdate initialZoom = CameraUpdateFactory.zoomTo(15f);
            map.moveCamera(initialZoom);

            if(mSelectedLocation!=null){
                //If a Selected Location is Available Show the Selection
                setSelectedLocation(mSelectedLocation);
            } else {
                //If Nor User Neither Selected Location Are Available, Zoom Back So User Can Select City
                CameraUpdate zoomBack = CameraUpdateFactory.zoomTo(5f);
                map.moveCamera(zoomBack);
            }


            map.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
                @Override
                public void onMapLongClick(LatLng latLng) {
                    mSelectedLocation = latLng;
                    setSelectedLocation(mSelectedLocation);
                }
            });
        }
    }

    private void setSelectedLocation(LatLng selectedLocation){
        //Move Camera to New Location
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLng(selectedLocation);
        mMap.animateCamera(cameraUpdate);

        //Add a Marker with Animation
        mMap.clear();
        Marker newMarker = mMap.addMarker(new MarkerOptions().position(selectedLocation));
        dropPinEffect(newMarker);


        //Show Confirm Button
        if (mConfirmLocation.getVisibility() != View.VISIBLE) {
            mConfirmLocation.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.popup_show));
            mConfirmLocation.setVisibility(View.VISIBLE);
            mMap.setPadding(0, 0, 0, 256);
        }
    }

    private void dropPinEffect(final Marker marker) {
        // Handler allows us to repeat a code block after a specified delay
        final android.os.Handler handler = new android.os.Handler();
        final long start = SystemClock.uptimeMillis();
        final long duration = 1500;

        // Use the bounce interpolator
        final android.view.animation.Interpolator interpolator =
                new BounceInterpolator();

        // Animate marker with a bounce updating its position every 15ms
        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                // Calculate t for bounce based on elapsed time
                float t = Math.max(
                        1 - interpolator.getInterpolation((float) elapsed
                                / duration), 0);
                // Set the anchor
                marker.setAnchor(0.5f, 1.0f + 14 * t);

                if (t > 0.0) {
                    // Post this event again 15ms from now.
                    handler.postDelayed(this, 15);
                } else { // done elapsing, show window
                    marker.showInfoWindow();
                }
            }
        });
    }

    public static PickLocationFragment newInstance(LatLng selectedLocation) {
        Bundle args = new Bundle();
        args.putParcelable(EXTRA_SELECTED_LOCATION, selectedLocation);

        PickLocationFragment fragment = new PickLocationFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
