package com.andc.mobilebargh.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.andc.mobilebargh.Controllers.BillCollection;
import com.andc.mobilebargh.Models.BillRecord;
import com.andc.mobilebargh.Models.BranchInfoRecord;
import com.andc.mobilebargh.Models.CustomError;
import com.andc.mobilebargh.Models.UsageHistoryRecord;
import com.andc.mobilebargh.Networking.Tasks.DeclareUsage;
import com.andc.mobilebargh.R;
import com.andc.mobilebargh.Utility.ErrorHandler;
import com.andc.mobilebargh.Utility.Listeners.DatePicker;

import java.util.ArrayList;

import ir.smartlab.persindatepicker.util.PersianCalendar;

/**
 * Created by Esbati on 12/19/2015.
 */
public class DeclareUsageFragment extends Fragment implements DeclareUsage.OnAsyncRequestComplete{

    private final static String EXTRA_BILL_ID = "extra_bill_id";

    //Global Views
    private View rootView;
    private Spinner mBillIdSpinner;
    private LinearLayout mFridayContainer;
    private LinearLayout mReactiveContainer;
    private TextView mDeclareDate;
    private TextView mSendUsage;

    //Global Attributes
    private String mBillId;
    private String mLastRead;

    @Override
    public void asyncResponse(ArrayList<CustomError> errors) {
        if(getActivity()==null)
            return;

        if(errors.get(0).mCode == 200){
            getActivity().finish();
        } else {
            ErrorHandler.showError(rootView, errors.get(0));
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        if(getArguments()!=null)
            mBillId = getArguments().getString(EXTRA_BILL_ID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_declare_usage, null);

        setupView(rootView);
        setupData();
        return rootView;
    }

    public void setupView(final View rootView){
        mFridayContainer = (LinearLayout)rootView.findViewById(R.id.declare_usage_friday_container);
        mReactiveContainer = (LinearLayout)rootView.findViewById(R.id.declare_usage_reactive_container);

        //Setup Bill Id Spinner
        mBillIdSpinner = (Spinner)rootView.findViewById(R.id.declare_usage_bill_id);
        ArrayAdapter<String> mSpinnerAdapter = new ArrayAdapter(getActivity(),  R.layout.category_spinner_item, BillCollection.get(getActivity()).getSpinnerArray());
        mSpinnerAdapter.setDropDownViewResource(R.layout.category_spinner_dropdown_item);
        mBillIdSpinner.setAdapter(mSpinnerAdapter);
        mBillIdSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                setupExtraFields();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        //Set Date Listeners
        mDeclareDate = (EditText)rootView.findViewById(R.id.declare_usage_date);
        View mDateContainer = rootView.findViewById(R.id.declare_usage_date_container);
        mDateContainer.setOnClickListener(new DatePicker(mDeclareDate));
        mDeclareDate.setOnClickListener(new DatePicker(mDeclareDate));
        mDeclareDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b)
                    new DatePicker(mDeclareDate).pickDate(mDeclareDate);
            }
        });

        //Set Send Button Listener
        mSendUsage = (TextView)rootView.findViewById(R.id.declare_usage_send);
        mSendUsage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String avgUsage = ((EditText) rootView.findViewById(R.id.declare_usage_avg)).getText().toString();
                if(TextUtils.isEmpty(avgUsage)){
                    ErrorHandler.showError(rootView, "مشترک گرامی مصرف میان باری شما نمی تواند خالی باشد.");
                    return;
                }

                String declareDate = mDeclareDate.getText().toString();
                String highUsage = ((EditText) rootView.findViewById(R.id.declare_usage_high)).getText().toString();
                String lowUsage = ((EditText) rootView.findViewById(R.id.declare_usage_low)).getText().toString();
                String fridayUsage = ((EditText) rootView.findViewById(R.id.declare_usage_friday)).getText().toString();
                String reactiveUsage = ((EditText) rootView.findViewById(R.id.declare_usage_reactive)).getText().toString();

                String selectedBillId = BillCollection.get(getActivity()).getBills().get(mBillIdSpinner.getSelectedItemPosition()).mBillId;
                if (checkDateRange(selectedBillId, declareDate)) {
                    new DeclareUsage(DeclareUsageFragment.this,
                            selectedBillId,
                            avgUsage,
                            highUsage,
                            lowUsage,
                            fridayUsage,
                            reactiveUsage,
                            declareDate).execute();
                }
            }
        });
    }

    public void setupData(){
        //Set Spinner Selection
        if(!TextUtils.isEmpty(mBillId))
        {
            ArrayList<BillRecord> mBills = BillCollection.get(getActivity()).getBills();
            for(int i = 0 ; i < mBills.size() ; i++)
                if(mBills.get(i).mBillId.equalsIgnoreCase(mBillId)){
                    mBillIdSpinner.setSelection(i);
                    break;
                }
        }

        mDeclareDate.setText(new PersianCalendar().getPersianShortDate());
    }

    public void setupExtraFields(){
        ArrayList<BranchInfoRecord> mBranchesInfo = (ArrayList<BranchInfoRecord>)BranchInfoRecord
                .find(BranchInfoRecord.class, BranchInfoRecord.LABEL_BILL_ID + "= ?", mBillId);

        //Hide Extra Fields from Normal Users, Show Them for Demand Users
        if(mBranchesInfo!=null && mBranchesInfo.size() > 0)
            if(mBranchesInfo.get(0).isDemand){
                mFridayContainer.setVisibility(View.VISIBLE);
                mReactiveContainer.setVisibility(View.VISIBLE);
            } else {
                mFridayContainer.setVisibility(View.GONE);
                mReactiveContainer.setVisibility(View.GONE);
            }
    }

    public boolean checkDateRange(String billId, String declareDate){
        //Declared Date is Invalid if it is after Current Date
        if (declareDate.compareTo(new PersianCalendar().getPersianShortDate()) > 0) {
            Snackbar.make(rootView, "تاریخ وارد شده صحیح نمی باشد!", Snackbar.LENGTH_SHORT).show();
            return false;
        }

        //Get Usage History by Bill Id sorted Based on Current Read Date in Descending Order
        ArrayList<UsageHistoryRecord> mSortedUsageRecords = (ArrayList<UsageHistoryRecord>) UsageHistoryRecord.find(
                UsageHistoryRecord.class, UsageHistoryRecord.LABEL_BILL_ID + " = ?  ORDER BY F_curr_rdg_date DESC", billId);

        //Get Raw Last Read Date (In English)
        mLastRead = null;
        if(mSortedUsageRecords.size() > 0)
            mLastRead = mSortedUsageRecords.get(0).FCurrRdgDate;

        //Declare Date is Invalid if there is No Last Read Date
        if (TextUtils.isEmpty(mLastRead)){
            Snackbar.make(rootView, "تاریخ قرائت قبلی برای این انشعاب موجود نمی باشد.", Snackbar.LENGTH_SHORT).show();
            return false;
        }

        //Declare Date is Invalid if it is before Last Read Date
        if (declareDate.compareTo(mLastRead) <= 0) {
            Snackbar.make(rootView, "کنتور شما پس از این تاریخ قرائت شده است.", Snackbar.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    public static DeclareUsageFragment newInstance(String billId) {

        Bundle args = new Bundle();
        args.putString(EXTRA_BILL_ID, billId);

        DeclareUsageFragment fragment = new DeclareUsageFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
