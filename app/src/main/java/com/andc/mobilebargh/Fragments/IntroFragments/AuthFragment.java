package com.andc.mobilebargh.Fragments.IntroFragments;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.andc.mobilebargh.Activities.IntroActivity;
import com.andc.mobilebargh.Activities.MainActivity;
import com.andc.mobilebargh.Activities.SplashActivity;
import com.andc.mobilebargh.Controllers.BillCollection;
import com.andc.mobilebargh.Controllers.PreferencesHelper;
import com.andc.mobilebargh.Models.CustomError;
import com.andc.mobilebargh.Networking.WSHelper;
import com.andc.mobilebargh.R;
import com.andc.mobilebargh.Services.AlarmManagerHelper;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.util.ArrayList;

/**
 * Created by Esbati on 5/29/2016.
 */
public class AuthFragment extends Fragment {

    //Global Views
    private SwipeRefreshLayout mSwipeRefresh;
    private EditText mCodeEditText;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_authentication, null, false);

        setupView(rootView);
        return rootView;
    }

    public void setupView(View rootView){
        mSwipeRefresh = (SwipeRefreshLayout)rootView.findViewById(R.id.swipe_refresh);
        mSwipeRefresh.setEnabled(false);
        mCodeEditText = (EditText)rootView.findViewById(R.id.auth_code);
        mCodeEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 5) {
                    //Close Keyboard
                    View view = getActivity().getCurrentFocus();
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }

                    //Start Load Animation and Disable Code EditText
                    mSwipeRefresh.setRefreshing(true);
                    mCodeEditText.setEnabled(false);
                    new AuthenticateUser(PreferencesHelper.load(PreferencesHelper.KEY_CELLPHONE), charSequence.toString()).execute();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private class AuthenticateUser extends AsyncTask<Void, Void, ArrayList<CustomError>> {
        private String mCellphone;
        private String mAuthCode;

        public AuthenticateUser(String cellphone, String authCode) {
            mCellphone = cellphone;
            mAuthCode = authCode;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList<CustomError> doInBackground(Void... voids) {
            return WSHelper.authenticate(mCellphone, mAuthCode);
        }

        @Override
        protected void onPostExecute(ArrayList<CustomError> errors) {
            //Stop Loading Animation and Enable EditText Again
            mCodeEditText.setEnabled(true);
            if(mSwipeRefresh.isRefreshing())
                mSwipeRefresh.setRefreshing(false);

            //Show Default Message if no Response Received From Server
            if(errors==null || errors.size()==0){
                Snackbar.make(((IntroActivity) getActivity()).mParentView, R.string.error_async_no_response, Snackbar.LENGTH_LONG).show();
                return;
            }

            //Show Response Message if Unsuccessful, Else Enter Application
            if (errors.get(0).mCode != 200)
                Snackbar.make(((IntroActivity) getActivity()).mParentView, errors.get(0).mMsg, Snackbar.LENGTH_LONG).show();
            else
                enterApplication();
        }

        public void enterApplication(){
            //Register User Login Info with Parse Server
            registerWithParse();
            //registerWithFabric();
            AlarmManagerHelper.registerWithAlarmManager(getActivity());

            //Move to Splash Screen if Any Bills Available to Load, Else Skip to Main Activity
            Intent i;
            if(BillCollection.get(getActivity()).getBills()!= null && BillCollection.get(getActivity()).getBills().size()>0)
                i = new Intent(getActivity(), SplashActivity.class);
            else
                i = new Intent(getActivity(), MainActivity.class);

            startActivity(i);
            getActivity().finish();
        }

        public void registerWithParse(){
            //Deactivate Previous Login Object if User Previously Signed In On This Device
            String loginId = PreferencesHelper.load(PreferencesHelper.KEY_LOGIN_ID);
            if(!TextUtils.isEmpty(loginId)){
                ParseQuery<ParseObject> parseQuery = ParseQuery.getQuery("LoginInfo");
                parseQuery.getInBackground(loginId, new GetCallback<ParseObject>() {
                    @Override
                    public void done(ParseObject loginObject, ParseException e) {
                        if (e == null) {
                            loginObject.put("isActive", false);
                            loginObject.saveEventually();
                        }
                    }
                });
            }

            //Register New Login Object with Server
            WifiManager manager = (WifiManager)getActivity().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            WifiInfo info = manager.getConnectionInfo();
            String macAddress = info.getMacAddress();
            String installationId = ParseInstallation.getCurrentInstallation().getInstallationId();


            final ParseObject testObject = new ParseObject("LoginInfo");
            testObject.put("Cellphone", PreferencesHelper.load(PreferencesHelper.KEY_CELLPHONE));
            testObject.put("InstallationId", installationId);
            testObject.put("MacAddress", macAddress);
            testObject.put("InitialVersion", PreferencesHelper.APP_CURRENT_VERSION);
            testObject.put("isActive", true);
            testObject.saveEventually(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    PreferencesHelper.save(PreferencesHelper.KEY_LOGIN_ID, testObject.getObjectId());
                }
            });
        }
    }

  /*  private void registerWithFabric() {
        Crashlytics.setUserIdentifier(PreferencesHelper.load(PreferencesHelper.KEY_CELLPHONE));
        Crashlytics.setInt(UpdateFile.TAG_UPDATE_VERSION_CODE, PreferencesHelper.APP_CURRENT_VERSION);
    } */
}
