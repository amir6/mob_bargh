package com.andc.mobilebargh.Fragments.RequestFragments.TariffChangeFragmemts

import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.andc.mobilebargh.Activities.IRequestTariff
import com.andc.mobilebargh.Controllers.BillCollection
import com.andc.mobilebargh.Fragments.DialogFramgments.ResponseStatusDialog
import com.andc.mobilebargh.Fragments.DialogFramgments.WaringDialog
import com.andc.mobilebargh.Fragments.ServiceFragments.BaseFragment
import com.andc.mobilebargh.R
import com.andc.mobilebargh.Utility.Util.ServerStatus
import com.andc.mobilebargh.ViewModel.TariffVm
import kotlinx.android.synthetic.main.fragment_request_receive_info.*
import kotlinx.android.synthetic.main.fragment_tariff_recieve_info.view.*


class TarrifReceiveInfoFragment: BaseFragment(), View.OnClickListener {
     private lateinit var mViewModel: TariffVm
    private lateinit var mListener: IRequestTariff
    var mBills: ArrayList<String> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel = activity.run { ViewModelProviders.of(this!!, viewModelFactory).get(TariffVm::class.java)}
        mViewModel.checkTariffStat.observe(this, object: Observer<ServerStatus?> {
            override fun onChanged(t: ServerStatus?) {
                if(t == ServerStatus.RECEIVE){
                    progress_receive_info.hide()
                    mListener.onNextClick()

                }

                if(t == ServerStatus.ERROR){
                    progress_receive_info.hide()
                    btn_request_receive_info.isEnabled = true
                    var dialog : ResponseStatusDialog = ResponseStatusDialog(activity)
                    dialog.showFail()

                }
            }
        })
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.fragment_tariff_recieve_info, container, false)
        mListener = activity as IRequestTariff
        displayHelp();

        return view
    }

    private fun displayHelp() {
        WaringDialog.displayHelpMsgDoc(activity, resources.getString(R.string.tariff_header_text), resources.getString(R.string.tariff_change_content_info), true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btn_request_receive_info.setOnClickListener(this)
        initBillIds(view)
    }

    override fun onClick(v: View?) {
         if(isValidate()){
             progress_receive_info.show()
             btn_request_receive_info.isEnabled = false
             var name: String = edt_auto_bill_ids.text.toString()
             mViewModel.getBranchDetail(name)


         }
    }



    private fun isValidate(): Boolean {
        var isValid = true
        if(edt_auto_bill_ids.text.toString().isEmpty()){
            tv_error_bill_id.visibility = View.VISIBLE
            isValid =false

        }else{
            tv_error_bill_id.visibility = View.GONE
        }

        return isValid
    }

    private fun initBillIds(v: View) {
        mBills.addAll(BillCollection.get(activity).billIds)
        var adapter = ArrayAdapter(activity, android.R.layout.simple_spinner_item, mBills)
        v.edt_auto_bill_ids.threshold = 1
        v.edt_auto_bill_ids.setAdapter(adapter)
    }

    companion object {
     fun newInstance() = TarrifReceiveInfoFragment().apply {
         arguments.apply {  }
     }
 }


}