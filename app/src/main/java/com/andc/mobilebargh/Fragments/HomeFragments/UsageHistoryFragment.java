package com.andc.mobilebargh.Fragments.HomeFragments;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.andc.mobilebargh.Adapter.UsageHistoryAdapter;
import com.andc.mobilebargh.Models.UsageHistoryRecord;
import com.andc.mobilebargh.R;
import com.andc.mobilebargh.Utility.Components.HeaderFragment;
import com.andc.mobilebargh.Utility.Components.ObservableHorizontalScrollView;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;

/**
 * Created by win on 11/17/2015.
 */
public class UsageHistoryFragment extends HeaderFragment {
    //Fragment Tags
    public final static int mTitle = R.string.tabs_usage_history;
    public final static String TAG_BILL_ID = "bill_id";
    public final static String CHART_MODE_VALUE_KEY = "chart_mode";
    public final static String SORT_DESC_VALUE_KEY = "sort_desc";
    public final static String SORT_BASE_VALUE_KEY = "sort_base";

    //Fragment Variables
    private boolean isFirstTime = true;
    private boolean isChartMode;
    private boolean isHeaderDetached;
    private boolean sortIsDesc;
    private String sortBasedOn;
    private int screenWidth;
    private int headerWidth;
    private int listWidth;
    private int listScrollPosition;

    //Global Var
    private View rootView;
    private String mBillId;
    private ArrayList<UsageHistoryRecord> mUsageRecords;

    //Usage List
    private ObservableHorizontalScrollView mHeaderContainer;
    private LinearLayout mListHeader;
    private ObservableHorizontalScrollView mUsageTable;
    private ListView mListView;
    private UsageHistoryAdapter mListAdapter;

    //Usage Charts
    private LinearLayout mUsageChart;
    private FrameLayout mChartContainer;
    private int mUnitType;
    private int mChartType;
    private Spinner mChartUnitSpinner;
    private ArrayAdapter<String> mUnitSpinnerAdapter;

    //Sort Constants
    private final static String Sort_Usage_saleyear = "saleyear";
    private final static String Sort_Usage_saleprd = "saleprd";
    private final static String Sort_Usage_FPrevRdgDate = "F_prev_rdg_date";
    private final static String Sort_Usage_FCurrRdgDate = "F_curr_rdg_date";
    private final static String Sort_Usage_useact = "useact";
    private final static String Sort_Usage_prdamt = "prdamt";
    private final static String Sort_Usage_crdbtot = "crdbtot";
    private final static String Sort_Usage_bilamt = "bilamt";

    @Override
    public boolean moveFragmentHeaderToActivity() {

        if(actionBarHandlerInterface == null)
            return false;

        mHeaderContainer = (ObservableHorizontalScrollView) getFragmentHeader();
        if (actionBarHandlerInterface.moveHeaderToActivity(mHeaderContainer)) {
            //If new Header Attached Successfully, Remove Old Header and Setup New Header
            mListHeader.setVisibility(View.GONE);
            mListHeader = (LinearLayout) mHeaderContainer.findViewById(R.id.usage_list_header);
            setupListHeader(mListHeader);
            isHeaderDetached = true;

            mHeaderContainer.setScrollViewListener(new ObservableHorizontalScrollView.ScrollViewListener() {
                @Override
                public void onScrollChanged(ObservableHorizontalScrollView scrollView, int x, int y, int oldx, int oldy) {
                    mUsageTable.scrollTo(x, y);
                }
            });

            mUsageTable.setScrollViewListener(new ObservableHorizontalScrollView.ScrollViewListener() {
                @Override
                public void onScrollChanged(ObservableHorizontalScrollView scrollView, int x, int y, int oldx, int oldy) {
                    mHeaderContainer.scrollTo(x, y);
                    listScrollPosition = x;
                }
            });


            mHeaderContainer.getViewTreeObserver().addOnGlobalLayoutListener(
                    new ViewTreeObserver.OnGlobalLayoutListener() {
                        @Override
                        public void onGlobalLayout() {
                            if(isFirstTime) {
                                //On Fragment Shown for the First Time in Pager, Scroll List and Header to Right
                                mUsageTable.scrollTo(listWidth, 0);
                                mHeaderContainer.scrollTo(listWidth, 0);
                                listScrollPosition = listWidth;
                                isFirstTime = false;
                            } else {
                                //Else Scroll Header to Match List Position
                                mHeaderContainer.scrollTo(listScrollPosition, 0);
                            }

                            mHeaderContainer.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                        }
                    }
            );
            return true;
        }

        return false;
    }

    @Override
    protected View getFragmentHeader() {
        return LayoutInflater.from(getActivity()).inflate(R.layout.list_header_usage, null);
    }

    @Override
    public boolean onRefreshFragment(String newBillId) {
        mBillId = newBillId;
        fetchSortedUsageRecords(newBillId, sortIsDesc, sortBasedOn);
        setupChart(rootView, mChartType, mUnitType);
        return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        if(savedInstanceState!=null) {
            isChartMode = savedInstanceState.getBoolean(CHART_MODE_VALUE_KEY);
            sortIsDesc = savedInstanceState.getBoolean(SORT_DESC_VALUE_KEY);
            sortBasedOn = savedInstanceState.getString(SORT_BASE_VALUE_KEY);
        } else {
            //Default Sort Order
            sortIsDesc = true;
            sortBasedOn = Sort_Usage_FCurrRdgDate;
        }

        //Get Sorted Usage History Records from DB
        mBillId = getArguments().getString(TAG_BILL_ID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        rootView = inflater.inflate(R.layout.fragment_usage_history, container, false);

        setupView(rootView);
        setupList(rootView);
        setupPageWidth();
        return rootView;
    }

    private void setupView(final View rootView){
        //Define Views
        mUsageChart = (LinearLayout)rootView.findViewById(R.id.usage_charts);
        mUsageTable = (ObservableHorizontalScrollView)rootView.findViewById(R.id.usage_table);

        //Set Visibility Based on Current Mode (Chart Mode is Disabled By Default
        setViewMode(isChartMode);

        //Setup Chart Unit Spinner
        mUnitSpinnerAdapter = new ArrayAdapter<>(
                getActivity(), R.layout.spinner_item_dark, getResources().getStringArray(R.array.chart_units)
        );
        mUnitSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mChartUnitSpinner = (Spinner)rootView.findViewById(R.id.charts_unit_spinner);
        mChartUnitSpinner.setAdapter(mUnitSpinnerAdapter);
        mChartUnitSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mUnitType = i;
                //Setup Chart with the Selected Unit Type
                setupChart(rootView, 1, mUnitType);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void setupList(View rootView){
        //Setup List Header
        mListHeader = (LinearLayout)rootView.findViewById(R.id.usage_list_header);
        if(mListHeader!=null)
            setupListHeader(mListHeader);

        //Setup List
        mUsageRecords = fetchSortedUsageRecords(mBillId, sortIsDesc, sortBasedOn);
        mListAdapter = new UsageHistoryAdapter(getActivity(), mUsageRecords);
        mListView = (ListView)rootView.findViewById(R.id.list);
        mListView.setAdapter(mListAdapter);
    }

    private void setupListHeader(LinearLayout mListHeader){
        //Setup List Header Columns
        TextView mUsageYear = (TextView)mListHeader.findViewById(R.id.USAGE_saleyear);
        mUsageYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sortBasedOn == Sort_Usage_saleyear)
                    sortIsDesc = !sortIsDesc;
                else
                    sortBasedOn = Sort_Usage_saleyear;

                fetchSortedUsageRecords(mBillId, sortIsDesc, sortBasedOn);
            }
        });

        TextView mUsagePeriod = (TextView)mListHeader.findViewById(R.id.USAGE_saleprd);
        mUsagePeriod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sortBasedOn == Sort_Usage_saleprd)
                    sortIsDesc = !sortIsDesc;
                else
                    sortBasedOn = Sort_Usage_saleprd;

                fetchSortedUsageRecords(mBillId, sortIsDesc, sortBasedOn);
            }
        });

        TextView mLastRead = (TextView)mListHeader.findViewById(R.id.USAGE_FPrevRdgDate);
        mLastRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sortBasedOn == Sort_Usage_FPrevRdgDate)
                    sortIsDesc = !sortIsDesc;
                else
                    sortBasedOn = Sort_Usage_FPrevRdgDate;

                fetchSortedUsageRecords(mBillId, sortIsDesc, sortBasedOn);
            }
        });

        TextView mCurrRead = (TextView)mListHeader.findViewById(R.id.USAGE_FCurrRdgDate);
        mCurrRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sortBasedOn == Sort_Usage_FCurrRdgDate)
                    sortIsDesc = !sortIsDesc;
                else
                    sortBasedOn = Sort_Usage_FCurrRdgDate;

                fetchSortedUsageRecords(mBillId, sortIsDesc, sortBasedOn);
            }
        });

        TextView mUsageTotal = (TextView)mListHeader.findViewById(R.id.USAGE_useact);
        mUsageTotal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sortBasedOn == Sort_Usage_useact)
                    sortIsDesc = !sortIsDesc;
                else
                    sortBasedOn = Sort_Usage_useact;

                fetchSortedUsageRecords(mBillId, sortIsDesc, sortBasedOn);
            }
        });

        TextView mPayableCurrPrd = (TextView)mListHeader.findViewById(R.id.USAGE_prdamt);
        mPayableCurrPrd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sortBasedOn == Sort_Usage_prdamt)
                    sortIsDesc = !sortIsDesc;
                else
                    sortBasedOn = Sort_Usage_prdamt;

                fetchSortedUsageRecords(mBillId, sortIsDesc, sortBasedOn);
            }
        });

        TextView mPayableLastPrd = (TextView)mListHeader.findViewById(R.id.USAGE_crdbtot);
        mPayableLastPrd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sortBasedOn == Sort_Usage_crdbtot)
                    sortIsDesc = !sortIsDesc;
                else
                    sortBasedOn = Sort_Usage_crdbtot;

                fetchSortedUsageRecords(mBillId, sortIsDesc, sortBasedOn);
            }
        });

        TextView mPayableTotal = (TextView)mListHeader.findViewById(R.id.USAGE_bilamt);
        mPayableTotal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sortBasedOn == Sort_Usage_bilamt)
                    sortIsDesc = !sortIsDesc;
                else
                    sortBasedOn = Sort_Usage_bilamt;

                fetchSortedUsageRecords(mBillId, sortIsDesc, sortBasedOn);
            }
        });

        setSearchIndicator(mListHeader, sortIsDesc, sortBasedOn);
    }

    private void setupPageWidth(){
        //Set List Header Width
        WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        Point screenSize = new Point();
        display.getSize(screenSize);
        screenWidth = screenSize.x;

        mListView.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        //Get Page Width
                        listWidth = mListView.getWidth();
                        mUsageTable.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    }
                }
        );
    }

    private boolean setupChart(View rootView, int chartType, int unitNumber){
        //BarChart mChart = (BarChart)rootView.findViewById(R.id.charts);
        mChartContainer = (FrameLayout)rootView.findViewById(R.id.chart_container);
        mChartContainer.removeAllViews();

        BarChart mChart = new BarChart(getActivity());
        mChartContainer.addView(mChart);


        //Get Usage Records sorted Based on "Current Read" in Ascending Order
        ArrayList<UsageHistoryRecord> mChartUsageRecords;
        mChartUsageRecords = (ArrayList<UsageHistoryRecord>) UsageHistoryRecord.find(
                UsageHistoryRecord.class, UsageHistoryRecord.TAG_billid + " = ? ORDER BY F_curr_rdg_date", mBillId);
        if(mChartUsageRecords==null){
            //No Data to Show
            return false;
        }

        //Fill Chart Data
        mChart.setDescription("");
        mChart.setData(setChartData(unitNumber, mChartUsageRecords));
        mChart.animateX(1000);
        //setupLimitLine(mChart, 0);
        mChart.getAxisRight().setEnabled(false);

        Typeface normal = Typeface.createFromAsset(getActivity().getAssets(),"fonts/IRANSans(FaNum).ttf");
        mChart.getAxisLeft().setTypeface(normal);
        mChart.getXAxis().setTypeface(normal);
        mChart.invalidate();
        return true;
    }

    private void setupLimitLine(BarChart mChart, int avgUsage){
        //Return if There is no Average Data to Show
        if(avgUsage==0)
            return;

        //Set Limit Line
        LimitLine line = new LimitLine(84f);
        line.setLineColor(Color.BLACK);
        line.setLineWidth(4f);
        line.setTextColor(Color.BLACK);
        line.setTextSize(12f);
        line.setLabel("متوسط مصرف کشوری");

        //Assign Limit Line to Chart
        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.setDrawLimitLinesBehindData(true);
        leftAxis.addLimitLine(line);
    }

    private BarData setChartData(int unitType, ArrayList<UsageHistoryRecord> mChartUsageRecords){
        //Initiate Arrays
        ArrayList<String> mXAxis = new ArrayList<>();
        ArrayList<BarEntry> valueSet = new ArrayList<>();

        for(int counter = 0 ; counter < mChartUsageRecords.size() ; counter++){
            UsageHistoryRecord mUsageRecord = mChartUsageRecords.get(counter);

            //Get the Data Based on Chart Unit Type
            float barDataValue;
            switch (unitType){
                case 0:
                    barDataValue = (float)mUsageRecord.useact;
                    break;

                case 1:
                    barDataValue = (float)mUsageRecord.prdamt;
                    break;

                default:
                    barDataValue = (float)mUsageRecord.useact;
                    break;

            }

            //Set the Data
            BarEntry v1 = new BarEntry(barDataValue, counter);
            valueSet.add(v1);
            mXAxis.add(mUsageRecord.FPrevRdgDate);
        }

        //Set the Chart Unit Type Label
        String barUnitType;
        switch (unitType){
            case 0:
                barUnitType = "مصرف";
                break;
            case 1:
                barUnitType = "مبلغ";
                break;
            default:
                barUnitType = "مصرف";
                break;
        }

        BarDataSet barDataSet = new BarDataSet(valueSet, barUnitType + " دوره های گذشته ");
        barDataSet.setColor(getResources().getColor(R.color.andc_accent));

        BarData data = new BarData(mXAxis, barDataSet);
        return data;
    }

    private BarData setComparisonChartData(int unitType, ArrayList<UsageHistoryRecord> mChartUsageRecords){
        //Initiate Arrays
        ArrayList<String> mXAxis = new ArrayList<>();
        ArrayList<BarDataSet> mDataSets = new ArrayList<>();
        ArrayList<BarEntry> valueSet1 = new ArrayList<>();
        ArrayList<BarEntry> valueSet2 = new ArrayList<>();

        //Prepare Arrays for Insertion -> Make an Empty Chart BarData
        for (int i = 0; i < 6; i++) {
            BarEntry v = new BarEntry(0.000f, i);
            valueSet1.add(v);
            valueSet2.add(v);
            mXAxis.add("");
        }

        int lastSalePrd = 0;
        int currentYearChartIndex = 5;
        int previousYearChartIndex = 5;
        int arrayIndex = mChartUsageRecords.size()-1;
        for(int counter = 0 ; counter < 12 ; counter++){

            //Return if we reach the end of array
            if(arrayIndex < 0)
                break;

            //Starting from the latest Records Get the Record and Place it in the Proper Column
            UsageHistoryRecord mUsageRecord = mChartUsageRecords.get(arrayIndex);

            //If the Record is repeated or not valid
            //Repeat the loop with the same Counter but move to the next Record
            if(mUsageRecord.saleprd==99
                    || mUsageRecord.saleprd==lastSalePrd){
                counter--;
                arrayIndex--;
                continue;
            }

            //Get the Data Based on Chart Unit Type
            float barDataValue;
            switch (unitType){
                case 0:
                    barDataValue = (float)mUsageRecord.useact;
                    break;

                case 1:
                    barDataValue = (float)mUsageRecord.prdamt;
                    break;

                default:
                    barDataValue = (float)mUsageRecord.useact;
                    break;

            }

            //Set the Data as Current Period's Data
            if(counter<6){
                BarEntry v2 = new BarEntry(barDataValue, currentYearChartIndex);
                valueSet2.set(currentYearChartIndex, v2);
                mXAxis.set(currentYearChartIndex, " دوره " + mUsageRecord.saleprd);
                currentYearChartIndex--;
            }

            //Set the Data as  Previous Period's Data
            if(counter>=6){
                BarEntry v1 = new BarEntry(barDataValue, previousYearChartIndex);
                valueSet1.set(previousYearChartIndex, v1);
                previousYearChartIndex--;
            }

            lastSalePrd = mUsageRecord.saleprd;
            arrayIndex--;
        }

        //Set the Chart Unit Type Label
        String barUnitType;
        switch (unitType){
            case 0:
                barUnitType = "مصرف";
                break;
            case 1:
                barUnitType = "مبلغ";
                break;
            default:
                barUnitType = "مصرف";
                break;
        }

        BarDataSet barDataSet1 = new BarDataSet(valueSet1, barUnitType + " دوره های گذشته ");
        barDataSet1.setColor(getResources().getColor(android.R.color.holo_blue_dark));
        BarDataSet barDataSet2 = new BarDataSet(valueSet2, barUnitType + " دوره های جاری ");
        barDataSet2.setColor(getResources().getColor(R.color.andc_accent));

        mDataSets.add(barDataSet1);
        mDataSets.add(barDataSet2);

        BarData data = new BarData(mXAxis, mDataSets);
        return data;
    }

    public ArrayList<UsageHistoryRecord> fetchSortedUsageRecords(String billId, boolean sortIsDesc, String sortBasedOn){
        ArrayList<UsageHistoryRecord> mSortedUsageRecords;

        if(sortIsDesc)
            mSortedUsageRecords = (ArrayList<UsageHistoryRecord>) UsageHistoryRecord.find(
                    UsageHistoryRecord.class, UsageHistoryRecord.TAG_billid + " = ?  ORDER BY " + sortBasedOn + " DESC", billId);
        else
            mSortedUsageRecords = (ArrayList<UsageHistoryRecord>) UsageHistoryRecord.find(
                    UsageHistoryRecord.class, UsageHistoryRecord.TAG_billid + " = ?  ORDER BY "+ sortBasedOn, billId);

        //Update Adapter
        if(mListAdapter!=null)
            mListAdapter.setItems(mSortedUsageRecords);


        if(mListHeader != null)
            setSearchIndicator(mListHeader, sortIsDesc, sortBasedOn);
        return mSortedUsageRecords;
    }

    private void setSearchIndicator(LinearLayout listHeader, boolean sortIsDesc, String sortBasedOn){
        Drawable emptyDrawable = getResources().getDrawable(R.drawable.empty_drawable);
        Drawable disableDrawable = getResources().getDrawable(R.drawable.ic_new_chevron_disable_white_24dp);
        Drawable downDrawable = getResources().getDrawable(R.drawable.ic_new_chevron_down_white_24dp);
        Drawable upDrawable = getResources().getDrawable(R.drawable.ic_new_chevron_up_white_24dp);
        Drawable sortIndicator = sortIsDesc ? downDrawable : upDrawable;

        //Clear All TextView
        for (int i = 0 ; i < listHeader.getChildCount() ; i++){
            ((TextView)listHeader.getChildAt(i)).setCompoundDrawablesWithIntrinsicBounds(disableDrawable, null, null, null);
        }

        //Set Proper TextView
        switch(sortBasedOn){
            case "saleyear":
                ((TextView)listHeader.getChildAt(7)).setCompoundDrawablesWithIntrinsicBounds(sortIndicator, null, null, null);
                break;

            case "saleprd":
                ((TextView)listHeader.getChildAt(6)).setCompoundDrawablesWithIntrinsicBounds(sortIndicator, null, null, null);
                break;

            case "F_prev_rdg_date":
                ((TextView)listHeader.getChildAt(5)).setCompoundDrawablesWithIntrinsicBounds(sortIndicator, null, null, null);
                break;

            case "F_curr_rdg_date":
                ((TextView)listHeader.getChildAt(4)).setCompoundDrawablesWithIntrinsicBounds(sortIndicator, null, null, null);
                break;

            case "useact":
                ((TextView)listHeader.getChildAt(3)).setCompoundDrawablesWithIntrinsicBounds(sortIndicator, null, null, null);
                break;

            case "prdamt":
                ((TextView)listHeader.getChildAt(2)).setCompoundDrawablesWithIntrinsicBounds(sortIndicator, null, null, null);
                break;

            case "crdbtot":
                ((TextView)listHeader.getChildAt(1)).setCompoundDrawablesWithIntrinsicBounds(sortIndicator, null, null, null);
                break;

            case "bilamt":
                ((TextView)listHeader.getChildAt(0)).setCompoundDrawablesWithIntrinsicBounds(sortIndicator, null, null, null);
                break;

            default:
                ((TextView)listHeader.getChildAt(4)).setCompoundDrawablesWithIntrinsicBounds(sortIndicator, null, null, null);
                break;
        }
    }

    public void setViewMode(boolean isChartMode){
        if (isChartMode) {
            mUsageChart.setVisibility(View.VISIBLE);
            mUsageTable.setVisibility(View.INVISIBLE);
        } else {
            mUsageChart.setVisibility(View.INVISIBLE);
            mUsageTable.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        //Add Menu Item
        menu.add(0, R.id.show_charts, Menu.CATEGORY_SECONDARY, "نمودار")
                .setIcon(R.drawable.ic_chart_histogram_white_24dp)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        menu.findItem(R.id.show_charts).setIcon(isChartMode ? R.drawable.ic_view_list_white_24dp : R.drawable.ic_chart_histogram_white_24dp);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.show_charts:
                //Toggle Chart Mode
                isChartMode = !isChartMode;
                item.setIcon(isChartMode ? R.drawable.ic_view_list_white_24dp : R.drawable.ic_chart_histogram_white_24dp);
                setViewMode(isChartMode);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(CHART_MODE_VALUE_KEY, isChartMode);
        outState.putBoolean(SORT_DESC_VALUE_KEY, sortIsDesc);
        outState.putString(SORT_BASE_VALUE_KEY, sortBasedOn);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        setMenuVisibility(false);
    }

    public static Fragment newInstance (String bill_id){
        Bundle args = new Bundle();

        args.putString(TAG_BILL_ID, bill_id);
        Fragment fragment = new UsageHistoryFragment();
        fragment.setArguments(args);

        return fragment;
    }
}