package com.andc.mobilebargh.Fragments.DialogFramgments;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.andc.mobilebargh.Activities.MainActivity;
import com.andc.mobilebargh.Controllers.PreferencesHelper;
import com.andc.mobilebargh.Utility.BillAPI.Shenase;
import com.andc.mobilebargh.Models.BranchInfoRecord;
import com.andc.mobilebargh.Models.UsageHistoryRecord;
import com.andc.mobilebargh.R;

import java.util.ArrayList;

/**
 * Created by Esbati on 1/17/2016.
 */
public class PaymentDialog extends DialogFragment {
    public final static String EXTRA_BILL_ID = "bill_id";
    public final static String EXTRA_IS_MAINACTIVITY = "is_main_activity";

    //Global Views
    private AlertDialog mDialog;
    private View rootView;
    private String mBillId;
    private BranchInfoRecord mBillInfo;
    private UsageHistoryRecord mLastUsageRecord;
    private boolean isMainActivity;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        rootView = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_payment, null);
        setupData();

        mDialog = new AlertDialog.Builder(getActivity())
                .setView(rootView)
                .setTitle(R.string.dialog_payment_title)
                .setNegativeButton(getResources().getString(R.string.dialog_button_return), null)
                .setPositiveButton(getResources().getString(R.string.dialog_button_confirm), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {


                        String mTestPayId = Shenase.GenerateShenasePardakht(
                                mBillInfo.BillId, mBillInfo.CrDbTot, "" + mLastUsageRecord.saleyear, "" + mLastUsageRecord.saleprd
                        );

                        if (mBillId != null && mBillId != "" && mTestPayId != null && mTestPayId != "") {
                            /*Intent intent = new Intent(getActivity(),MainActivity.class);
                            startActivity(intent);*/
                            if (!isMainActivity)
                                getActivity().finish();
                            else
                                ((MainActivity) getActivity()).payBill(
                                        Long.parseLong(mBillInfo.BillId)
                                        , Long.parseLong(mBillInfo.PaymentId)
                                        , PreferencesHelper.load(PreferencesHelper.KEY_CELLPHONE)
                                );

                        }


                    }
                })
                .create();

        setupView(rootView);
        return mDialog;
    }

    @TargetApi(17)
    @Override
    public void onStart() {
        super.onStart();

        //Set Title Gravity
        final int alertTitle = this.getResources().getIdentifier("alertTitle", "id", "android");
        TextView messageText = (TextView) mDialog.findViewById(alertTitle);
        messageText.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
    }

    private void setupData() {
        //Get Bill Info
        mBillId = getArguments().getString(EXTRA_BILL_ID);
        isMainActivity = getArguments().getBoolean(EXTRA_IS_MAINACTIVITY);
        mBillInfo = new BranchInfoRecord();
        ArrayList<BranchInfoRecord> mBranchList = (ArrayList<BranchInfoRecord>) BranchInfoRecord.find(BranchInfoRecord.class, "Bill_id = ?", mBillId);
        if (mBranchList != null)
            if (mBranchList.size() > 0)
                mBillInfo = mBranchList.get(0);

        //Generate Test Pay ID
        mLastUsageRecord = new UsageHistoryRecord();
        ArrayList<UsageHistoryRecord> mSortedUsageRecords;
        mSortedUsageRecords = (ArrayList<UsageHistoryRecord>) UsageHistoryRecord.find(
                UsageHistoryRecord.class, UsageHistoryRecord.TAG_billid + " = ?  ORDER BY " + "F_curr_rdg_date" + " DESC", mBillId);

        if (mSortedUsageRecords != null && mSortedUsageRecords.size() > 0)
            mLastUsageRecord = mSortedUsageRecords.get(0);
    }

    private void setupView(View rootView) {
        TextView mPaymentOwnerName = (TextView) rootView.findViewById(R.id.payment_owner);
        TextView mPaymentPayAmount = (TextView) rootView.findViewById(R.id.payment_amount);
        TextView mPaymentPayDate = (TextView) rootView.findViewById(R.id.payment_pay_limit_date);
        TextView mPaymentBillId = (TextView) rootView.findViewById(R.id.payment_bill_id);
        TextView mPaymentPayId = (TextView) rootView.findViewById(R.id.payment_pay_id);
        TextView mPaymentSaleYear = (TextView) rootView.findViewById(R.id.payment_sale_year);
        TextView mPaymentSalePeriod = (TextView) rootView.findViewById(R.id.payment_sale_period);

        mPaymentOwnerName.setText(mBillInfo.OwnerName);
        mPaymentPayAmount.setText(mBillInfo.CrDbTot);
        mPaymentPayDate.setText(mBillInfo.LstPayLimitDate);
        mPaymentBillId.setText(mBillInfo.BillId);
        mPaymentPayId.setText(mBillInfo.PaymentId);
        mPaymentSaleYear.setText("" + mLastUsageRecord.saleyear);
        mPaymentSalePeriod.setText("" + mLastUsageRecord.saleprd);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == MainActivity.REQUEST_PAY_BILLS_TOP) {
            switch (resultCode) {

            }
        }
        //On Successful Payment Close Payment Dialog
    }

    public static DialogFragment newInstance(String billId, Boolean isMainActivity) {
        Bundle args = new Bundle();
        args.putString(EXTRA_BILL_ID, billId);
        args.putBoolean(EXTRA_IS_MAINACTIVITY, isMainActivity);
        DialogFragment mPaymentDialog = new PaymentDialog();
        mPaymentDialog.setArguments(args);

        return mPaymentDialog;
    }
}
