package com.andc.mobilebargh.Fragments.IntroFragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.andc.mobilebargh.R;

/**
 * Created by Esbati on 12/23/2015.
 */
public class IntroFragment extends Fragment {
    final static String EXTRA_TITLE = "extra_title";
    final static String EXTRA_DESCRIPTION = "extra_message";
    final static String EXTRA_IMAGE = "extra_image";

    private int mImageResId;
    private int mTitleResId;
    private int mDescriptionResId;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_intro, null, false);

        handleIntent();
        setupView(rootView);
        return rootView;
    }

    public void handleIntent(){
        mImageResId = getArguments().getInt(EXTRA_IMAGE);
        mTitleResId = getArguments().getInt(EXTRA_TITLE);
        mDescriptionResId = getArguments().getInt(EXTRA_DESCRIPTION);
    }

    public void setupView(View rootView){
        ImageView mIntroImage = (ImageView)rootView.findViewById(R.id.intro_image);
        mIntroImage.setImageResource(mImageResId);

        TextView mIntroTitle = (TextView)rootView.findViewById(R.id.intro_title);
        mIntroTitle.setText(mTitleResId);

        TextView mIntroDescription = (TextView)rootView.findViewById(R.id.intro_description);
        mIntroDescription.setText(mDescriptionResId);
    }

    public static Fragment newInstance (int intro_image, int intro_message, int intro_title){
        Bundle args = new Bundle();

        args.putInt(EXTRA_IMAGE, intro_image);
        args.putInt(EXTRA_TITLE, intro_title);
        args.putInt(EXTRA_DESCRIPTION, intro_message);
        Fragment fragment = new IntroFragment();
        fragment.setArguments(args);

        return fragment;
    }
}
