package com.andc.mobilebargh.Fragments.RequestFragments.EnquiryRequestFragment

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.andc.mobilebargh.Fragments.ServiceFragments.BaseFragment
import com.andc.mobilebargh.R
import com.andc.mobilebargh.ViewModel.EnquiryVm

class EnquiryReceiveInfoFagment: BaseFragment() {
    private lateinit var mViewModel: EnquiryVm

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel = ViewModelProviders.of(this, viewModelFactory).get(EnquiryVm::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater.inflate( R.layout.fragment_enquiry_receive_info, container, false)

        return  view
    }


    companion object {
         fun newInstance() = EnquiryReceiveInfoFagment().apply {
             arguments.apply {  }
         }
    }
}