package com.andc.mobilebargh.Fragments.RequestFragments.TariffChangeFragmemts

import android.app.Activity
import android.app.Dialog
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import com.andc.mobilebargh.Activities.IRequestTariff
import com.andc.mobilebargh.Fragments.DialogFramgments.ResponseStatusDialog
import com.andc.mobilebargh.Fragments.DialogFramgments.WaringDialog
import com.andc.mobilebargh.Fragments.ServiceFragments.BaseFragment
import com.andc.mobilebargh.Models.PersonalDocumentInfo
import com.andc.mobilebargh.R
import com.andc.mobilebargh.Utility.General
import com.andc.mobilebargh.Utility.Util.Constants.ANDC_FOLDER_NAME
import com.andc.mobilebargh.Utility.Util.Constants.COMMIT_DOC
import com.andc.mobilebargh.Utility.Util.Constants.INVOICE_BILL_COPY
import com.andc.mobilebargh.Utility.Util.Constants.NATIONAL_CARD_BACK
import com.andc.mobilebargh.Utility.Util.Constants.NATIONAL_CARD_FRONT
import com.andc.mobilebargh.Utility.Util.Constants.REQUEST_CODE_CAMERA
import com.andc.mobilebargh.Utility.Util.Constants.TARIFF_CHANGE_TAG
import com.andc.mobilebargh.Utility.Util.DocumentsFile
import com.andc.mobilebargh.Utility.Util.ServerStatus
import com.andc.mobilebargh.ViewModel.TariffVm
import kotlinx.android.synthetic.main.fragment_document.view.*
import kotlinx.android.synthetic.main.support_fragment_document.*
import java.io.File

class TariffDocumentFragment: BaseFragment(), View.OnClickListener {


    private lateinit var mListener:IRequestTariff
    private lateinit var mViewModel: TariffVm
    lateinit var mBundle: Bundle
    private lateinit var mCode : String
    private lateinit var mBillId : String
    internal var number: Int = 0
    var cart_up = NATIONAL_CARD_FRONT
    var cart_down = NATIONAL_CARD_BACK
    var sh = INVOICE_BILL_COPY
    var comm = COMMIT_DOC

    internal lateinit var filePath: File
    internal var folder_name = ANDC_FOLDER_NAME

    private lateinit var personalDocumentInfo: PersonalDocumentInfo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mListener = activity as IRequestTariff
        mViewModel = activity.run { ViewModelProviders.of(this!!, viewModelFactory).get(TariffVm::class.java) }
        mBillId = mViewModel.getBillId
        personalDocumentInfo = PersonalDocumentInfo()
        mCode = TARIFF_CHANGE_TAG
        setCode()

        mViewModel.checkTariffStat.observe(this, object: Observer<ServerStatus?> {
            override fun onChanged(t: ServerStatus?) {
                if(t == ServerStatus.SENT){
                    progress_send_document.hide()
                    mListener.onNextClick()
                }
                if(t == ServerStatus.ERROR){
                    progress_send_document.hide()
                    var dialog : ResponseStatusDialog = ResponseStatusDialog(activity)
                    dialog.showFail()
                }
            }
        })
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = layoutInflater.inflate(R.layout.fragment_tariff_document, container, false)
        init(view, folder_name)
        General.FontSize(activity, view, "IRANSans(FaNum)_Medium.ttf")
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        next_btn.setOnClickListener(this)
        cart_up_img!!.setOnClickListener {

            checkImage(view, R.id.cart_up_img, cart_up, 1)
        }

        cart_down_img!!.setOnClickListener{
            checkImage(view,R.id.cart_down_img,  cart_down, 2)
        }

        sh_img!!.setOnClickListener {
            checkImage(view, R.id.sh_img, sh, 3)
        }

        doc_one_img!!.setOnClickListener {
            checkImage(view, R.id.doc_one_img ,comm, 4)
        }
    }
    private fun init(view: View, folder_name: String) {


        val directory = File(Environment.getExternalStorageDirectory().toString() + "/" + folder_name)

        if (!directory.exists()) {
            directory.mkdirs()
        } else {


            val files = directory.listFiles()
            for (i in files!!.indices) {
                Log.d("Files" + files[i], "FileName:" + files[i].name)
                if (files[i].name.toString() == "$cart_up.jpg") {
                    var file = File(directory, "$cart_up.jpg")
                    var uri = Uri.fromFile(file)
                    view.cart_up_img!!.setImageURI(uri)
                    personalDocumentInfo.nationalCartFront = File(directory, "$cart_up.jpg")
                } else if (files[i].name.toString() == "$cart_down.jpg") {
                    var file = File(directory, "$cart_down.jpg")
                    var uri = Uri.fromFile(file)
                    view.cart_down_img!!.setImageURI(uri)
                    personalDocumentInfo.nationalCartFront = File(directory, "$cart_down.jpg")
                } else if (files[i].name.toString() == "$sh.jpg") {
                    var file = File(directory, "$sh.jpg")
                    var uri = Uri.fromFile(file)
                    view.sh_img!!.setImageURI(uri)
                    personalDocumentInfo.nationalCartFront = File(directory, "$sh.jpg")
                } else if (files[i].name.toString() == "$comm.jpg") {
                    var file = File(directory, "$comm.jpg")
                    var uri = Uri.fromFile(file)
                    view.doc_one_img!!.setImageURI(uri)
                    personalDocumentInfo.nationalCartFront = File(directory, "$comm.jpg")
                }
            }
        }

    }



    private fun setCode() {
        cart_up = NATIONAL_CARD_FRONT + "_" + mBillId + "_" + mCode
        cart_down = NATIONAL_CARD_BACK + "_" + mBillId + "_" + mCode
        sh =  INVOICE_BILL_COPY + "_" + mBillId + "_" + mCode
        comm = COMMIT_DOC + "_" + mBillId + "_" + mCode

    }
    private fun prepareInterface() {
        checkExistsImage(cart_up, 1)
        checkExistsImage(cart_down, 2)
        checkExistsImage(sh, 3)
        checkExistsImage(comm, 4)

    }
    private fun checkImage(view: View,id: Int , imageName: String, data: Int) {
        filePath = File(Environment.getExternalStorageDirectory().toString() + "/" + folder_name + "/" + imageName + ".jpg")
        if (filePath.exists()) {
            ImageDialog(view,id, imageName)
        } else {
            dialog(data)
        }
    }
    private fun chooseFile(index: Int, file: File?) {
        when (index) {
            1 -> personalDocumentInfo!!.nationalCartFront = file
            2 -> personalDocumentInfo!!.nationalCartBehind = file
            3 -> personalDocumentInfo!!.identityCart = file
            4 -> personalDocumentInfo!!.commitment = file

        }
    }
    private fun checkExistsImage(imageName: String, index: Int) {
        filePath = File(Environment.getExternalStorageDirectory().toString() + "/" + folder_name + "/" + imageName + ".jpg")
        if (filePath.exists())
            chooseFile(index, filePath)
        else
            chooseFile(index, null)
    }
    private fun isExistsFile(imageName: String): Boolean{
        filePath = File(Environment.getExternalStorageDirectory().toString() + "/" + folder_name + "/" + imageName + ".jpg")
        return (filePath.exists())

    }
    private fun Validate(): Boolean {

        var isValidate = true
        if(isExistsFile(cart_up)) {
            tv_error_front_card.visibility = View.GONE

        }
        else {
            tv_error_front_card.visibility = View.VISIBLE
            cart_up_txt.visibility = View.GONE
            isValidate = false
        }
        if(isExistsFile(cart_down)) {
            tv_error_back_card.visibility = View.GONE
        }
        else {
            tv_error_back_card.visibility = View.VISIBLE
            cart_down_txt.visibility = View.GONE
            isValidate =false
        }
        if(isExistsFile(sh)) {
            tv_error_invoice_bill.visibility = View.GONE

        }else
        {tv_error_invoice_bill.visibility =View.VISIBLE
            sh_txt.visibility = View.GONE
            isValidate =false}
        if (isExistsFile(comm)) {
            tv_error_ownership.visibility = View.GONE
        }else {
            isValidate =false
            tv_error_ownership.visibility = View.VISIBLE
            doc_one_txt.visibility = View.GONE
        }

        return isValidate
    }
    private fun ImageDialog(v: View,id:Int,  imageName: String) {

        val layoutInflater = LayoutInflater.from(v.context)
        val view = layoutInflater.inflate(R.layout.dialog_image, null)

        val aleartDialogBuilder = AlertDialog.Builder(v.context)
        aleartDialogBuilder.setView(view)
        aleartDialogBuilder.setCancelable(true)


        val delete = view.findViewById<View>(R.id.delete_img) as Button
        val perv = view.findViewById<View>(R.id.per_image) as Button
        val img = view.findViewById<View>(R.id.c_image) as ImageView


        val alertDialog = aleartDialogBuilder.create()


        val dir = File(Environment.getExternalStorageDirectory().toString() + "/" + folder_name)
        //Bitmap out = Bitmap.createScaledBitmap(bitmap, 1166, 2048, true);

        val file = File(dir, "$imageName.jpg")
        val bmp = BitmapFactory.decodeFile(file.toString())
        img.setImageBitmap(bmp)

        delete.setOnClickListener {
            //delete image
            file.delete()

            when(id){
                R.id.cart_up_img -> cart_up_img.setImageDrawable(ContextCompat.getDrawable(activity!!, R.drawable.camera_icon))
                R.id.cart_down_img -> cart_down_img.setImageDrawable(ContextCompat.getDrawable(activity!!, R.drawable.camera_icon))
                R.id.sh_img -> sh_img.setImageDrawable(ContextCompat.getDrawable(activity!!, R.drawable.camera_icon))
                R.id.doc_one_img -> doc_one_img.setImageDrawable(ContextCompat.getDrawable(activity!!, R.drawable.camera_icon))
            }
            alertDialog.dismiss()
        }




        perv.setOnClickListener { alertDialog.dismiss() }
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alertDialog.show()

    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        var uri: Uri? = null

        if (data != null) {
            uri = data.data
        }
        if (requestCode == 0 && resultCode == Activity.RESULT_OK && data != null) {
            when (number) {
                1 -> DocumentsFile.saveImage(getContext()!!, cart_up, uri!!, cart_up_img!!)
                2 -> DocumentsFile.saveImage(getContext()!!, cart_down, uri!!, cart_down_img!!)
                3 -> DocumentsFile.saveImage(getContext()!!, sh, uri!!, sh_img!!)
                4 -> DocumentsFile.saveImage(getContext()!!, comm, uri!!, doc_one_img!!)

            }

        } else if (requestCode == 1 && resultCode == Activity.RESULT_OK && data != null && data.data != null) {
            DocumentsFile.saveImage(getContext()!!, cart_up, uri!!, cart_up_img!!)
        } else if (requestCode == 2 && resultCode == Activity.RESULT_OK && data != null && data.data != null) {
            DocumentsFile.saveImage(getContext()!!, cart_down, uri!!, cart_down_img!!)
        } else if (requestCode == 3 && resultCode == Activity.RESULT_OK && data != null && data.data != null) {
            DocumentsFile.saveImage(getContext()!!, sh, uri!!, sh_img!!)
        } else if (requestCode == 4 && resultCode == Activity.RESULT_OK && data != null && data.data != null) {
            DocumentsFile.saveImage(getContext()!!, comm, uri!!, doc_one_img!!)
        }


    }
    //**************************************************************************************************
    private fun dialog(num: Int) {
        val ad = AlertDialog.Builder(getContext()!!)
                .create()
        ad.setCancelable(true)
        //ad.setTitle("s");
        ad.setMessage("برای ارسال عکس، از گالری یا دوربین استفاده کنید.")
        ad.setButton(Dialog.BUTTON_POSITIVE, "گالری") { dialog, which ->

            val i = Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            startActivityForResult(i, num)

        }
        ad.setButton(Dialog.BUTTON_NEGATIVE, "دوربین") { dialog, which ->
            number = num
            if(ActivityCompat.checkSelfPermission(activity!!.applicationContext ,android.Manifest.permission.CAMERA ) == PackageManager.PERMISSION_GRANTED){
                val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(intent, 0)}
            else{
                grantPersmission()
            }
        }
        ad.show()
    }
    private fun grantPersmission() {
        if(ActivityCompat.checkSelfPermission(activity!!.applicationContext ,android.Manifest.permission.CAMERA ) != PackageManager.PERMISSION_GRANTED){
            requestPermissions(arrayOf(android.Manifest.permission.CAMERA), REQUEST_CODE_CAMERA)

        }
    }
    companion object {
        fun newInstance() = TariffDocumentFragment().apply {
            arguments.apply {  }
        }
    }
      override fun onClick(v: View?) {
        when(v?.id){
            next_btn.id -> {
                prepareInterface()
                if(Validate()){
                        progress_send_document.show()
                        mViewModel.sendTariffChange(personalDocumentInfo)
                }

            }
            back_btn.id -> {
                WaringDialog.exitDialog(activity,resources.getString(R.string.dialog_exit_body))
            }

        }
    }
}