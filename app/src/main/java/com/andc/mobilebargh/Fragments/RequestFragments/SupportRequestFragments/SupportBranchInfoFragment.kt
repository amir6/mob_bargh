package com.andc.mobilebargh.Fragments.RequestFragments.SupportRequestFragments

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.andc.mobilebargh.Activities.IRequest
import com.andc.mobilebargh.Fragments.DialogFramgments.WaringDialog
import com.andc.mobilebargh.Fragments.ServiceFragments.BaseFragment
import com.andc.mobilebargh.Models.*
import com.andc.mobilebargh.R
import com.andc.mobilebargh.Utility.General
import com.andc.mobilebargh.repository.model.BranchData
import com.andc.mobilebargh.ViewModel.ReceiveBranchVm
import com.andc.mobilebargh.databinding.FragmentBranchInfoBinding
import kotlinx.android.synthetic.main.fragment_current_user.*


class SupportBranchInfoFragment: BaseFragment(), View.OnClickListener {

   lateinit var mListener: IRequest
   lateinit var mViewModel: ReceiveBranchVm
    lateinit var billData : BillData
    private lateinit var mBinding: FragmentBranchInfoBinding




    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_branch_info, null, false)

        mListener = activity as IRequest
        billData = BillData()
        mBinding.data = BranchData()
        var v = mBinding.root
        General.FontSize(activity, v, "IRANSans(FaNum)_Medium.ttf")
        return v
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel = activity.run {ViewModelProviders.of(this!!, viewModelFactory).get(ReceiveBranchVm::class.java)  }
        mViewModel.getDetail.observe(this, object: Observer<BranchData?> {
            override fun onChanged(t: BranchData?) {

                if (t != null) {
                    mBinding.data = t



                }

            }
        })
    }






    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        next_btn.setOnClickListener(this)
        back_btn.setOnClickListener(this)

    }
    override fun onClick(v: View?) {
        when(v?.id) {
           next_btn.id -> mListener.OnNextClick()
            back_btn.id -> WaringDialog.exitDialog(activity,resources.getString(R.string.dialog_exit_body))


        }
    }



    companion object {


        fun newInstance()= SupportBranchInfoFragment().apply {

       arguments.apply {  }
        }

    }

}