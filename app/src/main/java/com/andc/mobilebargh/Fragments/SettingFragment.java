package com.andc.mobilebargh.Fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.andc.mobilebargh.Controllers.BillCollection;
import com.andc.mobilebargh.Models.BillRecord;
import com.andc.mobilebargh.Models.UpdateFile;
import com.andc.mobilebargh.Networking.Tasks.CheckUpdate;
import com.andc.mobilebargh.Networking.Tasks.SyncBills;
import com.andc.mobilebargh.R;
import com.andc.mobilebargh.Utility.Cell.HeaderCell;
import com.andc.mobilebargh.Utility.Cell.ShadowSectionCell;
import com.andc.mobilebargh.Utility.Cell.TextCheckCell;
import com.andc.mobilebargh.Utility.Cell.TextDetailSettingsCell;
import com.andc.mobilebargh.Utility.Cell.TextInfoCell;
import com.andc.mobilebargh.Utility.Cell.TextSettingsCell;
import com.andc.mobilebargh.Controllers.PreferencesHelper;
import com.andc.mobilebargh.Utility.Components.BottomSheet;
import com.andc.mobilebargh.Utility.Components.LayoutHelper;

/**
 * Created by Esbati on 2/10/2016.
 */
public class SettingFragment extends Fragment implements CheckUpdate.OnAsyncRequestComplete{

    //Global Views
    private View rootView;
    protected Dialog visibleDialog = null;
    private LinearLayout mSettingContainer;
    private LinearLayout mBillContainer;
    private TextSettingsCell mDeadLineAlert;

    //Setting Values
    private boolean isBillContainerShown;
    public final static String[] deadlineValues = {"هیچوقت", "یک روز", "سه روز", "یک هفته", "دو هفته", "یک ماه"};
    public final static int[] deadlineDays = {0, 1, 3, 7, 14, 30};

    @Override
    public void asyncResponse(UpdateFile updateFile) {
        if(updateFile==null)
            return;

        if(updateFile.mVersionCode>PreferencesHelper.getCurrentVersion())
            CheckUpdate.promoteUpdate(getActivity(), updateFile);
        else
            Toast.makeText(getActivity(), "برنامه به روز می باشد!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_setting_empty, container, false);

        setupView(rootView);
        return rootView;
    }

    private void setupView(View rootView){
        mSettingContainer = (LinearLayout)rootView.findViewById(R.id.setting_container);
        addSettings(mSettingContainer);
    }

    private void addSettings(LinearLayout settingContainer){
        //SharedPreferences preferences = getActivity().getSharedPreferences();
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());


        //>>>SMS Configuration<<<
        HeaderCell mInfoSection = new HeaderCell(getActivity());
        mInfoSection.setText(getString(R.string.setting_info));
        settingContainer.addView(mInfoSection);

        TextDetailSettingsCell mCellphoneField = new TextDetailSettingsCell(getActivity());
        mCellphoneField.setTextAndValue(PreferencesHelper.load(PreferencesHelper.KEY_CELLPHONE), getString(R.string.setting_info_cellphone), true);
        settingContainer.addView(mCellphoneField);

        settingContainer.addView(new ShadowSectionCell(getActivity()));


        //>>>Send & Receive Configuration<<<
        HeaderCell mConnectivity = new HeaderCell(getActivity());
        mConnectivity.setText("ارسال و دریافت اطلاعات");
        settingContainer.addView(mConnectivity);

        TextDetailSettingsCell mReceiveBills = new TextDetailSettingsCell(getActivity());
        mReceiveBills.setTextAndValue("دریافت قبض جدید", "چاپ, پیامک, نوتیفیکیشن", true);
        mReceiveBills.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //showBillSheet();
                showBillContainer();
            }
        });
        settingContainer.addView(mReceiveBills);

        TextCheckCell mSplashLoading = new TextCheckCell(getActivity());
        mSplashLoading.setTextAndCheck("بروزرسانی قبض ها به هنگام ورود به برنامه", PreferencesHelper.isOptionActive(PreferencesHelper.KEY_SPLASH_LOADING), false);
        mSplashLoading.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PreferencesHelper.toggleOption(PreferencesHelper.KEY_SPLASH_LOADING);
                ((TextCheckCell) view).setChecked(PreferencesHelper.isOptionActive(PreferencesHelper.KEY_SPLASH_LOADING));
            }
        });
        settingContainer.addView(mSplashLoading);
        settingContainer.addView(new ShadowSectionCell(getActivity()));

        //>>>Notification Configuration<<<
        HeaderCell mNotificationSection = new HeaderCell(getActivity());
        mNotificationSection.setText("اعلان ها");
        settingContainer.addView(mNotificationSection);

        mDeadLineAlert = new TextSettingsCell(getActivity());
        mDeadLineAlert.setTextAndValue("اخطار پیش از سررسید مهلت پرداخت", "" + deadlineValues[PreferencesHelper.loadInt(PreferencesHelper.KEY_REMIND_ME_BEFORE)], false);
        mDeadLineAlert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("از چه مدت قبل");
                final NumberPicker numberPicker = new NumberPicker(getActivity());
                numberPicker.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
                numberPicker.setMinValue(0);
                numberPicker.setMaxValue(5);
                numberPicker.setDisplayedValues(deadlineValues);
                numberPicker.setValue(PreferencesHelper.loadInt(PreferencesHelper.KEY_REMIND_ME_BEFORE));
                builder.setView(numberPicker);
                builder.setNegativeButton(R.string.dialog_button_confirm, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        PreferencesHelper.saveInt(PreferencesHelper.KEY_REMIND_ME_BEFORE, numberPicker.getValue());
                        setDeadlineAlert();
                    }
                });
                builder.create().show();
            }
        });
        settingContainer.addView(mDeadLineAlert);
        settingContainer.addView(new ShadowSectionCell(getActivity()));


        //>>>Backup & Recovery Configuration<<<
        HeaderCell mUpdateSection = new HeaderCell(getActivity());
        mUpdateSection.setText("بروزرسانی");
        settingContainer.addView(mUpdateSection);

        TextCheckCell mIntroUpdateCheck = new TextCheckCell(getActivity());
        mIntroUpdateCheck.setTextAndCheck("بروزرسانی برنامه به هنگام ورود", PreferencesHelper.isOptionActive(PreferencesHelper.KEY_AUTO_UPDATE), true);
        mIntroUpdateCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PreferencesHelper.toggleOption(PreferencesHelper.KEY_AUTO_UPDATE);
                ((TextCheckCell) view).setChecked(PreferencesHelper.isOptionActive(PreferencesHelper.KEY_AUTO_UPDATE));
            }
        });
        settingContainer.addView(mIntroUpdateCheck);

        TextDetailSettingsCell mCheckForUpdate = new TextDetailSettingsCell(getActivity());
        mCheckForUpdate.setTextAndValue("بروزرسانی برنامه", "آخرین بررسی: " + PreferencesHelper.load(PreferencesHelper.KEY_UPDATE_LAST_CHECK), true);
        mCheckForUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new CheckUpdate(SettingFragment.this).execute();
            }
        });
        settingContainer.addView(mCheckForUpdate);
        settingContainer.addView(new ShadowSectionCell(getActivity()));

        //Application Version
        String versionName;
        try{
            PackageInfo info = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            versionName = info.versionName;
        } catch (Exception e){
            Toast.makeText(getActivity(), "مشکل در بازیابی نام نسخه", Toast.LENGTH_SHORT).show();
            versionName = "1.x.x";
        }

        TextInfoCell mApplicationVersion = new TextInfoCell(getActivity());
        mApplicationVersion.setText("خدمات مشترکین نسخه ی آزمایشی " + versionName);
        settingContainer.addView(mApplicationVersion);
    }

    private void showBillSheet(){
        //Create Bottom Sheet
        BottomSheet.Builder builder = new BottomSheet.Builder(getActivity());
        builder.setApplyTopPaddings(false);
        LinearLayout linearLayout = new LinearLayout(getActivity());
        linearLayout.setOrientation(LinearLayout.VERTICAL);

        //Set Rows
        for (int a = 0; a < BillCollection.get(getActivity()).getBills().size() ; a++) {
            //Get Row Data
            final BillRecord bill = BillCollection.get(getActivity()).getBills().get(a);

            //Create Row and Set Data
            LinearLayout bottomRow = (LinearLayout)LayoutInflater.from(getActivity()).inflate(R.layout.list_item_setting_bill, null);
            ((TextView)bottomRow.findViewById(R.id.bill_title)).setText(BillCollection.get(getActivity()).getFace().get(a));
            CheckBox shouldPrint = (CheckBox)bottomRow.findViewById(R.id.bill_print);
            shouldPrint.setTag(a);
            shouldPrint.setChecked(bill.mShouldPrint);
            shouldPrint.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    boolean isChecked = ((CheckBox)view).isChecked();
                    BillCollection.get(getActivity()).getBills().get((Integer)view.getTag()).mShouldPrint = isChecked;
                }
            });
            CheckBox shouldSMS = (CheckBox)bottomRow.findViewById(R.id.bill_sms);
            shouldSMS.setTag(a);
            shouldSMS.setChecked(bill.mShouldSMS);
            shouldSMS.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //boolean isChecked = ((CheckBox) view).isChecked();
                    //BillCollection.get(getActivity()).getBills().get((Integer)view.getTag()).mShouldSMS = isChecked;
                    Toast.makeText(getActivity(), "در حال حاضر امکان فعال سازی این گزینه وجود ندارد.", Toast.LENGTH_SHORT).show();
                }
            });
            CheckBox shouldPush = (CheckBox)bottomRow.findViewById(R.id.bill_push);
            shouldPush.setTag(a);
            shouldPush.setChecked(bill.mShouldPush);
            shouldPush.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    boolean isChecked = ((CheckBox) view).isChecked();
                    BillCollection.get(getActivity()).getBills().get((Integer) view.getTag()).mShouldPush = isChecked;
                }
            });

            linearLayout.addView(bottomRow, LayoutHelper.createLinear(LayoutHelper.MATCH_PARENT, 48));
        }

        //Add Footer
        BottomSheet.BottomSheetCell cell = new BottomSheet.BottomSheetCell(getActivity(), 2);
        cell.setBackgroundResource(R.drawable.list_selector);
        cell.setTextAndIcon("بازگشت", 0);
        cell.setTextColor(0xffcd5a5a);
        cell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                visibleDialog.dismiss();
            }
        });
        linearLayout.addView(cell, LayoutHelper.createLinear(LayoutHelper.MATCH_PARENT, 48));

        //Show
        builder.setCustomView(linearLayout);
        BottomSheet bottomSheet = builder.create();
        bottomSheet.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                visibleDialog = null;
                BillCollection.get(getActivity()).saveChanges();
                PreferencesHelper.setOption(PreferencesHelper.KEY_SYNC_NECESSARY, true);
                new SyncBills().execute();
            }
        });
        bottomSheet.show();
        visibleDialog = bottomSheet;
    }

    public void showBillContainer(){
        if(!isBillContainerShown){
            //Create and Set Bill Container if It is Null
            if(mBillContainer==null)
                setupBillContainer();

            isBillContainerShown = true;
            mSettingContainer.addView(mBillContainer, 5);
        } else {
            mSettingContainer.removeView(mBillContainer);
            isBillContainerShown = false;
            BillCollection.get(getActivity()).saveChanges();
            PreferencesHelper.setOption(PreferencesHelper.KEY_SYNC_NECESSARY, true);
            new SyncBills().execute();
        }
    }

    public void setupBillContainer(){
        mBillContainer = new LinearLayout(getActivity());
        mBillContainer.setOrientation(LinearLayout.VERTICAL);
        mBillContainer.setBackground(getResources().getDrawable(R.drawable.greydivider));

        //Set Rows
        for (int a = 0; a < BillCollection.get(getActivity()).getBills().size(); a++) {
            //Get Row Data
            final BillRecord bill = BillCollection.get(getActivity()).getBills().get(a);

            //Create Row and Set Data
            LinearLayout bottomRow = (LinearLayout) LayoutInflater.from(getActivity()).inflate(R.layout.list_item_setting_bill, null);
            ((TextView) bottomRow.findViewById(R.id.bill_title)).setText(BillCollection.get(getActivity()).getFace().get(a));
            CheckBox shouldPrint = (CheckBox) bottomRow.findViewById(R.id.bill_print);
            shouldPrint.setTag(a);
            shouldPrint.setChecked(bill.mShouldPrint);
            shouldPrint.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    boolean isChecked = ((CheckBox) view).isChecked();
                    BillCollection.get(getActivity()).getBills().get((Integer) view.getTag()).mShouldPrint = isChecked;
                }
            });
            CheckBox shouldSMS = (CheckBox) bottomRow.findViewById(R.id.bill_sms);
            shouldSMS.setTag(a);
            shouldSMS.setChecked(bill.mShouldSMS);
            shouldSMS.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //boolean isChecked = ((CheckBox) view).isChecked();
                    //BillCollection.get(getActivity()).getBills().get((Integer)view.getTag()).mShouldSMS = isChecked;
                    Toast.makeText(getActivity(), "در حال حاضر امکان فعال سازی این گزینه وجود ندارد.", Toast.LENGTH_SHORT).show();
                }
            });
            CheckBox shouldPush = (CheckBox) bottomRow.findViewById(R.id.bill_push);
            shouldPush.setTag(a);
            shouldPush.setChecked(bill.mShouldPush);
            shouldPush.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    boolean isChecked = ((CheckBox) view).isChecked();
                    BillCollection.get(getActivity()).getBills().get((Integer) view.getTag()).mShouldPush = isChecked;
                }
            });

            mBillContainer.addView(bottomRow, LayoutHelper.createLinear(LayoutHelper.MATCH_PARENT, 48));
        }
    }

    public void setDeadlineAlert() {
        mDeadLineAlert.setTextAndValue("اخطار سر رسید مهلت پرداخت", "" + deadlineValues[PreferencesHelper.loadInt(PreferencesHelper.KEY_REMIND_ME_BEFORE)], false);
    }
}
