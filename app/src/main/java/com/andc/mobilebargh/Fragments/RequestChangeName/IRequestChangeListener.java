package com.andc.mobilebargh.Fragments.RequestChangeName;

import com.andc.mobilebargh.Models.CurrentUserInfo;
import com.andc.mobilebargh.Models.NewUserInfo;
import com.andc.mobilebargh.Models.PersonalDocumentInfo;

/**
 * Created by AFalah on 3/10/2018.
 */

public interface IRequestChangeListener {

    public void ListenerButtonNextClicked();
    public void ListenerButtonBackClicked();
    public void setNewUserInfo(NewUserInfo newUserInfo);
    public NewUserInfo getNewUserInfo();
    public void setCurrentUserInfo(CurrentUserInfo currentUserInfo);
    public CurrentUserInfo getCurrentUserInfo();
    public void setPersonalDocumentInfo(PersonalDocumentInfo personalDocumentInfo);
    public PersonalDocumentInfo getPersonalDocumentInfo();
    public void setFollowUpCode(String fCode);
    public String getFollowUpCode();

}
