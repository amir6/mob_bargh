package com.andc.mobilebargh.Fragments.RequestFragments;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;


import com.andc.mobilebargh.Activities.ChangeNameActivity;
import com.andc.mobilebargh.Activities.EnquiryActivity;
import com.andc.mobilebargh.Activities.FragmentActivity;
import com.andc.mobilebargh.Activities.TarrifActivity;
import com.andc.mobilebargh.Adapter.RequestItemsAdapter;

import com.andc.mobilebargh.Fragments.DialogFramgments.RequestSubCategoryFragment;
import com.andc.mobilebargh.R;

public class ListCategoryFragment extends Fragment implements AdapterView.OnItemClickListener {



    private GridView mItems;
    private RequestItemsAdapter mAdapter;
    public final static String SALE_SERVICE_CATEGORY_TAG = "1";
    public final static String CORRECTION_INFO_CATEGORY_TAG = "2";
    public final static String BIILING_CATEGORY_TAG = "3";
    public final static String SALE_CATEGORY_TAG = "4";
    public final static String CHANGE_NAME_TAG = "5";
    public final static String CHANGE_TARRIF_TAG = "6";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_request_grid_main, container, false);
        mItems = view.findViewById(R.id.request_grid_view);
        mAdapter = new RequestItemsAdapter(getActivity());
        mItems.setAdapter(mAdapter);
        mItems.setOnItemClickListener(this);

        return view;
    }

    public void Onclick(int position){

    }



    @Override
    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
        RequestSubCategoryFragment requestDialog = new RequestSubCategoryFragment();
        Fragment fragment = null;
        switch (mAdapter.images[position]){

            case R.drawable.after_sale:
                requestDialog.show(getFragmentManager(), SALE_SERVICE_CATEGORY_TAG);
                break;
            case R.drawable.price_bill:
               requestDialog.show(getFragmentManager(), BIILING_CATEGORY_TAG);
                break;
            case R.drawable.meter:
                requestDialog.show(getFragmentManager(), SALE_CATEGORY_TAG);
                break;
            case R.drawable.change_name:
               ((FragmentActivity)getActivity()).getSupportActionBar().hide();
                Intent intent = new Intent(getActivity(), ChangeNameActivity.class);
                startActivity(intent);

                break;
            case R.drawable.info:
                Intent enquiryIntent =  new Intent(getActivity(), EnquiryActivity.class);
                startActivity(enquiryIntent);
                break;
            case R.drawable.bill_correction:
                requestDialog.show(getFragmentManager(), CORRECTION_INFO_CATEGORY_TAG);
                break;
            case R.drawable.message:
                Toast.makeText(getActivity(), "message", Toast.LENGTH_LONG).show();
                break;
            case R.drawable.change_tarrif:
                Intent tariffIntent =  new Intent(getActivity(), TarrifActivity.class);
                startActivity(tariffIntent);
                break;

        }
        if(fragment != null) {
            getFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).commit();

        }

    }
}
