package com.andc.mobilebargh.Fragments.HomeFragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.andc.mobilebargh.Activities.MainActivity;
import com.andc.mobilebargh.Fragments.DialogFramgments.PaymentDialog;
import com.andc.mobilebargh.Utility.BillAPI.Shenase;
import com.andc.mobilebargh.Controllers.BillCollection;
import com.andc.mobilebargh.Models.BillRecord;
import com.andc.mobilebargh.Models.BranchInfoRecord;
import com.andc.mobilebargh.Models.PaymentRecord;
import com.andc.mobilebargh.R;
import com.andc.mobilebargh.Utility.Components.RefreshableFragment;
import com.andc.mobilebargh.Utility.ErrorHandler;
import com.kbeanie.imagechooser.api.ChooserType;
import com.kbeanie.imagechooser.api.ChosenImage;
import com.kbeanie.imagechooser.api.ChosenImages;
import com.kbeanie.imagechooser.api.ImageChooserListener;
import com.kbeanie.imagechooser.api.ImageChooserManager;
import com.kbeanie.imagechooser.exceptions.ChooserException;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by win on 11/17/2015.
 */
public class InfoFragment extends RefreshableFragment implements ImageChooserListener {

    //Fragment Tags
    public final static int PICK_PHOTO_CODE = 1046;
    public final static int mTitle = R.string.tabs_branch_info;
    public final static String TAG_BILL_ID = "bill_id";

    //Global Var
    private View rootView;
    private BillRecord mBill;
    private BranchInfoRecord mBranchInfo;
    private PaymentRecord mPendingPayment;
    private CircleImageView mInfoImage;
    private ImageChooserManager imageChooserManager;
    private View mPayButton;

    @Override
    public boolean onRefreshFragment(String newBillId) {
        fetchData(newBillId);
        setupView(rootView);
        return true;
    }

    @Override
    public void onImageChosen(final ChosenImage chosenImage) {
        BillRecord currentBill = BillCollection.get(getActivity()).getCurrentBill();
        if (currentBill!= null){
            currentBill.mImageUri = chosenImage.getFileThumbnail();
            currentBill.save();

            //Refresh Views
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mInfoImage.setImageURI(Uri.parse(chosenImage.getFileThumbnail()));
                    ((MainActivity) getActivity()).setDrawerBackground();
                }
            });
        }
    }

    @Override
    public void onError(String s) {

    }

    @Override
    public void onImagesChosen(ChosenImages chosenImages) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        fetchData(getArguments().getString(TAG_BILL_ID));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        rootView = inflater.inflate(R.layout.fragment_information, container, false);

        setupView(rootView);
        return rootView;
    }

    private void fetchData(String mBillId){
        //Get Branch Info
        mBranchInfo = new BranchInfoRecord();
        ArrayList<BranchInfoRecord> mBranchList = (ArrayList<BranchInfoRecord>) BranchInfoRecord
                .find(BranchInfoRecord.class, BranchInfoRecord.LABEL_BILL_ID + " = ?", mBillId);
        if(mBranchList!=null)
            if(mBranchList.size()>0)
                mBranchInfo = mBranchList.get(0);


        //Get Bill Info
        mBill = new BillRecord();
        ArrayList<BillRecord> mBillList = (ArrayList<BillRecord>) BillRecord
                .find(BillRecord.class, BillRecord.LABEL_BILL_ID + " = ?", mBillId);
        if(mBillList!=null)
            if(mBillList.size()>0)
                mBill = mBillList.get(0);


        //Get Pending Payments
        mPendingPayment = null;
        ArrayList<PaymentRecord> mPendingPayments = (ArrayList<PaymentRecord>) PaymentRecord
                .find(PaymentRecord.class, PaymentRecord.LABEL_PAYMENT_Bill_Id + " = ?", mBillId);
        if(mPendingPayments!=null && mPendingPayments.size()>0)
            mPendingPayment = mPendingPayments.get(0);
    }

    private void setupView(View rootView){
        setupPayButton(rootView);
        setupInfoHeader(rootView);
        setupInfoBody(rootView);

    }

    private void setupPayButton(final View rootView){
        //Setup Pay Button
        mPayButton = rootView.findViewById(R.id.pay_bill);

        if(TextUtils.isEmpty(mBranchInfo.CrDbTot) || Integer.parseInt(mBranchInfo.CrDbTot)<1000)
            mPayButton.setBackgroundColor(getResources().getColor(R.color.darker_gray));
        else
            mPayButton.setBackgroundColor(getResources().getColor(R.color.andc_accent));

        mPayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Hide Payment Tutorial if Visible
                //if (mTutorialOverlay.getVisibility() != View.GONE)
                //    mTutorialOverlay.setVisibility(View.GONE);

                //Block Payment if PaymentRecord is available
                if (mPendingPayment != null) {
                    ErrorHandler.showError(rootView, "بدهی شما پرداخت گردیده, تا تایید پرداخت خود شکیبا باشید.");
                    return;
                }

                if (mBranchInfo.CrDbTot == null || mBranchInfo.CrDbTot == "" || Integer.parseInt(mBranchInfo.CrDbTot) < 1000) {
                    ErrorHandler.showError(rootView, "بدهی شما از حد مجاز کمتر می باشد.");
                    return;
                }

                DialogFragment mPayBill = PaymentDialog.newInstance(mBill.mBillId,true);
                mPayBill.show(getChildFragmentManager(), "PayBills");


                //TODO Add Both MPG and MPL
                /*
                if (BankAPIHelper.isTopInstalled(getActivity())) {
                    DialogFragment mPayBill = PaymentDialog.newInstance(mBill.mBillId);
                    mPayBill.show(getChildFragmentManager(), "PayBills");
                } else {
                    AlertDialog dialog = new AlertDialog.Builder(getActivity())
                            //.setView(mDialogView)
                            .setTitle(getResources().getString(R.string.dialog_download_top_title))
                            .setMessage(getResources().getString(R.string.dialog_download_top_body))
                            .setNegativeButton(getResources().getString(R.string.dialog_button_return), null)
                            .setPositiveButton(getResources().getString(R.string.dialog_download_top_download), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface arg0, int arg1) {
                                    BankAPIHelper.downloadTop(getActivity());
                                }
                            }).create();
                    CustomDialog.show(dialog);
                }
                */
            }
        });
    }

    private void setupInfoHeader(View rootView){
        //Define Views
        mInfoImage = (CircleImageView) rootView.findViewById(R.id.info_image);
        TextView mInfoBillTitle = (TextView) rootView.findViewById(R.id.info_bill_title);
        TextView mInfoBillId = (TextView) rootView.findViewById(R.id.info_bill_id);
        TextView mInfoBillOwner = (TextView) rootView.findViewById(R.id.info_bill_owner);

        //Set Data
        if(!TextUtils.isEmpty(mBill.mImageUri))
            mInfoImage.setImageURI(Uri.parse(mBill.mImageUri));
        else
            mInfoImage.setImageResource(R.drawable.test_circle_image);

        if(mBill.mBillTitle.equalsIgnoreCase("")){
            mInfoBillTitle.setVisibility(View.GONE);
        } else{
            mInfoBillTitle.setVisibility(View.VISIBLE);
            mInfoBillTitle.setText(mBill.mBillTitle);
        }

        mInfoBillId.setText(mBill.mBillId);
        mInfoBillOwner.setText(mBranchInfo.OwnerName);

        //Set Listeners
        mInfoImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Return if there is no Current Bill Available
                if (BillCollection.get(getActivity()).getCurrentBill() == null)
                    return;

                //TODO If Frame is Empty Open Gallery to Pick Photo, Else Show the Photo
                imageChooserManager = new ImageChooserManager(getParentFragment(), ChooserType.REQUEST_PICK_PICTURE);
                imageChooserManager.setImageChooserListener(InfoFragment.this);
                try {
                    imageChooserManager.choose();
                } catch (ChooserException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setupInfoBody(View rootView){
        //Setup Page Body
        LinearLayout mImportantContainer = (LinearLayout)rootView.findViewById(R.id.info_important_container);
        LinearLayout mBranchContainer = (LinearLayout)rootView.findViewById(R.id.info_branch_container);

        //Clear View In Case of Refresh
        mImportantContainer.removeAllViews();
        mBranchContainer.removeAllViews();

        //Prepare Data Block's View
        LinearLayout mRow;
        TextView mTitle;
        TextView mData;

        Typeface bold = Typeface.createFromAsset(getActivity().getAssets(),"fonts/IRANSans(FaNum)_Bold.ttf");

        //Add Provider Info
        mRow = (LinearLayout)getActivity().getLayoutInflater().inflate(R.layout.info_row_layout, null);
        mTitle = (TextView)mRow.findViewById(R.id.title);
        mData = (TextView)mRow.findViewById(R.id.data);

        //Get Provider Info
        if(mBranchInfo!= null && mBranchInfo.BillId != null && mBranchInfo.BillId.length() == 13){
            final String mProviderCode = mBranchInfo.BillId.substring(8, 11);

            mTitle.setText(getResources().getString(R.string.TAG_Provider));
            mData.setText(BranchInfoRecord.mProviders.get(mProviderCode));
            mData.setTextColor(getResources().getColor(R.color.holo_blue_dark));
            mData.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Direct User to Provider Website
                    if(!TextUtils.isEmpty(BranchInfoRecord.mProvidersWebsite.get(mProviderCode))){
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(BranchInfoRecord.mProvidersWebsite.get(mProviderCode)));
                        startActivity(intent);
                    }
                }
            });
            mBranchContainer.addView(mRow);
        }


        for (int i = 0 ; i < 9 ; i++){
            mRow = (LinearLayout)getActivity().getLayoutInflater().inflate(R.layout.info_row_layout, null);
            mTitle = (TextView)mRow.findViewById(R.id.title);
            mData = (TextView)mRow.findViewById(R.id.data);

            //Set Data Blocks
            switch (i){
                //Info Important
                case 0:
                    //Set Title
                    if(mPendingPayment ==null)
                        mTitle.setText(getResources().getString(R.string.TAG_CrDbTot));
                    else
                        mTitle.setText(getResources().getString(R.string.TAG_CrDbTot) + " (درحال تایید)");

                    mTitle.setTextColor(Color.BLACK);

                    //Set Color
                    if(mPendingPayment != null && !TextUtils.isEmpty(mBranchInfo.CrDbTot)){
                        int paidAmount = Shenase.getAmount(mPendingPayment.mPaymentId);
                        int debt = Integer.parseInt(mBranchInfo.CrDbTot) - paidAmount;

                        mData.setText("(" + paidAmount + ") " + debt);
                    } else {
                        mData.setText(mBranchInfo.CrDbTot);
                    }

                    if(!TextUtils.isEmpty(mBranchInfo.CrDbTot) && Integer.parseInt(mBranchInfo.CrDbTot) < 1000)
                        mData.setTextColor(ContextCompat.getColor(getActivity(), R.color.holo_green_dark));
                    else
                        mData.setTextColor(Color.RED);

                    mImportantContainer.addView(mRow);
                    mTitle.setTypeface(bold);
                    break;

                case 1:
                    mTitle.setText(getResources().getString(R.string.TAG_LstPayLimitDate));
                    mTitle.setTextColor(Color.BLACK);
                    mData.setText(mBranchInfo.LstPayLimitDate);
                    mData.setTextColor(ContextCompat.getColor(getActivity(), android.R.color.holo_green_dark));
                    mImportantContainer.addView(mRow);

                    mTitle.setTypeface(bold);
                    break;

                //Info Branch
                case 2:
                    mTitle.setText(getResources().getString(R.string.TAG_FabrikNumber));
                    mData.setText(mBranchInfo.FabrikNumber);
                    mBranchContainer.addView(mRow);
                    break;

                case 3:
                    mTitle.setText(getResources().getString(R.string.TAG_Adress));
                    mData.setText(mBranchInfo.Adress);
                    mBranchContainer.addView(mRow);
                    break;

                case 4:
                    mTitle.setText(getResources().getString(R.string.TAG_TrfHCode));
                    mData.setText(mBranchInfo.TrfHCode);
                    mBranchContainer.addView(mRow);
                    break;

                case 5:
                    mTitle.setText(getResources().getString(R.string.TAG_Phs));
                    mData.setText(mBranchInfo.Phs);
                    mBranchContainer.addView(mRow);
                    break;

                case 6:
                    mTitle.setText(getResources().getString(R.string.TAG_Amp));
                    mData.setText(mBranchInfo.Amp);
                    mBranchContainer.addView(mRow);
                    break;

                case 7:
                    mTitle.setText(getResources().getString(R.string.TAG_PwrCnt));
                    mData.setText(mBranchInfo.PwrCnt);
                    mBranchContainer.addView(mRow);
                    break;


                default:
                    mRow.setVisibility(View.GONE);
                    break;
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode != Activity.RESULT_OK)
            return;

        switch (requestCode) {
            case ChooserType.REQUEST_PICK_PICTURE:
                imageChooserManager.submit(requestCode, data);
                break;

            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    public static Fragment newInstance (String bill_id){
        Bundle args = new Bundle();

        args.putString(TAG_BILL_ID, bill_id);
        Fragment fragment = new InfoFragment();
        fragment.setArguments(args);

        return fragment;
    }
}
