package com.andc.mobilebargh.Fragments.ServiceFragments

import android.arch.lifecycle.ViewModelProvider
import com.andc.mobilebargh.Injection.Injectable

import dagger.android.support.DaggerFragment
import javax.inject.Inject

open class BaseFragment : DaggerFragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory




}