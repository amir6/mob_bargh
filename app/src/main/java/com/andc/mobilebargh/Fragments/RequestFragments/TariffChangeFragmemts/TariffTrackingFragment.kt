package com.andc.mobilebargh.Fragments.RequestFragments.TariffChangeFragmemts

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.andc.mobilebargh.Fragments.ServiceFragments.BaseFragment
import com.andc.mobilebargh.R
import com.andc.mobilebargh.ViewModel.TariffVm
import com.andc.mobilebargh.repository.model.ResponseData
import kotlinx.android.synthetic.main.fragment_track_ign.*

class TariffTrackingFragment: BaseFragment() {

private lateinit var mViewModel: TariffVm
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel = activity.run { ViewModelProviders.of(this!!, viewModelFactory).get(TariffVm::class.java) }
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var view = inflater.inflate(R.layout.fragment_track_ign, null, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btn_tracking_finish.setOnClickListener{
            activity!!.finish()
        }

        mViewModel.tariffResponseData.observe(this,object: Observer<ResponseData?> {
            override fun onChanged(t: ResponseData?) {

                t?.let {

                    number_follow_up.text = t.REF_CODE

                }

            }
        } )
    }


    companion object {
        fun newInstance() = TariffTrackingFragment().apply {
            arguments.apply {  }
        }
    }

}