package com.andc.mobilebargh.Fragments.RequestChangeName;


import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.andc.mobilebargh.Activities.IRequest;
import com.andc.mobilebargh.Activities.RequestActivity;
import com.andc.mobilebargh.Models.PersonalDocumentInfo;
import com.andc.mobilebargh.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import static android.app.Activity.RESULT_OK;
import static com.andc.mobilebargh.Utility.General.FontSize;
import static com.andc.mobilebargh.Utility.Util.DocumentsFile.saveImage;


public class DocumentFragment extends Fragment {
    private IRequestChangeListener mListener;



    private Button btnNext, btnBack;
    String type;
    int number;

    Bitmap bmpp;
    int x;

    private ImageView cart_up_img, cart_down_img, sh_img, doc_one_img, doc_tow_img, doc_three_img,
            comitment_one_img, other_img;

    private String cart_up = "cart_up", cart_down = "cart_down", sh = "sh", one = "doc_one", tow = "doc_tow",
            three = "doc_three", comm = "commitment", other = "other";


    File filePath;
    String folder_name = "Andc";

    private PersonalDocumentInfo personalDocumentInfo;
    //this is test

    private int PICK_IMAGE_REQUEST = 1;

    public DocumentFragment() {
        // Required empty public constructor
    }


    public static DocumentFragment newInstance() {
        DocumentFragment fragment = new DocumentFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        personalDocumentInfo = new PersonalDocumentInfo();

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
       mListener = (IRequestChangeListener)getActivity();

    }
    public void setListener(IRequestChangeListener listener) {
        this.mListener = listener;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_document, container, false);

        init(view, folder_name);

        FontSize(getActivity().getApplicationContext(), view, "IRANSans(FaNum)_Medium.ttf");

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prepareInterface();
                mListener.setPersonalDocumentInfo(personalDocumentInfo);
                mListener.ListenerButtonNextClicked();

            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });


        cart_up_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = cart_up;
                checkImage(view, type, 1);
            }
        });

        cart_down_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = cart_down;
                checkImage(view, type, 2);
            }
        });

        sh_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = sh;
                checkImage(view, type, 3);
            }
        });

        doc_one_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = one;
                checkImage(view, type, 4);
            }
        });

        doc_tow_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = tow;
                checkImage(view, type, 5);
            }
        });

        doc_three_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = three;
                checkImage(view, type, 6);
            }
        });

        comitment_one_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = comm;
                checkImage(view, type, 7);
            }
        });

        other_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = other;
                checkImage(view, type, 8);
            }
        });


        return view;

    }

    private void prepareInterface() {
        checkExistsImage(cart_up,1);
        checkExistsImage(cart_down,2);
        checkExistsImage(sh,3);
        checkExistsImage(comm,4);
        checkExistsImage(one,5);
        checkExistsImage(tow,6);
        checkExistsImage(three,7);
        checkExistsImage(other,8);
    }

    private void checkExistsImage(String imageName, int index) {
        filePath = new File(Environment.getExternalStorageDirectory() + "/" + folder_name + "/" + imageName + ".jpg");
        if (filePath.exists())
            chooseFile(index, filePath);
        else
            chooseFile(index,null);
    }

    private void chooseFile(int index, File file) {
        switch (index) {
            case 1:
                personalDocumentInfo.setNationalCartFront(file);
                break;
            case 2:
                personalDocumentInfo.setNationalCartBehind(file);
                break;
            case 3:
                personalDocumentInfo.setIdentityCart(file);
                break;
            case 4:
                personalDocumentInfo.setCommitment(file);
                break;
            case 5:
                personalDocumentInfo.setOwnershipDoc1(file);
                break;
            case 6:
                personalDocumentInfo.setOwnershipDoc2(file);
                break;
            case 7:
                personalDocumentInfo.setOwnershipDoc3(file);
                break;
            case 8:
                personalDocumentInfo.setOther(file);
                break;
        }
    }

    private void checkImage(View view, String imageName, int data) {
        filePath = new File(Environment.getExternalStorageDirectory() + "/" + folder_name + "/" + imageName + ".jpg");
        if (filePath.exists()) {
            ImageDialog(view, imageName);
        } else {
            dialog(data);
        }
    }

    private void init(View view, String folder_name) {

        btnNext = (Button) view.findViewById(R.id.next_btn);
        btnBack = (Button) view.findViewById(R.id.back_btn);

        cart_up_img = (ImageView) view.findViewById(R.id.cart_up_img);
        cart_down_img = (ImageView) view.findViewById(R.id.cart_down_img);
        sh_img = (ImageView) view.findViewById(R.id.sh_img);
        doc_one_img = (ImageView) view.findViewById(R.id.doc_one_img);
        doc_tow_img = (ImageView) view.findViewById(R.id.doc_tow_img);
        doc_three_img = (ImageView) view.findViewById(R.id.doc_three_img);
        comitment_one_img = (ImageView) view.findViewById(R.id.comitment_one_img);
        other_img = (ImageView) view.findViewById(R.id.other_img);


        File directory = new File(Environment.getExternalStorageDirectory() + "/" + folder_name);

        if (!directory.exists()) {
            //Toast.makeText(view.getContext(),"ok"+directory,Toast.LENGTH_SHORT).show();
            directory.mkdirs();
        } else {


            File[] files = directory.listFiles();
            for (int i = 0; i < files.length; i++) {
                Log.d("Files" + files[i], "FileName:" + files[i].getName());
                if (files[i].toString().equals(cart_up + ".jpg")) {
                    Uri file = Uri.fromFile(new File(directory, cart_up + ".jpg"));
                    cart_up_img.setImageURI(file);
                    personalDocumentInfo.setNationalCartFront(new File(directory, cart_up + ".jpg"));
                } else if (files[i].getName().toString().equals(cart_down + ".jpg")) {
                    Uri file = Uri.fromFile(new File(directory, cart_down + ".jpg"));
                    cart_down_img.setImageURI(file);
                    personalDocumentInfo.setNationalCartFront(new File(directory, cart_down + ".jpg"));
                } else if (files[i].getName().toString().equals(sh + ".jpg")) {
                    Uri file = Uri.fromFile(new File(directory, sh + ".jpg"));
                    sh_img.setImageURI(file);
                    personalDocumentInfo.setNationalCartFront(new File(directory, sh + ".jpg"));
                } else if (files[i].getName().toString().equals(comm + ".jpg")) {
                    Uri file = Uri.fromFile(new File(directory, comm + ".jpg"));
                    comitment_one_img.setImageURI(file);
                    personalDocumentInfo.setNationalCartFront(new File(directory, comm + ".jpg"));
                } else if (files[i].getName().toString().equals(one + ".jpg")) {
                    Uri file = Uri.fromFile(new File(directory, one + ".jpg"));
                    doc_one_img.setImageURI(file);
                    personalDocumentInfo.setNationalCartFront(new File(directory, one + ".jpg"));
                } else if (files[i].getName().toString().equals(tow + ".jpg")) {
                    Uri file = Uri.fromFile(new File(directory, tow + ".jpg"));
                    doc_tow_img.setImageURI(file);
                    personalDocumentInfo.setNationalCartFront(new File(directory, tow + ".jpg"));
                } else if (files[i].getName().toString().equals(three + ".jpg")) {
                    Uri file = Uri.fromFile(new File(directory, three + ".jpg"));
                    doc_three_img.setImageURI(file);
                    personalDocumentInfo.setNationalCartFront(new File(directory, three + ".jpg"));
                } else if (files[i].getName().toString().equals(other + ".jpg")) {

                    Uri file = Uri.fromFile(new File(directory, other + ".jpg"));
                    other_img.setImageURI(file);
                    personalDocumentInfo.setNationalCartFront(new File(directory, other + ".jpg"));
                }
            }
        }

    }


    private void ImageDialog(View v, String imageName) {

        LayoutInflater layoutInflater = LayoutInflater.from(v.getContext());
        View view = layoutInflater.inflate(R.layout.dialog_image, null);

        final AlertDialog.Builder aleartDialogBuilder = new AlertDialog.Builder(v.getContext());
        aleartDialogBuilder.setView(view);
        aleartDialogBuilder.setCancelable(true);


        Button delete = (Button) view.findViewById(R.id.delete_img);
        Button perv = (Button) view.findViewById(R.id.per_image);
        final ImageView img = (ImageView) view.findViewById(R.id.c_image);


        final AlertDialog alertDialog = aleartDialogBuilder.create();


        File dir = new File(Environment.getExternalStorageDirectory() + "/" + folder_name);
        //Bitmap out = Bitmap.createScaledBitmap(bitmap, 1166, 2048, true);

        final File file = new File(dir, imageName + ".jpg");
        Bitmap bmp = BitmapFactory.decodeFile(file.toString());
        img.setImageBitmap(bmp);

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //delete image
                file.delete();
                alertDialog.dismiss();
            }
        });


        perv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri uri = null;

        if (data != null) {
            uri = data.getData();
        }
        if (requestCode == 0 && resultCode == RESULT_OK && data != null) {
            switch (number) {
                case 1:
                    saveImage(getContext(),type, uri, cart_up_img);
                    break;
                case 2:
                    saveImage(getContext(), type, uri, cart_down_img);
                    break;
                case 3:
                    saveImage(getContext(),type, uri, sh_img);
                    break;
                case 4:
                    saveImage(getContext(),type, uri, doc_one_img);
                    break;
                case 5:
                    saveImage(getContext(), type, uri, doc_tow_img);
                    break;
                case 6:
                    saveImage(getContext(),type, uri, doc_three_img);
                    break;
                case 7:
                    saveImage(getContext(),type, uri, comitment_one_img);
                    break;
                case 8:
                    saveImage(getContext(), type, uri, other_img);
                    break;
            }

        } else if (requestCode == 1 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            saveImage(getContext(), cart_up, uri, cart_up_img);
        } else if (requestCode == 2 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            saveImage(getContext() ,cart_down, uri, cart_down_img);
        } else if (requestCode == 3 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            saveImage(getContext() , sh, uri, sh_img);
        } else if (requestCode == 4 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            saveImage(getContext(), one, uri, doc_one_img);
        } else if (requestCode == 5 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            saveImage(getContext(), tow, uri, doc_tow_img);
        } else if (requestCode == 6 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            saveImage(getContext(), three, uri, doc_three_img);
        } else if (requestCode == 7 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            saveImage(getContext(), comm, uri, comitment_one_img);
        } else if (requestCode == 8 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            saveImage(getContext(), other, uri, other_img);
        }


    }

    //**************************************************************************************************
    private void dialog(final int num) {
        AlertDialog ad = new AlertDialog.Builder(getContext())
                .create();
        ad.setCancelable(true);
        //ad.setTitle("s");
        ad.setMessage("برای ارسال عکس، از گالری یا دوربین استفاده کنید.");
        ad.setButton(Dialog.BUTTON_POSITIVE, "گالری", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, num);
            }
        });
        ad.setButton(Dialog.BUTTON_NEGATIVE, "دوربین", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                number = num;
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, 0);
            }
        });
        ad.show();
    }

    //**************************************************************************************************


    public void onDetach() {
        mListener = null;
        super.onDetach();
    }
}