package com.andc.mobilebargh.Fragments.RequestFragments.InvoiceFragment

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.andc.mobilebargh.Activities.IRequestInvoice
import com.andc.mobilebargh.Fragments.DialogFramgments.ResponseStatusDialog
import com.andc.mobilebargh.Fragments.DialogFramgments.WaringDialog
import com.andc.mobilebargh.Fragments.RequestFragments.SupportRequestFragments.SupportBranchInfoFragment
import com.andc.mobilebargh.Fragments.ServiceFragments.BaseFragment
import com.andc.mobilebargh.Models.BillData
import com.andc.mobilebargh.R
import com.andc.mobilebargh.Utility.General
import com.andc.mobilebargh.Utility.Util.ServerStatus
import com.andc.mobilebargh.ViewModel.InvoiceVm
import com.andc.mobilebargh.databinding.FragmentBranchInfoBinding
import com.andc.mobilebargh.repository.model.BranchData
import kotlinx.android.synthetic.main.fragment_branch_info.*


class InvoiceBranchInfoFragment: BaseFragment(), View.OnClickListener {
 private lateinit var mViewModel: InvoiceVm
    private lateinit var mBinding: FragmentBranchInfoBinding
    lateinit var billData : BillData
    lateinit var mListener: IRequestInvoice
    lateinit var mBundle: Bundle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel = activity.run { ViewModelProviders.of(this!!, viewModelFactory).get(InvoiceVm::class.java) }
        mViewModel.getBranchInfo.observe(this, object: Observer<BranchData?> {
            override fun onChanged(t: BranchData?) {

                if (t != null) {
                    mBinding.data = t



                }

            }
        })
        mViewModel.checkBillIssue.observe(this, object: Observer<ServerStatus?> {
            override fun onChanged(t: ServerStatus?) {
                t.let {
                    if(t == ServerStatus.RECEIVE){
                        progress_send_data.hide()
                        mListener.OnNextClick()

                    }else if(t == ServerStatus.ERROR) {
                        var dialog : ResponseStatusDialog = ResponseStatusDialog(activity)
                        dialog.showFail()
                        next_btn.isEnabled = true
                        back_btn.isEnabled = true
                    }
                }
            }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_branch_info, null, false)
        mListener = activity as IRequestInvoice
        billData = BillData()
        mBinding.data = BranchData()
        var v = mBinding.root
        General.FontSize(activity, v, "IRANSans(FaNum)_Medium.ttf")
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        next_btn.setOnClickListener(this)
        back_btn.setOnClickListener(this)

    }
    override fun onClick(v: View?) {
        when(v?.id) {

                next_btn.id -> {
                    if(mBundle.getInt("type" )== 0){
                        mViewModel.getBillImage()
                        progress_send_data.show()
                        next_btn.isEnabled = false
                        back_btn.isEnabled = false
                    }else {
                        mListener.OnNextClick()
                    }
                }

            back_btn.id -> WaringDialog.exitDialog(activity,resources.getString(R.string.dialog_exit_body))


        }
    }



    companion object {


        fun newInstance(type: Int)= InvoiceBranchInfoFragment().apply {
            mBundle = Bundle()
            arguments.apply { mBundle.putInt("type", type) }
        }

    }

}