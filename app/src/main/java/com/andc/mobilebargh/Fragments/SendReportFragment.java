package com.andc.mobilebargh.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.andc.mobilebargh.Activities.FragmentActivity;
import com.andc.mobilebargh.Controllers.BillCollection;
import com.andc.mobilebargh.Models.BillRecord;
import com.andc.mobilebargh.Models.CustomError;
import com.andc.mobilebargh.Networking.Tasks.SendReport;
import com.andc.mobilebargh.R;
import com.andc.mobilebargh.Utility.ErrorHandler;
import com.andc.mobilebargh.Utility.Listeners.DatePicker;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import ir.smartlab.persindatepicker.util.PersianCalendar;

/**
 * Created by Tenkei on 5/14/2016.
 */
public class SendReportFragment extends Fragment implements
        SendReport.OnAsyncRequestComplete, GoogleApiClient.ConnectionCallbacks{

    private final static String EXTRA_BILL_ID = "extra_bill_id";
    private final static int REQUEST_PICK_LOCATION = 342;

    //Global Var
    private View rootView;
    private Spinner mBillIdSpinner;
    private EditText mTitle;
    private EditText mDate;
    private EditText mAddress;
    private EditText mDescription;
    private TextView mSendBtn;

    //Location
    private String mBillId;
    private Location mCurrentLocation;
    private LatLng mSelectedLocation;
    private GoogleApiClient mGoogleApiClient;

    @Override
    public void asyncResponse(ArrayList<CustomError> errors) {
        if(getActivity()==null)
            return;

        if(errors.get(0).mCode == 200){
            getActivity().finish();
        } else {
            ErrorHandler.showError(rootView, errors.get(0));
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        if(getArguments()!=null)
            mBillId = getArguments().getString(EXTRA_BILL_ID);

        if (mGoogleApiClient == null)
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addConnectionCallbacks(this)
                    .addApi(LocationServices.API)
                    .build();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_send_report, null, false);

        setupView(rootView);
        setupData();

        return rootView;
    }

    @Override
    public void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    private void setupView(View rootView){
        mDescription = (EditText)rootView.findViewById(R.id.new_report_description);

        //Setup Bill Id Spinner
        mBillIdSpinner = (Spinner)rootView.findViewById(R.id.new_report_bill_id);
        ArrayAdapter<String> mSpinnerAdapter = new ArrayAdapter(getActivity(),  R.layout.category_spinner_item, BillCollection.get(getActivity()).getSpinnerArray());
        mSpinnerAdapter.setDropDownViewResource(R.layout.category_spinner_dropdown_item);
        mBillIdSpinner.setAdapter(mSpinnerAdapter);

        mTitle = (EditText)rootView.findViewById(R.id.new_report_title);

        //Set Date Listeners
        mDate = (EditText)rootView.findViewById(R.id.new_report_date);
        View mDateContainer = rootView.findViewById(R.id.new_report_date_container);
        mDateContainer.setOnClickListener(new DatePicker(mDate));
        mDate.setOnClickListener(new DatePicker(mDate));
        mDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b)
                    new DatePicker(mDate).pickDate(mDate);
            }
        });


        //Set Address Listeners
        mAddress = (EditText)rootView.findViewById(R.id.new_report_address);
        mAddress.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b && mSelectedLocation == null) {
                    Intent intent = new Intent(getActivity(), FragmentActivity.class);
                    intent.putExtra(FragmentActivity.EXTRA_FRAGMENT_TYPE, R.id.fragment_pick_location);

                    //Send User Location If Known
                    if (mCurrentLocation != null)
                        intent.putExtra(FragmentActivity.EXTRA_LOCATION, new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()));

                    startActivityForResult(intent, REQUEST_PICK_LOCATION);
                }
            }
        });
        View mAddressIcon = rootView.findViewById(R.id.new_report_location);
        mAddressIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), FragmentActivity.class);
                intent.putExtra(FragmentActivity.EXTRA_FRAGMENT_TYPE, R.id.fragment_pick_location);

                //Pass Selected Location If Available, Else Send User Location if Known
                if (mSelectedLocation != null)
                    intent.putExtra(FragmentActivity.EXTRA_LOCATION, mSelectedLocation);
                else if (mCurrentLocation != null)
                    intent.putExtra(FragmentActivity.EXTRA_LOCATION, new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()));

                startActivityForResult(intent, REQUEST_PICK_LOCATION);
            }
        });


        //Setup Send Button
        mSendBtn = (TextView)rootView.findViewById(R.id.new_report_send);
        mSendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(mTitle.getText().toString())) {
                    Toast.makeText(getActivity(), "عنوان گزارش نمی تواند خالی باشد.", Toast.LENGTH_SHORT).show();
                    return;
                } else if (TextUtils.isEmpty(mDate.getText().toString())) {
                    Toast.makeText(getActivity(), "تاریخ گزارش نمی تواند خالی باشد.", Toast.LENGTH_SHORT).show();
                    return;
                } else if (TextUtils.isEmpty(mDescription.getText().toString())) {
                    Toast.makeText(getActivity(), "توضیحات گزارش نمی تواند خالی باشد.", Toast.LENGTH_SHORT).show();
                    return;
                }

                String selectedBillId = BillCollection.get(getActivity()).getBills().get(mBillIdSpinner.getSelectedItemPosition()).mBillId;
                new SendReport(SendReportFragment.this
                        , selectedBillId
                        , mTitle.getText().toString()
                        , mDate.getText().toString()
                        , mAddress.getText().toString()
                        , mSelectedLocation == null ? 0 : mSelectedLocation.latitude
                        , mSelectedLocation == null ? 0 : mSelectedLocation.longitude
                        , mDescription.getText().toString()
                ).execute();
            }
        });
    }

    private void setupData(){
        //Set Spinner Selection
        if(!TextUtils.isEmpty(mBillId)){
            ArrayList<BillRecord> mBills = BillCollection.get(getActivity()).getBills();
            for(int i = 0 ; i < mBills.size() ; i++)
                if(mBills.get(i).mBillId.equalsIgnoreCase(mBillId)){
                    mBillIdSpinner.setSelection(i);
                    break;
                }
        }

        //Set Current Date
        mDate.setText(new PersianCalendar().getPersianShortDate());
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case REQUEST_PICK_LOCATION:
                if(resultCode==Activity.RESULT_OK)
                    if(data!=null){
                        //Set Report Location
                        LatLng selectedLocation = data.getExtras().getParcelable(PickLocationFragment.EXTRA_SELECTED_LOCATION);
                        mSelectedLocation = new LatLng(selectedLocation.latitude, selectedLocation.longitude);

                        //Set Report Address If Needed
                        if(TextUtils.isEmpty(mAddress.getText().toString())){
                            //Get Address From Location
                            List<Address> addresses = null;
                            Locale lIran = new Locale("fa_IR");
                            Geocoder gcd = new Geocoder(getActivity(), lIran);
                            try{
                                addresses = gcd.getFromLocation(mSelectedLocation.latitude, mSelectedLocation.longitude, 1);
                            } catch (IOException e){
                                e.printStackTrace();
                                Log.e("GeoCoder", e.getMessage(), e);
                                Toast.makeText(getActivity(), "نام مکان مورد نظر یافت نشد, لطف آدرس را به صورت دستی وارد نمایید.", Toast.LENGTH_SHORT).show();
                            }

                            //Create Address String
                            if(addresses!=null && addresses.size()>0) {
                                String address = "";

                                String locality = addresses.get(0).getLocality();
                                String subLocality = addresses.get(0).getSubLocality();
                                String thoroughfare = addresses.get(0).getThoroughfare();
                                String featureName = addresses.get(0).getFeatureName();

                                //Add City Name
                                if (!TextUtils.isEmpty(locality))
                                    address = locality;

                                //Add Neighborhood Name
                                if (!TextUtils.isEmpty(subLocality))
                                    address = address + ", " + subLocality;

                                //Add Extra Detail if Available
                                if (!TextUtils.isEmpty(thoroughfare)) {
                                    address = address + ", " + thoroughfare;
                                } else if (!TextUtils.isEmpty(featureName)) {
                                    address = address + ", " + featureName;
                                }

                                mAddress.setText(address);
                            }
                        }
                    }
                break;
        }
    }

    public static SendReportFragment newInstance(String billId) {

        Bundle args = new Bundle();
        args.putString(EXTRA_BILL_ID, billId);

        SendReportFragment fragment = new SendReportFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
