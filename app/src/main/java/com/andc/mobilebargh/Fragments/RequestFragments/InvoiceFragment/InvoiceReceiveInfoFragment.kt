package com.andc.mobilebargh.Fragments.RequestFragments.InvoiceFragment

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.andc.mobilebargh.Activities.IRequest
import com.andc.mobilebargh.Activities.IRequestInvoice
import com.andc.mobilebargh.Controllers.BillCollection
import com.andc.mobilebargh.Fragments.DialogFramgments.ResponseStatusDialog
import com.andc.mobilebargh.Fragments.DialogFramgments.WaringDialog
import com.andc.mobilebargh.Fragments.ServiceFragments.BaseFragment
import com.andc.mobilebargh.R
import com.andc.mobilebargh.Utility.General
import com.andc.mobilebargh.Utility.Util.ServerStatus
import com.andc.mobilebargh.ViewModel.InvoiceVm
import kotlinx.android.synthetic.main.fragment_invoice_receive_info.*
import kotlinx.android.synthetic.main.fragment_invoice_receive_info.view.*


class InvoiceReceiveInfoFragment: BaseFragment(), View.OnClickListener {


    lateinit var mListener: IRequestInvoice
    lateinit var mViewModel: InvoiceVm
    var mBills: ArrayList<String> = ArrayList()
    lateinit var mBundle: Bundle


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel = activity.run { ViewModelProviders.of(this!!,viewModelFactory ).get(InvoiceVm::class.java) }
        mViewModel .cheCkStatus.observe(this, object: Observer<ServerStatus?> {
            override fun onChanged(t: ServerStatus?) {
                if( t == ServerStatus.RECEIVE){
                    progress_receive_info.hide()
                    mListener.OnNextClick()
                }
                if(t == ServerStatus.ERROR){
                    btn_request_receive_info.isEnabled = true
                    var dialog : ResponseStatusDialog = ResponseStatusDialog(activity)
                    dialog.showFail()
                    progress_receive_info.hide()

                }

            }
        } )

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
      var view = inflater.inflate(R.layout.fragment_invoice_receive_info, container, false)
        mListener = activity as IRequestInvoice
        General.FontSize(activity, view, "IRANSans(FaNum)_Medium.ttf")
        initBillIds(view)
        displayHelp()
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btn_request_receive_info.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        if(validate()) {
            progress_receive_info.show()
            btn_request_receive_info.isEnabled = false
            var name: String = edt_auto_bill_ids.text.toString()
            mViewModel.getBranchDetail(name)
        }
    }
    private fun displayHelp() {
        if (mBundle.get("type") == 0){
            WaringDialog.displayHelpMsg(activity,resources.getString(R.string.title_issue_bill), resources.getString(R.string.invoice_issue_content_info), false)
        }
        if (mBundle.get("type")== 1){
            WaringDialog.displayHelpMsg(activity, resources.getString(R.string.title_exmaine_bill),resources.getString(R.string.examine_bill_content_inof), true)
        }
        if(mBundle.get("type")== 2){
            WaringDialog.displayHelpMsg(activity, resources.getString(R.string.title_settlement_bill),resources.getString(R.string.settlement_bill_content_info), true)

        }
        if (mBundle.get("type")== 3){
            WaringDialog.displayHelpMsg(activity,resources.getString(R.string.title_calculation_bill),resources.getString(R.string.calculate_bill_content_info), false)
        }


    }

    private fun validate(): Boolean {
        var isValid: Boolean = true
        if(edt_auto_bill_ids.text.toString().isEmpty()){
            tv_error_bill_id.visibility = View.VISIBLE
            isValid =false

        }else{
            tv_error_bill_id.visibility = View.GONE
        }

        return isValid
    }

    private fun initBillIds(v: View) {
        mBills.addAll(BillCollection.get(activity).billIds)
        var adapter = ArrayAdapter(activity, android.R.layout.simple_spinner_item, mBills)
        v.edt_auto_bill_ids.threshold = 1
        v.edt_auto_bill_ids.setAdapter(adapter)
    }

    companion object {

          fun newInstance(type: Int)= InvoiceReceiveInfoFragment().apply {
              mBundle = Bundle()
              arguments.apply {
                  mBundle.putInt("type", type)
              }
          }
    }


}