package com.andc.mobilebargh.Fragments.IntroFragments;

import android.animation.LayoutTransition;
import android.app.Fragment;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;

import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextSwitcher;
import android.widget.TextView;

import com.andc.mobilebargh.Activities.IntroActivity;
import com.andc.mobilebargh.Controllers.PreferencesHelper;
import com.andc.mobilebargh.Models.CustomError;
import com.andc.mobilebargh.Networking.WSHelper;
import com.andc.mobilebargh.R;
import com.andc.mobilebargh.Utility.ViewHelper;

import java.util.ArrayList;

/**
 * Created by Esbati on 12/23/2015.
 */
public class LoginFragment extends android.support.v4.app.Fragment {

    //Global Views
    private SwipeRefreshLayout mSwipeRefresh;
    private LinearLayout mViewContainer;
    private TextSwitcher mTextSwitcher;
    private EditText mName;
    private EditText mFamilyName;
    private EditText mNationalCode;
    private EditText mCellphone;
    private ImageView mCellphoneAlert;
    private TextView mAlertText;
    private Button mLoginBtn;

    private boolean userIsRegistered;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_login, null, false);

        setupView(rootView);
        if(!TextUtils.isEmpty(PreferencesHelper.load(PreferencesHelper.KEY_CELLPHONE)))
            mCellphone.setText(PreferencesHelper.load(PreferencesHelper.KEY_CELLPHONE));
        return rootView;
    }

    private void setupView(View rootView){
        //Wrappers
        mViewContainer = (LinearLayout)rootView.findViewById(R.id.login_container);
        final TextInputLayout mNameWrapper = (TextInputLayout)rootView.findViewById(R.id.login_name_wrapper);
        final TextInputLayout mFamilyNameWrapper = (TextInputLayout)rootView.findViewById(R.id.login_family_name_wrapper);
        final TextInputLayout mNationalCodeWrapper = (TextInputLayout)rootView.findViewById(R.id.login_national_code_wrapper);
        mSwipeRefresh = (SwipeRefreshLayout)rootView.findViewById(R.id.swipe_refresh);
        mSwipeRefresh.setEnabled(false);

        //Data Fields
        mName = (EditText)rootView.findViewById(R.id.login_name);
        mFamilyName = (EditText)rootView.findViewById(R.id.login_family_name);
        mNationalCode = (EditText)rootView.findViewById(R.id.login_national_code);
        mCellphone = (EditText)rootView.findViewById(R.id.login_cellphone);

        //Action Views
        mCellphoneAlert = (ImageView)rootView.findViewById(R.id.login_cellphone_alert);
        mAlertText = (TextView)rootView.findViewById(R.id.login_alert_text);
        mLoginBtn = (Button)rootView.findViewById(R.id.login_button);
        ViewHelper.colorView(mLoginBtn, 0xffcc0000, R.color.andc_accent);

        //Setup TextSwitcher
        mTextSwitcher = (TextSwitcher)rootView.findViewById(R.id.login_registered_or_not);
        mTextSwitcher.setInAnimation(getActivity(), android.R.anim.slide_in_left);
        mTextSwitcher.setOutAnimation(getActivity(), android.R.anim.slide_out_right);
        mTextSwitcher.setText(getResources().getString(R.string.login_registered_already));

        //Set Click Listeners
        mCellphoneAlert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAlertText.setVisibility(View.VISIBLE);
            }
        });

        mLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });

        mTextSwitcher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //If User Already Registered, Hide Extra Fields and Show Appropriate Text and Button
                if(!userIsRegistered){
                    userIsRegistered = true;

                    //Remove Extra Views
                    mViewContainer.removeView(mNameWrapper);
                    mViewContainer.removeView(mFamilyNameWrapper);
                    mViewContainer.removeView(mNationalCodeWrapper);

                    mLoginBtn.setText(R.string.login_sign_in);
                } else {
                    userIsRegistered = false;

                    //Remove Extra Views
                    mViewContainer.addView(mNationalCodeWrapper, 0);
                    mViewContainer.addView(mFamilyNameWrapper, 0);
                    mViewContainer.addView(mNameWrapper, 0);

                    mLoginBtn.setText(R.string.login_sign_up);
                }
            }
        });

        //Setup Layout Transition
        LayoutTransition mLayoutTransition = mViewContainer.getLayoutTransition();
        mLayoutTransition.addTransitionListener(new LayoutTransition.TransitionListener() {
            @Override
            public void startTransition(LayoutTransition layoutTransition, ViewGroup viewGroup, View view, int transitionType) {
                if (transitionType == LayoutTransition.APPEARING && !userIsRegistered) {
                    mTextSwitcher.setText(getResources().getString(R.string.login_registered_already));
                }
            }

            @Override
            public void endTransition(LayoutTransition layoutTransition, ViewGroup viewGroup, View view, int transitionType) {
                if (transitionType == LayoutTransition.DISAPPEARING && userIsRegistered) {
                    mTextSwitcher.setText(getResources().getString(R.string.login_want_to_register));
                }
            }
        });
    }

    public void login(){

        //Close Keyboard
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

        //Check Cellphone
        String cellphone = mCellphone.getText().toString();
        if(cellphone.length()!=11) {
            Snackbar.make(((IntroActivity) getActivity()).mParentView, "شماره موبایل نا معتبر می باشد", Snackbar.LENGTH_LONG).show();
            return;
        }

        //Save The Cellphone
        PreferencesHelper.initiate();
        PreferencesHelper.save(PreferencesHelper.KEY_CELLPHONE, cellphone);

        //Start Load Animation and Disable Login Button
        mSwipeRefresh.setRefreshing(true);
        mLoginBtn.setEnabled(false);

        //Register New User, if User is Already Registered Log In User
        if(userIsRegistered){
            new SignInTask(mCellphone.getText().toString()).execute();
        } else {
            new SignUpTask(
                    mCellphone.getText().toString(),
                    mName.getText().toString(),
                    mFamilyName.getText().toString(),
                    mNationalCode.getText().toString()
            ).execute();
        }
    }

    private class SignUpTask extends AsyncTask<Void, Void, ArrayList<CustomError>>{
        String mCellphone;
        String mName;
        String mFamilyName;
        String mNationalCode;

        public SignUpTask(String cellphone, String name, String familyName, String nationalCode) {
            mCellphone = cellphone;
            mName = name;
            mFamilyName = familyName;
            mNationalCode = nationalCode;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList<CustomError> doInBackground(Void... voids) {
            return WSHelper.signUp(mCellphone, mName, mFamilyName, mNationalCode);
        }

        @Override
        protected void onPostExecute(ArrayList<CustomError> errors) {
            handleResponse(errors);
        }
    }

    private class SignInTask extends AsyncTask<Void, Void, ArrayList<CustomError>>{
        String mCellphone;

        public SignInTask(String cellphone) {
            mCellphone = cellphone;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList<CustomError> doInBackground(Void... voids) {
            return WSHelper.signIn(mCellphone);
        }

        @Override
        protected void onPostExecute(ArrayList<CustomError> errors) {
            handleResponse(errors);
        }
    }

    public void handleResponse(ArrayList<CustomError> errors){
        //Stop Loading Animation and Enable Button Again
        mLoginBtn.setEnabled(true);
        if(mSwipeRefresh.isRefreshing())
            mSwipeRefresh.setRefreshing(false);

        //Show Default Message if no Response Received From Server
        if(errors==null || errors.size()==0){
            Snackbar.make(((IntroActivity) getActivity()).mParentView, R.string.error_async_no_response, Snackbar.LENGTH_LONG).show();
            return;
        }

        //Show Response Message
        Snackbar.make(((IntroActivity) getActivity()).mParentView, errors.get(0).mMsg, Snackbar.LENGTH_LONG).show();

        //Move to Auth Page if Successful
        if (errors.get(0).mCode == 200 || errors.get(0).mCode == 20) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    //Move to Splash Screen
                    getActivity().getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.fragment_container, new AuthFragment())
                            .addToBackStack("auth_fragment")
                            .commit();
                }
            }, 200);
        }
    }
}
