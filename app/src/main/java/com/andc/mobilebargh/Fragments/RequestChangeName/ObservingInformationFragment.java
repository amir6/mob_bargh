package com.andc.mobilebargh.Fragments.RequestChangeName;


import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.andc.mobilebargh.Controllers.PreferencesHelper;
import com.andc.mobilebargh.Controllers.SecurityHelper;
import com.andc.mobilebargh.Models.CurrentUserInfo;
import com.andc.mobilebargh.Models.NewUserInfo;
import com.andc.mobilebargh.Models.PersonalDocumentInfo;
import com.andc.mobilebargh.Models.UpdateFile;
import com.andc.mobilebargh.R;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.andc.mobilebargh.Utility.General.FontSize;


public class ObservingInformationFragment extends Fragment {


    //EditText legalMobile, legalPhone, legalNumberSave, legalCompany, legalNewsPaper, legalEconomyCode, legalPost, legalAddress, legalMoasse, legalActivtyCode, legalPostWriterCode, legalWriterAddress, legalEmail;
    //EditText realNationalCode, realFirstName, realLastName, realIdentityNo, realPlaceIssuance, realPostCode, realBranchingAddress, realWritterPost, realWritterAddress, realMail;
    TextView title_1, title_2, title_3, title_4, title_5, title_6, title_7, title_8, title_9, title_10, title_11, title_12, title_13, title_14, title_15;
    TextView ans_1, ans_2, ans_3, ans_4, ans_5, ans_6, ans_7, ans_8, ans_9, ans_10, ans_11, ans_12, ans_13, ans_14, ans_15;
    IRequestChangeListener mListener;
    private Button btnNext, btnBack;
    private NewUserInfo mUserInfo;

    public static final String method_declare_current_branch = "ChangeNameRequest/";
    public static final String services_custom = "/api/CustomService/";
    public static final String fallback_addFormDataPartress = "https://mobilebargh.pec.ir/adpmobile";

    AVLoadingIndicatorView avi;
    //WEB SERVICE
    public static final String KEY_BillId = "BillId";
    public static final String KEY_FirstName = "FirstName";
    public static final String KEY_LastName = "LastName";
    public static final String KEY_Comments = "Comments";
    public static final String KEY_IsLegal = "IsLegal";
    public static final String KEY_FatherName = "FatherName";
    public static final String KEY_BirthCertificateId = "BirthCertificateId";
    public static final String KEY_NationalCard = "NationalCardId";
    public static final String KEY_BirthDate = "BirthDate";
    public static final String KEY_IssuePlace = "IssuePlace";
    public static final String KEY_IsMale = "IsMale";
    public static final String KEY_CompanyName = "CompanyName";
    public static final String KEY_CompanyRegistrationId = "CompanyRegistrationId";
    public static final String KEY_BussinessCode = "BussinessCode";
    public static final String KEY_CompanyCode = "CompanyCode";
    public static final String KEY_NewspaperNo = "NewspaperNo";
    public static final String KEY_NewspaperDate = "NewspaperDate";
    public static final String KEY_EconomicCode = "EconomicCode";
    public static final String KEY_TelNumber = "TelNumber";
    public static final String KEY_MobileNumber = "MobileNumber";
    public static final String KEY_Serviceadd = "ServiceAdd";
    public static final String KEY_ServicePostCode = "ServicePostCode";
    public static final String KEY_Contactadd = "ContactAdd";
    public static final String KEY_ContactPostCode = "ContactPostCode";
    public static final String KEY_Email = "Email";
    public static final String KEY_item1 = "item1";
    public static final String KEY_item2 = "item2";
    public static final String KEY_item3 = "item3";
    public static final String KEY_item4 = "item4";

    private boolean isReal;
    private static final MediaType MEDIA_TYPE_JPEG = MediaType.parse("image/jpeg");
    private static final String CONTENT_TYPE = "application/x-www-form-urlencoded";
    private NewUserInfo mNewUserInfo;
    private CurrentUserInfo mCurrentUserInfo;
    private PersonalDocumentInfo mPersonalDocumentInfo;

    private final static String NationalCartFrontName = "cart_up.jpg";
    private final static String NationalCartBehindName = "cart_down.jpg";
    private final static String IdentityCartName = "sh.jpg";
    private final static String CommitmentName = "commitment.jpg";

    private final static String KEY_ERROR_CODE = "ErrorCode";
    private final static String KEY_ERROR_Message = "ErrorMsg";
    private final static String KEY_REF_CODE = "RefCode";
    private final static String KEY_MESSAGE = "Message";
    private final static String KEY_ERROR = "error";

    private String ErrorFailedConnect;

    public static ObservingInformationFragment newInstance() {
        ObservingInformationFragment fragment = new ObservingInformationFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_observing_information, container, false);
        init(view);
        mNewUserInfo = mListener.getNewUserInfo();

        mCurrentUserInfo = mListener.getCurrentUserInfo();
        mPersonalDocumentInfo = mListener.getPersonalDocumentInfo();
        mUserInfo = mListener.getNewUserInfo();

        Log.d("!Test!", "Ghabz no : " + mCurrentUserInfo.billID);

        preparedata();
        FontSize(getActivity().getApplicationContext(), view, "IRANSans(FaNum)_Medium.ttf");
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InvokeFollowUpCode mAsync = new InvokeFollowUpCode();
                mAsync.execute();

            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.ListenerButtonBackClicked();
            }
        });

        return view;
    }

    private void preparedata() {
        //REAL
        if (mUserInfo.typeUser == 1) {
            title_1.setText(getActivity().getResources().getString(R.string.login_national_code));
            title_2.setText(getActivity().getResources().getString(R.string.family));
            title_3.setText(getActivity().getResources().getString(R.string.moshtarek_name));
            title_4.setText(getActivity().getResources().getString(R.string.real_born));
            title_5.setText(getActivity().getResources().getString(R.string.shenasname_str));
            title_6.setText(getActivity().getResources().getString(R.string.real_place_issuance));
            title_7.setText(getActivity().getResources().getString(R.string.sex));
            title_8.setText(getActivity().getResources().getString(R.string.phone_number));
            title_9.setText(getActivity().getResources().getString(R.string.mobile_number));
            title_10.setText(getActivity().getResources().getString(R.string.branching_address));
            title_11.setText(getActivity().getResources().getString(R.string.post_code));
            title_12.setText(getActivity().getResources().getString(R.string.writter_address));
            title_13.setText(getActivity().getResources().getString(R.string.writter_post));
            title_14.setText(getActivity().getResources().getString(R.string.email));

            ans_1.setText(mUserInfo.realNationalCode);
            ans_2.setText(mUserInfo.realLastName);
            ans_3.setText(mUserInfo.realFirstName);
            ans_4.setText(mUserInfo.realBirthDate);
            ans_5.setText(mUserInfo.realIdentityNo);
            ans_6.setText(mUserInfo.realPlaceIssuance);
            if (mUserInfo.sex == 1)
                ans_7.setText("مرد");
            else if (mUserInfo.sex == 2)
                ans_7.setText("زن");
            else
                ans_7.setText("");
            ans_8.setText(mUserInfo.realPhoneNumber);
            ans_9.setText(mUserInfo.realMobileNo);
            ans_10.setText(mUserInfo.realBranchingAddress);
            ans_11.setText(mUserInfo.realPostCode);
            ans_12.setText(mUserInfo.realWritterAddress);
            ans_13.setText(mUserInfo.realWritterPost);
            ans_14.setText(mUserInfo.realMail);
        }
        //حقوقی
        else if (mUserInfo.typeUser == 2) {
            title_1.setText(getActivity().getResources().getString(R.string.mobile_number));
            title_2.setText(getActivity().getResources().getString(R.string.phone_number));
            title_3.setText(getActivity().getResources().getString(R.string.sabt_no));
            title_4.setText(getActivity().getResources().getString(R.string.news_no));
            title_5.setText(getActivity().getResources().getString(R.string.news_date));
            title_6.setText(getActivity().getResources().getString(R.string.economy_no));
            title_7.setText(getActivity().getResources().getString(R.string.branching_address));
            title_8.setText(getActivity().getResources().getString(R.string.phone_number));
            title_9.setText(getActivity().getResources().getString(R.string.mobile_number));
            title_10.setText(getActivity().getResources().getString(R.string.post_code));
            title_11.setText(getActivity().getResources().getString(R.string.activity_code));
            title_12.setText(getActivity().getResources().getString(R.string.moase_code));
            title_13.setText(getActivity().getResources().getString(R.string.writter_address));
            title_14.setText(getActivity().getResources().getString(R.string.writter_post));
            title_15.setText(getActivity().getResources().getString(R.string.email));

            ans_1.setText(mUserInfo.legalMobile);
            ans_2.setText(mUserInfo.legalPhone);
            ans_3.setText(mUserInfo.legalNumberSave);
            ans_4.setText(mUserInfo.legalNewsPaper);
            ans_5.setText(mUserInfo.legalNewsPaperDate);
            ans_6.setText(mUserInfo.legalEconomyCode);
            ans_7.setText(mUserInfo.legalAddress);
            ans_8.setText(mUserInfo.legalPhone);
            ans_9.setText(mUserInfo.legalMobile);
            ans_10.setText(mUserInfo.legalPost);
            ans_11.setText(mUserInfo.legalActivtyCode);
            ans_12.setText(mUserInfo.legalMoasse);
            ans_13.setText(mUserInfo.legalWriterAddress);
            ans_14.setText(mUserInfo.legalPostWriterCode);
            ans_15.setText(mUserInfo.legalEmail);
        }
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
        mListener = (IRequestChangeListener)getActivity();

    }




    public void onDetach() {
        mListener = null;
        super.onDetach();
    }

    private void init(View view) {
        btnNext = (Button) view.findViewById(R.id.next_btn);
        btnBack = (Button) view.findViewById(R.id.back_btn);
        title_1 = (TextView) view.findViewById(R.id.title_1);
        title_2 = (TextView) view.findViewById(R.id.title_2);
        title_3 = (TextView) view.findViewById(R.id.title_3);
        title_4 = (TextView) view.findViewById(R.id.title_4);
        title_5 = (TextView) view.findViewById(R.id.title_5);
        title_6 = (TextView) view.findViewById(R.id.title_6);
        title_7 = (TextView) view.findViewById(R.id.title_7);
        title_8 = (TextView) view.findViewById(R.id.title_8);
        title_9 = (TextView) view.findViewById(R.id.title_9);
        title_10 = (TextView) view.findViewById(R.id.title_10);
        title_11 = (TextView) view.findViewById(R.id.title_11);
        title_12 = (TextView) view.findViewById(R.id.title_12);
        title_13 = (TextView) view.findViewById(R.id.title_13);
        title_14 = (TextView) view.findViewById(R.id.title_14);
        title_15 = (TextView) view.findViewById(R.id.title_15);

        ans_1 = (TextView) view.findViewById(R.id.txt_ans_1);
        ans_2 = (TextView) view.findViewById(R.id.txt_ans_2);
        ans_3 = (TextView) view.findViewById(R.id.txt_ans_3);
        ans_4 = (TextView) view.findViewById(R.id.txt_ans_4);
        ans_5 = (TextView) view.findViewById(R.id.txt_ans_5);
        ans_6 = (TextView) view.findViewById(R.id.txt_ans_6);
        ans_7 = (TextView) view.findViewById(R.id.txt_ans_7);
        ans_8 = (TextView) view.findViewById(R.id.txt_ans_8);
        ans_9 = (TextView) view.findViewById(R.id.txt_ans_9);
        ans_10 = (TextView) view.findViewById(R.id.txt_ans_10);
        ans_11 = (TextView) view.findViewById(R.id.txt_ans_11);
        ans_12 = (TextView) view.findViewById(R.id.txt_ans_12);
        ans_13 = (TextView) view.findViewById(R.id.txt_ans_13);
        ans_14 = (TextView) view.findViewById(R.id.txt_ans_14);
        ans_15 = (TextView) view.findViewById(R.id.txt_ans_15);

        avi = (AVLoadingIndicatorView) view.findViewById(R.id.avi_followup);
    }


    public class InvokeFollowUpCode extends AsyncTask<Void, Void, String> {

        // OkHttpClient client = new OkHttpClient();
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            avi.show();
        }


        @Override
        protected String doInBackground(Void... voids) {
            RequestBody formBody = null;
            ErrorFailedConnect = "";
            try {
                formBody = createFormBody();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (formBody != null)
                Log.d("!Test!", "formbody full");
            else
                Log.d("!Test!", "formbody is empty");

            final Request request = new Request.Builder()
                    .url(getBaseUrl() + services_custom + method_declare_current_branch)
                    //.url("http://192.168.0.76:8085" + services_custom + method_declare_current_branch)
                    .addHeader("Token", SecurityHelper.getToken())
                    .addHeader("Content-Type", CONTENT_TYPE)
                    .post(formBody)
                    .build();

            try {
                Response response = client.newCall(request).execute();
                if (response.isSuccessful()) {
                    return response.body().string();
                } else
                    Log.d("!Test!", "Response is Failed");
            } catch (IOException e) {
                Log.d("!Test!", "IO Exception : " + e.getMessage());
                ErrorFailedConnect = e.toString();
            }
            return "";
        }


        @Override
        protected void onPostExecute(String followUpCode) {
            super.onPostExecute(followUpCode);
            avi.hide();
            //Toast.makeText(getContext(), "Follow Up Code is : " + followUpCode, Toast.LENGTH_SHORT).show();
            Log.d("!Test!", "follow up is : " + followUpCode);
            try {
                parseJson(followUpCode);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void parseJson(String jsonString) throws JSONException {
        JSONObject mainJson = new JSONObject(jsonString);
        if (mainJson == null)
            return;
        JSONArray jsonArrayError = mainJson.optJSONArray(KEY_ERROR);
        if (jsonArrayError == null) {
            String refCode = mainJson.optString(KEY_REF_CODE);
            int errorCode = mainJson.optInt(KEY_ERROR_CODE);
            String message = mainJson.optString(KEY_MESSAGE);

            mListener.setFollowUpCode(refCode);
        } else {
            JSONObject object = jsonArrayError.optJSONObject(0);
            if (object != null) {
                int errorCode = object.optInt(KEY_ERROR_CODE);
                String errorMsg = mainJson.optString(KEY_ERROR_Message);
                Toast.makeText(getContext(), "Error Code is :" + String.valueOf(errorCode)
                        + " \n " + "Error Message is : " + errorMsg, Toast.LENGTH_SHORT).show();
            }

        }

        mListener.ListenerButtonNextClicked();

    }

    private RequestBody createFormBody() throws IOException {

        isReal = (mNewUserInfo.typeUser == 1 ? true : false);
        RequestBody formBody = null;

        if (isReal) {

            MultipartBody.Builder m = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    // .addFormDataPart(KEY_BillId, mListener.getCurrentUserInfo().billID)
                    .addFormDataPart(KEY_BillId, mCurrentUserInfo.billID)
                    .addFormDataPart(KEY_FirstName, mNewUserInfo.realFirstName)
                    .addFormDataPart(KEY_LastName, mNewUserInfo.realLastName)
                    //.addFormDataPart(KEY_Comments,mNewUserInfo.realLastName )
                    .addFormDataPart(KEY_IsLegal, String.valueOf(!isReal))
                    .addFormDataPart(KEY_FatherName, mNewUserInfo.realFatehrName)
                    .addFormDataPart(KEY_BirthCertificateId, mNewUserInfo.realIdentityNo)
                    .addFormDataPart(KEY_NationalCard, mNewUserInfo.realNationalCode)
                    .addFormDataPart(KEY_BirthDate, mNewUserInfo.realBirthDate)
                    .addFormDataPart(KEY_IssuePlace, mNewUserInfo.realPlaceIssuance)
                    .addFormDataPart(KEY_IsMale, String.valueOf((mNewUserInfo.sex == 1 ? true : false)))
                    .addFormDataPart(KEY_TelNumber, mNewUserInfo.realPhoneNumber)
                    .addFormDataPart(KEY_MobileNumber, mNewUserInfo.realMobileNo)
                    .addFormDataPart(KEY_Serviceadd, mNewUserInfo.realBranchingAddress)
                    .addFormDataPart(KEY_ServicePostCode, mNewUserInfo.realPostCode)
                    .addFormDataPart(KEY_Contactadd, mNewUserInfo.realWritterAddress)
                    .addFormDataPart(KEY_ContactPostCode, mNewUserInfo.realWritterPost)
                    .addFormDataPart(KEY_Email, mNewUserInfo.realMail);

            if (mPersonalDocumentInfo.getNationalCartFront() != null)
                m = m.addFormDataPart(KEY_item1, NationalCartFrontName, RequestBody.create(MEDIA_TYPE_JPEG, mPersonalDocumentInfo.getNationalCartFront()));
            if (mPersonalDocumentInfo.getNationalCartBehind() != null)
                m = m.addFormDataPart(KEY_item2, NationalCartBehindName, RequestBody.create(MEDIA_TYPE_JPEG, mPersonalDocumentInfo.getNationalCartBehind()));
            if (mPersonalDocumentInfo.getIdentityCart() != null)
                m = m.addFormDataPart(KEY_item3, IdentityCartName, RequestBody.create(MEDIA_TYPE_JPEG, mPersonalDocumentInfo.getIdentityCart()));
            if (mPersonalDocumentInfo.getCommitment() != null)
                m = m.addFormDataPart(KEY_item4, CommitmentName, RequestBody.create(MEDIA_TYPE_JPEG, mPersonalDocumentInfo.getCommitment()));
            formBody = m.build();
        } else {
            MultipartBody.Builder m = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart(KEY_BillId, mCurrentUserInfo.billID)
                    // .addFormDataPart(KEY_Comments, )
                    .addFormDataPart(KEY_IsLegal, String.valueOf(!isReal))
                    .addFormDataPart(KEY_CompanyName, mNewUserInfo.legalCompany)
                    .addFormDataPart(KEY_CompanyRegistrationId, mNewUserInfo.legalNumberSave)
                    .addFormDataPart(KEY_BussinessCode, mNewUserInfo.legalActivtyCode)
                    .addFormDataPart(KEY_CompanyCode, mNewUserInfo.legalMoasse)
                    .addFormDataPart(KEY_NewspaperNo, mNewUserInfo.legalNewsPaper)
                    .addFormDataPart(KEY_NewspaperDate, mNewUserInfo.legalNewsPaperDate)
                    .addFormDataPart(KEY_EconomicCode, mNewUserInfo.legalEconomyCode)
                    .addFormDataPart(KEY_TelNumber, mNewUserInfo.legalPhone)
                    .addFormDataPart(KEY_MobileNumber, mNewUserInfo.legalMobile)
                    .addFormDataPart(KEY_Serviceadd, mNewUserInfo.legalAddress)
                    .addFormDataPart(KEY_ServicePostCode, mNewUserInfo.legalPost)
                    .addFormDataPart(KEY_Contactadd, mNewUserInfo.legalWriterAddress)
                    .addFormDataPart(KEY_ContactPostCode, mNewUserInfo.legalPostWriterCode)
                    .addFormDataPart(KEY_Email, mNewUserInfo.legalEmail);

            if (mPersonalDocumentInfo.getNationalCartFront() != null)
                m = m.addFormDataPart(KEY_item1, NationalCartFrontName, RequestBody.create(MEDIA_TYPE_JPEG, mPersonalDocumentInfo.getNationalCartFront()));
            if (mPersonalDocumentInfo.getNationalCartBehind() != null)
                m = m.addFormDataPart(KEY_item2, NationalCartBehindName, RequestBody.create(MEDIA_TYPE_JPEG, mPersonalDocumentInfo.getNationalCartBehind()));
            if (mPersonalDocumentInfo.getIdentityCart() != null)
                m = m.addFormDataPart(KEY_item3, IdentityCartName, RequestBody.create(MEDIA_TYPE_JPEG, mPersonalDocumentInfo.getIdentityCart()));
            if (mPersonalDocumentInfo.getCommitment() != null)
                m = m.addFormDataPart(KEY_item4, CommitmentName, RequestBody.create(MEDIA_TYPE_JPEG, mPersonalDocumentInfo.getCommitment()));
            formBody = m.build();

        }
        return formBody;
    }

    public static String getBaseUrl() {
        String baseUrl = PreferencesHelper.load(UpdateFile.TAG_SERVER_ADDRESS);

        if (!TextUtils.isEmpty(baseUrl))
            return baseUrl;
        else
            return fallback_addFormDataPartress;
    }
}
