package com.andc.mobilebargh.Fragments.DialogFramgments;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.andc.mobilebargh.R;
import com.andc.mobilebargh.Utility.General;


public class WaringDialog {


    public static void showPermission(Activity context, String message){

        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_message, null, false);
        dialog.setView(view);
        TextView mMessage  = view.findViewById(R.id.tv_main);
        Button btnyes = view.findViewById(R.id.btn_yes);
        AlertDialog alertDialog = dialog.create();
        alertDialog.show();
        btnyes.setOnClickListener(new View.OnClickListener() {
                                      @Override
                                      public void onClick(View v) {
                                          if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                              Intent intent = new Intent();
                                              intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                              Uri uri = Uri.fromParts("package", context.getPackageName(), null);
                                              intent.setData(uri);
                                              context.startActivity(intent);
                                              alertDialog.dismiss();
                                          }
                                      }
                                  });
                Button btnNo = view.findViewById(R.id.btn_no);
                btnNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                        context.finish();
                    }
                });
        mMessage.setText(message);

    }

    public static void exitDialog(Activity context, String message){

        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_message, null, false);
        dialog.setView(view);
        TextView mMessage  = view.findViewById(R.id.tv_main);
        Button btnyes = view.findViewById(R.id.btn_yes);
        AlertDialog alertDialog = dialog.create();
        alertDialog.show();
        btnyes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              alertDialog.dismiss();
                context.finish();
            }
        });
        Button btnNo = view.findViewById(R.id.btn_no);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

            }
        });
        mMessage.setText(message);

    }

    public static void displayHelpMsg(Context context, String title, String content, boolean isDisplay){

        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_help_info, null, false);
        General.FontSize(context,view, "IRANSans(FaNum)_Medium.ttf");
        dialog.setView(view);
        Button btnyes = view.findViewById(R.id.btn_yes);
        AlertDialog alertDialog = dialog.create();
        alertDialog.show();
        TextView tvTitle  = view.findViewById(R.id.tv_help_title);
        TextView tvContent = view.findViewById(R.id.tv_help_content);
        LinearLayout layout = view.findViewById(R.id.lay_guide_document);
        tvTitle.setText(title);
        tvContent.setText(content);
        if(!isDisplay){
            layout.setVisibility(View.GONE);
        }
        btnyes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

            }
        });

    }


    public static void displayHelpMsgDoc(Context context, String title, String content, boolean isDisplay){

        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_help_info, null, false);
        General.FontSize(context,view, "IRANSans(FaNum)_Medium.ttf");
        dialog.setView(view);
        Button btnyes = view.findViewById(R.id.btn_yes);
        AlertDialog alertDialog = dialog.create();
        alertDialog.show();
        TextView tvTitle  = view.findViewById(R.id.tv_help_title);
        TextView tvContent = view.findViewById(R.id.tv_help_content);
        LinearLayout layout = view.findViewById(R.id.lay_guide_document);
        TextView tvDoc1 = view.findViewById(R.id.tv_guide_documnet_1);
        TextView tvDoc2 = view.findViewById(R.id.tv_guide_documnet_2);
        TextView tvDoc3 = view.findViewById(R.id.tv_guide_documnet_3);
        tvDoc1.setText(context.getResources().getString(R.string.help_document_activity_license_content));
        tvDoc2.setText(context.getResources().getString(R.string.help_documnet_business_license_content));
        tvDoc3.setText(context.getResources().getString(R.string.help_document_invoice_content));
        tvTitle.setText(title);
        tvContent.setText(content);
        if(!isDisplay){
            layout.setVisibility(View.GONE);
        }
        btnyes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

            }
        });

    }

}
