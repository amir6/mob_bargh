package com.andc.mobilebargh.Fragments.RequestChangeName;


import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.andc.mobilebargh.Controllers.BillCollection;
import com.andc.mobilebargh.Controllers.PreferencesHelper;
import com.andc.mobilebargh.Controllers.SecurityHelper;
import com.andc.mobilebargh.Fragments.DialogFramgments.PaymentDialog;
import com.andc.mobilebargh.Models.BillRecord;
import com.andc.mobilebargh.Models.BranchInfoRecord;
import com.andc.mobilebargh.Models.CurrentBranch;
import com.andc.mobilebargh.Models.CurrentUserInfo;
import com.andc.mobilebargh.Models.CustomJson;
import com.andc.mobilebargh.Models.ErrorMessage;
import com.andc.mobilebargh.Models.PaymentRecord;
import com.andc.mobilebargh.Models.UpdateFile;
import com.andc.mobilebargh.R;
import com.andc.mobilebargh.Utility.BillAPI.CheckDigitsUtility;
import com.andc.mobilebargh.Utility.ErrorHandler;
import com.reginald.editspinner.EditSpinner;
import com.transitionseverywhere.TransitionManager;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.andc.mobilebargh.Utility.General.FontSize;


public class CurrentUserFragment extends Fragment implements View.OnClickListener {

    private IRequestChangeListener mListener;
    TextView txUserName, txAddress, txArea, txRamz, txBedehiMasraf, txBedehiEnshab, txBedehiOrther,
            txPhase, txAmper, txPower, txEnsheab, txTarefe  , txFabricNumber;
    //EditText etGhabzNo;
    Button btnPayDebt, btnNext, btnBack;
    Spinner spinnerGhabzNo;
    ArrayList<String> spinnerArray = new ArrayList<>();
    String ghbazNoHolder;
    public static final String method_declare_current_branch = "GetSimpleBranchDataView/";
    public static final String services_custom = "/api/CustomService/";
    public static final String fallback_address = "https://mobilebargh.pec.ir/adpmobile";
    AVLoadingIndicatorView avi;
    AutoCompleteTextView mEditSpinner ;

    ArrayList<ErrorMessage> arrError = new ArrayList<>();
    boolean validationAccept = false;
    private LinearLayout viewWebServiceState;
    private TextView txWebServiceState ;
    private ImageView imgWebServiceState;
    private Animation animationFade;
    private Button mBtmGetBillInfo ;

    private BranchInfoRecord mBranchInfo;
    private PaymentRecord mPendingPayment;
    private BillRecord mBill;


    private void init(View view) {
        txUserName = (TextView) view.findViewById(R.id.user_name);
        txAddress = (TextView) view.findViewById(R.id.addres);
        txArea = (TextView) view.findViewById(R.id.area);
        txRamz = (TextView) view.findViewById(R.id.txt_ramz);
        txBedehiMasraf = (TextView) view.findViewById(R.id.bedehi_masraf);
        txBedehiEnshab = (TextView) view.findViewById(R.id.bedehi_ensheab);
        txBedehiOrther = (TextView) view.findViewById(R.id.bedehi_orther);
        txPhase = (TextView) view.findViewById(R.id.phase);
        txAmper = (TextView) view.findViewById(R.id.amper);
        txPower = (TextView) view.findViewById(R.id.power);
        txEnsheab = (TextView) view.findViewById(R.id.ensheab);
        txTarefe = (TextView) view.findViewById(R.id.tarefe);
        txFabricNumber =(TextView) view.findViewById(R.id.txt_fabric_number);
        // etGhabzNo = (EditText) view.findViewById(R.id.et_ghabz_no);
   //     btnSearchGhabz = (Button) view.findViewById(R.id.btn_search_ghabz);
        btnNext = (Button) view.findViewById(R.id.next_btn);
        btnBack = (Button) view.findViewById(R.id.back_btn);
        avi = (AVLoadingIndicatorView) view.findViewById(R.id.avi);
        viewWebServiceState = (LinearLayout) view.findViewById(R.id.cardview_state_webservice);
        txWebServiceState = (TextView) view.findViewById(R.id.txt_state_web);
        imgWebServiceState = (ImageView) view.findViewById(R.id.img_state_web);
        mBtmGetBillInfo =  view.findViewById(R.id.btn_request_receive_info);
        btnPayDebt = (Button) view.findViewById(R.id.btn_pay_debt);
    }

    public static CurrentUserFragment newInstance() {
        CurrentUserFragment fragment = new CurrentUserFragment();
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (IRequestChangeListener)getActivity();

        }


    public void onDetach() {
        mListener = null;
        super.onDetach();
    }


    private void fetchData(String mBillId){
        //Get Branch Info
        mBranchInfo = new BranchInfoRecord();
        ArrayList<BranchInfoRecord> mBranchList = (ArrayList<BranchInfoRecord>) BranchInfoRecord
                .find(BranchInfoRecord.class, BranchInfoRecord.LABEL_BILL_ID + " = ?", mBillId);
        if(mBranchList!=null)
            if(mBranchList.size()>0)
                mBranchInfo = mBranchList.get(0);

        //Get Bill Info
        mBill = new BillRecord();
        ArrayList<BillRecord> mBillList = (ArrayList<BillRecord>) BillRecord
                .find(BillRecord.class, BillRecord.LABEL_BILL_ID + " = ?", mBillId);
        if(mBillList!=null)
            if(mBillList.size()>0)
                mBill = mBillList.get(0);

        //Get Pending Payments
        mPendingPayment = null;
        ArrayList<PaymentRecord> mPendingPayments = (ArrayList<PaymentRecord>) PaymentRecord
                .find(PaymentRecord.class, PaymentRecord.LABEL_PAYMENT_Bill_Id + " = ?", mBillId);
        if(mPendingPayments!=null && mPendingPayments.size()>0)
            mPendingPayment = mPendingPayments.get(0);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        validationAccept = false;
        final View view = inflater.inflate(R.layout.fragment_current_user, container, false);
        FontSize(getActivity().getApplicationContext(), view, "IRANSans(FaNum)_Medium.ttf");
        init(view);
        btnPayDebt.setEnabled(false);

        btnPayDebt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchData(ghbazNoHolder);
                if (mPendingPayment != null) {
                    ErrorHandler.showError(view, "بدهی شما پرداخت گردیده, تا تایید پرداخت خود شکیبا باشید.");
                    return;
                }

                if (mBranchInfo.CrDbTot == null || mBranchInfo.CrDbTot == "" || Integer.parseInt(mBranchInfo.CrDbTot) < 1000) {
                    ErrorHandler.showError(view, "بدهی شما از حد مجاز کمتر می باشد.");
                    return;
                }
                DialogFragment mPayBill = PaymentDialog.newInstance(mBill.mBillId,false);
                mPayBill.show(getChildFragmentManager(), "PayBills");
            }
        });

        //SET SPINNER
        spinnerArray.clear();
        spinnerArray.addAll(BillCollection.get(getActivity()).getBillIds());

        final ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, spinnerArray);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        mEditSpinner = view.findViewById(R.id.edt_auto_bill_ids);
        mEditSpinner.setAdapter(spinnerAdapter);


        animationFade = AnimationUtils.loadAnimation(getContext(),R.anim.fade);
        animationFade.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                viewWebServiceState.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });


        setPreInfo();
//        btnSearchGhabz.setOnClickListener(this);
        btnBack.setOnClickListener(this);
        btnNext.setOnClickListener(this);
        mBtmGetBillInfo.setOnClickListener(this);

       //TransitionManager.beginDelayedTransition(cardViewSync);
        ghbazNoHolder = "";
        return view;
    }

    private void setPreInfo() {
        if (mListener != null) {
            CurrentUserInfo mInfo = mListener.getCurrentUserInfo();
            if(mInfo != null) {
                txUserName.setText(mInfo.Name);
                txAddress.setText(mInfo.Address);
                txArea.setText(mInfo.Region);
                //txRamz.setText(mInfo.ramz);
                txBedehiMasraf.setText(mInfo.bedehiMasraf);
                txBedehiEnshab.setText(mInfo.bedehiEnshab);
                txBedehiOrther.setText(mInfo.bedehiOrther);
                txPhase.setText(mInfo.phase);
                txAmper.setText(mInfo.amper);
                txPower.setText(mInfo.power);
                txEnsheab.setText(mInfo.ensheab);
                txTarefe.setText(mInfo.tarefe);
            }
        }
    }

    private void emptyAllTextBox() {
        txUserName.setText("");
        txAddress.setText("");
        txArea.setText("");
        txRamz.setText("");
        txFabricNumber.setText("");
        txBedehiMasraf.setText("");
        txBedehiEnshab.setText("");
        txBedehiOrther.setText("");
        txPhase.setText("");
        txAmper.setText("");
        txPower.setText("");
        txEnsheab.setText("");
        txTarefe.setText("");
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_pay_debt:
                break;
                case R.id.btn_request_receive_info:
                    ghbazNoHolder = mEditSpinner.getText().toString();
                    btnPayDebt.setEnabled(false);
                    if (addNewBill(ghbazNoHolder)) {
                        validationAccept = false;
                        emptyAllTextBox();
                        CurrentBranchAsync currentBranchAsync = new CurrentBranchAsync();
                        currentBranchAsync.execute(ghbazNoHolder);
                    }
                    break;
            case R.id.back_btn:
                if (mListener != null)
                    mListener.ListenerButtonBackClicked();
                break;
            case R.id.next_btn:
                if (mListener != null)
                    if (TextUtils.isEmpty(ghbazNoHolder)) {
                        Toast.makeText(getContext(), getResources().getString(R.string.empty_ghabz), Toast.LENGTH_SHORT).show();
                    }
                    else if (!validationAccept) {
                        Toast.makeText(getContext(), getResources().getString(R.string.get_info_fail), Toast.LENGTH_LONG).show();
                    }
                    else {
                        // SAVE DATA
                        CurrentUserInfo currentUserInfo = new CurrentUserInfo(ghbazNoHolder
                                , txUserName.getText().toString()
                                , txAddress.getText().toString()
                                , txArea.getText().toString()
                                , txRamz.getText().toString()
                                , txBedehiMasraf.getText().toString()
                                , txBedehiEnshab.getText().toString()
                                , txBedehiOrther.getText().toString()
                                , txPhase.getText().toString()
                                , txAmper.getText().toString()
                                , txPower.getText().toString()
                                , txEnsheab.getText().toString()
                                , txTarefe.getText().toString()
                                ,txFabricNumber.getText().toString()
                        );
                        mListener.setCurrentUserInfo(currentUserInfo);
                        mListener.ListenerButtonNextClicked();
                    }


                break;
        }
    }

    public static String getBaseUrl() {
        String baseUrl = PreferencesHelper.load(UpdateFile.TAG_SERVER_ADDRESS);

        if (!TextUtils.isEmpty(baseUrl))
            return baseUrl;
        else
            return fallback_address;
    }

    public class CurrentBranchAsync extends AsyncTask<String, Void, String> {
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            avi.show();
        }

        @Override
        protected String doInBackground(String... ghabzNo) {

            final Request request = new Request.Builder()
                    .url(getBaseUrl() + services_custom + method_declare_current_branch + ghabzNo[0])
                    .addHeader("Token", SecurityHelper.getToken())
                    .get()
                    .build();
            try {
                Response response = client.newCall(request).execute();
                if (response.isSuccessful())
                    return response.body().string();
                else
                    Log.d("!Test!", "Response is Failed");
            } catch (IOException e) {
                Log.d("!Test!", e.toString());
            }
            return "";
        }

        @Override
        protected void onPostExecute(String json) {
            super.onPostExecute(json);

            //EXTRACT JSON
            JSONObject object = null;
            try {
                object = new JSONObject(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            CustomJson customJson = new CustomJson(object);
            JSONObject jData = null;
            try {
                jData = customJson.getData();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            setInfo(CurrentBranch.parseJson(jData));
            avi.hide();
        }
    }


    public void setInfo(CurrentBranch info) {
        if (info != null) {
            try {
                txUserName.setText(info.CompanyName);
                txAddress.setText(info.HomeAdderss);
                txArea.setText(info.RegionName);
                txRamz.setText(info.BranchCode);
                txBedehiMasraf.setText(info.CrDbTot);
                txBedehiEnshab.setText(info.CrDBBranchSale);
                txBedehiOrther.setText(info.CrDbOtherService);
                txPhase.setText(info.Phs);
                txAmper.setText(info.Amp);
                txPower.setText(info.PwrCnt);
                txEnsheab.setText(info.BranchKindCodeName);
                txTarefe.setText(info.TrfHCodeName);
                txFabricNumber.setText("");
                validationAccept = true;
                btnPayDebt.setEnabled(true);
                showSuccess();

            } catch (Exception ex) {
                ex.getStackTrace();
            }
        }else{
            showFail();
        }
    }

    private void showSuccess() {
        viewWebServiceState.setVisibility(View.VISIBLE);
        txWebServiceState.setText(R.string.success_webservice);
        txWebServiceState.setTextColor(getResources().getColor(R.color.green_4));
        imgWebServiceState.setImageResource(R.drawable.success_round);
        //SET ANIMATION
        viewWebServiceState.startAnimation(animationFade);
    }

    private void showFail() {
        viewWebServiceState.setVisibility(View.VISIBLE);
        txWebServiceState.setText(R.string.failed_webservice);
        txWebServiceState.setTextColor(getResources().getColor(R.color.red_2));
        imgWebServiceState.setImageResource(R.drawable.fail_round);
        //SET ANIMATION
        viewWebServiceState.startAnimation(animationFade);
    }

    private void showErrorMessage(String messText) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage(messText);
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    private boolean addNewBill(String newBillId) {
        //Return Error if Bill Id is Empty or Is not a Valid Number
        if (TextUtils.isEmpty(newBillId)) {
            showErrorMessage(getResources().getString(R.string.empty_ghabz));
            return false;
        }

        //Check to See if the Bill Id is Valid
        if (!TextUtils.isDigitsOnly(newBillId) || !CheckDigitsUtility.isDigitValid(newBillId)) {
            showErrorMessage(getResources().getString(R.string.empty_ghabz));
            return false;
        }

        return true;
    }
}
