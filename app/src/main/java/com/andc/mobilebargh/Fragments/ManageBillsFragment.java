package com.andc.mobilebargh.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andc.mobilebargh.Adapter.ManageBillAdapter;
import com.andc.mobilebargh.R;
import com.kbeanie.imagechooser.api.ChooserType;

/**
 * Created by Esbati on 4/23/2016.
 */
public class ManageBillsFragment extends Fragment {

    public final static int PICK_PHOTO_CODE = 1046;
    public final static String EXTRA_IS_EDITED = "isEdited";

    //Global Views
    private View rootView;

    //Recycler View
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mRecyclerAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.list_recycler_simple, null);

        setupList(rootView);
        return rootView;
    }



    public void setupList(View rootView){
        mRecyclerView = (RecyclerView)rootView.findViewById(R.id.list);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter
        mRecyclerAdapter = new ManageBillAdapter(this);
        mRecyclerView.setAdapter(mRecyclerAdapter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case ChooserType.REQUEST_PICK_PICTURE:
                ((ManageBillAdapter)mRecyclerAdapter).onActivityResult(requestCode, resultCode, data);
                break;

            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }
}
