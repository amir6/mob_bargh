package com.andc.mobilebargh.Fragments.RequestFragments.TariffChangeFragmemts

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.andc.mobilebargh.Activities.IRequestTariff
import com.andc.mobilebargh.Fragments.DialogFramgments.WaringDialog
import com.andc.mobilebargh.Fragments.ServiceFragments.BaseFragment
import com.andc.mobilebargh.Injection.ViewModelFactory
import com.andc.mobilebargh.Models.BillData
import com.andc.mobilebargh.R
import com.andc.mobilebargh.Utility.General
import com.andc.mobilebargh.ViewModel.TariffVm
import com.andc.mobilebargh.databinding.FragmentBranchInfoBinding
import com.andc.mobilebargh.repository.model.BranchData
import kotlinx.android.synthetic.main.fragment_branch_info.*


class TariffBranchInfoFragments: BaseFragment(), View.OnClickListener {


    private lateinit var mBinding: FragmentBranchInfoBinding
    lateinit var billData : BillData
    private lateinit var mViewMode: TariffVm
    private lateinit var mListenr : IRequestTariff

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewMode = activity.run { ViewModelProviders.of(this!! , viewModelFactory).get(TariffVm::class.java)}
        mViewMode.getBranchInfo.observe(this, object: Observer<BranchData?> {
            override fun onChanged(t: BranchData?) {
                if(t != null){
                    mBinding.data = t
                }
            }
        })

    }




    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_branch_info, null, false)
        mListenr = activity as IRequestTariff
        billData = BillData()
        mBinding.data =  BranchData()
        var v = mBinding.root
        General.FontSize(activity, v, "IRANSans(FaNum)_Medium.ttf")
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        next_btn.setOnClickListener(this)
        back_btn.setOnClickListener(this)

    }




    override fun onClick(v: View?) {
       when(v?.id){
           next_btn.id -> mListenr.onNextClick()
           back_btn.id -> {
               WaringDialog.exitDialog(activity,resources.getString(R.string.dialog_exit_body))
           }


       }
    }







    companion object {
        fun newInstance() = TariffBranchInfoFragments().apply {
            arguments.apply {  }
        }
    }

}