package com.andc.mobilebargh.Fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.andc.mobilebargh.R;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Esbati on 12/20/2015.
 */
public class ContactUsFragment extends Fragment {

    private static int count;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_contact_us, null);

        setupView(rootView);
        if(count > 7)
            revealViews(rootView);
        return rootView;
    }

    private void setupView(final View rootView){
        CircleImageView mPeccoIcon = (CircleImageView)rootView.findViewById(R.id.pecco_logo);
        mPeccoIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count++;
                if (count > 7)
                    revealViews(rootView);
            }
        });

        TextView mContactViaWeb = (TextView)rootView.findViewById(R.id.andc_website);
        mContactViaWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Direct User to Our Website
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://" + getResources().getString(R.string.andc_website)));
                startActivity(intent);
            }
        });

        TextView mContactViaMail = (TextView)rootView.findViewById(R.id.andc_email);
        mContactViaMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Let User Send a Mail
                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("mailto:Support@ANDC.ir"));
                intent.putExtra(Intent.EXTRA_SUBJECT, "خدمات مشترکین");
                startActivity(intent);
            }
        });

        TextView mContactPeccoWeb = (TextView)rootView.findViewById(R.id.pecco_website);
        mContactPeccoWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Direct User to Our Website
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://" + getResources().getString(R.string.pecco_website)));
                startActivity(intent);
            }
        });

        TextView mContactPeccoMail = (TextView)rootView.findViewById(R.id.pecco_number);
        mContactPeccoMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Let User Send a Mail
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.putExtra(Intent.EXTRA_PHONE_NUMBER, getResources().getString(R.string.pecco_contact_number));
                intent.setData(Uri.parse("tel:0212318"));
                startActivity(intent);
            }
        });
    }

    public void revealViews(View rootView){
        rootView.findViewById(R.id.andc_logo).setVisibility(View.VISIBLE);

        rootView.findViewById(R.id.contact_us_title).setVisibility(View.VISIBLE);
        rootView.findViewById(R.id.contact_us_title_detail).setVisibility(View.VISIBLE);
        rootView.findViewById(R.id.contact_us_sub_title).setVisibility(View.VISIBLE);

        rootView.findViewById(R.id.contact_us_extra_divider).setVisibility(View.VISIBLE);
        rootView.findViewById(R.id.contact_us_description).setVisibility(View.VISIBLE);

        rootView.findViewById(R.id.andc_website).setVisibility(View.VISIBLE);
        rootView.findViewById(R.id.andc_email).setVisibility(View.VISIBLE);
    }
}