package com.andc.mobilebargh.Fragments.RequestChangeName;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.andc.mobilebargh.R;


public class TrackIGNFragment extends Fragment {

    TextView number_follow_up;
    Button btnFinish;
    IRequestChangeListener mListener;

    public static TrackIGNFragment newInstance() {
        TrackIGNFragment fragment = new TrackIGNFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (IRequestChangeListener)getActivity();
    }




    public void onDetach() {
        mListener = null;
        super.onDetach();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_track_ign, container, false);
        init(view);
        return view;
    }

    private void init(View view)
    {
        btnFinish = (Button) view.findViewById(R.id.btn_tracking_finish);
        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
        number_follow_up = (TextView) view.findViewById(R.id.number_follow_up);
    }

}
