package com.andc.mobilebargh.Fragments.RequestFragments.TariffChangeFragmemts

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.provider.SyncStateContract
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.andc.mobilebargh.Activities.IRequestTariff
import com.andc.mobilebargh.Fragments.DialogFramgments.WaringDialog
import com.andc.mobilebargh.Fragments.ServiceFragments.BaseFragment
import com.andc.mobilebargh.R
import com.andc.mobilebargh.Utility.Util.Constants
import com.andc.mobilebargh.Utility.Util.Constants.TARIFF_AGRICULTURE_CODE
import com.andc.mobilebargh.Utility.Util.Constants.TARIFF_BUSINESS_CODE
import com.andc.mobilebargh.Utility.Util.Constants.TARIFF_GENERAL_CODE
import com.andc.mobilebargh.Utility.Util.Constants.TARIFF_GENERAL_SPECIAL_CODE
import com.andc.mobilebargh.Utility.Util.Constants.TARIFF_HOME_CODE
import com.andc.mobilebargh.Utility.Util.Constants.TARIFF_HOME_SPECIAL_CODE
import com.andc.mobilebargh.Utility.Util.Constants.TARIFF_INDUSTRIAL_CODE
import com.andc.mobilebargh.ViewModel.TariffVm
import kotlinx.android.synthetic.main.fragment_tariff_change_info.*
import kotlinx.android.synthetic.main.fragment_tariff_change_info.view.*

class TariffChangeInfoFragment: BaseFragment(), View.OnClickListener {


    private lateinit var mListenr : IRequestTariff
    lateinit var mTariffList: ArrayList<String>
    lateinit var mAdapter: ArrayAdapter<String>
    private var mTariffCode: String = "0"
    private lateinit var mViewModel : TariffVm

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel = activity.run {ViewModelProviders.of(this!!, viewModelFactory).get(TariffVm::class.java)}

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAdapter(view)
        next_btn.setOnClickListener(this)
        back_btn.setOnClickListener(this)
        spn_tariff_change.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                selectItem(position)
            }
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
       var view = inflater.inflate(R.layout.fragment_tariff_change_info, container, false)
        mListenr = activity as IRequestTariff

        return view
    }

    private fun selectItem(position: Int) {
        when(position){
            1 -> mTariffCode = TARIFF_HOME_CODE
            2 -> mTariffCode = TARIFF_HOME_SPECIAL_CODE
            3 -> mTariffCode = TARIFF_GENERAL_CODE
            4 -> mTariffCode = TARIFF_GENERAL_SPECIAL_CODE
            5 -> mTariffCode = TARIFF_AGRICULTURE_CODE
            6 -> mTariffCode = TARIFF_INDUSTRIAL_CODE
            7 -> mTariffCode = TARIFF_BUSINESS_CODE
        }
    }

    private fun initAdapter(v: View) {
        mTariffList  = ArrayList()
        mTariffList.addAll(resources.getStringArray(R.array.tariff_list))
        mAdapter = ArrayAdapter(activity, android.R.layout.simple_list_item_1, mTariffList)
        v.spn_tariff_change.adapter = mAdapter
        v.tv_title.visibility = View.VISIBLE
    }
    override fun onClick(v: View?) {
        when(v?.id) {
            next_btn.id -> {
                if(isValidate()){
                    mViewModel.setTariffCode(mTariffCode)
                    mListenr.onNextClick()
                }

            }
            back_btn.id -> {

                WaringDialog.exitDialog(activity,resources.getString(R.string.dialog_exit_body))
            }
        }
    }

    private fun isValidate(): Boolean {
            var isValid =true
        if(spn_tariff_change.selectedItemPosition == 0){
            tv_error_tariff_type.visibility = View.VISIBLE
            isValid = false
        }else {
            tv_error_tariff_type.visibility = View.GONE
        }

        return isValid
    }

    companion object {
        fun newInstance() = TariffChangeInfoFragment().apply {
            arguments.apply {  }
        }
    }

}