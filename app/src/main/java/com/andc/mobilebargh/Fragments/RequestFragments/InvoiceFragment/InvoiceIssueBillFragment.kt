package com.andc.mobilebargh.Fragments.RequestFragments.InvoiceFragment

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.ImageDecoder
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.andc.mobilebargh.Fragments.ServiceFragments.BaseFragment
import com.andc.mobilebargh.R
import com.andc.mobilebargh.Utility.Util.DocumentsFile.Companion.getResizedBitmap
import com.andc.mobilebargh.ViewModel.InvoiceVm
import com.andc.mobilebargh.repository.model.BillResponse
import com.andc.mobilebargh.repository.model.ResponseData
import com.bumptech.glide.Glide

import kotlinx.android.synthetic.main.fragment_invoice_issue_bill.*
import java.io.File
import java.io.FileOutputStream
import android.provider.MediaStore
import android.support.design.widget.Snackbar
import com.andc.mobilebargh.Utility.Util.Constants.ANDC_BILL_FOLDER
import com.andc.mobilebargh.Utility.Util.Constants.ANDC_FOLDER_NAME


class InvoiceIssueBillFragment: BaseFragment(), View.OnClickListener {

    val bitmap: Bitmap ?= null
     var   file: File? = null
    private lateinit  var rootView: View
    private lateinit var mViewModel: InvoiceVm
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel = activity.run { ViewModelProviders.of(this!!, viewModelFactory).get(InvoiceVm::class.java) }
        mViewModel.getBillResponse.observe(this, object : Observer<BillResponse?> {
            override fun onChanged(t: BillResponse?) {
                val options = BitmapFactory.Options()
                options.outHeight = 400
                options.outWidth = 300
                var decodeString = Base64.decode(t!!.IMAGE_Format, Base64.DEFAULT)


                try {
                    val dir = File(Environment.getExternalStorageDirectory().toString() + "/" + ANDC_FOLDER_NAME + "/" + ANDC_BILL_FOLDER)
                   if(!dir.exists()){
                       dir.mkdir()
                   }
                    file = File(dir, "vv.jpg")
                    val fOut: FileOutputStream

                    fOut = FileOutputStream(file)
                    fOut.write(decodeString)
                    fOut.flush()
                    fOut.close()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                Glide.with(activity!!).load(decodeString).into(img_issue_bill)

            }

        })
    }
    fun getResizedBitmap(image: Bitmap, maxSize: Int): Bitmap {
        var width = image.width
        var height = image.height

        val bitmapRatio = width.toFloat() / height.toFloat()
        if (bitmapRatio > 1) {
            width = maxSize
            height = (width / bitmapRatio).toInt()
        } else {
            height = maxSize
            width = (height * bitmapRatio).toInt()
        }
        return Bitmap.createScaledBitmap(image, width, height, true)
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView  = inflater.inflate(R.layout.fragment_invoice_issue_bill, container, false)

        return rootView
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btn_store_gallery.setOnClickListener(this)

    }
    override fun onClick(v: View?) {
        if(file != null) {
            MediaStore.Images.Media.insertImage(activity!!.contentResolver, file!!.path, "", "")
            Snackbar.make(rootView, R.string.dialog_save_gallery_success, Snackbar.LENGTH_SHORT).show()
        }
    }


   companion object {
       fun newInstance() = InvoiceIssueBillFragment().apply {
           arguments.apply {  }
       }
   }


}