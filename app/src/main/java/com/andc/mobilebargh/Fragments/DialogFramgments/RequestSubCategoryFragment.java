package com.andc.mobilebargh.Fragments.DialogFramgments;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;

import com.andc.mobilebargh.Activities.FragmentActivity;
import com.andc.mobilebargh.Activities.InvoiceActivity;
import com.andc.mobilebargh.Activities.RequestActivity;
import com.andc.mobilebargh.Adapter.RequestSubItemAdapter;
import com.andc.mobilebargh.R;



import static com.andc.mobilebargh.Fragments.RequestFragments.ListCategoryFragment.BIILING_CATEGORY_TAG;
import static com.andc.mobilebargh.Fragments.RequestFragments.ListCategoryFragment.CHANGE_NAME_TAG;
import static com.andc.mobilebargh.Fragments.RequestFragments.ListCategoryFragment.CORRECTION_INFO_CATEGORY_TAG;
import static com.andc.mobilebargh.Fragments.RequestFragments.ListCategoryFragment.SALE_CATEGORY_TAG;
import static com.andc.mobilebargh.Fragments.RequestFragments.ListCategoryFragment.SALE_SERVICE_CATEGORY_TAG;
import static com.andc.mobilebargh.Utility.Util.Constants.INTENT_DATA_POSITION;

public class RequestSubCategoryFragment extends DialogFragment implements AdapterView.OnItemClickListener {

  ListView mList;
  String mTag;
  String[] mCategoryList;
  RequestSubItemAdapter mAdapter;




  @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
      View v = inflater.inflate(R.layout.fragment_request_sub_category, container, false);

      mList = v.findViewById(R.id.sub_category_list);
      mTag = getTag();
      initAdapter();
      mList.setOnItemClickListener(this);
      return v;
    }

  @NonNull
  @Override
  public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
    Dialog dialog = super.onCreateDialog(savedInstanceState);

    // request a window without the title
    dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
    return dialog;
  }

  @Override
  public void onStart() {
    super.onStart();

    int width = ViewGroup.LayoutParams.MATCH_PARENT;
    int height = ViewGroup.LayoutParams.WRAP_CONTENT;
    getDialog().getWindow().setLayout(width, height);

  }

  private void initAdapter() {
      switch (mTag){
        case SALE_SERVICE_CATEGORY_TAG:
          mCategoryList = getActivity().getResources().getStringArray(R.array.after_sale_list);
          break;
        case SALE_CATEGORY_TAG:
          mCategoryList = getActivity().getResources().getStringArray(R.array.request_sale_list);
          break;
        case CORRECTION_INFO_CATEGORY_TAG:
          mCategoryList = getActivity().getResources().getStringArray(R.array.info_correction_list);
          break;
        case BIILING_CATEGORY_TAG:
          mCategoryList = getActivity().getResources().getStringArray(R.array.billing_list);
          break;


      }
       mAdapter =  new RequestSubItemAdapter(getActivity(), mCategoryList);
        mList.setAdapter(mAdapter);

    }


  @Override
  public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    Fragment fragment = null;
        if(mTag == CHANGE_NAME_TAG) {


      }

      if(mTag == SALE_SERVICE_CATEGORY_TAG){
        Intent intent = new Intent(getActivity(), RequestActivity.class);
        dismiss();
        intent.putExtra(INTENT_DATA_POSITION, position);
        startActivity(intent);
        }
    if(mTag == BIILING_CATEGORY_TAG){
      Intent billIntent = new Intent(getActivity(), InvoiceActivity.class);
      dismiss();
      billIntent.putExtra(INTENT_DATA_POSITION , position);
      startActivity(billIntent);
      }
      if(fragment != null) {
        getFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).commit();

      }

  }
}






