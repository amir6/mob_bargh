package com.andc.mobilebargh.Fragments.HomeFragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andc.mobilebargh.Activities.MainActivity;
import com.andc.mobilebargh.Utility.Components.HeaderFragment;
import com.andc.mobilebargh.Models.BranchInfoRecord;
import com.andc.mobilebargh.R;
import com.andc.mobilebargh.Utility.DepthPageTransformer;
import com.andc.mobilebargh.Utility.Components.RefreshableFragment;
import com.kbeanie.imagechooser.api.ChooserType;

import java.util.ArrayList;

/**
 * Created by win on 4/11/2015.
 */
public class HomeFragment extends RefreshableFragment{

    public final static String EXTRA_BILL_ID = "bill_id";

    //Global Variables
    private String mBillingID;
    private BranchInfoRecord mBranchInfo;
    private int previousPosition;

    //Global Views
    private View rootView;
    private View mTutorialOverlay;
    private ViewPager mPager;


    @Override
    public boolean onRefreshFragment(String newBillId){
        mBillingID = newBillId;
        /**
         * FIXME Program Give Null Pointer Exception on fragments.OnRefresh()
         * Because Selected Fragment Reference after Rotate Changes
         * Hence on Async Complete old Selected Fragment Get refreshed
         */
        if(getChildFragmentManager().getFragments()!=null)
            for(int i = 0 ; i<getChildFragmentManager().getFragments().size() ; i++)
                if(getChildFragmentManager().getFragments().get(i)!=null)
                    ((RefreshableFragment)getChildFragmentManager().getFragments().get(i)).onRefreshFragment(newBillId);

        return true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        rootView = inflater.inflate(R.layout.fragment_home, container, false);

        mBillingID = getArguments().getString(EXTRA_BILL_ID);
        ((MainActivity) getActivity()).mSwipeRefresh.setEnabled(true);

        mTutorialOverlay = rootView.findViewById(R.id.tutorial_container);

        setupData();
        setupPager(rootView);
        setupTabs(mPager);

        mPager.setCurrentItem(2);
        return rootView;
    }

    public void setupData(){
        //Get Total Payable Amount
        mBranchInfo = new BranchInfoRecord();
        ArrayList<BranchInfoRecord> mBranchList = (ArrayList<BranchInfoRecord>) BranchInfoRecord.find(BranchInfoRecord.class, "Bill_id = ?", mBillingID);
        if(mBranchList!=null)
            if(mBranchList.size()>0)
                mBranchInfo = mBranchList.get(0);
    }

    private void setupPager(View rootView){
        //Setup ViewPager
        mPager = (ViewPager)rootView.findViewById(R.id.pager);
        InfoPagerAdapter mPagerAdapter = new InfoPagerAdapter(getChildFragmentManager(), getActivity());
        mPager.setAdapter(mPagerAdapter);
        mPager.setPageTransformer(true, new DepthPageTransformer());
        mPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Fragment currentFragment = getChildFragmentManager().findFragmentByTag("android:switcher:" + R.id.pager + ":" + position);
                Fragment previousFragment = getChildFragmentManager().findFragmentByTag("android:switcher:" + R.id.pager + ":" + previousPosition);


                if(currentFragment != null && currentFragment instanceof HeaderFragment)
                    ((HeaderFragment) currentFragment).moveFragmentHeaderToActivity();
                else if(previousFragment != null && previousFragment instanceof HeaderFragment)
                    ((HeaderFragment) previousFragment).clearFragmentHeader();

                previousPosition = position;
            }
        });
    }

    private void setupTabs(ViewPager pager){
        TabLayout mTabs = (TabLayout)rootView.findViewById(R.id.tab_layout);
        mTabs.setupWithViewPager(pager);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //Route Result Back to Children
        switch(requestCode){
            case ChooserType.REQUEST_PICK_PICTURE:
                getChildFragmentManager().getFragments().get(0).onActivityResult(requestCode, resultCode, data);
                break;

            case MainActivity.REQUEST_PAY_BILLS_TOP:
                Snackbar.make(rootView.findViewById(R.id.pager), "پرداخت با موفقیت انجام گردید", Snackbar.LENGTH_LONG )
                        .setAction("مشاهده", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                mPager.setCurrentItem(0);
                            }
                        }).show();
                break;

            default:
                break;
        }
    }

    public class InfoPagerAdapter extends FragmentPagerAdapter {
        public int tabTitles[] = new int[] {
                BillingHistoryFragment.mTitle,
                UsageHistoryFragment.mTitle,
                InfoFragment.mTitle
        };

        public int tabIcons[] = new int[] {
                android.R.drawable.ic_menu_agenda,
                android.R.drawable.ic_menu_sort_by_size,
                android.R.drawable.ic_menu_info_details
        };

        final private int PAGE_COUNT = 3;
        private Context mContext;

        public InfoPagerAdapter(FragmentManager fm, Context context){
            super(fm);
            mContext = context;
        }

        @Override
        public int getCount(){
            return PAGE_COUNT;
        }

        @Override
        public Fragment getItem(int position){
            switch (position){
                case 2:
                    return InfoFragment.newInstance(mBillingID);

                case 1:
                    return UsageHistoryFragment.newInstance(mBillingID);

                case 0:
                    return BillingHistoryFragment.newInstance(mBillingID);

                default:
                    return InfoFragment.newInstance(mBillingID);
            }

        }

        @Override
        public CharSequence getPageTitle(int position){
            return getActivity().getResources().getString(tabTitles[position]);
        }
    }

    public static Fragment newInstance (String bill_id){
        Bundle args = new Bundle();

        args.putString(EXTRA_BILL_ID, bill_id);
        Fragment fragment = new HomeFragment();
        fragment.setArguments(args);

        return fragment;
    }
}
