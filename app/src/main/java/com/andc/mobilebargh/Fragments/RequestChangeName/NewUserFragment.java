package com.andc.mobilebargh.Fragments.RequestChangeName;


import android.app.AlertDialog;
import android.content.Context;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.andc.mobilebargh.Models.NewUserInfo;
import com.andc.mobilebargh.R;
import com.andc.mobilebargh.Utility.Tools;

import ir.smartlab.persindatepicker.PersianDatePicker;
import ir.smartlab.persindatepicker.util.PersianCalendar;

import static com.andc.mobilebargh.Utility.General.FontSize;


public class NewUserFragment extends Fragment {

    EditText legalMobile, legalPhone, legalNumberSave, legalCompany, legalNewsPaper,
            legalEconomyCode, legalPost, legalAddress, legalMoasse, legalActivtyCode,
            legalPostWriterCode, legalWriterAddress, legalEmail, legalPrePhone, legalNewsPaperDate;
    EditText realNationalCode, realLastName, realFirstName, realBirth, realIdentityNo, realPlaceIssuance,
            realMobileNo, realPhoneNo, realPostCode, realBranchingAddress, realWritterPost,
            realWritterAddress, realMail, realFatherName, realPrePhone;
    RadioButton realRdbMen, realRdbWomen;
    RadioButton rdbReal, rdbLogal;
    public Button next, back;
    RadioGroup radioGroup;
    RadioButton rdbMen, rdbWomen;
    int sex_real, i;
    LinearLayout legal, real;


    private IRequestChangeListener mListener;


    public static NewUserFragment newInstance() {
        NewUserFragment fragment = new NewUserFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (IRequestChangeListener)getActivity();

    }



    public void onDetach() {
        mListener = null;
        super.onDetach();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_new_user, container, false);
        init(view);
        checkExistsNewUserData();
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rdbReal.isChecked()) {
                    if (Tools.checkIsEmptyEdittext(realNationalCode)) {
                        realNationalCode.requestFocus();
                        realNationalCode.setError(getResources().getString(R.string.fill_edittext));
                    } else if (realNationalCode.getText().toString().length() != 10) {
                        realNationalCode.requestFocus();
                        realNationalCode.setError(getResources().getString(R.string.check_national_number));
                    } else if (Tools.checkIsEmptyEdittext(realIdentityNo)) {
                        realIdentityNo.requestFocus();
                        realIdentityNo.setError(getResources().getString(R.string.fill_edittext));
                    } else if (Tools.checkIsEmptyEdittext(realFirstName)) {
                        realFirstName.requestFocus();
                        realFirstName.setError(getResources().getString(R.string.fill_edittext));
                    } else if (Tools.stringContainsNumber(realFirstName.getText().toString())) {
                        realFirstName.requestFocus();
                        realFirstName.setError(getResources().getString(R.string.contain_number_edittext));
                    } else if (Tools.checkIsEmptyEdittext(realLastName)) {
                        realLastName.requestFocus();
                        realLastName.setError(getResources().getString(R.string.fill_edittext));
                    } else if (Tools.stringContainsNumber(realLastName.getText().toString())) {
                        realLastName.requestFocus();
                        realLastName.setError(getResources().getString(R.string.contain_number_edittext));
                    } else if (Tools.checkIsEmptyEdittext(realBirth)) {
                        realBirth.requestFocus();
                        realBirth.setError(getResources().getString(R.string.fill_edittext));
                    } else if (Tools.checkIsEmptyEdittext(realFatherName)) {
                        realFatherName.requestFocus();
                        realFatherName.setError(getResources().getString(R.string.fill_edittext));
                    } else if (Tools.stringContainsNumber(realFatherName.getText().toString())) {
                        realFatherName.requestFocus();
                        realFatherName.setError(getResources().getString(R.string.contain_number_edittext));
                    } else if (Tools.checkIsEmptyEdittext(realMobileNo)) {
                        realMobileNo.requestFocus();
                        realMobileNo.setError(getResources().getString(R.string.fill_edittext));
                    } else if (realMobileNo.getText().toString().length() != 9) {
                        realMobileNo.requestFocus();
                        realMobileNo.setError(getResources().getString(R.string.check_mobile_number));
                    } else if (Tools.checkIsEmptyEdittext(realPhoneNo)) {
                        realPhoneNo.requestFocus();
                        realPhoneNo.setError(getResources().getString(R.string.fill_edittext));
                    } else if (realPhoneNo.getText().toString().length() < 6) {
                        realPhoneNo.requestFocus();
                        realPhoneNo.setError(getResources().getString(R.string.phone_less_six));
                    } else if (Tools.checkIsEmptyEdittext(realBranchingAddress)) {
                        realBranchingAddress.requestFocus();
                        realBranchingAddress.setError(getResources().getString(R.string.fill_edittext));
                    } else if (Tools.checkIsEmptyEdittext(realPostCode)) {
                        realPostCode.requestFocus();
                        realPostCode.setError(getResources().getString(R.string.fill_edittext));
                    } else if (realPostCode.getText().toString().length() != 10) {
                        realPostCode.requestFocus();
                        realPostCode.setError(getResources().getString(R.string.check_post_number));
                    } else if (!Tools.checkIsEmptyEdittext(realWritterPost) && (realWritterPost.getText().toString().length() != 10)) {
                        realWritterPost.requestFocus();
                        realWritterPost.setError(getResources().getString(R.string.check_post_number));

                    } else {
                        mListener.setNewUserInfo(prepareData());
                        mListener.ListenerButtonNextClicked();
                    }
                } else if (rdbLogal.isChecked()) {
                    if (Tools.checkIsEmptyEdittext(legalCompany)) {
                        legalCompany.requestFocus();
                        legalCompany.setError(getResources().getString(R.string.fill_edittext));
                    } else if (Tools.checkIsEmptyEdittext(legalNumberSave)) {
                        legalNumberSave.requestFocus();
                        legalNumberSave.setError(getResources().getString(R.string.fill_edittext));
                    } else if (Tools.checkIsEmptyEdittext(legalPhone)) {
                        legalPhone.requestFocus();
                        legalPhone.setError(getResources().getString(R.string.fill_edittext));
                    } else if (legalPhone.getText().toString().length() < 6) {
                        legalPhone.requestFocus();
                        legalPhone.setError(getResources().getString(R.string.phone_less_six));
                    } else if (Tools.checkIsEmptyEdittext(legalMobile)) {
                        legalMobile.requestFocus();
                        legalMobile.setError(getResources().getString(R.string.fill_edittext));
                    } else if (legalMobile.getText().toString().length() != 9) {
                        legalMobile.requestFocus();
                        legalMobile.setError(getResources().getString(R.string.check_mobile_number));
                    } else if (Tools.checkIsEmptyEdittext(legalAddress)) {
                        legalAddress.requestFocus();
                        legalAddress.setError(getResources().getString(R.string.fill_edittext));
                    } else if (Tools.checkIsEmptyEdittext(legalPost)) {
                        legalPost.requestFocus();
                        legalPost.setError(getResources().getString(R.string.fill_edittext));
                    } else if (legalPost.getText().toString().length() != 10) {
                        legalPost.requestFocus();
                        legalPost.setError(getResources().getString(R.string.check_post_number));
                    } else if (!Tools.checkIsEmptyEdittext(legalPostWriterCode) && (legalPostWriterCode.getText().toString().length() != 10)) {

                        legalPostWriterCode.requestFocus();
                        legalPostWriterCode.setError(getResources().getString(R.string.check_post_number));

                    } else {
                        mListener.setNewUserInfo(prepareData());
                        mListener.ListenerButtonNextClicked();
                    }
                } else {
                    Toast.makeText(getContext(), "Radio Legal and Real Not Checked", Toast.LENGTH_SHORT).show();
                }
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.ListenerButtonBackClicked();
            }
        });


        realBirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePicker(realBirth);
            }
        });

        legalNewsPaperDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePicker(legalNewsPaperDate);
            }
        });

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (radioGroup.getCheckedRadioButtonId()) {
                    case R.id.radio_real:
                        real.setVisibility(View.VISIBLE);
                        legal.setVisibility(View.INVISIBLE);
                        break;
                    case R.id.radio_legal:
                        real.setVisibility(View.INVISIBLE);
                        legal.setVisibility(View.VISIBLE);
                        break;
                }
            }
        });

        final int[] m = new int[10];

        realNationalCode.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            /*    if (start >= 9) {
                    for (int i = 0; i <= 9; i++) {
                        int y = 10 - i;
                        m[i] = Integer.parseInt(String.valueOf(s.charAt(i))) * y;
                        // Toast.makeText(view.getContext(), "number : "+s.charAt(i)+" i :"+i,Toast.LENGTH_SHORT).show();
                    }
                    int x = 0;
                    for (int i = 0; i < 9; i++) {
                        x = x + m[i];
                    }
                    x = x % 11;
                    x = 11 - x;
                    if (x == m[9]) {
                        Snackbar.make(view, "کد ملی صحیح میباشد" + m[9], Snackbar.LENGTH_LONG).show();
                    } else {
                        Snackbar.make(view, "کد ملی صحیح نمی باشد" + m[9], Snackbar.LENGTH_LONG).show();
                    }
                }*/
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        return view;
    }


    private NewUserInfo prepareData() {
        NewUserInfo newUserInfo = null;
        if (rdbLogal.isChecked()) {

            newUserInfo = new NewUserInfo(legalMobile.getText().toString(),
                    legalPhone.getText().toString() == "" ? "" : legalPhone.getText().toString(),
                    legalNumberSave.getText().toString() == "" ? "" : legalNumberSave.getText().toString(),
                    legalCompany.getText().toString() == "" ? "" : legalCompany.getText().toString(),
                    legalNewsPaper.getText().toString() == "" ? "" : legalNewsPaper.getText().toString(),
                    legalEconomyCode.getText().toString() == "" ? "" : legalEconomyCode.getText().toString(),
                    legalPost.getText().toString() == "" ? "" : legalPost.getText().toString(),
                    legalAddress.getText().toString() == "" ? "" : legalAddress.getText().toString(),
                    legalMoasse.getText().toString() == "" ? "" : legalMoasse.getText().toString(),
                    legalActivtyCode.getText().toString() == "" ? "" : legalActivtyCode.getText().toString(),
                    legalPostWriterCode.getText().toString() == "" ? "" : legalPostWriterCode.getText().toString(),
                    legalWriterAddress.getText().toString() == "" ? "" : legalWriterAddress.getText().toString(),
                    legalEmail.getText().toString() == "" ? "" : legalEmail.getText().toString(),
                    legalNewsPaperDate.getText().toString());
        } else if (rdbReal.isChecked()) {
            if (rdbMen.isChecked())
                sex_real = 1;
            else
                sex_real = 2;
            newUserInfo = new NewUserInfo(realNationalCode.getText().toString() == "" ? "" : realNationalCode.getText().toString(),
                    realFirstName.getText().toString() == null ? "" : realFirstName.getText().toString(),
                    realLastName.getText().toString(),
                    realIdentityNo.getText().toString(),
                    realPlaceIssuance.getText().toString(),
                    sex_real,
                    realPhoneNo.getText().toString(),
                    realMobileNo.getText().toString(),
                    realPostCode.getText().toString(),
                    realBranchingAddress.getText().toString(),
                    realWritterPost.getText().toString(),
                    realWritterAddress.getText().toString(),
                    realMail.getText().toString(),
                    realBirth.getText().toString(),
                    realFatherName.getText().toString(),
                    realPrePhone.getText().toString());
        }
        return newUserInfo;
    }

    private void checkExistsNewUserData() {
        if (mListener.getNewUserInfo() != null) {
            if (mListener.getNewUserInfo().typeUser == 1) {
                radioGroup.check(R.id.radio_real);
                real.setVisibility(View.VISIBLE);
                legal.setVisibility(View.INVISIBLE);
                realNationalCode.setText(mListener.getNewUserInfo().realNationalCode);
                realFirstName.setText(mListener.getNewUserInfo().realFirstName);
                realLastName.setText(mListener.getNewUserInfo().realLastName);
                realIdentityNo.setText(mListener.getNewUserInfo().realIdentityNo);
                realPlaceIssuance.setText(mListener.getNewUserInfo().realPlaceIssuance);
                realPhoneNo.setText(mListener.getNewUserInfo().realPhoneNumber);
                realMobileNo.setText(mListener.getNewUserInfo().realMobileNo);
                if (sex_real == 1) {
                    rdbMen.setChecked(true);
                } else if (sex_real == 2) {
                    rdbMen.setChecked(true);
                }
                realPostCode.setText(mListener.getNewUserInfo().realPostCode);
                realBranchingAddress.setText(mListener.getNewUserInfo().realBranchingAddress);
                realWritterPost.setText(mListener.getNewUserInfo().realWritterPost);
                realWritterAddress.setText(mListener.getNewUserInfo().realWritterAddress);
                realMail.setText(mListener.getNewUserInfo().realMail);
                realBirth.setText(mListener.getNewUserInfo().realBirthDate);
                realPrePhone.setText(mListener.getNewUserInfo().realPrePhone);
                realFatherName.setText(mListener.getNewUserInfo().realFatehrName);
            } else if (mListener.getNewUserInfo().typeUser == 2) {
                radioGroup.check(R.id.radio_legal);
                //      rdbReal.setChecked(false);
                real.setVisibility(View.INVISIBLE);
                legal.setVisibility(View.VISIBLE);

                legalMobile.setText(mListener.getNewUserInfo().legalMobile);
                legalPhone.setText(mListener.getNewUserInfo().legalPhone);
                legalNumberSave.setText(mListener.getNewUserInfo().legalNumberSave);
                legalCompany.setText(mListener.getNewUserInfo().legalCompany);
                legalNewsPaper.setText(mListener.getNewUserInfo().legalNewsPaper);
                legalEconomyCode.setText(mListener.getNewUserInfo().legalEconomyCode);
                legalPost.setText(mListener.getNewUserInfo().legalPost);
                legalAddress.setText(mListener.getNewUserInfo().legalAddress);
                legalMoasse.setText(mListener.getNewUserInfo().legalMoasse);
                legalActivtyCode.setText(mListener.getNewUserInfo().legalActivtyCode);
                legalPostWriterCode.setText(mListener.getNewUserInfo().legalPostWriterCode);
                legalWriterAddress.setText(mListener.getNewUserInfo().legalWriterAddress);
                legalEmail.setText(mListener.getNewUserInfo().legalEmail);
                legalNewsPaperDate.setText(mListener.getNewUserInfo().legalNewsPaperDate);
            } else {
                real.setVisibility(View.VISIBLE);
                legal.setVisibility(View.INVISIBLE);
                radioGroup.check(R.id.radio_real);
            }

        } else {
            real.setVisibility(View.VISIBLE);
            legal.setVisibility(View.INVISIBLE);
            radioGroup.check(R.id.radio_real);
        }
    }

    private void init(View view) {

        FontSize(getActivity().getApplicationContext(), view, "IRANSans(FaNum)_Medium.ttf");

        next = (Button) view.findViewById(R.id.new_next_btn);
        back = (Button) view.findViewById(R.id.new_back_btn);
        radioGroup = (RadioGroup) view.findViewById(R.id.type_persone_rdg);
        real = (LinearLayout) view.findViewById(R.id.real);
        legal = (LinearLayout) view.findViewById(R.id.legal);
        rdbReal = (RadioButton) view.findViewById(R.id.radio_real);
        rdbLogal = (RadioButton) view.findViewById(R.id.radio_legal);

        //LEGAL
        legalMobile = (EditText) view.findViewById(R.id.mobile_legal);
        legalPhone = (EditText) view.findViewById(R.id.phone_legal);
        legalNumberSave = (EditText) view.findViewById(R.id.number_save_legal);
        legalCompany = (EditText) view.findViewById(R.id.company_legal);
        legalNewsPaper = (EditText) view.findViewById(R.id.news_paper);
        legalEconomyCode = (EditText) view.findViewById(R.id.economy_code_legal);
        legalPost = (EditText) view.findViewById(R.id.post_legal);
        legalAddress = (EditText) view.findViewById(R.id.addres_legal);
        legalMoasse = (EditText) view.findViewById(R.id.mos_legal);
        legalActivtyCode = (EditText) view.findViewById(R.id.activty_code_legal);
        legalPostWriterCode = (EditText) view.findViewById(R.id.writer_post_legal);
        legalWriterAddress = (EditText) view.findViewById(R.id.address_writer_legal);
        legalEmail = (EditText) view.findViewById(R.id.email_legal);
        legalNewsPaperDate = (EditText) view.findViewById(R.id.news_paper_date);

        //REAL
        realNationalCode = (EditText) view.findViewById(R.id.national_code_real);
        realFirstName = (EditText) view.findViewById(R.id.firstname_real);
        realLastName = (EditText) view.findViewById(R.id.lastname_real);
        realIdentityNo = (EditText) view.findViewById(R.id.identity_no_real);
        realNationalCode = (EditText) view.findViewById(R.id.national_code_real);
        realFirstName = (EditText) view.findViewById(R.id.firstname_real);
        realLastName = (EditText) view.findViewById(R.id.lastname_real);
        realIdentityNo = (EditText) view.findViewById(R.id.identity_no_real);
        realBirth = (EditText) view.findViewById(R.id.et_birth);
        realPlaceIssuance = (EditText) view.findViewById(R.id.place_issuance_real);
        realPhoneNo = (EditText) view.findViewById(R.id.phone_real);
        realMobileNo = (EditText) view.findViewById(R.id.mobile_real);
        realRdbMen = (RadioButton) view.findViewById(R.id.rdb_men_real);
        realRdbWomen = (RadioButton) view.findViewById(R.id.rdb_women_real);
        realPostCode = (EditText) view.findViewById(R.id.post_code_real);
        realBranchingAddress = (EditText) view.findViewById(R.id.branching_address_real);
        realWritterPost = (EditText) view.findViewById(R.id.writter_post_real);
        realWritterAddress = (EditText) view.findViewById(R.id.writter_address_real);
        realPrePhone = (EditText) view.findViewById(R.id.phone_real_p);
        realMail = (EditText) view.findViewById(R.id.mail_real);
        realFatherName = (EditText) view.findViewById(R.id.father_name);
        realMail = (EditText) view.findViewById(R.id.mail_real);

        rdbMen = (RadioButton) view.findViewById(R.id.rdb_men_real);
        rdbWomen = (RadioButton) view.findViewById(R.id.rdb_women_real);
        if (rdbMen.isSelected())
            sex_real = 1;
        else if (rdbWomen.isSelected())
            sex_real = 2;
        else
            sex_real = 0;
    }

    private void datePicker(final EditText editText) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        final View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_persian_date, null, false);
        final PersianDatePicker persianDatePicker = (PersianDatePicker) view.findViewById(R.id.persianDate);
        builder.setIcon(android.R.drawable.ic_dialog_info)
                .setView(view)
                //.setTitle(v.getContext().getResources().getText(R.string.dialog_select_date));
                .setPositiveButton(getContext().getResources().getText(R.string.dialog_button_confirm), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        final PersianCalendar persianCalendar = persianDatePicker.getDisplayPersianDate();
                        String day;
                        String month;
                        String year = String.valueOf(persianCalendar.getPersianYear());

                        if (persianCalendar.getPersianDay() <= 9)
                            day = "0" + String.valueOf(persianCalendar.getPersianDay());
                        else
                            day = String.valueOf(persianCalendar.getPersianDay());

                        if (persianCalendar.getPersianMonth() <= 9)
                            month = "0" + String.valueOf(persianCalendar.getPersianMonth());
                        else
                            month = String.valueOf(persianCalendar.getPersianMonth());

                        editText.setText(year + "/" + month + "/" + day);
                    }
                })
                .setNegativeButton(getContext().getResources().getText(R.string.dialog_button_return), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        //DO TASK
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

}
