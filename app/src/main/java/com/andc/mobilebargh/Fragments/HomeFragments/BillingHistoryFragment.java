package com.andc.mobilebargh.Fragments.HomeFragments;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.andc.mobilebargh.Adapter.BillingHistoryAdapter;
import com.andc.mobilebargh.Models.BillingHistoryRecord;
import com.andc.mobilebargh.R;
import com.andc.mobilebargh.Utility.Components.HeaderFragment;

import java.util.ArrayList;

/**
 * Created by win on 11/17/2015.
 */
public class BillingHistoryFragment extends HeaderFragment {
    //Fragment Tags
    public final static int mTitle = R.string.tabs_billing_history;
    public final static String TAG_BILL_ID = "bill_id";
    public final static String SORT_DESC_VALUE_KEY = "sort_desc";
    public final static String SORT_BASE_VALUE_KEY = "sort_base";

    //Fragment Variables
    boolean sortIsDesc;
    String sortBasedOn;

    //Global Views
    private String mBillId;
    private ArrayList<BillingHistoryRecord> mBillingRecords;
    private ListView mListView;
    private LinearLayout mListHeader;
    private BillingHistoryAdapter mListAdapter;

    //Sort Constants
    private final static String BANK_NAME = "bankcode";
    private final static String PAY_DATE = "F_pay_date";
    private final static String RECEIVED_DATE = "Frcpt_date";
    private final static String PAY_AMOUNT = "rcptamt";

    @Override
    protected View getFragmentHeader() {
        return mListHeader;
    }

    @Override
    public boolean onRefreshFragment(String newBillId) {
        mBillId = newBillId;
        fetchSortedBillingRecords(mBillId, sortIsDesc, sortBasedOn);
        return true;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        if(savedInstanceState!=null){
            sortIsDesc = savedInstanceState.getBoolean(SORT_DESC_VALUE_KEY);
            sortBasedOn = savedInstanceState.getString(SORT_BASE_VALUE_KEY);
        }else {
            //Set Default Search Value
            sortIsDesc = true;
            sortBasedOn = PAY_DATE;
        }

        //Get Sorted Billing History Records from DB Based on Bill Id
        mBillId = getArguments().getString(TAG_BILL_ID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View rootView = inflater.inflate(R.layout.fragment_billing_history, container, false);

        setupList(rootView);
        return rootView;
    }

    private void setupList(View rootView){
        //Setup List Header
        mListHeader = (LinearLayout)rootView.findViewById(R.id.billing_list_header);
        setupListHeader(mListHeader);

        //Setup List
        mBillingRecords = fetchSortedBillingRecords(mBillId, sortIsDesc, sortBasedOn);
        mListAdapter = new BillingHistoryAdapter(getActivity(), mBillingRecords);

        mListView = (ListView)rootView.findViewById(R.id.list);
        mListView.setAdapter(mListAdapter);
    }

    public void setupListHeader(LinearLayout mListHeader){
        //Setup List Header
        TextView mBankName = (TextView)mListHeader.findViewById(R.id.billing_bank_name);
        mBankName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sortBasedOn == BANK_NAME) {
                    sortIsDesc = !sortIsDesc;
                } else {
                    sortBasedOn = BANK_NAME;
                }

                fetchSortedBillingRecords(mBillId, sortIsDesc, sortBasedOn);
            }
        });

        TextView mPayDate = (TextView)mListHeader.findViewById(R.id.billing_pay_date);
        mPayDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sortBasedOn == PAY_DATE){
                    sortIsDesc = !sortIsDesc;
                } else {
                    sortBasedOn = PAY_DATE;
                }

                fetchSortedBillingRecords(mBillId, sortIsDesc, sortBasedOn);
            }
        });

        TextView mReceiveDate = (TextView)mListHeader.findViewById(R.id.billing_receive_date);
        mReceiveDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sortBasedOn == RECEIVED_DATE){
                    sortIsDesc = !sortIsDesc;
                } else {
                    sortBasedOn = RECEIVED_DATE;
                }

                fetchSortedBillingRecords(mBillId, sortIsDesc, sortBasedOn);
            }
        });

        TextView mPayAmount = (TextView)mListHeader.findViewById(R.id.billing_pay_amount);
        mPayAmount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sortBasedOn == PAY_AMOUNT) {
                    sortIsDesc = !sortIsDesc;
                } else {
                    sortBasedOn = PAY_AMOUNT;
                }

                fetchSortedBillingRecords(mBillId, sortIsDesc, sortBasedOn);
            }
        });

        setSearchIndicator(mListHeader, sortIsDesc, sortBasedOn);
    }

    private void setSearchIndicator(LinearLayout listHeader, boolean sortIsDesc, String sortBasedOn){
        Drawable emptyDrawable = getResources().getDrawable(R.drawable.empty_drawable);
        Drawable disableDrawable = getResources().getDrawable(R.drawable.ic_new_chevron_disable_white_24dp);
        Drawable downDrawable = getResources().getDrawable(R.drawable.ic_new_chevron_down_white_24dp);
        Drawable upDrawable = getResources().getDrawable(R.drawable.ic_new_chevron_up_white_24dp);
        Drawable sortIndicator = sortIsDesc?downDrawable:upDrawable;


        //Clear Other TextView
        for (int i = 0 ; i < listHeader.getChildCount() ; i++){
            ((TextView)listHeader.getChildAt(i)).setCompoundDrawablesWithIntrinsicBounds(disableDrawable, null, null, null);
        }

        switch(sortBasedOn){
            case "saleyear":
                ((TextView)listHeader.getChildAt(7)).setCompoundDrawablesWithIntrinsicBounds(sortIndicator, null, null, null);
                break;

            case "saleprd":
                ((TextView)listHeader.getChildAt(6)).setCompoundDrawablesWithIntrinsicBounds(sortIndicator, null, null, null);
                break;

            case "F_prev_rdg_date":
                ((TextView)listHeader.getChildAt(5)).setCompoundDrawablesWithIntrinsicBounds(sortIndicator, null, null, null);
                break;

            case "F_curr_rdg_date":
                ((TextView)listHeader.getChildAt(4)).setCompoundDrawablesWithIntrinsicBounds(sortIndicator, null, null, null);
                break;

            case "bankcode":
                ((TextView)listHeader.getChildAt(3)).setCompoundDrawablesWithIntrinsicBounds(sortIndicator, null, null, null);
                break;

            case "F_pay_date":
                ((TextView)listHeader.getChildAt(2)).setCompoundDrawablesWithIntrinsicBounds(sortIndicator, null, null, null);
                break;

            case "Frcpt_date":
                ((TextView)listHeader.getChildAt(1)).setCompoundDrawablesWithIntrinsicBounds(sortIndicator, null, null, null);
                break;

            case "rcptamt":
                ((TextView)listHeader.getChildAt(0)).setCompoundDrawablesWithIntrinsicBounds(sortIndicator, null, null, null);
                break;

            default:
                ((TextView)listHeader.getChildAt(2)).setCompoundDrawablesWithIntrinsicBounds(sortIndicator, null, null, null);
                break;
        }
    }

    public ArrayList<BillingHistoryRecord> fetchSortedBillingRecords(String billId, boolean sortIsDesc, String sortBasedOn){
        //Fetch Data From DB in Sorted Order
        ArrayList<BillingHistoryRecord> mSortedBillingRecords;

        if(sortIsDesc)
            mSortedBillingRecords = (ArrayList<BillingHistoryRecord>) BillingHistoryRecord.find(
                    BillingHistoryRecord.class, BillingHistoryRecord.TAG_billid + " = ?  ORDER BY " + sortBasedOn + " DESC", billId);
        else
            mSortedBillingRecords = (ArrayList<BillingHistoryRecord>) BillingHistoryRecord.find(
                    BillingHistoryRecord.class, BillingHistoryRecord.TAG_billid + " = ?  ORDER BY "+ sortBasedOn, billId);

        //Update Adapter With New Data
        if(mListAdapter!=null)
            mListAdapter.setItems(mSortedBillingRecords);

        //Set Search Indicator on List Header
        if(mListHeader!=null)
            setSearchIndicator(mListHeader, sortIsDesc, sortBasedOn);

        return mSortedBillingRecords;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(SORT_DESC_VALUE_KEY, sortIsDesc);
        outState.putString(SORT_BASE_VALUE_KEY, sortBasedOn);
    }

    public static Fragment newInstance (String bill_id){
        Bundle args = new Bundle();

        args.putString(TAG_BILL_ID, bill_id);
        Fragment fragment = new BillingHistoryFragment();
        fragment.setArguments(args);

        return fragment;
    }
}
