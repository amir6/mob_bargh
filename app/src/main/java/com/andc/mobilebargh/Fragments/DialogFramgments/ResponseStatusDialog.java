package com.andc.mobilebargh.Fragments.DialogFramgments;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.andc.mobilebargh.R;

public class ResponseStatusDialog {

    private LinearLayout viewWebServiceState;
    private TextView txWebServiceState;
    private ImageView imgWebServiceState;
    private Context mContext;
    private Animation animationFade;
    private AlertDialog mDialog;

    public ResponseStatusDialog(Context context){
        this.mContext =context;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_response_status, null, false);
        builder.setView(view);
        mDialog = builder.create();
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        viewWebServiceState = view.findViewById(R.id.layout_state_webservice);
        txWebServiceState = view.findViewById(R.id.txt_state_web);
        imgWebServiceState = view.findViewById(R.id.img_state_web);
        animationFade = AnimationUtils.loadAnimation(context,R.anim.fade);
        animationFade.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                    mDialog.show();
            }

            @Override
            public void onAnimationEnd(Animation animation) {
               mDialog.dismiss();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    public  void showSuccess() {

        txWebServiceState.setText(R.string.success_webservice);
        txWebServiceState.setTextColor(mContext.getResources().getColor(R.color.green_4));
        imgWebServiceState.setImageResource(R.drawable.success_round);
        //SET ANIMATION
        viewWebServiceState.startAnimation(animationFade);
    }

    public void showFail() {
        mDialog.show();
        txWebServiceState.setText(R.string.failed_webservice);
        txWebServiceState.setTextColor(mContext.getResources().getColor(R.color.red_2));
        imgWebServiceState.setImageResource(R.drawable.fail_error_problem);
        //SET ANIMATION
        viewWebServiceState.startAnimation(animationFade);
    }



}
