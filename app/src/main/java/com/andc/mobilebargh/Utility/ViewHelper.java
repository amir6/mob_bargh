package com.andc.mobilebargh.Utility;

import android.graphics.PorterDuff;
import android.os.Build;
import android.view.View;
import com.andc.mobilebargh.MobileBargh;

/**
 * Created by Esbati on 5/28/2016.
 */
public class ViewHelper {

    public static void colorView(View view, int color, int fallBackColor){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            view.getBackground().setColorFilter(color, PorterDuff.Mode.MULTIPLY);
        else
            view.setBackgroundColor(MobileBargh.getContext().getResources().getColor(fallBackColor));
    }
}
