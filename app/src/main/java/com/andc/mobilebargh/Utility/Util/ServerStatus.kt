package com.andc.mobilebargh.Utility.Util

enum class ServerStatus {

    ERROR,
    SENT,
    RECEIVE,
    LOADING
}