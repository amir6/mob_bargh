package com.andc.mobilebargh.Utility.Listeners;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.andc.mobilebargh.R;

import ir.smartlab.persindatepicker.PersianDatePicker;
import ir.smartlab.persindatepicker.util.PersianCalendar;


/**
 * Created by Tenkei on 3/14/2016.
 */
@TargetApi(17)
public class DatePicker implements View.OnClickListener{
    TextView mDateView;

    public DatePicker(View dateView){
        mDateView = (TextView)dateView;
    }

    @Override
    public void onClick(View v) {
        pickDate(v);
    }

    public void pickDate(View v){
        final View persiandate = LayoutInflater.from(v.getContext()).inflate(R.layout.dialog_fragment_persian_date, null, false);

        AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
        builder.setIcon(android.R.drawable.ic_dialog_info);
        builder.setView(persiandate);
        builder.setTitle(v.getContext().getResources().getText(R.string.dialog_select_date));
        builder.setPositiveButton(v.getContext().getResources().getText(R.string.dialog_button_confirm), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                PersianDatePicker persianDatePicker = (PersianDatePicker) persiandate.findViewById(R.id.persianDate);
                PersianCalendar persianCalendar = persianDatePicker.getDisplayPersianDate();
                String day;
                String month;
                String year = String.valueOf(persianCalendar.getPersianYear());

                if(persianCalendar.getPersianDay()<=9)
                    day = "0" + String.valueOf(persianCalendar.getPersianDay());
                else
                    day = String.valueOf(persianCalendar.getPersianDay());

                if(persianCalendar.getPersianMonth()<=9)
                    month = "0" + String.valueOf(persianCalendar.getPersianMonth());
                else
                    month = String.valueOf(persianCalendar.getPersianMonth());

                mDateView.setText(year + "/" + month + "/" + day);
            }
        });
        builder.setNegativeButton(v.getContext().getResources().getText(R.string.dialog_button_return), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                //DO TASK
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();

        //Set Title Gravity
        final int alertTitle = v.getContext().getResources().getIdentifier("alertTitle", "id", "android");
        TextView messageText = (TextView) dialog.findViewById(alertTitle);
        messageText.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
    }
}
