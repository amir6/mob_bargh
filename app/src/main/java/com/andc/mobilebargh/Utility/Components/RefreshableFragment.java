package com.andc.mobilebargh.Utility.Components;

import android.support.v4.app.Fragment;

/**
 * Created by Esbati on 12/27/2015.
 */
public abstract class RefreshableFragment extends Fragment {
    public abstract boolean onRefreshFragment(String newBillId);
}
