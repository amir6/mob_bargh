package com.andc.mobilebargh.Utility.Util

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import android.util.Base64
import android.util.Base64OutputStream
import android.widget.ImageView
import com.andc.mobilebargh.Utility.Util.Constants.ANDC_FOLDER_NAME
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*




class DocumentsFile {


    companion object  {

        internal lateinit var filePath: File
        @JvmStatic
        fun saveImage(context: Context, imageName: String, uri: Uri, image_view: ImageView): File? {
            var file: File? = null
            try {
                val bitmap = MediaStore.Images.Media.getBitmap(context!!.getContentResolver(), uri)
                val dir = File(Environment.getExternalStorageDirectory().toString() + "/" + ANDC_FOLDER_NAME)
                //Bitmap out = Bitmap.createScaledBitmap(bitmap, 1166, 2048, true);

                file = File(dir, "$imageName.jpg")
                val fOut: FileOutputStream
                try {
                    fOut = FileOutputStream(file)
                    var btm = getResizedBitmap(bitmap , 300)
                    btm.compress(Bitmap.CompressFormat.JPEG, 100, fOut)
                    fOut.flush()
                    fOut.close()
                    btm.recycle()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                val bitFile = BitmapFactory.decodeFile(file.toString())
                image_view.setImageBitmap(bitFile)




            } catch (e: IOException) {
                e.printStackTrace()

            }

            return file
        }

        fun getResizedBitmap(image: Bitmap, maxSize: Int): Bitmap {
            var width = image.width
            var height = image.height

            val bitmapRatio = width.toFloat() / height.toFloat()
            if (bitmapRatio > 1) {
                width = maxSize
                height = (width / bitmapRatio).toInt()
            } else {
                height = maxSize
                width = (height * bitmapRatio).toInt()
            }
            return Bitmap.createScaledBitmap(image, width, height, true)
        }
    }




}