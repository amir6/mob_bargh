package com.andc.mobilebargh.Utility;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

/**
 * Created by hassan kazemy on 3/13/2018.
 */

public class General {


//**************************************************************************************************

    static public void FontSize(final Context context, final View v,String font) {
        try {
            final Typeface custom_font = Typeface.createFromAsset(context.getAssets(), "fonts/"+font);
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    FontSize(context, child,font);
                }
            } else if (v instanceof TextView || v instanceof Spinner) {
                ((TextView) v).setTypeface(custom_font);
                //((TextView) v).setTextAppearance(context, R.style.fontForNotificationLandingPage);
            }else if(v instanceof AutoCompleteTextView){
                ((AutoCompleteTextView)v).setTypeface(custom_font);
            }
        } catch (Exception e) {
        }
    }



}
