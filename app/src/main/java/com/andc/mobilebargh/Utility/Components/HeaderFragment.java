package com.andc.mobilebargh.Utility.Components;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

/**
 * Created by Esbati on 12/27/2015.
 */
public abstract class HeaderFragment extends RefreshableFragment {

    protected ActionBarHandlerInterface actionBarHandlerInterface;
    protected abstract View getFragmentHeader();


    public boolean moveFragmentHeaderToActivity(){
        if(actionBarHandlerInterface!=null && getFragmentHeader() != null)
            return actionBarHandlerInterface.moveHeaderToActivity(getFragmentHeader());
        else
            return false;
    }

    public void clearFragmentHeader(){
        if(actionBarHandlerInterface!=null)
            actionBarHandlerInterface.clearActionBar();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getParentFragment() != null){
            if (getParentFragment() instanceof ActionBarHandlerInterface)
                actionBarHandlerInterface = (ActionBarHandlerInterface) getParentFragment();
            else if (getActivity() instanceof ActionBarHandlerInterface)
                actionBarHandlerInterface = (ActionBarHandlerInterface) getActivity();
            else
                throw new ClassCastException("Hosting activity/fragment must implement ActionBarHandlerInterface");
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //if(actionBarHandlerInterface!=null)
            //actionBarHandlerInterface.moveHeaderToActivity(getFragmentHeader());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if(actionBarHandlerInterface!=null)
            actionBarHandlerInterface.clearActionBar();
    }

    public interface ActionBarHandlerInterface {
        boolean moveHeaderToActivity(View fragmentHeader);
        void clearActionBar();
    }
}
