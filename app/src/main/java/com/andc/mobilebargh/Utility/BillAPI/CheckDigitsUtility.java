package com.andc.mobilebargh.Utility.BillAPI;

import java.math.RoundingMode;
import java.text.DecimalFormat;

/**
 * Created by y.jafari on 9/22/2015.
 */
public class CheckDigitsUtility {

    public static boolean isDigitValid(String billId)
    {
        if(billId.length()!=13)
            return false;

        String baseDigit = billId.substring(0,billId.length()-1);
        int chkSum = GetCheckDigit(baseDigit);
        if(billId.endsWith(String.valueOf(chkSum)))
            return true;
        else
            return false;
    }

    public static int GetCheckDigit(String baseDigit)
    {
        byte[] dv = { 2, 3, 4, 5, 6, 7 };
        char[] chdigits = baseDigit.toCharArray();
        int sumWithCoef = 0;
        int k = chdigits.length - 1;
        for (int i = 0; i < chdigits.length; i++)
        {
            sumWithCoef += Integer.parseInt(String.valueOf(chdigits[k])) * dv[(i % dv.length)];
            k--;
        }
        int rem = sumWithCoef % 11;
        int chkDgt = 0;
        if (rem > 1)
            chkDgt = 11 - rem;
        return chkDgt;
    }

    public static String AppendCheckDigit(long  baseDigit)
    {
        int chckDigit = GetCheckDigit(String.valueOf(baseDigit));
        return (String.valueOf(baseDigit) + String.valueOf(chckDigit));

    }

    public static String padRight(String s, int n) {
        return String.format("%1$-" + n + "s", s);
    }

    public static String padLeft(String s, int n) {
        Long number = Long.parseLong(s.trim());
        return String.format("%0" + String.valueOf(n) + "d", number);
    }

    public static String roundFloating(double number, int position)
    {
        String format = "#.";
        for (int i=0; i<position; i++)
            format += "#";
        DecimalFormat df = new DecimalFormat(format);
        df.setRoundingMode(RoundingMode.HALF_UP);
        return df.format(number);
    }

    public static String trimLeadingZeros(String strInput){
        if (strInput == null)
            return "-1";
        if (strInput.isEmpty())
            return "-1";
        String rv = strInput;
        try {
            rv = rv.replaceAll("^0+(?!$)", "");
        }catch (Exception e){
            e.printStackTrace();
        }
        return rv;
    }
}
