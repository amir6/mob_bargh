package com.andc.mobilebargh.Utility;

/**
 * Created by win on 6/1/2015.
 */
public class ConvertorNumberToPersian {

    /**
     *
     * @param input
     * this method is writted to convert english format of date to persian format to show to user
     * input characters replace with persian numbers then return
     *
     * @return input
     *
     */
    public static String toPersianNumber(String input){
        String[] persian = { "۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹" };

        if(input!=null)
            if(input.length()>0)
                for (int j=0; j<persian.length; j++)
                    input = input.replace(String.valueOf(j), persian[j]);

        return input;
    }

    public static String englishToPersianNumber(String input){
        if(input==null)
            return input;

        char[] arabicChars = {'٠','١','٢','٣','٤','٥','٦','٧','٨','٩'};
        StringBuilder builder = new StringBuilder();

        for(int i =0;i<input.length();i++)
        {
            if(Character.isDigit(input.charAt(i)))
            {
                builder.append(arabicChars[(int)(input.charAt(i))-48]);
            } else {
                builder.append(input.charAt(i));
            }
        }

        return  builder.toString();
    }

}
