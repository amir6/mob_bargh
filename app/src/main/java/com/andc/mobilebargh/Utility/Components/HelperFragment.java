package com.andc.mobilebargh.Utility.Components;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.andc.mobilebargh.Controllers.TutorialHelper;
import com.andc.mobilebargh.R;

/**
 * Created by Esbati on 4/27/2016.
 */
public abstract class HelperFragment extends Fragment {

    protected abstract String getTutorialTag();
    protected abstract void onTutorialShown();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(!TextUtils.isEmpty(getTutorialTag()) && TutorialHelper.shouldTutorialShown(getTutorialTag())){
            onTutorialShown();
            TutorialHelper.tutorialCompleted(getTutorialTag());
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        //Add Help Option if There is Tutorial Available
        if(!TextUtils.isEmpty(getTutorialTag()))
            menu.add(0, R.id.help, Menu.CATEGORY_SYSTEM, R.string.help)
            .setIcon(R.drawable.ic_help_circle_white_24dp)
            .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.help:
                onTutorialShown();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
