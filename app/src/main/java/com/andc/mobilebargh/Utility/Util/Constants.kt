package com.andc.mobilebargh.Utility.Util

import okhttp3.MediaType

object Constants {

    const val DISCONNECT_BRANCH_TEMPORARY_TAG = "9"
    const val RECONNECT_BRANCH_TAG = "16"
    const val COLLECT__BRANCH_TEMPORARY_TAG = "6"
    const val REINSTALL_BARNCH_TAG = "10"
    const val COLLECT_BRANCH_PERMANENT_TAG = "7"
    const val INTERNAL_MOVE_METER_TAG = "21"
    const val METER_TEST_TAG = "8"
    const val CORRECT_BARNCH_SERVICE_TAG = "20"

    const val EXAMINE_INVOICE_TAG = "17"
    const val SETTLEMENT_INVOICE_TAG = "18"
    const val INVOICE_ISSUE_TAG = "13"
    const val CALCULATE_INVOICE_TAG = "12"

    const val TARIFF_CHANGE_TAG = "15"

    const val TARIFF_HOME_CODE = "10"
    const val TARIFF_HOME_SPECIAL_CODE = "100"
    const val TARIFF_GENERAL_SPECIAL_CODE = "200"
    const val TARIFF_GENERAL_CODE = "20"
    const val TARIFF_AGRICULTURE_CODE = "30"
    const val TARIFF_INDUSTRIAL_CODE = "40"
    const val TARIFF_BUSINESS_CODE = "50"


    const val NATIONAL_CARD_FRONT = "1"
    const val NATIONAL_CARD_BACK = "2"
    const val INVOICE_BILL_COPY = "3"
    const val COMMIT_DOC = "4"
    const val OTHER_FIRST_DOC = "5"
    const val OTHER_SECOND_DOC = "6"
    const val OTHER_THIRD_DOC = "7"
    const val OTHER_DOC = "8"
    const val ANDC_FOLDER_NAME = "Andc"
    const val ANDC_BILL_FOLDER = "Bill"

    const val REQUEST_CODE_WRITE_EXTERNAL_STORAGE = 1001
    const val REQUEST_CODE_CAMERA = 1002


    const val SALE_SERVICE_TYPE_CODE = "ORDER_TYPE"
    const val READ_DATE = "READ_DATE"
    const val SALE_SERVICE_BILL_ID = "BILL_IDENTIFIER"
    const val IMAGE_ITEM_1 = "item1"
    const val IMAGE_ITEM_2 = "item2"
    const val IMAGE_ITEM_3 = "item3"
    const val IMAGE_ITEM_4 = "item4"
    val MEDIA_TYPE_JPEG = MediaType.parse("image/jpeg")
    const val USAGE_ACTIVE_AVERAGE = "ACTIVE_NORMAL_READ"
    const val USAGE_ACTIVE_PEAK = "ACTIVE_PEAK_READ"
    const val USAGE_ACTIVE_LOW = "ACTIVE_LOW_READ"
    const val USAGE_ACTIVE_WEEKEND = "ACTIVE_WEEKEND_READ"
    const val USAGE_REACTIVE_NORMAL = "REACTIVE_NORMAL_READ"
    const val USAGE_DEMAND = "READ_DEMAND"
    const val TARIFF_CODE ="TARIFF_CODE"

    const val INTENT_DATA_POSITION = "position"






}