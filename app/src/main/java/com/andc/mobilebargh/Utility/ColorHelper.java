package com.andc.mobilebargh.Utility;

import android.content.res.TypedArray;
import android.util.TypedValue;


import com.andc.mobilebargh.MobileBargh;
import com.andc.mobilebargh.R;

/**
 * Created by Tenkei on 6/6/2016.
 */
public class ColorHelper {
    public static int getAccentColor() {
        TypedValue typedValue = new TypedValue();

        TypedArray a = MobileBargh.getContext().obtainStyledAttributes(typedValue.data, new int[]{R.attr.colorAccent});
        int color = a.getColor(0, 0);

        a.recycle();

        return color;
    }
}
