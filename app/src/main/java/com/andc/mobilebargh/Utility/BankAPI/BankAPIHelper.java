package com.andc.mobilebargh.Utility.BankAPI;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;

/**
 * Created by Esbati on 1/16/2016.
 */
public class BankAPIHelper {

    public static final String URL_TOP = "https://tavanir.pec.ir/adpmobile/downloads/apk/topapp.apk";

    public static boolean isTopInstalled(Context context) {
        PackageManager pm = context.getPackageManager();
        boolean app_installed;
        try {
            pm.getPackageInfo("ir.tgbs.peccharge", PackageManager.GET_ACTIVITIES);
            app_installed = true;
        }
        catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }

    public static void downloadTop(Context context) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(URL_TOP));
        context.startActivity(browserIntent);
    }
}
