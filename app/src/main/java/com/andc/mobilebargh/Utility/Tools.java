package com.andc.mobilebargh.Utility;

import android.widget.EditText;
import android.widget.TextView;

import com.andc.mobilebargh.Models.ErrorMessage;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Ak on 4/7/2018.
 */

public class Tools {

    public static boolean checkEditTextNotEmpty(EditText editText) {
        if (editText.getText().toString().equals(null) ||
                editText.getText().toString().trim().equals("") ||
                editText.getText().toString().trim().length() == 0)
            return false;
        else
            return true;
    }

    public static void showError(ArrayList<ErrorMessage> arrError) {
        for (int i = 0; i < arrError.size(); i++) {
            ErrorMessage errorMessage = arrError.get(i);
            errorMessage.getEtError().setError(errorMessage.getErrorMessage());
        }
    }

    public static boolean checkIsEmptyEdittext(EditText editText){
        if (editText.getText().toString().trim().equals("") || editText.getText().toString().trim().isEmpty()
                || editText.getText().toString().trim().length() == 0)
            return true;
        else
            return false ;
    }

    public static boolean checkIsEmptyTextview(TextView textView){
        if (textView.getText().toString().trim().equals("") || textView.getText().toString().trim().isEmpty()
                || textView.getText().toString().trim().length() == 0)
            return true;
        else
            return false ;
    }

    public static boolean stringContainsNumber( String s )
    {
        Pattern p = Pattern.compile( "[0-9]" );
        Matcher m = p.matcher( s );

        return m.find();
    }

}
