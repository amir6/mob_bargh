package com.andc.mobilebargh.Utility.BillAPI;

import static com.andc.mobilebargh.Utility.BillAPI.CheckDigitsUtility.*;

/**
 * Created by y.jafari on 9/22/2015.
 */
public class Shenase {

    public static int getAmount(String paymentId){
        int amount = Integer.valueOf(paymentId);
        amount = amount / 100000;
        amount *= 1000;

        return  amount;
    }

    public  String GetShenasePardakht(String shenaseGhabz, double amt, int saleYear, int salePrd)
    {
        String shePardakht =  GenerateShenasePardakht(shenaseGhabz, String.valueOf(amt / 1000),String.valueOf(saleYear), String.valueOf(salePrd));
        long chkDgt = GetCheckDigit(String.format("{%d}{%d}", Long.getLong(shenaseGhabz), Long.getLong(shePardakht)));
        return String.format("{%s}{%d}",shePardakht,chkDgt);
    }

    public static String GenerateShenasePardakht(String BillID, String amount, String year, String prd)
    {
        if (BillID.trim().length() != 13) try {
            //throw new CustomException(new Exception("BillID Lenght is not valid"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        long t = Long.parseLong(amount);
        t = t - (t % 1000);
        long tmpAmount = t / 1000;


        String payID = GenerateShenasePardakht(tmpAmount, Long.parseLong(year.substring(year.length() - 1, 2) + padLeft(prd, 2).trim()));
        int chkDigit = GetCheckDigit(BillID + trimLeadingZeros(payID));
        payID = payID + String.valueOf(chkDigit);

        return payID;
    }

    public static String GenerateShenasePardakht(long cost, long saleCode)
    {
        String strRes = String.format("%s%s", String.valueOf(cost), padLeft(String.valueOf(saleCode), 3));
        return padLeft(AppendCheckDigit(Long.parseLong(strRes)), 12);
    }

    public static String GenerateShenaseGhabz(String pCode, int coCode, int serviceCode)
    {
        String bCode  = padLeft(String.valueOf(pCode),8).substring(0, 8);
        String strRes = String.format("%s%s%s", padLeft(String.valueOf(bCode), 8), padLeft(String.valueOf(coCode), 3), padLeft(String.valueOf(serviceCode), 1));
        return AppendCheckDigit(Long.parseLong(strRes));
    }
}
