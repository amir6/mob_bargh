package com.andc.mobilebargh.Utility;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.view.View;
import android.widget.TextView;

import com.andc.mobilebargh.MobileBargh;


/**
 * Created by Esbati on 2/7/2016.
 */
public class CustomDialog {

    @TargetApi(17)
    public static void show(AlertDialog dialog){
        dialog.show();

        //Set Title Gravity
        final int alertTitle = MobileBargh.getContext().getResources().getIdentifier("alertTitle", "id", "android");
        TextView messageText = (TextView) dialog.findViewById(alertTitle);

        messageText.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);

        /*
        AlertDialog dialog = (AlertDialog) getDialog();
        try {
            Field mAlert = AlertDialog.class.getDeclaredField("mAlert");
            mAlert.setAccessible(true);
            Object alertController = mAlert.get(dialog);

            Field mTitleView = alertController.getClass().getDeclaredField("mTitleView");
            mTitleView.setAccessible(true);

            TextView title = (TextView) mTitleView.get(alertController);
            title.setTextColor(0xff33b5e5);

        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        */
    }
}
