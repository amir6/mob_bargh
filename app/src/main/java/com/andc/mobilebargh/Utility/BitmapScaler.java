package com.andc.mobilebargh.Utility;

import android.graphics.Bitmap;
import android.widget.ImageView;

/**
 * Created by Esbati on 12/30/2015.
 */
public class BitmapScaler {
    // Scale and maintain aspect ratio given a desired width
    // BitmapScaler.scaleToFitWidth(bitmap, 100);
    public static Bitmap scaleToFitWidth(Bitmap b, int width)
    {
        float factor = width / (float) b.getWidth();
        return Bitmap.createScaledBitmap(b, width, (int) (b.getHeight() * factor), true);
    }


    // Scale and maintain aspect ratio given a desired height
    // BitmapScaler.scaleToFitHeight(bitmap, 100);
    public static Bitmap scaleToFitHeight(Bitmap b, int height)
    {
        float factor = height / (float) b.getHeight();
        return Bitmap.createScaledBitmap(b, (int) (b.getWidth() * factor), height, true);
    }

    public static Bitmap scaleWithImageViewHighQuality(Bitmap bitmap, ImageView view)
    {
        //Don't Resize if the Bitmap Is already Smaller then View
        if(view.getWidth()>bitmap.getWidth() || view.getHeight()>bitmap.getHeight())
            //We Stretch Bitmap to Fit View
            return bitmap;

        float viewRatio = (float)view.getWidth()/(float)view.getHeight();
        float bitmapRatio = (float)bitmap.getWidth()/(float)bitmap.getHeight();

        //if View Width Ratio is Larger Then Bitmap
        if(viewRatio>bitmapRatio)
            //Scale Bitmap with View Width
            return scaleToFitWidth(bitmap, view.getWidth());
        else
            //Else Scale Bitmap with View Height
            return scaleToFitHeight(bitmap, view.getHeight());
    }
}
