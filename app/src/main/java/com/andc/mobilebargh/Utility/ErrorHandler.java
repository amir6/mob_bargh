package com.andc.mobilebargh.Utility;

import android.support.design.widget.Snackbar;
import android.view.View;


import com.andc.mobilebargh.MobileBargh;
import com.andc.mobilebargh.Models.CustomError;
import com.andc.mobilebargh.R;

/**
 * Created by Esbati on 2/7/2016.
 */
public class ErrorHandler {

    public enum ErrorState{
        ConnectingToServer(R.string.error_async_connecting_to_server),
        ConnectionFailed(R.string.error_async_connection_failed),
        NoResponse(R.string.error_async_no_response),
        UpdateProgressing(R.string.error_async_progressing),
        NoDataReceived(R.string.error_async_no_data_received),
        DataUnreadable(R.string.error_async_data_unreadable),
        UpdatingDatabase(R.string.error_async_updating_database),
        UpdateSuccessful(R.string.error_async_update_successful),
        UpdateFailed(R.string.error_async_update_failed);

        private int mErrorMsgResId;

        ErrorState(int errorMsgResId){
            mErrorMsgResId = errorMsgResId;
        }

        public String getMessage(){
            return MobileBargh.getContext().getResources().getString(mErrorMsgResId);
        }
    }


    //Show Default Errors
    public static void showError(View parentView, ErrorState error){
        switch (error){

            default:
                Snackbar.make(parentView, error.getMessage(), Snackbar.LENGTH_LONG).show();
        }
    }

    //Show Custom Errors from Server
    public static void showError(View parentView, CustomError error){
        Snackbar.make(parentView, error.mMsg, Snackbar.LENGTH_LONG).show();
    }

    //Show Error By Message
    public static void showError(View parentView, String errorMsg){
        Snackbar.make(parentView, errorMsg, Snackbar.LENGTH_LONG).show();
    }
}
