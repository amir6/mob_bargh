package com.andc.mobilebargh.Utility.Util

import com.andc.mobilebargh.Models.*
import com.andc.mobilebargh.repository.model.BranchData
import com.andc.mobilebargh.repository.model.DataObject
import com.andc.mobilebargh.repository.model.ErrorData
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type
import com.google.gson.Gson
import org.json.JSONArray
import org.json.JSONObject


class CurrentBranchDeserilize : JsonDeserializer<BranchData> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): BranchData {


        var dataJSON: DataObject = Gson().fromJson(json, DataObject::class.java)
        var data: String = dataJSON.data
        var error: Array<ErrorData> = dataJSON.error
        var errorCode: Int = error.get(0).ErrorCode
        if (errorCode == 200)
            return Gson().fromJson(data, BranchData::class.java)
        else return BranchData()
        /*    else
            return Gson().fromJson(errorCode, BranchData::class.java)


    }*/
    }
}