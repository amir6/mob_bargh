package com.andc.mobilebargh.Utility;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Point;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;


import com.andc.mobilebargh.MobileBargh;

import java.util.Hashtable;

public class AndroidUtilities {
    public static int statusBarHeight = 0;
    public static float density = 1;
    public static DisplayMetrics displayMetrics = new DisplayMetrics();
    public static Point displaySize = new Point();
    public static boolean isTablet;
    public static int leftBaseline;
    public static boolean usingHardwareInput;
    private static final Hashtable<String, Typeface> typefaceCache = new Hashtable<>();

    static {
        statusBarHeight = getStatusBarHeight();
        density = MobileBargh.getContext().getResources().getDisplayMetrics().density;
        //leftBaseline = isTablet() ? 80 : 72;
        leftBaseline = 80;
        checkDisplaySize();
    }

    public static int dp(float value) {
        if (value == 0) {
            return 0;
        }
        return (int) Math.ceil(density * value);
    }

    public static float dpf2(float value) {
        if (value == 0) {
            return 0;
        }
        return density * value;
    }

    public static Typeface getTypeface(String assetPath) {
        synchronized (typefaceCache) {
            if (!typefaceCache.containsKey(assetPath)) {
                try {
                    Typeface t = Typeface.createFromAsset(MobileBargh.getContext().getAssets(), assetPath);
                    typefaceCache.put(assetPath, t);
                } catch (Exception e) {
                    Log.e("Typefaces", "Could not get typeface '" + assetPath + "' because " + e.getMessage());
                    return null;
                }
            }

            return typefaceCache.get(assetPath);
        }
    }

    public static void checkDisplaySize() {
        try {
            Configuration configuration = MobileBargh.getContext().getResources().getConfiguration();
            usingHardwareInput = configuration.keyboard != Configuration.KEYBOARD_NOKEYS && configuration.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_NO;
            WindowManager manager = (WindowManager) MobileBargh.getContext().getSystemService(Context.WINDOW_SERVICE);
            if (manager != null) {
                Display display = manager.getDefaultDisplay();
                if (display != null) {
                    display.getMetrics(displayMetrics);
                    display.getSize(displaySize);
                    Log.d("tmessages", "display size = " + displaySize.x + " " + displaySize.y + " " + displayMetrics.xdpi + "x" + displayMetrics.ydpi);
                }
            }
        } catch (Exception e) {
            Log.e("tmessages", e.getMessage(), e);
        }
    }

    public static int getStatusBarHeight() {
        if(statusBarHeight <= 0) {
            int resourceId = MobileBargh.getContext().getResources().getIdentifier("status_bar_height", "dimen", "android");
            if (resourceId > 0) {
                statusBarHeight = MobileBargh.getContext().getResources().getDimensionPixelSize(resourceId);
            }
        }

        return statusBarHeight;
    }

    public static boolean isTablet() {
        return isTablet;
    }

    public static boolean isSmallTablet() {
        float minSide = Math.min(displaySize.x, displaySize.y) / density;
        return minSide <= 700;
    }

    public static void runOnUIThread(Runnable runnable) {
        runOnUIThread(runnable, 0);
    }

    public static void runOnUIThread(Runnable runnable, long delay) {
        if (delay == 0) {
            MobileBargh.getHandler().post(runnable);
        } else {
            MobileBargh.getHandler().postDelayed(runnable, delay);
        }
    }
}
