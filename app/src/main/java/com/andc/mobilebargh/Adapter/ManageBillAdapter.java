package com.andc.mobilebargh.Adapter;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.andc.mobilebargh.Controllers.BillCollection;
import com.andc.mobilebargh.Controllers.PreferencesHelper;
import com.andc.mobilebargh.Fragments.ManageBillsFragment;
import com.andc.mobilebargh.Models.BillRecord;
import com.andc.mobilebargh.R;
import com.kbeanie.imagechooser.api.ChooserType;
import com.kbeanie.imagechooser.api.ChosenImage;
import com.kbeanie.imagechooser.api.ChosenImages;
import com.kbeanie.imagechooser.api.ImageChooserListener;
import com.kbeanie.imagechooser.api.ImageChooserManager;
import com.kbeanie.imagechooser.exceptions.ChooserException;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Esbati on 12/15/2015.
 */
public class ManageBillAdapter extends RecyclerView.Adapter<ManageBillAdapter.ViewHolder> implements
        ImageChooserListener {

    public final static int PICK_PHOTO_CODE = 1046;

    private Context mContext;
    private Fragment mFragment;
    private LayoutInflater mLayoutInflater;
    private boolean mIsEdited;
    private String mFocusedBillId;
    private ImageChooserManager imageChooserManager;

    public ManageBillAdapter(Fragment fragment) {
        mFragment = fragment;
        mContext = fragment.getContext();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        //Vote Fields
        private CircleImageView mBillImage;
        private EditText mBillTitle;
        private EditText mBillId;
        private ImageView mRemoveBill;

        public ViewHolder(View v){
            super(v);
            mBillImage = (CircleImageView)v.findViewById(R.id.bill_image);
            mBillTitle = (EditText)v.findViewById(R.id.bill_title);
            mBillId = (EditText)v.findViewById(R.id.bill_id);
            mRemoveBill = (ImageView)v.findViewById(R.id.remove_bill);
        }
    }

    @Override
    public int getItemCount() {
        return BillCollection.get(mContext).getBills().size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public ManageBillAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(mLayoutInflater==null)
            mLayoutInflater = LayoutInflater.from(parent.getContext());

        //Inflate View and Create ViewHolder
        View v = mLayoutInflater.inflate(R.layout.list_item_manage_bill, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ManageBillAdapter.ViewHolder holder, final int position) {
        final BillRecord bill = BillCollection.get(mContext).getBills().get(position);

        if(!TextUtils.isEmpty(bill.mImageUri))
            holder.mBillImage.setImageURI(Uri.parse(bill.mImageUri));
        else
            holder.mBillImage.setImageResource(R.drawable.test_circle_image);

        holder.mBillImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO If Frame is Empty Open Gallery to Pick Photo, Else Show the Photo
                mFocusedBillId = bill.mBillId;
                imageChooserManager = new ImageChooserManager(mFragment, ChooserType.REQUEST_PICK_PICTURE);
                imageChooserManager.setImageChooserListener(ManageBillAdapter.this);
                try{
                    imageChooserManager.choose();
                } catch (ChooserException e){
                    e.printStackTrace();
                }
            }
        });


        holder.mBillId.setText(bill.mBillId);
        holder.mBillTitle.setText(bill.mBillTitle);
        holder.mBillTitle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                BillCollection.get(mContext).getBills().get(position).mBillTitle = charSequence.toString();
                BillCollection.get(mContext).getBills().get(position).save();

                if(mIsEdited == false){
                    mIsEdited = true;
                    PreferencesHelper.setOption(PreferencesHelper.KEY_SYNC_NECESSARY, true);
                    mFragment.getActivity().setResult(Activity.RESULT_OK, new Intent().putExtra(ManageBillsFragment.EXTRA_IS_EDITED, true));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        holder.mRemoveBill.setOnClickListener(new View.OnClickListener() {
            @TargetApi(17)
            @Override
            public void onClick(View view) {
                //Remove Confirmation Dialog
                AlertDialog dialog = new AlertDialog.Builder(mContext)
                        //.setView(mDialogView)
                        .setTitle(mContext.getString(R.string.dialog_remove_bill_title))
                        .setMessage(mContext.getString(R.string.dialog_remove_bill_body))
                        .setNegativeButton(mContext.getString(R.string.dialog_button_return), null)
                        .setPositiveButton(mContext.getString(R.string.dialog_button_confirm), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface arg0, int arg1) {
                                //Remove Selected Item From Bills
                                BillCollection.get(mContext).remove(bill);

                                //Remove Selected Item From ListView
                                ManageBillAdapter.this.notifyItemRemoved(position);

                                //Notify Main Activity of Changes
                                if(mIsEdited == false){
                                    mIsEdited = true;
                                    mFragment.getActivity().setResult(Activity.RESULT_OK, new Intent().putExtra(ManageBillsFragment.EXTRA_IS_EDITED, true));
                                }
                            }
                        }).create();

                dialog.show();

                //Set Title Gravity
                final int alertTitle = mContext.getResources().getIdentifier("alertTitle", "id", "android");
                TextView messageText = (TextView)dialog.findViewById(alertTitle);

                messageText.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.mBillTitle.setTextColor(mContext.getResources().getColor(R.color.holo_red_dark));
                holder.mBillTitle.requestFocus();
                InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(holder.mBillTitle, InputMethodManager.SHOW_IMPLICIT);
            }
        });
    }

    @Override
    public void onImageChosen(ChosenImage chosenImage) {
        for(int i = 0 ; i < BillCollection.get(mContext).getBills().size() ; i++){
            BillRecord bill = BillCollection.get(mContext).getBills().get(i);
            if(bill.mBillId.equalsIgnoreCase(mFocusedBillId)){
                //Save Changes
                bill.mImageUri = chosenImage.getFileThumbnail();
                bill.save();

                //Notify Main Activity of Changes
                if(mIsEdited == false){
                    mIsEdited = true;
                    mFragment.getActivity().setResult(Activity.RESULT_OK, new Intent().putExtra(ManageBillsFragment.EXTRA_IS_EDITED, true));
                }

                //Refresh View
                final int position = i;
                mFragment.getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        notifyItemChanged(position);
                    }
                });

                return;
            }
        }
    }

    @Override
    public void onError(String s) {

    }

    @Override
    public void onImagesChosen(ChosenImages chosenImages) {

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode != Activity.RESULT_OK)
            return;

        switch (requestCode) {
            case ChooserType.REQUEST_PICK_PICTURE:
                imageChooserManager.submit(requestCode, data);
                break;
        }
    }
}
