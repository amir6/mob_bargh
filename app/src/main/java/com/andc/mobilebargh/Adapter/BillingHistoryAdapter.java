package com.andc.mobilebargh.Adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.andc.mobilebargh.Models.BillingHistoryRecord;
import com.andc.mobilebargh.Models.PaymentRecord;
import com.andc.mobilebargh.R;

import java.util.ArrayList;

/**
 * Created by win on 11/18/2015.
 */
public class BillingHistoryAdapter extends BaseAdapter {

    private static int defaultColor = 0;

    private Context mContext;
    private BillingHistoryRecord mBillingRecord;
    private ArrayList<BillingHistoryRecord> mBillingRecords;

    public BillingHistoryAdapter(Context context, ArrayList<BillingHistoryRecord> billingRecords){
        mContext = context;
        mBillingRecords = billingRecords;
    }

    @Override
    public int getCount() {
        return mBillingRecords.size();
    }

    public void setItems(ArrayList<BillingHistoryRecord> newBillingRecords){
        mBillingRecords = newBillingRecords;
        this.notifyDataSetChanged();
    }

    @Override
    public BillingHistoryRecord getItem(int position) {
        return mBillingRecords.get(position);
    }

    @Override
    public long getItemId(int position) {
        return Long.parseLong(mBillingRecords.get(position).billid);
    }

    public View getView(int position, View convertView, final ViewGroup parent) {
        if(convertView == null){
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.list_item_billing, parent, false);
        }

        mBillingRecord = mBillingRecords.get(position);
        TextView mFPayDate = (TextView)convertView.findViewById(R.id.FPayDate);
        TextView mFrcptDate = (TextView)convertView.findViewById(R.id.FrcptDate);
        TextView mrcptamt = (TextView)convertView.findViewById(R.id.rcptamt);
        TextView mbankcode = (TextView)convertView.findViewById(R.id.bankcode);

        mFPayDate.setText(mBillingRecord.FPayDate);

        if(mBillingRecord.FrcptDate.equals(PaymentRecord.EXTRA_RECEIPT_DATE))
            mFrcptDate.setText("در حال تایید");
        else
            mFrcptDate.setText(mBillingRecord.FrcptDate);

        mrcptamt.setText(String.valueOf(mBillingRecord.rcptamt));
        mbankcode.setText(BillingHistoryRecord.mBanks.get(mBillingRecord.bankcode));

        if(defaultColor==0)
            defaultColor = mrcptamt.getCurrentTextColor();

        int color;
        if(mBillingRecord.isTemporary)
            color = ContextCompat.getColor(mContext, R.color.holo_orange_dark);
        else
            color = defaultColor;


        mFPayDate.setTextColor(color);
        mFrcptDate.setTextColor(color);
        mrcptamt.setTextColor(color);
        mbankcode.setTextColor(color);
        return convertView;
    }
}
