package com.andc.mobilebargh.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.andc.mobilebargh.Models.UsageHistoryRecord;
import com.andc.mobilebargh.R;

import java.util.ArrayList;

/**
 * Created by win on 11/18/2015.
 */
public class UsageHistoryAdapter extends BaseAdapter {

    Context mContext;
    UsageHistoryRecord mUsageRecord;
    ArrayList<UsageHistoryRecord> mUsageRecords;

    public UsageHistoryAdapter(Context context){
        mContext = context;
    }

    public UsageHistoryAdapter(Context context, ArrayList<UsageHistoryRecord> usageRecords){
        mContext = context;
        mUsageRecords = usageRecords;
    }

    @Override
    public int getCount() {
        return mUsageRecords.size();
    }

    public void setItems(ArrayList<UsageHistoryRecord> newBillingRecords){
        mUsageRecords = newBillingRecords;
        this.notifyDataSetChanged();
    }

    @Override
    public UsageHistoryRecord getItem(int position) {
        return mUsageRecords.get(position);
    }

    @Override
    public long getItemId(int position) {
        return Long.parseLong(mUsageRecords.get(position).billid);
    }

    public View getView(int position, View convertView, final ViewGroup parent) {
        if(convertView == null){
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.list_item_usage, parent, false);
        }

        mUsageRecord = mUsageRecords.get(position);
        TextView msaleyear = (TextView)convertView.findViewById(R.id.saleyear);
        msaleyear.setText(String.valueOf(mUsageRecord.saleyear));
        TextView msaleprd = (TextView)convertView.findViewById(R.id.saleprd);
        msaleprd.setText(String.valueOf(mUsageRecord.saleprd));
        TextView mFPrevRdgDate = (TextView)convertView.findViewById(R.id.FPrevRdgDate);
        mFPrevRdgDate.setText(mUsageRecord.FPrevRdgDate);
        TextView mFCurrRdgDate = (TextView)convertView.findViewById(R.id.FCurrRdgDate);
        mFCurrRdgDate.setText(mUsageRecord.FCurrRdgDate);
        TextView museact = (TextView)convertView.findViewById(R.id.useact);
        museact.setText(String.valueOf(mUsageRecord.useact));
        TextView mprdamt = (TextView)convertView.findViewById(R.id.prdamt);
        mprdamt.setText(String.valueOf(mUsageRecord.prdamt));
        TextView mcrdbtot = (TextView)convertView.findViewById(R.id.crdbtot);
        mcrdbtot.setText(String.valueOf(mUsageRecord.crdbtot));
        TextView mbilamt = (TextView)convertView.findViewById(R.id.bilamt);
        mbilamt.setText(String.valueOf(mUsageRecord.bilamt));

        return convertView;
    }
}
