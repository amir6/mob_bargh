package com.andc.mobilebargh.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.andc.mobilebargh.R;


public class RequestItemsAdapter extends BaseAdapter {

  private Context mContext;
  private LayoutInflater mInflater;


    public RequestItemsAdapter(Context context){
        mContext = context;
        mInflater = LayoutInflater.from(context);
    }

    public int[] images = {
            R.drawable.after_sale,
            R.drawable.meter,
         // R.drawable.calculator,
            R.drawable.price_bill,
            R.drawable.change_name,
            R.drawable.change_tarrif,
            R.drawable.info,
            R.drawable.bill_correction,
            R.drawable.message

    };
    private String[] lables ={
      "خدمات پس از فروش" ,"خدمات فروش انشعاب", "صورتحساب برق","تغییرنام","تغییر تعرفه", "پیگیری تقاضا", "اصلاح اطلاعات", "صندوق پیام"

    };


    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public Object getItem(int position) {
        return images[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
       if(convertView == null) {
           convertView = mInflater.inflate(R.layout.fragement_request_main_items, parent, false);
       }
           TextView tv = convertView.findViewById(R.id.tv_request_items);

           ImageView imgbutton = convertView.findViewById(R.id.button_image_request_items);
           imgbutton.setScaleType(ImageView.ScaleType.FIT_CENTER);
           imgbutton.setPadding(8,8,8,8);


           tv.setText(lables[position]);
           imgbutton.setImageDrawable(mContext.getResources().getDrawable(images[position]));
           return convertView;
    }


}


