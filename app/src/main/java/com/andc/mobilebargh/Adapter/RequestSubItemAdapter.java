package com.andc.mobilebargh.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.andc.mobilebargh.R;


public class RequestSubItemAdapter extends BaseAdapter {


    private String[] mList;
    private LayoutInflater mInflater;


    public RequestSubItemAdapter(Context context, String[] list){
        mList = list;
        mInflater = LayoutInflater.from(context);


        }



    @Override
    public int getCount() {
        return mList.length;
    }

    @Override
    public Object getItem(int position) {
        return mList[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            convertView = mInflater.inflate(R.layout.fragment_request_sub_items, parent, false);
        }
            TextView tvItems = convertView.findViewById(R.id.tv_request_category_sale);
            tvItems.setText(mList[position]);
            return convertView;
    }
}
