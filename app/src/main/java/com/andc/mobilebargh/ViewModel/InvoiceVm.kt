package com.andc.mobilebargh.ViewModel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.support.annotation.MainThread
import com.andc.mobilebargh.Models.PersonalDocumentInfo
import com.andc.mobilebargh.Utility.Util.Constants
import com.andc.mobilebargh.Utility.Util.Constants.SALE_SERVICE_BILL_ID
import com.andc.mobilebargh.Utility.Util.ServerStatus
import com.andc.mobilebargh.repository.Remote.IService
import com.andc.mobilebargh.repository.model.*
import com.google.gson.Gson
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import okhttp3.MultipartBody
import okhttp3.RequestBody
import javax.inject.Inject


class InvoiceVm @Inject constructor(var service:IService , application: Application): AndroidViewModel(application) {

    private val mSubscription = CompositeDisposable()
    private var mCurrentBranch = MutableLiveData<BranchData>()
    private var mResponseData = MutableLiveData<ResponseData>()
    private var mBillResponse = MutableLiveData<BillResponse>()
    private var mStatus = MutableLiveData<ServerStatus>()
    private var mBillIssueStat = MutableLiveData<ServerStatus>()
    private var mBillId: String = "0"
    private var mDate: String = "0"
    private  lateinit var  mMeterData: MeterData

    override fun onCleared() {
        super.onCleared()
        mSubscription.clear()
    }
    val cheCkStatus: LiveData<ServerStatus>
        get() = mStatus
    val checkBillIssue: LiveData<ServerStatus>
        get() = mBillIssueStat


   val getResponseData: LiveData<ResponseData>
    get() = mResponseData

    val getBranchInfo : LiveData<BranchData>
      get() = mCurrentBranch

    val getBillResponse : LiveData<BillResponse>
    get() = mBillResponse

    val getBillId : String
     get() = mBillId

       fun setDate(date:String){
            mDate = date
    }

   public fun setMeterValue(data :MeterData){
       mMeterData = data
   }


    public fun getBillImage(){
            service.getBillImageService(createIssueBillBody()).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).
                    subscribe(object: Observer<String?> {
                        override fun onComplete() {

                        }

                        override fun onSubscribe(d: Disposable) {
                            mSubscription.add(d)
                        }

                        override fun onNext(t: String) {
                           // var data = ResponseData()
                            var s = t.subSequence(1 , t.length - 1).toString().replace("\\\\","").replace("\\", "")

                          var data = Gson().fromJson(s.toString() , BillResponse::class.java)
                            if(data.ErrorMsg.equals("MSG_000")){
                                mBillResponse.value = data
                                mBillIssueStat.value = ServerStatus.RECEIVE
                            }else{
                                mBillIssueStat.value = ServerStatus.ERROR
                            }

                        }

                        override fun onError(e: Throwable) {
                            mBillIssueStat.value = ServerStatus.ERROR
                        }
                    })

    }


    public fun sendSettlement(documnet: PersonalDocumentInfo){
        service.sendsettlementService(createSettlementBody(documnet))
                .observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(object: Observer<ResponseData?> {
                    override fun onComplete() {

                    }

                    override fun onSubscribe(d: Disposable) {
                        mSubscription.add(d)
                    }

                    override fun onNext(t: ResponseData) {
                        if(t.ErrorCode == "200"){
                            mResponseData.value = t
                            mStatus.value = ServerStatus.SENT

                        }else {
                            mStatus.value = ServerStatus.ERROR
                        }
                    }

                    override fun onError(e: Throwable) {
                        mStatus.value = ServerStatus.ERROR
                    }
                })

    }

    public fun createIssueBillBody():RequestBody{
        var builder = MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart(SALE_SERVICE_BILL_ID, mBillId)
        return builder.build()
    }

    public fun createSettlementBody(documnet: PersonalDocumentInfo):RequestBody{
        var builder = MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart(Constants.READ_DATE,mDate)
                .addFormDataPart(Constants.SALE_SERVICE_BILL_ID, mBillId)
                .addFormDataPart(Constants.USAGE_ACTIVE_AVERAGE,if(mMeterData.activeNormal.isEmpty()) "0" else mMeterData.activeNormal)
                .addFormDataPart(Constants.USAGE_ACTIVE_PEAK, if(mMeterData.activePeak.isEmpty()) "0" else mMeterData.activePeak)
                .addFormDataPart(Constants.USAGE_ACTIVE_LOW, if(mMeterData.activeLow.isEmpty()) "0" else mMeterData.activeLow)
                .addFormDataPart(Constants.USAGE_ACTIVE_WEEKEND, if(mMeterData.activeWeekend.isEmpty()) "0" else mMeterData.activeWeekend)
                .addFormDataPart(Constants.USAGE_DEMAND, if(mMeterData.demandVal.isEmpty()) "0" else mMeterData.demandVal)
                .addFormDataPart(Constants.USAGE_REACTIVE_NORMAL, if(mMeterData.reactiveNormal.isEmpty()) "0" else mMeterData.reactiveNormal)
                .addFormDataPart(Constants.IMAGE_ITEM_1, Constants.NATIONAL_CARD_FRONT, RequestBody.create(Constants.MEDIA_TYPE_JPEG, documnet.nationalCartFront))
                .addFormDataPart(Constants.IMAGE_ITEM_2, Constants.NATIONAL_CARD_BACK, RequestBody.create(Constants.MEDIA_TYPE_JPEG, documnet.nationalCartBehind))
                .addFormDataPart(Constants.IMAGE_ITEM_3, Constants.INVOICE_BILL_COPY, RequestBody.create(Constants.MEDIA_TYPE_JPEG, documnet.identityCart))
                .addFormDataPart(Constants.IMAGE_ITEM_4, Constants.COMMIT_DOC, RequestBody.create(Constants.MEDIA_TYPE_JPEG, documnet.commitment))

        return builder.build()


    }
    public fun createBody(documnet: PersonalDocumentInfo): RequestBody {

        var builder: MultipartBody.Builder  = MultipartBody . Builder ()
                .setType(MultipartBody.FORM)
                .addFormDataPart(Constants.READ_DATE, mDate)
                .addFormDataPart(Constants.SALE_SERVICE_BILL_ID, mBillId)
                .addFormDataPart(Constants.IMAGE_ITEM_1, Constants.NATIONAL_CARD_FRONT, RequestBody.create(Constants.MEDIA_TYPE_JPEG, documnet.nationalCartFront))
                .addFormDataPart(Constants.IMAGE_ITEM_2, Constants.NATIONAL_CARD_BACK, RequestBody.create(Constants.MEDIA_TYPE_JPEG, documnet.nationalCartBehind))
                .addFormDataPart(Constants.IMAGE_ITEM_3, Constants.INVOICE_BILL_COPY, RequestBody.create(Constants.MEDIA_TYPE_JPEG, documnet.identityCart))
                .addFormDataPart(Constants.IMAGE_ITEM_4, Constants.COMMIT_DOC, RequestBody.create(Constants.MEDIA_TYPE_JPEG, documnet.commitment))

        return  builder.build()
    }

    public fun sendExamineInvoice(documnet: PersonalDocumentInfo) {
        service.sendExamineInvoice(createBody(documnet))
                .observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(object : Observer<ResponseData?> {
                    override fun onComplete() {

                    }

                    override fun onSubscribe(d: Disposable) {
                        mSubscription.add(d)
                    }

                    override fun onNext(t: ResponseData) {

                       if(t.ErrorCode == "200"){
                           mResponseData.value = t
                           mStatus.value = ServerStatus.SENT
                       }else {
                           mStatus.value = ServerStatus.ERROR
                       }
                    }

                    override fun onError(e: Throwable) {
                        mStatus.value = ServerStatus.ERROR
                    }
                })
    }

     public fun getBranchDetail(billId: String){
      /*  mCode = code*/
        mBillId = billId
        service.getBranchDetail(billId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(object: Observer<DataObject?> {
                    override fun onComplete() {
                        /* mStatus.value = ServerStatus.RECEIVE*/
                    }

                    override fun onSubscribe(d: Disposable) {
                        mSubscription.add(d)
                    }

                    override fun onNext(t: DataObject) {
                        if(t.error.get(0).ErrorCode == 200) {
                            mCurrentBranch.value = Gson().fromJson(t.data, BranchData::class.java)
                            mStatus.value = ServerStatus.RECEIVE
                        }else {
                            mStatus.value = ServerStatus.ERROR
                        }

                    }

                    override fun onError(e: Throwable) {
                        mStatus.value = ServerStatus.ERROR

                    }
                })

    }




}