package com.andc.mobilebargh.ViewModel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.telephony.ServiceState
import com.andc.mobilebargh.Models.PersonalDocumentInfo
import com.andc.mobilebargh.Utility.Util.Constants.COMMIT_DOC
import com.andc.mobilebargh.Utility.Util.Constants.IMAGE_ITEM_1
import com.andc.mobilebargh.Utility.Util.Constants.IMAGE_ITEM_2
import com.andc.mobilebargh.Utility.Util.Constants.IMAGE_ITEM_3
import com.andc.mobilebargh.Utility.Util.Constants.IMAGE_ITEM_4
import com.andc.mobilebargh.Utility.Util.Constants.INVOICE_BILL_COPY
import com.andc.mobilebargh.Utility.Util.Constants.MEDIA_TYPE_JPEG
import com.andc.mobilebargh.Utility.Util.Constants.NATIONAL_CARD_BACK
import com.andc.mobilebargh.Utility.Util.Constants.NATIONAL_CARD_FRONT
import com.andc.mobilebargh.Utility.Util.Constants.SALE_SERVICE_BILL_ID
import com.andc.mobilebargh.Utility.Util.Constants.SALE_SERVICE_TYPE_CODE


import com.andc.mobilebargh.repository.Remote.IService
import com.andc.mobilebargh.Utility.Util.ServerStatus
import com.andc.mobilebargh.repository.model.BranchData
import com.andc.mobilebargh.repository.model.DataObject
import com.andc.mobilebargh.repository.model.ResponseData
import com.google.gson.Gson
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import okhttp3.FormBody
import okhttp3.MultipartBody
import okhttp3.RequestBody
import javax.inject.Inject

class ReceiveBranchVm @Inject constructor(var service: IService, application: Application):AndroidViewModel(application) {


    private val mSubscription = CompositeDisposable()
    private var mCurrentBranch = MutableLiveData<BranchData>()
    private var mResponseData = MutableLiveData<ResponseData>()
    private var mStatus = MutableLiveData<ServerStatus>()
    private var mSendStatus = MutableLiveData<ServerStatus>()
    private var mCode: String = "0"
    private var mBillId: String = "0"

    override fun onCleared() {
        super.onCleared()
        mSubscription.clear()
    }
    val getDetail : LiveData<BranchData>
        get() = mCurrentBranch

    val cheCkStatus: LiveData<ServerStatus>
        get() = mStatus

    val getResponseData: LiveData<ResponseData>
       get() = mResponseData

    val checkSendStatus: LiveData<ServerStatus>
       get() = mSendStatus

    var getRequestCode : String = ""
        get() = mCode
    var getBillId : String = ""
        get() = mBillId

   public fun getBranchDetail(billId: String, code:String){
       mCode = code
       mBillId = billId
        service.getBranchDetail(billId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(object: Observer<DataObject?> {
                    override fun onComplete() {
                     /* mStatus.value = ServerStatus.RECEIVE*/
                    }

                    override fun onSubscribe(d: Disposable) {
                       mSubscription.add(d)
                    }

                    override fun onNext(t: DataObject) {
                        if(t.error.get(0).ErrorCode == 200) {
                            mStatus.value = ServerStatus.RECEIVE
                            mCurrentBranch.value = Gson().fromJson(t.data, BranchData::class.java)
                        }else {
                            mStatus.value = ServerStatus.ERROR
                        }

                    }

                    override fun onError(e: Throwable) {
                       mStatus.value = ServerStatus.ERROR

                    }
                })

   }
    public fun createBody(documnet: PersonalDocumentInfo): RequestBody{

      var builder: MultipartBody.Builder  = MultipartBody . Builder ()
                .setType(MultipartBody.FORM)
                .addFormDataPart(SALE_SERVICE_TYPE_CODE, mCode)
                .addFormDataPart(SALE_SERVICE_BILL_ID, mBillId)
                .addFormDataPart(IMAGE_ITEM_1,NATIONAL_CARD_FRONT ,RequestBody.create(MEDIA_TYPE_JPEG , documnet.nationalCartFront))
                .addFormDataPart(IMAGE_ITEM_2, NATIONAL_CARD_BACK , RequestBody.create(MEDIA_TYPE_JPEG, documnet.nationalCartBehind))
                .addFormDataPart(IMAGE_ITEM_3,INVOICE_BILL_COPY , RequestBody.create(MEDIA_TYPE_JPEG, documnet.identityCart))
                .addFormDataPart(IMAGE_ITEM_4,COMMIT_DOC, RequestBody.create(MEDIA_TYPE_JPEG, documnet.commitment))

        return  builder.build()
    }

    public fun sendData(documnet: PersonalDocumentInfo){

        service.sendSalesService(createBody(documnet)).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(object: Observer<ResponseData?> {
                    override fun onComplete() {

                    }

                    override fun onSubscribe(d: Disposable) {
                       mSubscription.add(d)
                    }

                    override fun onNext(t: ResponseData) {
                        if(t.ErrorCode.equals("200")) {
                            mResponseData.value = t
                            mSendStatus.value = ServerStatus.SENT
                        }else {
                            mSendStatus.value = ServerStatus.ERROR
                        }
                    }

                    override fun onError(e: Throwable) {
                        mSendStatus.value = ServerStatus.ERROR

                    }
                })


    }


}