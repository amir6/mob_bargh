package com.andc.mobilebargh.Networking.WebService;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.andc.mobilebargh.Controllers.SecurityHelper;
import com.andc.mobilebargh.Networking.WSHelper;
import com.andc.mobilebargh.R;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by AFalah on 3/12/2018.
 */

public class WebServiceTools extends WSHelper {
    public String jsonString ;
    private Context context ;

    public WebServiceTools(Context context) {
        this.context = context;
    }

    public  void getUserInfoWithGhabzNo(String ghabzNo) {
        OkHttpClient client = new OkHttpClient();
        final Request request = new Request.Builder()
                .url(getBaseUrl()+services_custom + method_declare_current_branch  + ghabzNo  )
                .addHeader("Token", SecurityHelper.getToken())
                .get()
                .build();

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                Toast.makeText(context,context.getResources().getString(R.string.falture_Get_info) +"\n" + e.toString(), Toast.LENGTH_LONG).show();

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                jsonString  = response.body().string();
            }
        });
/*        CurrentBranchAsync currentBranchAsync = new CurrentBranchAsync();
        currentBranchAsync.execute(ghabzNo);*/
    }

    public class CurrentBranchAsync extends AsyncTask<String,Void,String>{
        ProgressBar progressBar = new ProgressBar(context);

        @Override
        protected void onPreExecute() {
      //      super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String jsonObject) {
            super.onPostExecute(jsonObject);
            jsonString = jsonObject ;
            progressBar.setVisibility(View.GONE);
        }

        @Override
        protected String doInBackground(String... ghabzNo) {
            String json  = "";
            OkHttpClient client = new OkHttpClient();
            final Request request = new Request.Builder()
                    .url(getBaseUrl()+services_custom + method_declare_current_branch  + ghabzNo  )
                    .addHeader("Token", SecurityHelper.getToken())
                    .get()
                    .build();
            try {
                Response response = client.newCall(request).execute();
                if (response.isSuccessful()){
                    
                    json = response.body().string();}

            } catch (IOException e) {
                e.printStackTrace();
            }

            return json;
        }
    }

    public String getJsonString(){
        return jsonString;
    }

}
