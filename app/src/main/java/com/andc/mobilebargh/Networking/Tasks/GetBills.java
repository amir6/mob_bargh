package com.andc.mobilebargh.Networking.Tasks;

import android.os.AsyncTask;
import android.util.Log;
import android.util.Pair;

import com.andc.mobilebargh.Activities.SplashActivity;
import com.andc.mobilebargh.Models.BillData;
import com.andc.mobilebargh.Models.BillingHistoryRecord;
import com.andc.mobilebargh.Models.CustomError;
import com.andc.mobilebargh.Models.PaymentRecord;
import com.andc.mobilebargh.Networking.WSHelper;
import com.andc.mobilebargh.Utility.ErrorHandler;

import java.util.ArrayList;

/**
 * Created by win on 11/24/2015.
 */

public class GetBills extends AsyncTask<Void, ErrorHandler.ErrorState, ArrayList<CustomError>> {

    private OnAsyncRequestComplete mCaller;
    private ArrayList<String> mBillIds;

    // Interface to be implemented by caller
    public interface OnAsyncRequestComplete {
        void asyncResponse(ArrayList<CustomError> errors);
    }

    public GetBills(Object caller){
        mBillIds = new ArrayList<>();

        if(caller instanceof OnAsyncRequestComplete) {
            mCaller = (OnAsyncRequestComplete) caller;
        }else {
            mCaller = null;
            Log.d("ASyncTask", "Caller must Implement Task Interface");
        }
    }

    public GetBills(Object caller, String billId){
        this(caller);
        mBillIds.add(billId);
    }

    public GetBills(Object caller, ArrayList<String> billIds){
        this(caller);
        mBillIds = billIds;
    }

    protected void onPreExecute() {
    }

    protected ArrayList<CustomError> doInBackground(Void... urls) {
        ArrayList<CustomError> mErrors = new ArrayList<>();
        ArrayList<BillData> mBillsData= new ArrayList<>();

        //Get Data
        for(String billId:mBillIds){
            Pair<ArrayList<CustomError>, BillData> response = WSHelper.fetchBill(billId);
            if(response!=null){
                if(response.first != null)
                    mErrors.addAll(response.first);

                if(response.second != null)
                    mBillsData.add(response.second);
            }
        }

        //Save Received Data
        if(mBillsData!=null && mBillsData.size()>0){
            publishProgress(ErrorHandler.ErrorState.UpdatingDatabase);
            for(BillData billData:mBillsData){
                BillData.clear(billData.mBranchInfo.BillId);
                billData.save();

                //Get Pending Payments
                ArrayList<PaymentRecord> mPendingPayments =
                        (ArrayList<PaymentRecord>) PaymentRecord.find(
                                PaymentRecord.class,
                                PaymentRecord.LABEL_PAYMENT_Bill_Id + " = ?",
                                billData.mBranchInfo.BillId
                        );

                //Match Payments Record Against Billing Histories, Removes Duplicates
                if(mPendingPayments!=null)
                    for(PaymentRecord pendingPayment : mPendingPayments)
                        for(BillingHistoryRecord mBillingRecord : billData.mBillingHistory)
                            if(pendingPayment.equals(mBillingRecord)) {
                                mPendingPayments.remove(pendingPayment);
                                pendingPayment.delete();
                                break;
                            }

                //Add Temporary Billing for Remaining Payments
                if(mPendingPayments!=null)
                    for(PaymentRecord pendingPayment:mPendingPayments)
                        pendingPayment.toBillingHistory().save();
            }
        }

        return mErrors;
    }

    protected void onPostExecute(ArrayList<CustomError> errors) {
        if(mCaller!=null)
            mCaller.asyncResponse(errors);
    }

    @Override
    protected void onProgressUpdate(ErrorHandler.ErrorState ...values) {
        ErrorHandler.ErrorState mState = values[0];
        if(mCaller instanceof SplashActivity){
            ((SplashActivity) mCaller).updateSplashScreen(mState);
        }
    }
}