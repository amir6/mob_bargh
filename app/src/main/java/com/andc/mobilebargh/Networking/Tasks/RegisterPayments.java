package com.andc.mobilebargh.Networking.Tasks;

import android.os.AsyncTask;

import com.andc.mobilebargh.Models.PaymentRecord;
import com.andc.mobilebargh.Networking.WSHelper;

/**
 * Created by Esbati on 2/1/2016.
 */
public class RegisterPayments extends AsyncTask<Void, Void, Boolean> {

    private PaymentRecord mUnregisteredPayment;

    public RegisterPayments(PaymentRecord payment) {
        super();
        mUnregisteredPayment = payment;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        return WSHelper.registerPayment(mUnregisteredPayment);
    }

    @Override
    protected void onPostExecute(Boolean wasSuccessful) {
        super.onPostExecute(wasSuccessful);
        if (wasSuccessful) {
            mUnregisteredPayment.isRegistered = true;
            mUnregisteredPayment.save();
        }
    }
}
