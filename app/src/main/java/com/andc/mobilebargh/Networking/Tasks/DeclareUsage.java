package com.andc.mobilebargh.Networking.Tasks;

import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import com.andc.mobilebargh.Controllers.SecurityHelper;
import com.andc.mobilebargh.Models.CustomError;
import com.andc.mobilebargh.Networking.WSHelper;
import com.andc.mobilebargh.Utility.ErrorHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Esbati on 7/9/2016.
 */
public class DeclareUsage extends AsyncTask<Void, Void, ArrayList<CustomError>> {

    public final static String TAG_DECLARE_USAGE_BILL_ID = "BillId";
    public final static String TAG_DECLARE_USAGE_AVG = "AvgAct";
    public final static String TAG_DECLARE_USAGE_HIGH = "HighAct";
    public final static String TAG_DECLARE_USAGE_LOW = "LowAct";
    public final static String TAG_DECLARE_USAGE_FRIDAY = "FridayAct";
    public final static String TAG_DECLARE_USAGE_REACTIVE = "ReactiveAct";
    public final static String TAG_DECLARE_USAGE_DATE = "DeclareDate";

    private String mBillId;
    private String mAvgUsage;
    private String mHighUsage;
    private String mLowUsage;
    private String mFridayUsage;
    private String mReactiveUsage;
    private String mDeclareDate;
    private OnAsyncRequestComplete mCaller;

    // Interface to be implemented by caller
    public interface OnAsyncRequestComplete {
        void asyncResponse(ArrayList<CustomError> errors);
    }

    public DeclareUsage(Object caller,
                        String billId,
                        String avgUsage,
                        String highUsage,
                        String lowUsage,
                        String fridayUsage,
                        String reactiveUsage,
                        String declareDate) {

        if(caller instanceof OnAsyncRequestComplete) {
            mCaller = (OnAsyncRequestComplete) caller;
        }else {
            mCaller = null;
            Log.d("ASyncTask", "Caller must Implement Task Interface");
        }

        mBillId = billId;
        mAvgUsage = avgUsage;
        mHighUsage = highUsage;
        mLowUsage = lowUsage;
        mFridayUsage = fridayUsage;
        mReactiveUsage = reactiveUsage;
        mDeclareDate = declareDate;

        //ak
        if(mAvgUsage.isEmpty() || mAvgUsage == null){
            mAvgUsage = "-1";
        }
        if(mHighUsage.isEmpty() || mHighUsage == null){
            mHighUsage = "-1";
        }
        if(mLowUsage.isEmpty() || mLowUsage == null){
            mLowUsage = "-1";
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected ArrayList<CustomError> doInBackground(Void... voids) {
        ArrayList<CustomError> errors = null;

        OkHttpClient okHttpClient = new OkHttpClient();
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");

        //Create JSON Body
        JSONObject jsonObject = new JSONObject();
        try{

            jsonObject.put(TAG_DECLARE_USAGE_BILL_ID, mBillId)
                    .put(TAG_DECLARE_USAGE_DATE, mDeclareDate)
                    .put(TAG_DECLARE_USAGE_AVG, Integer.parseInt(mAvgUsage))
                    .put(TAG_DECLARE_USAGE_HIGH, Integer.parseInt(mHighUsage))
                    .put(TAG_DECLARE_USAGE_LOW, Integer.parseInt(mLowUsage));

            if(!TextUtils.isEmpty(mFridayUsage))
                jsonObject.put(TAG_DECLARE_USAGE_FRIDAY, Integer.parseInt(mFridayUsage));

            if(!TextUtils.isEmpty(mReactiveUsage))
                jsonObject.put(TAG_DECLARE_USAGE_REACTIVE, Integer.parseInt(mReactiveUsage));
        } catch (JSONException e){
            e.printStackTrace();
        }
        RequestBody jsonBody = RequestBody.create(JSON, jsonObject.toString());

        //Create Form Body
        FormBody.Builder formBody = new FormBody.Builder()
                .add(TAG_DECLARE_USAGE_BILL_ID, mBillId)
                .add(TAG_DECLARE_USAGE_AVG, mAvgUsage)
                .add(TAG_DECLARE_USAGE_HIGH, mHighUsage)
                .add(TAG_DECLARE_USAGE_LOW, mLowUsage)
                .add(TAG_DECLARE_USAGE_DATE, mDeclareDate);

        if(!TextUtils.isEmpty(mFridayUsage))
            formBody.add(TAG_DECLARE_USAGE_FRIDAY, mFridayUsage);

        if(!TextUtils.isEmpty(mReactiveUsage))
            formBody.add(TAG_DECLARE_USAGE_REACTIVE, mReactiveUsage);

        //Create Multipart
        MultipartBody.Builder multipartBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart(TAG_DECLARE_USAGE_BILL_ID, mBillId)
                .addFormDataPart(TAG_DECLARE_USAGE_AVG, mAvgUsage)
                .addFormDataPart(TAG_DECLARE_USAGE_HIGH, mHighUsage)
                .addFormDataPart(TAG_DECLARE_USAGE_LOW, mLowUsage)
                .addFormDataPart(TAG_DECLARE_USAGE_DATE, mDeclareDate);

        if(!TextUtils.isEmpty(mFridayUsage))
            multipartBody.addFormDataPart(TAG_DECLARE_USAGE_FRIDAY, mFridayUsage);

        if(!TextUtils.isEmpty(mReactiveUsage))
            multipartBody.addFormDataPart(TAG_DECLARE_USAGE_REACTIVE, mReactiveUsage);


        Request request = new Request.Builder()
                .url(WSHelper.fallback_address + WSHelper.services_custom + WSHelper.method_declare_usage)
                .addHeader("Token", SecurityHelper.getToken())
                .post(multipartBody.build())
                .build();

        try{
            Response response = okHttpClient.newCall(request).execute();
            if(response.isSuccessful()) {
                //Parse Response
                JSONObject jsonResponse = new JSONObject(response.body().string());

                //Get Server Response
                JSONArray errorJSON = jsonResponse.getJSONArray(CustomError.TAG_ERROR);
                errors = CustomError.parseJSON(errorJSON);
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        return errors;
    }

    protected void onPostExecute(ArrayList<CustomError> errors) {
        //If Failed to Connect To Server Create Connection Failed Error
        if(errors==null || errors.size()==0) {
            CustomError connectionError = new CustomError();
            connectionError.mCode = ErrorHandler.ErrorState.ConnectionFailed.ordinal();
            connectionError.mMsg = ErrorHandler.ErrorState.ConnectionFailed.getMessage();
            errors = new ArrayList<>();
            errors.add(connectionError);
        }

        if(mCaller!=null)
            mCaller.asyncResponse(errors);
    }
}
