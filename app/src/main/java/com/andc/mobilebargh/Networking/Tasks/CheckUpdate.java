package com.andc.mobilebargh.Networking.Tasks;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import com.andc.mobilebargh.Controllers.PreferencesHelper;
import com.andc.mobilebargh.Models.UpdateFile;
import com.andc.mobilebargh.Networking.WSHelper;
import com.andc.mobilebargh.R;
import com.andc.mobilebargh.Utility.CustomDialog;

import ir.smartlab.persindatepicker.util.PersianCalendar;

/**
 * Created by Esbati on 5/2/2016.
 */
public class CheckUpdate extends AsyncTask<Void,Void, UpdateFile> {

    // Interface to be implemented by caller
    public interface OnAsyncRequestComplete {
        void asyncResponse(UpdateFile updateFile);
    }

    private OnAsyncRequestComplete mCaller;

    public CheckUpdate(Object caller) {
        if(caller instanceof OnAsyncRequestComplete) {
            mCaller = (OnAsyncRequestComplete) caller;
        }else {
            mCaller = null;
            Log.d("ASyncTask", "Caller must Implement Task Interface");
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected UpdateFile doInBackground(Void... voids) {
        return WSHelper.getCurrentVersion();
    }

    @Override
    protected void onPostExecute(UpdateFile updateFile) {
        //On Successful Response Save Check-Update Date and Latest Version Available
        //TODO Save Last Update Date Only if the User is Promoted With Update!!
        if(updateFile!=null){
            PreferencesHelper.save(PreferencesHelper.KEY_UPDATE_LAST_CHECK, new PersianCalendar().getPersianShortDate());
            PreferencesHelper.saveInt(PreferencesHelper.KEY_UPDATE_LATEST_VERSION, updateFile.mVersionCode);

            if(!TextUtils.isEmpty(updateFile.mServerAddress))
                PreferencesHelper.save(UpdateFile.TAG_SERVER_ADDRESS, updateFile.mServerAddress);
        }

        if(mCaller!=null)
            mCaller.asyncResponse(updateFile);
    }

    public static void promoteUpdate(final Context context, final UpdateFile updateFile){
        AlertDialog dialog = new AlertDialog.Builder(context)
                //.setView(mDialogView)
                .setTitle("بروزرسانی برنامه")
                .setMessage(updateFile.mUpdateMsg)
                .setNegativeButton(context.getResources().getString(R.string.dialog_button_return), null)
                .setPositiveButton(context.getResources().getString(R.string.dialog_button_update), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(updateFile.mUpdateUrl));
                        context.startActivity(i);
                    }
                }).create();

        CustomDialog.show(dialog);
    }
}
