package com.andc.mobilebargh.Networking.Tasks;

import android.os.AsyncTask;

import com.andc.mobilebargh.Controllers.PreferencesHelper;
import com.andc.mobilebargh.Models.BillRecord;
import com.andc.mobilebargh.Models.CustomError;
import com.andc.mobilebargh.Networking.WSHelper;

import java.util.ArrayList;

import ir.smartlab.persindatepicker.util.PersianCalendar;

/**
 * Created by Tenkei on 5/31/2016.
 */
public class SyncBills extends AsyncTask<Void, Void, ArrayList<CustomError>> {

    ArrayList<BillRecord> mBills;

    public SyncBills() {
        super();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mBills = (ArrayList<BillRecord>)BillRecord.listAll(BillRecord.class);
    }

    @Override
    protected ArrayList<CustomError> doInBackground(Void... voids) {
        return WSHelper.syncBills(mBills);
    }

    @Override
    protected void onPostExecute(ArrayList<CustomError> errors) {
        //On Successful Result Return Should Sync Flag to False and Save Sync Date
        if (errors!=null && errors.size()>0 && errors.get(0).mCode == 200) {
            PreferencesHelper.setOption(PreferencesHelper.KEY_SYNC_NECESSARY, false);
            PreferencesHelper.save(PreferencesHelper.KEY_SYNC_LAST_DATE, new PersianCalendar().getPersianShortDateTime());
        }
    }
}
