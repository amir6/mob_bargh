package com.andc.mobilebargh.Networking.Tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.andc.mobilebargh.Controllers.SecurityHelper;
import com.andc.mobilebargh.Models.CustomError;
import com.andc.mobilebargh.Networking.WSHelper;
import com.andc.mobilebargh.Utility.ErrorHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Esbati on 7/9/2016.
 */
public class SendReport extends AsyncTask<Void, Void, ArrayList<CustomError>> {

    public final static String TAG_REPORT_BILL_ID = "BillId";
    public final static String TAG_REPORT_TITLE = "Title";
    public final static String TAG_REPORT_TIME = "Time";
    public final static String TAG_REPORT_DATE = "Date";
    public final static String TAG_REPORT_ADDRESS = "Address";
    public final static String TAG_REPORT_LAT = "Lat";
    public final static String TAG_REPORT_LNG = "Lng";
    public final static String TAG_REPORT_DETAIL = "Detail";

    private String mBillId;
    private String mTitle;
    private String mDate;
    private String mAddress;
    private Double mLat;
    private Double mLng;
    private String mDetail;
    private OnAsyncRequestComplete mCaller;

    // Interface to be implemented by caller
    public interface OnAsyncRequestComplete {
        void asyncResponse(ArrayList<CustomError> errors);
    }

    public SendReport(Object caller,
                      String billId,
                      String title,
                      String date,
                      String address,
                      Double lat,
                      Double lng,
                      String detail) {

        if(caller instanceof OnAsyncRequestComplete) {
            mCaller = (OnAsyncRequestComplete) caller;
        }else {
            mCaller = null;
            Log.d("ASyncTask", "Caller must Implement Task Interface");
        }

        mBillId = billId;
        mTitle = title;
        mDate = date;
        mAddress = address;
        mLat = lat;
        mLng = lng;
        mDetail = detail;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected ArrayList<CustomError> doInBackground(Void... voids) {
        ArrayList<CustomError> errors = null;

        OkHttpClient okHttpClient = new OkHttpClient();
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");

        //Create JSON Body
        JSONObject jsonObject = new JSONObject();
        try{
            jsonObject.put(TAG_REPORT_BILL_ID, mBillId)
                    .put(TAG_REPORT_TITLE, mTitle)
                    .put(TAG_REPORT_DATE, mDate)
                    .put(TAG_REPORT_ADDRESS, mAddress)
                    .put(TAG_REPORT_DETAIL, mDetail)
                    .put(TAG_REPORT_LAT, mLat.toString())
                    .put(TAG_REPORT_LNG, mLng.toString());
        } catch (JSONException e){
            e.printStackTrace();
        }
        RequestBody jsonBody = RequestBody.create(JSON, jsonObject.toString());

        //Create Form Body
        FormBody.Builder formBody = new FormBody.Builder()
                .add(TAG_REPORT_BILL_ID, mBillId)
                .add(TAG_REPORT_TITLE, mTitle)
                .add(TAG_REPORT_DATE, mDate)
                .add(TAG_REPORT_ADDRESS, mAddress)
                .add(TAG_REPORT_DETAIL, mDetail);

        if(mLat!=0 && mLng!=0){
            formBody.add(TAG_REPORT_LAT, mLat.toString())
                .add(TAG_REPORT_LNG, mLng.toString());
        }

        //Create Multipart
        MultipartBody.Builder multipartBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart(TAG_REPORT_BILL_ID, mBillId)
                .addFormDataPart(TAG_REPORT_TITLE, mTitle)
                .addFormDataPart(TAG_REPORT_DATE, mDate)
                .addFormDataPart(TAG_REPORT_ADDRESS, mAddress)
                .addFormDataPart(TAG_REPORT_DETAIL, mDetail);

        if(mLat!=0 && mLng!=0){
            multipartBody.addFormDataPart(TAG_REPORT_LAT, mLat.toString())
                    .addFormDataPart(TAG_REPORT_LNG, mLng.toString());
        }

        Request request = new Request.Builder()
                .url(WSHelper.fallback_address + WSHelper.services_custom + WSHelper.method_send_report)
                .addHeader("Token", SecurityHelper.getToken())
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .post(multipartBody.build())
                .build();

        try{
            Response response = okHttpClient.newCall(request).execute();
            if(response.isSuccessful()) {
                //Parse Response
                JSONObject jsonResponse = new JSONObject(response.body().string());

                //Get Server Response
                JSONArray errorJSON = jsonResponse.getJSONArray(CustomError.TAG_ERROR);
                errors = CustomError.parseJSON(errorJSON);
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        return errors;
    }

    protected void onPostExecute(ArrayList<CustomError> errors) {
        //If Failed to Connect To Server Create Connection Failed Error
        if(errors==null || errors.size()==0) {
            CustomError connectionError = new CustomError();
            connectionError.mCode = ErrorHandler.ErrorState.ConnectionFailed.ordinal();
            connectionError.mMsg = ErrorHandler.ErrorState.ConnectionFailed.getMessage();
            errors = new ArrayList<>();
            errors.add(connectionError);
        }

        if(mCaller!=null)
            mCaller.asyncResponse(errors);
    }
}
