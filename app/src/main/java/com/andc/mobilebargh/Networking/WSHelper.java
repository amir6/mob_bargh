package com.andc.mobilebargh.Networking;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.widget.Toast;

import com.andc.mobilebargh.Controllers.PreferencesHelper;
import com.andc.mobilebargh.Controllers.SecurityHelper;
import com.andc.mobilebargh.Models.BillData;
import com.andc.mobilebargh.Models.BillRecord;
import com.andc.mobilebargh.Models.CustomError;
import com.andc.mobilebargh.Models.PaymentRecord;
import com.andc.mobilebargh.Models.UpdateFile;
import com.andc.mobilebargh.Networking.Tasks.DeclareUsage;
import com.andc.mobilebargh.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Esbati on 2/3/2016.
 */
public class WSHelper {
    //public static final String pecco_address = "https://212.80.25.119/api/";
    //public static final String version_address = "http://services.andc.ir/api/CustomService/GetVersion";
    //ak
    public static final String version_address = "https://mobilebargh.pec.ir/adpmobile/api/CustomService/GetVersion";
    public static final String fallback_address = "https://mobilebargh.pec.ir/adpmobile";
    public static final String services_authenticate = "/api/AuthService/";
    public static final String services_custom = "/api/CustomService/";

    //Auth Services
    public static final String method_register = "RegisterUser/";
    public static final String method_login = "LoginUser/%s"; //CellPhone
    public static final String method_authenticate = "Authenticate/%s/%s"; //CellPhone/AuthCode

    //Data Services
    public static final String method_bill_get_branch_info = "GetBranchInfoData/";
    public static final String method_bill_get_branch_sale = "GetBranchSaleData/";
    public static final String method_bill_get_branch_receipt = "GetBranchReceiptData/";
    public static final String method_bill_get_data = "GetDataView/";
    public static final String method_payment_token = "BillGetToken/";
    public static final String method_payment_register = "RegisterPayment/";
    public static final String method_payment_register_mpl = "RegisterPaymentMPLFromParsian/";
    public static final String method_bills_sync = "GetSnippet/";
    public static final String method_send_report = "DeclareReport/";
    public static final String method_declare_usage = "DeclareUsage/";
    public static final String method_declare_blackout = "DeclareBlackout/";
    public static final String method_declare_current_branch = "GetSimpleBranchDataView/";

    //

    public static String getBaseUrl(){
        String baseUrl = PreferencesHelper.load(UpdateFile.TAG_SERVER_ADDRESS);

        if(!TextUtils.isEmpty(baseUrl))
            return baseUrl;
        else
            return fallback_address;
    }

    public static UpdateFile getCurrentVersion(){
        UpdateFile update = null;
        try {
            URL url = new URL(version_address);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()) {
                });
                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line);
                }
                bufferedReader.close();

                //Parse Response
                JSONObject responseJSON = new JSONObject(stringBuilder.toString());

                //Get Update Info
                JSONObject updateJSON = responseJSON.getJSONObject("data");
                update = UpdateFile.parseJSON(updateJSON);
            } catch (Exception e){
                Log.d("!Test!","GET VERSION ERROR : " + e.getLocalizedMessage());
            }finally{
                urlConnection.disconnect();
            }
        } catch(Exception e) {
            Log.e("ERROR", e.getMessage(), e);
        }

        return update;
    }

    public static ArrayList<CustomError> signUp(String cellphone, String name, String familyName, String nationalCode){
        ArrayList<CustomError> errors = null;

        try {
            URL url = new URL(getBaseUrl() + services_authenticate + method_register);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoInput(true);
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/json");
            SecurityHelper.addHTTPAuthorization(urlConnection);
            try {
                //Create Register User JSON
                JSONObject registerJSON = new JSONObject();
                registerJSON.put("MobileNo",cellphone);
                registerJSON.put("FirstName",name);
                registerJSON.put("LastName",familyName);
                registerJSON.put("NationalCode",nationalCode);
                registerJSON.put("NationalCode", nationalCode);

                //Write Message to the OutputStream
                OutputStreamWriter outputWriter = new OutputStreamWriter(urlConnection.getOutputStream(), "UTF-8");
                outputWriter.write(registerJSON.toString());
                outputWriter.flush();

                //Read InputStream
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line);
                }
                bufferedReader.close();

                //Parse Response
                JSONObject jsonResponse = new JSONObject(stringBuilder.toString());
                JSONArray errorJSON = jsonResponse.getJSONArray(CustomError.TAG_ERROR);
                errors = CustomError.parseJSON(errorJSON);
            } finally{
                urlConnection.disconnect();
            }
        } catch(Exception e) {
            Log.e("ERROR", e.getMessage(), e);
        }

        return errors;
    }

    public static ArrayList<CustomError> signIn(String cellphone){
        ArrayList<CustomError> errors = null;

        try {
            URL url = new URL(getBaseUrl()
                    + services_authenticate
                    + String.format(method_login , cellphone)
            );
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line);
                }
                bufferedReader.close();

                //Parse Response
                JSONObject jsonResponse = new JSONObject(stringBuilder.toString());
                JSONArray errorJSON = jsonResponse.getJSONArray(CustomError.TAG_ERROR);
                errors = CustomError.parseJSON(errorJSON);
            } finally{
                urlConnection.disconnect();
            }
        } catch(Exception e) {
            Log.e("ERROR", e.getMessage(), e);
        }

        return errors;
    }

    public static ArrayList<CustomError> authenticate(String cellphone, String authCode){
        ArrayList<CustomError> errors = null;

        try {
            URL url = new URL(getBaseUrl()
                    + services_authenticate
                    + String.format(method_authenticate , cellphone, authCode)
            );
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line);
                }
                bufferedReader.close();

                //Parse Response
                JSONObject jsonResponse = new JSONObject(stringBuilder.toString());

                //Get Server Response
                JSONArray errorJSON = jsonResponse.getJSONArray(CustomError.TAG_ERROR);
                errors = CustomError.parseJSON(errorJSON);

                //Get Token
                JSONObject dataJSON = jsonResponse.getJSONObject(CustomError.TAG_DATA);
                String token = dataJSON.getString(SecurityHelper.SECURITY_TOKEN);
                SecurityHelper.saveToken(token);

                //Get User's Bills
                ArrayList<BillRecord> bills = BillRecord.parseJSON(dataJSON.getJSONArray(BillRecord.TAG_BILL));
                for(BillRecord bill:bills)
                    bill.save();
            } finally{
                urlConnection.disconnect();
            }
        } catch(Exception e) {
            Log.e("ERROR", e.getMessage(), e);
        }

        return errors;
    }

    public static Pair<ArrayList<CustomError>, BillData> fetchBill(String billId){
        ArrayList<CustomError> errors = null;
        BillData billData = new BillData();

        try {
            URL url = new URL(getBaseUrl() + services_custom + method_bill_get_data + billId);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            SecurityHelper.addHTTPAuthorization(urlConnection);
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line);
                }
                bufferedReader.close();

                //Parse Response
                JSONObject jsonResponse = new JSONObject(stringBuilder.toString());

                //Get Server Response
                JSONArray errorJSON = jsonResponse.getJSONArray(CustomError.TAG_ERROR);
                errors = CustomError.parseJSON(errorJSON);

                //Get Token
                //FIXME Change Data Received Inside "Data" Field to Json Object
                //JSONObject dataJSON = jsonResponse.getJSONObject(CustomError.TAG_DATA);
                String dataJSON = jsonResponse.getString(CustomError.TAG_DATA);
                billData.fromJSON(new JSONObject(dataJSON));

            } finally{
                urlConnection.disconnect();
            }
        } catch(Exception e) {
            Log.e("ERROR", e.getMessage(), e);
        }

        return new Pair<>(errors, billData);
    }

    public static boolean registerPayment(PaymentRecord unregisteredPayment){
        try {
            URL url = new URL(getBaseUrl() + services_custom + method_payment_register);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoInput(true);
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/json");
            SecurityHelper.addHTTPAuthorization(urlConnection);
            try {
                //Write Message to the OutputStream
                OutputStreamWriter outputWriter = new OutputStreamWriter(urlConnection.getOutputStream());
                outputWriter.write(unregisteredPayment.toJSON().toString());
                outputWriter.flush();

                //Read InputStream
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line);
                }
                bufferedReader.close();

                //Parse Response
                String response = stringBuilder.toString();
                //JSONObject jsonResponse = new JSONObject(stringBuilder.toString());
            } finally{
                urlConnection.disconnect();
            }
        } catch(Exception e) {
            Log.e("ERROR", e.getMessage(), e);
            return false;
        }

        return true;
    }

    public static ArrayList<CustomError> declareUsage(String billId,
                                                      String avgUsage,
                                                      String highUsage,
                                                      String lowUsage,
                                                      String fridayUsage,
                                                      String reactiveUsage,
                                                      String declareDate) {
        ArrayList<CustomError> errors = null;

        OkHttpClient okHttpClient = new OkHttpClient();
        FormBody.Builder formBody = new FormBody.Builder()
                .add(DeclareUsage.TAG_DECLARE_USAGE_BILL_ID, billId)
                .add(DeclareUsage.TAG_DECLARE_USAGE_AVG, avgUsage)
                .add(DeclareUsage.TAG_DECLARE_USAGE_HIGH, highUsage)
                .add(DeclareUsage.TAG_DECLARE_USAGE_LOW, lowUsage)
                .add(DeclareUsage.TAG_DECLARE_USAGE_DATE, declareDate);

        if(!TextUtils.isEmpty(fridayUsage))
            formBody.add(DeclareUsage.TAG_DECLARE_USAGE_FRIDAY, fridayUsage);

        if(!TextUtils.isEmpty(reactiveUsage))
            formBody.add(DeclareUsage.TAG_DECLARE_USAGE_REACTIVE, reactiveUsage);

        Request request = new Request.Builder()
                .url(getBaseUrl() + services_custom + method_declare_usage)
                .header("Content-Type", "application/json")
                .addHeader("Token", SecurityHelper.getToken())
                .post(formBody.build())
                .build();

        try{
            Response response = okHttpClient.newCall(request).execute();
            if(response.isSuccessful()) {
                //Parse Response
                JSONObject jsonResponse = new JSONObject(response.body().string());

                //Get Server Response
                JSONArray errorJSON = jsonResponse.getJSONArray(CustomError.TAG_ERROR);
                errors = CustomError.parseJSON(errorJSON);
                }
        } catch (Exception e){
            e.printStackTrace();
        }

        return errors;
    }

    public static ArrayList<CustomError> syncBills(ArrayList<BillRecord> syncedBills){
        ArrayList<CustomError> errors = null;

        try {
            //URL url = new URL(getBaseUrl() + method_payment_register);
            URL url = new URL(getBaseUrl() + services_custom + method_bills_sync);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoInput(true);
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/json");
            SecurityHelper.addHTTPAuthorization(urlConnection);

            try {
                //Convert Bills to JSON and Send Them to Server
                JSONArray billsJSON = new JSONArray();
                for(int i = 0 ; i < syncedBills.size() ; i++)
                    billsJSON.put(syncedBills.get(i).toJSON());

                //Write Message to the OutputStream
                OutputStreamWriter outputWriter = new OutputStreamWriter(urlConnection.getOutputStream());
                outputWriter.write(billsJSON.toString());
                outputWriter.flush();

                //Read InputStream
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line);
                }
                bufferedReader.close();

                //Parse Response
                JSONObject jsonResponse = new JSONObject(stringBuilder.toString());
                JSONArray errorJSON = jsonResponse.getJSONArray(CustomError.TAG_ERROR);
                errors = CustomError.parseJSON(errorJSON);
            } finally{
                urlConnection.disconnect();
            }
        } catch(Exception e) {
            Log.e("ERROR", e.getMessage(), e);
        }

        return errors;
    }


    public String jsonString ;


    public  void getUserInfoWithGhabzNo(String ghabzNo, final Context context) {
        OkHttpClient client = new OkHttpClient();
        final Request request = new Request.Builder()
                .url(getBaseUrl()+services_custom + method_declare_current_branch  + ghabzNo  )
                .addHeader("Token", SecurityHelper.getToken())
                .get()
                .build();

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                Toast.makeText(context,context.getResources().getString(R.string.falture_Get_info) +"\n" + e.toString(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                jsonString  = response.body().string();
            }
        });
    }

    public String getJsonString(){
        return jsonString;
    }
}
