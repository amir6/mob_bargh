package com.andc.mobilebargh.Activities;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.andc.mobilebargh.Fragments.RequestChangeName.CurrentUserFragment;
import com.andc.mobilebargh.Fragments.RequestChangeName.DocumentFragment;
import com.andc.mobilebargh.Fragments.RequestChangeName.IRequestChangeListener;
import com.andc.mobilebargh.Fragments.RequestChangeName.NewUserFragment;
import com.andc.mobilebargh.Fragments.RequestChangeName.ObservingInformationFragment;
import com.andc.mobilebargh.Fragments.RequestChangeName.TrackIGNFragment;
import com.andc.mobilebargh.Models.CurrentUserInfo;
import com.andc.mobilebargh.Models.NewUserInfo;
import com.andc.mobilebargh.Models.PersonalDocumentInfo;
import com.andc.mobilebargh.R;

/**
 * Created by AK on 5/12/2018.
 */

public class ChangeNameActivity  extends AppCompatActivity implements IRequestChangeListener {
    //TextView txTrackIGN, txObsInfo, txDoc, txNewUser ,txCurrUser;
    ImageView imgTrackIGN, imgObsInfo, imgDoc, imgNewUser,imgCurrUser;
    //Button btnNext ;
    private int selectFragement = 1;
    private NewUserInfo newUserInfoHolder;
    private CurrentUserInfo currentUserInfoHolder ;
    private PersonalDocumentInfo personalDocumentInfoHolder;
    private String followUpCodeHolder;
 


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_change_name);
        getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);

        init();
        prepareFrg(1);
    }



    private void setFragment(Fragment frm) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction frmTrans = fragmentManager.beginTransaction();
        if (frm != null) {
            frmTrans.replace(R.id.content, frm);
            frmTrans.commit();
        }
    }

    private void prepareFrg(int pos) {
        switch (pos) {
            case 1:
                setFragment(CurrentUserFragment.newInstance());
//                btnBack.setEnabled(false);
                imgCurrUser.setColorFilter(getResources().getColor(R.color.green_3));
                imgNewUser.setColorFilter(Color.BLACK);
                imgDoc.setColorFilter(Color.BLACK);
                imgObsInfo.setColorFilter(Color.BLACK);
                imgTrackIGN.setColorFilter(Color.BLACK);
                selectFragement = 1;
//                btnNext.setText(getResources().getString(R.string.next_btn));
                break;
            case 2:
                setFragment(NewUserFragment.newInstance());
                //  btnBack.setEnabled(true);
                imgCurrUser.setColorFilter(getResources().getColor(R.color.green_3));
                imgNewUser.setColorFilter(getResources().getColor(R.color.green_3));
                imgDoc.setColorFilter(Color.BLACK);
                imgObsInfo.setColorFilter(Color.BLACK);
                imgTrackIGN.setColorFilter(Color.BLACK);
                selectFragement = 2;
//                btnNext.setText(getResources().getString(R.string.next_btn));
                break;
            case 3:
                setFragment(DocumentFragment.newInstance());
                imgCurrUser.setColorFilter(getResources().getColor(R.color.green_3));
                imgNewUser.setColorFilter(getResources().getColor(R.color.green_3));
                imgDoc.setColorFilter(getResources().getColor(R.color.green_3));
                imgObsInfo.setColorFilter(Color.BLACK);
                imgTrackIGN.setColorFilter(Color.BLACK);
                selectFragement = 3;
                break;
            case 4:
                setFragment(ObservingInformationFragment.newInstance());
                imgCurrUser.setColorFilter(getResources().getColor(R.color.green_3));
                imgNewUser.setColorFilter(getResources().getColor(R.color.green_3));
                imgDoc.setColorFilter(getResources().getColor(R.color.green_3));
                imgObsInfo.setColorFilter(getResources().getColor(R.color.green_3));
                imgTrackIGN.setColorFilter(Color.BLACK);
                selectFragement = 4;
                break;
            case 5:
                setFragment(TrackIGNFragment.newInstance());
                imgCurrUser.setColorFilter(getResources().getColor(R.color.green_3));
                imgNewUser.setColorFilter(getResources().getColor(R.color.green_3));
                imgDoc.setColorFilter(getResources().getColor(R.color.green_3));
                imgObsInfo.setColorFilter(getResources().getColor(R.color.green_3));
                imgTrackIGN.setColorFilter(getResources().getColor(R.color.green_3));
                selectFragement = 5;
                break;
        }
    }

    private void init() {
        imgNewUser = (ImageView) findViewById(R.id.img_new_user);
        imgDoc = (ImageView) findViewById(R.id.img_doc);
        imgObsInfo = (ImageView) findViewById(R.id.img_obs_info);
        imgTrackIGN = (ImageView) findViewById(R.id.img_track_ign);
        imgCurrUser = (ImageView) findViewById(R.id.img_curr_user);
//        btnNext = (Button) view.findViewById(R.id.btn_next);
//        btnBack = (Button) view.findViewById(R.id.btn_back);
        // txCurrUser = (TextView) view.findViewById(R.id.txt_curr_user);

    }


    @Override
    public void ListenerButtonNextClicked() {
        switch (selectFragement) {
            case 1:
                prepareFrg(2);
                break;
            case 2:
                prepareFrg(3);
                break;
            case 3:
                prepareFrg(4);
                break;
            case 4:
                prepareFrg(5);
                break;
            case 5:
                Toast.makeText(this, "Done!", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void ListenerButtonBackClicked() {
        switch (selectFragement) {
            case 2:
                prepareFrg(1);
                break;
            case 3:
                prepareFrg(2);
                break;
            case 4:
                prepareFrg(3);
                break;
            case 5:
                prepareFrg(4);
                break;
        }
    }

    @Override
    public void setNewUserInfo(NewUserInfo newUserInfo) {
        newUserInfoHolder = newUserInfo ;
    }

    @Override
    public NewUserInfo getNewUserInfo() {
        return newUserInfoHolder;
    }

    @Override
    public void setCurrentUserInfo(CurrentUserInfo currentUserInfo) {
        currentUserInfoHolder = currentUserInfo;
    }

    @Override
    public CurrentUserInfo getCurrentUserInfo() {
        return currentUserInfoHolder;
    }

    @Override
    public void setPersonalDocumentInfo(PersonalDocumentInfo personalDocumentInfo) {
        personalDocumentInfoHolder = personalDocumentInfo;
    }

    @Override
    public PersonalDocumentInfo getPersonalDocumentInfo() {
        return personalDocumentInfoHolder;
    }

    @Override
    public void setFollowUpCode(String fCode) {
        followUpCodeHolder = fCode;
    }

    @Override
    public String getFollowUpCode() {
        return followUpCodeHolder;
    }




}
