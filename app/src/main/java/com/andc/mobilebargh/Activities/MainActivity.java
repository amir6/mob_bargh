package com.andc.mobilebargh.Activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.res.Configuration;

import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;

import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.andc.mobilebargh.Controllers.BillCollection;
import com.andc.mobilebargh.Controllers.PreferencesHelper;
import com.andc.mobilebargh.Controllers.SecurityHelper;
import com.andc.mobilebargh.Controllers.TutorialHelper;
import com.andc.mobilebargh.Fragments.DialogFramgments.AddBillDialog;
import com.andc.mobilebargh.Fragments.DialogFramgments.WaringDialog;
import com.andc.mobilebargh.Fragments.HomeFragments.HomeFragment;
import com.andc.mobilebargh.Fragments.ManageBillsFragment;
import com.andc.mobilebargh.MobileBargh;
import com.andc.mobilebargh.Models.BillRecord;
import com.andc.mobilebargh.Models.CustomError;
import com.andc.mobilebargh.Models.PaymentRecord;
import com.andc.mobilebargh.Models.UpdateFile;
import com.andc.mobilebargh.Networking.Tasks.CheckUpdate;
import com.andc.mobilebargh.Networking.Tasks.GetBills;
import com.andc.mobilebargh.Networking.Tasks.RegisterPayments;
import com.andc.mobilebargh.Networking.Tasks.SyncBills;
import com.andc.mobilebargh.Networking.WSHelper;
import com.andc.mobilebargh.R;
import com.andc.mobilebargh.Utility.Components.HeaderFragment;
import com.andc.mobilebargh.Utility.Components.RefreshableFragment;
import com.andc.mobilebargh.Utility.CustomDialog;
import com.andc.mobilebargh.Utility.ErrorHandler;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;


import ir.pec.mpl.pecpayment.view.PaymentInitiator;
import ir.smartlab.persindatepicker.util.PersianCalendar;
import ir.tgbs.peccharge.billing.PecChargeBillingService;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.andc.mobilebargh.Utility.Util.Constants.REQUEST_CODE_CAMERA;
import static com.andc.mobilebargh.Utility.Util.Constants.REQUEST_CODE_WRITE_EXTERNAL_STORAGE;

public class MainActivity extends AppCompatActivity implements
        CheckUpdate.OnAsyncRequestComplete,
        GetBills.OnAsyncRequestComplete,
        HeaderFragment.ActionBarHandlerInterface {

    //Activity Key Tags
    public final static int REQUEST_PAY_BILLS_MPL = 1;
    public final static int REQUEST_PAY_BILLS_TOP = 2;
    public final static int REQUEST_ADD_BILL = 11;
    public final static int REQUEST_MANAGE_BILLS = 12;
    public final static int REQUEST_SECONDARY_ACTIONS = 22;
    public final static int REQUEST_DRAWER_OPTIONS = 23;
    private final static String ACC_SETTING_VALUE_KEY = "acc_setting";
    private final static String CURRENT_ITEM_VALUE_KEY = "current_item";

    //Activity States
    private boolean isAccSettingOpen;
    private int mCurrentItemId;
    private MenuItem mHomeInfo;

    //Global Views
    private Toolbar mToolbar;
    private AppBarLayout mAppBar;
    public View mParentView;
    public SwipeRefreshLayout mSwipeRefresh;

    //Drawer
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private NavigationView mNVDrawer;

    //Drawer Header
    private View mDrawerHeader;
    private View mDrawerHeaderContainer;
    private ImageView mDrawerBackground;
    private ImageView mAccSettingIndicator;
    private TextView mDrawerTutorial;
    private TextView mDrawerBillTitle;
    private TextView mDrawerBillId;

    //Pay Service
    //private GoogleApiClient client;
    public PecChargeBillingService billingService;
    private Intent bankIntent;
    private PaymentRecord mCurrentPayment;

    @Override
    public boolean moveHeaderToActivity(View fragmentHeader) {
        //Clear AppBar, Leave Activity Toolbar
        for(int i = 1 ; i < mAppBar.getChildCount() ; i++)
            mAppBar.removeViewAt(i);

        if(fragmentHeader != null) {
            //Remove Fragment Header from Its Parent if Needed
            if (fragmentHeader.getParent() != null)
                ((LinearLayout) fragmentHeader.getParent()).removeView(fragmentHeader);

            //Add Fragment Header to Activity
            mAppBar.addView(fragmentHeader);
            if(fragmentHeader.getVisibility() != View.VISIBLE)
                fragmentHeader.setVisibility(View.VISIBLE);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void clearActionBar() {
        //Clear Fragment Header, Leaving Activity Toolbar
        for(int i = 1 ; i < mAppBar.getChildCount() ; i++)
            mAppBar.removeViewAt(i);
    }

    @Override
    public void asyncResponse(ArrayList<CustomError> errors) {

        if(mSwipeRefresh.isRefreshing())
            mSwipeRefresh.setRefreshing(false);

        boolean wasSuccessful = true;
        if(errors==null | errors.size()==0) {
            ErrorHandler.showError(mParentView, ErrorHandler.ErrorState.ConnectionFailed);
            wasSuccessful = false;
        } else {
            for (CustomError error : errors)
                if (error.mCode != 200) {
                    ErrorHandler.showError(mParentView, error);
                    wasSuccessful = false;
                }
        }

        if(wasSuccessful) {
            ErrorHandler.showError(mParentView, ErrorHandler.ErrorState.UpdateSuccessful);
            refreshFragment();
        }
    }

    @Override
    public void asyncResponse(final UpdateFile updateFile) {
        if(updateFile==null)
            return;

        if(updateFile.mVersionCode>PreferencesHelper.getCurrentVersion())
            if(updateFile.mUpdateType==UpdateFile.UpdateType.Optional){
                //If Update is Optional, Promote User Only when Auto Update is Enabled
                if(PreferencesHelper.isOptionActive(PreferencesHelper.KEY_AUTO_UPDATE))
                    CheckUpdate.promoteUpdate(this, updateFile);
            } else {
                //If Update is Mandatory, Don't Let User Inside Before Updating
                AlertDialog dialog = new AlertDialog.Builder(this)
                        //.setView(mDialogView)
                        .setTitle("بروزرسانی برنامه")
                        .setMessage(updateFile.mUpdateMsg)
                        .setNegativeButton(getResources().getString(R.string.dialog_button_exit), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface arg0, int arg1) {
                                MainActivity.this.finish();
                            }
                        }).setPositiveButton(getResources().getString(R.string.dialog_button_update), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface arg0, int arg1) {
                                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(updateFile.mUpdateUrl));
                                startActivity(i);
                            }
                        }).setCancelable(true).create();

                CustomDialog.show(dialog);
            }
    }
    private void grantPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if(ContextCompat.checkSelfPermission(this , Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_WRITE_EXTERNAL_STORAGE);

            }

        }
    }

    @TargetApi(17)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        new CheckUpdate(this).execute();


/*
        DialogProperties properties = new DialogProperties();
        properties.selection_mode = DialogConfigs.SINGLE_MODE;
        properties.selection_type = DialogConfigs.FILE_SELECT;
        properties.root = new File(DialogConfigs.STORAGE_DIR+"sdcard");
        properties.error_dir = new File(DialogConfigs.DEFAULT_DIR);
        properties.offset = new File(DialogConfigs.DEFAULT_DIR);
        properties.extensions = null;
        FilePickerDialog dialogg = new FilePickerDialog(MainActivity.this,properties);;
        dialogg.setTitle("Select a File");
        dialogg.show();

*/




        setupView();
        setupToolbar();
        setupDrawer();
        handleSavedInstance(savedInstanceState);
        bindBankService();

        //If the Activity is Created for the First Time
        if(savedInstanceState==null) {
            //Show Message if Any Available Only First Time Entering Activity
            if(getIntent()!=null && getIntent().getExtras()!=null){
                ErrorHandler.ErrorState mSplashResult = (ErrorHandler.ErrorState)getIntent().getExtras().getSerializable(SplashActivity.EXTRA_RESULT);
                if(mSplashResult != null)
                    ErrorHandler.showError(mParentView, mSplashResult);
            }

            //Show Home if Any Bill Available, Else show Add Bill Dialog
            if (BillCollection.get(this).getCurrentBill() != null) {
                selectDrawerItem(mHomeInfo);
            } else {
                DialogFragment addBill = new AddBillDialog();
                addBill.show(getSupportFragmentManager(), "Add_Bill");
            }
        }

        grantPermission();
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == REQUEST_CODE_WRITE_EXTERNAL_STORAGE){
            if(grantResults.length > 0 && grantResults[0] != PackageManager.PERMISSION_GRANTED){
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (!shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)){
                        WaringDialog.showPermission(this, "برای ارسال مدارک باید به برنامه اجازه دسترسی به پوشه فایل بدهید");


                        return;

                    }else{
                        grantPermission();
                    }
                }
            }

        }

    }

    private void handleSavedInstance(Bundle savedInstanceState){
        /**
         * If the Activity is just Recreated:
         * -Drawer Open/Close State Is Self-Retained
         * -Drawer Selected Item Is Self-Retained
         * -We Have to Restore Drawer Account Setting State
         * -Also We have To save CurrentItemID for swapping Drawer Sub Menus
         **/
        if(savedInstanceState==null){
            isAccSettingOpen = false;
            mCurrentItemId = R.id.home_info;
        } else{
            isAccSettingOpen = savedInstanceState.getBoolean(ACC_SETTING_VALUE_KEY);
            mCurrentItemId = savedInstanceState.getInt(CURRENT_ITEM_VALUE_KEY);
        }

        syncDrawerState();
    }

    private void setupView(){
        //Define Main Views
        mParentView = findViewById(R.id.main_content);
        mSwipeRefresh = (SwipeRefreshLayout)findViewById(R.id.swipeContainer);
        mSwipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //new FetchBills(MainActivity.this, BillCollection.get(MainActivity.this).getCurrentBill().mBillId).execute();
                new GetBills(MainActivity.this, BillCollection.get(MainActivity.this).getCurrentBill().mBillId).execute();
            }
        });

        //Setup Layout Transition
        //LayoutTransition layoutTransition = new LayoutTransition();
        //layoutTransition.enableTransitionType(LayoutTransition.CHANGING);
        //mSwipeRefresh.setLayoutTransition(layoutTransition);
    }

    //region Setup Drawer Operations
    private void setupToolbar(){
        mAppBar = (AppBarLayout)findViewById(R.id.appbar);
        mToolbar = (Toolbar)findViewById(R.id.toolbar);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    mDrawerLayout.openDrawer(GravityCompat.START);
                }
            }
        });
        setSupportActionBar(mToolbar);
    }

    private void setupDrawer(){
        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.app_name, R.string.app_name){
            public void onDrawerClosed(View view) {
                isAccSettingOpen = false;
                syncDrawerState();
            }
            public void onDrawerOpened(View view){
                if(IntroActivity.mBillIdCount>7){
                    Intent intent = new Intent(Intent.ACTION_SENDTO);
                    intent.setData(Uri.parse("mailto:amir.bi80@gmail.com"));
                    intent.putExtra(Intent.EXTRA_SUBJECT, "خدمات مشترکین - گزارش خرابی");
                    mNVDrawer.getMenu().findItem(R.id.contact_me).setIntent(intent);
                    mNVDrawer.getMenu().findItem(R.id.contact_me).setVisible(true);
                }
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        mNVDrawer = (NavigationView)findViewById(R.id.nv_drawer);
        mNVDrawer.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                return selectDrawerItem(item);
            }
        });

        mHomeInfo = mNVDrawer.getMenu().getItem(0);
        setupDrawerHeader();
    }

    public void setupDrawerHeader(){
        mDrawerHeader = mNVDrawer.inflateHeaderView(R.layout.drawer_header);
        mDrawerHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Hide Drawer Tutorial if Being Shown
                if(mDrawerTutorial.getVisibility()!=View.GONE)
                    mDrawerTutorial.setVisibility(View.GONE);

                //Change Drawer Account Setting State and Show Proper View
                isAccSettingOpen = !isAccSettingOpen;
                syncDrawerState();
            }
        });

        mDrawerHeaderContainer = mDrawerHeader.findViewById(R.id.drawer_header_container);
        mDrawerBillTitle = (TextView)mDrawerHeader.findViewById(R.id.drawer_header_bill_title);
        mDrawerBillId = (TextView)mDrawerHeader.findViewById(R.id.drawer_header_bill_id);
        mAccSettingIndicator = (ImageView)mDrawerHeader.findViewById(R.id.drawer_header_indicator);

        //Setup Drawer Tutorial
        mDrawerTutorial = (TextView)mDrawerHeader.findViewById(R.id.drawer_header_tutorial);
        if(TutorialHelper.shouldTutorialShown(TutorialHelper.TUTORIAL_DRAWER)){
            mDrawerTutorial.setVisibility(View.VISIBLE);
            TutorialHelper.tutorialCompleted(TutorialHelper.TUTORIAL_DRAWER);
        } else {
            mDrawerTutorial.setVisibility(View.GONE);
        }


        mDrawerBackground = (ImageView) mDrawerHeader.findViewById(R.id.drawer_header_background);
        mDrawerBackground.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        // gets called after layout has been done but before display
                        // so we can get the height then hide the view
                        syncDrawerHeader();
                        mDrawerBackground.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    }

                });
    }

    private boolean selectDrawerItem(MenuItem item){
        boolean wasMenuItemHandled = true;
        Fragment fragment = null;
        Intent activityIntent;

        //if set to True will Refresh Fragments Instead of Remaking them
        if(false){
            if(item.getItemId()==mCurrentItemId)
            mDrawerLayout.closeDrawer(mNVDrawer);
            refreshFragment();
            return true;
        }

        switch(item.getItemId()){
            case R.id.home_info:
                if(BillCollection.get(this).getCurrentBill()!=null)
                    fragment = HomeFragment.newInstance(BillCollection.get(MainActivity.this).getCurrentBill().mBillId);
                else
                    Snackbar.make(mParentView, "قبضی جهت نمایش موجود نمی باشد.", Snackbar.LENGTH_INDEFINITE)
                            .setActionTextColor(getResources().getColor(R.color.holo_blue_light))
                            .setAction("افزودن قبض", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    DialogFragment addBill = new AddBillDialog();
                                    addBill.show(getSupportFragmentManager(), "Add_Bill");
                                }
                            }).show();
                break;

            case R.id.requests:
                activityIntent = new Intent(this, FragmentActivity.class);
                activityIntent.putExtra(FragmentActivity.EXTRA_FRAGMENT_TYPE, item.getItemId());
                activityIntent.putExtra(FragmentActivity.EXTRA_BILL_ID, BillCollection.get(this).getCurrentBill().mBillId);
                startActivity(activityIntent);
                break;

            case R.id.send_report:
            case R.id.declare_blackout:
            case R.id.declare_usage:
                activityIntent = new Intent(this, FragmentActivity.class);
                activityIntent.putExtra(FragmentActivity.EXTRA_FRAGMENT_TYPE, item.getItemId());
                activityIntent.putExtra(FragmentActivity.EXTRA_BILL_ID, BillCollection.get(this).getCurrentBill().mBillId);
                startActivityForResult(activityIntent, REQUEST_SECONDARY_ACTIONS);
                break;

            case R.id.setting:
            case R.id.contact_us:
                activityIntent = new Intent(this, FragmentActivity.class);
                activityIntent.putExtra(FragmentActivity.EXTRA_FRAGMENT_TYPE, item.getItemId());
                startActivityForResult(activityIntent, REQUEST_DRAWER_OPTIONS);
                break;

            case R.id.add_bill:
                DialogFragment addBill = new AddBillDialog();
                addBill.show(getSupportFragmentManager(), "Add_Bill");
                break;

            case R.id.remove_bill:
                if(BillCollection.get(this).getCurrentBill()!=null){
                    //Remove Confirmation Dialog
                    AlertDialog dialog = new AlertDialog.Builder(this)
                            //.setView(mDialogView)
                            .setTitle(getResources().getString(R.string.dialog_remove_bill_title))
                            .setMessage(getResources().getString(R.string.dialog_remove_bill_body))
                            .setNegativeButton(getResources().getString(R.string.dialog_button_return), null)
                            .setPositiveButton(getResources().getString(R.string.dialog_button_confirm), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface arg0, int arg1) {
                                    BillCollection.get(MainActivity.this).remove(BillCollection.get(MainActivity.this).getCurrentBill());

                                    if(BillCollection.get(MainActivity.this).getBills().size()>0) {
                                        BillCollection.get(MainActivity.this).setCurrentBill(BillCollection.get(MainActivity.this).getBills().get(0));
                                        syncDrawerHeader();
                                        refreshFragment();
                                    }
                                }
                            }).create();

                    CustomDialog.show(dialog);
                }
                break;

            case R.id.manage_bills:
                activityIntent = new Intent(this, FragmentActivity.class);
                activityIntent.putExtra(FragmentActivity.EXTRA_FRAGMENT_TYPE, item.getItemId());
                startActivityForResult(activityIntent, REQUEST_MANAGE_BILLS);
                break;

            default:
                //Show Selected Profile (Dynamically Added at Run Time)
                BillRecord selectedBill = BillCollection.get(MainActivity.this).find(item.getTitle().toString());
                BillCollection.get(MainActivity.this).setCurrentBill(selectedBill);
                syncDrawerHeader();
                refreshFragment();
                wasMenuItemHandled = true;
                break;
        }

        if(fragment!=null) {
            FragmentManager fm = getSupportFragmentManager();

            //Pop Top of Fragment Stack
            if(fm.getBackStackEntryCount()>0)
                fm.popBackStack();

            //Add New Fragment Either Normally or On Top
            if(fragment instanceof HomeFragment)
                fm.beginTransaction().replace(R.id.fragment_container, fragment).commit();
            else
                fm.beginTransaction().replace(R.id.fragment_container, fragment).addToBackStack("SecondLayer").commit();
        }

        if(item.getGroupId()==R.id.drawer_main_actions){
            item.setChecked(true);
            if(item.getItemId()== R.id.home_info) {
                mCurrentItemId = item.getItemId();
                setTitle(item.getTitle());
            }
        }

        mDrawerLayout.closeDrawer(mNVDrawer);
        return wasMenuItemHandled;
    }
    //endregion

    //region Update UI Operations
    public void refreshFragment(){
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if(BillCollection.get(this).getCurrentBill() != null)
            if(currentFragment != null && currentFragment instanceof RefreshableFragment)
                ((RefreshableFragment)currentFragment).onRefreshFragment(BillCollection.get(this).getCurrentBill().mBillId);
    }

    public void setDrawerBackground(){
        if(BillCollection.get(this).getCurrentBill()==null){
            return;
        }

        //Get Background Uri and Show It if Available, Else Hide Background
        String imageUri = BillCollection.get(this).getCurrentBill().mImageUri;
        if (imageUri!=null) {
            mDrawerBackground.setImageURI(Uri.parse(imageUri));
            mDrawerHeaderContainer.setBackgroundResource(R.drawable.bg_gradiant_footer);
        } else{
            mDrawerBackground.setImageResource(R.drawable.bg_drawer_default);
            mDrawerHeaderContainer.setBackgroundResource(android.R.color.transparent);
        }
    }

    private void syncDrawerHeader(){
        if(BillCollection.get(this).getCurrentBill()!=null) {
            if (TextUtils.isEmpty(BillCollection.get(this).getCurrentBill().mBillTitle))
                mDrawerBillTitle.setText("شناسه قبض:");
            else
                mDrawerBillTitle.setText(BillCollection.get(this).getCurrentBill().mBillTitle);

            mDrawerBillId.setText(BillCollection.get(this).getCurrentBill().mBillId);
            setDrawerBackground();
        } else {
            mDrawerBillTitle.setText("جهت افزودن قبض بر روی اینجا کلیک نمایید.");
        }
    }

    public void syncDrawerState(){
        if (!isAccSettingOpen) {
            mAccSettingIndicator.setImageResource(R.drawable.ic_chevron_down_white_48dp);
            mNVDrawer.getMenu().clear();
            mNVDrawer.inflateMenu(R.menu.drawer_items);
            mNVDrawer.setCheckedItem(mCurrentItemId);
        } else {
            mAccSettingIndicator.setImageResource(R.drawable.ic_chevron_up_white_48dp);
            mNVDrawer.getMenu().clear();
            mNVDrawer.inflateMenu(R.menu.drawer_account_setting);

            //Add Accounts
            for(int i = 0 ; i < BillCollection.get(this).getBills().size() ; i++){
                if(BillCollection.get(this).getBills().get(i).equals(BillCollection.get(this).getCurrentBill()))
                    mNVDrawer.getMenu().add(R.id.drawer_accounts, Menu.NONE, Menu.NONE, BillCollection.get(this).getFace().get(i))
                            .setIcon(R.drawable.ic_comment_account_outline_grey600_48dp).setChecked(true);
                else
                    mNVDrawer.getMenu().add(R.id.drawer_accounts, Menu.NONE, Menu.NONE, BillCollection.get(this).getFace().get(i))
                            .setIcon(R.drawable.ic_comment_account_outline_grey600_48dp).setChecked(false);
            }
        }
    }
    //endregion

    public void bindBankService(){
        bankIntent = new Intent("ir.tgbs.peccharge.billing.PecChargeBillingService.BIND");
        bankIntent.setPackage("ir.tgbs.peccharge");
        bindService(bankIntent, new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                billingService = PecChargeBillingService.Stub.asInterface(service);
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                billingService = null;
            }
        }, BIND_AUTO_CREATE);

        //client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    public void payBill(String billId, String payId){
        try{
            Bundle bundle = billingService.getBillPaymentIntent(1, bankIntent.getPackage(), billId, payId, null);
            if (bundle.getInt("RESPONSE_CODE") != 0 && bundle.getString("RESPONSE_MESSAGE") != null) {
                Toast.makeText(this, bundle.getString("RESPONSE_MESSAGE"), Toast.LENGTH_LONG).show();
                return;
            }

            PendingIntent pendingIntent = bundle.getParcelable("INTENT");
            if (pendingIntent != null) {
                //If Current Payment is not Null Another Payment is in Progress
                if(mCurrentPayment!=null) {
                    ErrorHandler.showError(mParentView, "پرداخت دیگری در حال اجرا می باشد");
                    return;
                } else {
                    mCurrentPayment = new PaymentRecord();
                    mCurrentPayment.setBillInfo(billId, payId);
                }

                startIntentSenderForResult(pendingIntent.getIntentSender(), REQUEST_PAY_BILLS_TOP, null, 0, 0, 0);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }
    }

   /* @Override
    protected void onResume() {
        super.onResume();
        Toast.makeText(this, "hello world", Toast.LENGTH_SHORT).show();
    }*/

    public void payBill(final long billId, final long payId, String mobileNo)
    {
        //Show Loading
        if(!mSwipeRefresh.isRefreshing())
            mSwipeRefresh.setRefreshing(true);

        OkHttpClient okHttpClient = new OkHttpClient();
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");

        //Create JSON Body
        JSONObject jsonObject = new JSONObject();
        try{
            jsonObject.put("BillId", billId)
                    .put("PayId", payId)
                    .put("MobileNo", mobileNo);
        } catch (JSONException e){
            e.printStackTrace();
        }

        Request request = new Request.Builder()
                .url(WSHelper.getBaseUrl() + WSHelper.services_custom + WSHelper.method_payment_token)
                .addHeader("Token", SecurityHelper.getToken())
                .post(RequestBody.create(JSON, jsonObject.toString()))
                .build();

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Stop Loading Animation
                        if(mSwipeRefresh.isRefreshing())
                            mSwipeRefresh.setRefreshing(false);

                        //Show Error
                        Toast.makeText(MobileBargh.getContext(), R.string.error_async_connection_failed, Toast.LENGTH_SHORT).show();
                    }
                });
            }


            @Override
            public void onResponse(Call call, Response response) throws IOException
            {
                String mPaymentToken = null;
                if(response.isSuccessful()) {
                    try{
                        //Parse Response
                        JSONObject jsonResponse = new JSONObject(response.body().string());

                        //Get Server Response
                        JSONArray errorJSON = jsonResponse.getJSONArray(CustomError.TAG_ERROR);
                        ArrayList<CustomError> errors = CustomError.parseJSON(errorJSON);

                        //Get Payment Token
                        JSONObject dataJSON = jsonResponse.getJSONObject(CustomError.TAG_DATA).getJSONObject("Data");

                        if(dataJSON != null){
                            mPaymentToken = dataJSON.getString("Token");
                        }
                    } catch (Exception e){
                        e.printStackTrace();
                        Log.e("JSONParser", e.getMessage(), e);
                    }

                    final String paymentToken = mPaymentToken;
                    if(!TextUtils.isEmpty(paymentToken))
                        runOnUiThread(new Runnable()
                        {
                            @Override
                            public void run() {
                                //Stop Loading Animation
                                if(mSwipeRefresh.isRefreshing())
                                    mSwipeRefresh.setRefreshing(false);

                                //Show Payment Dialog
                                Intent paymentIntent = new Intent(MainActivity.this, PaymentInitiator.class);
                                paymentIntent.putExtra("Type", "2");
                                paymentIntent.putExtra("Token", paymentToken);
                            //    paymentIntent.putExtra("TSPEnabled",1);
                                MainActivity.this.startActivityForResult(paymentIntent, REQUEST_PAY_BILLS_MPL);
                            }
                        });
                }
            }
        });
    }

    //region Activity LifeCycle Callbacks
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed(){
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
            return;
        }

        if(getSupportFragmentManager().getBackStackEntryCount()>0){
            super.onBackPressed();

            setTitle(mHomeInfo.getTitle());
            mNVDrawer.setCheckedItem(mHomeInfo.getItemId());
        } else {
            //Exit Confirmation Dialog
            AlertDialog dialog = new AlertDialog.Builder(this)
                    //.setView(mDialogView)
                    .setTitle(getResources().getString(R.string.dialog_exit_title))
                    .setMessage(getResources().getString(R.string.dialog_exit_body))
                    .setNegativeButton(getResources().getString(R.string.dialog_button_return), null)
                    .setPositiveButton(getResources().getString(R.string.dialog_button_exit), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                            MainActivity.super.onBackPressed();
                        }
                    }).create();
            CustomDialog.show(dialog);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(ACC_SETTING_VALUE_KEY, isAccSettingOpen);
        outState.putInt(CURRENT_ITEM_VALUE_KEY, mCurrentItemId);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch(requestCode){
            case REQUEST_ADD_BILL:
                ErrorHandler.showError(mParentView,
                        String.format(getString(R.string.error_add_bill_successful), BillCollection.get(this).getCurrentBill().mBillId)
                );

                //Show the Added Account
                syncDrawerHeader();
                selectDrawerItem(mHomeInfo);
                //refreshFragment();

                //Fetch New Bill Data From Network
                mSwipeRefresh.setRefreshing(true);
                //new FetchBills(this, BillCollection.get(this).getCurrentBill().mBillId).execute();
                new GetBills(this, BillCollection.get(this).getCurrentBill().mBillId).execute();
                break;

            case REQUEST_MANAGE_BILLS:
                //Is it Necessary?!
                //BillCollection.get(this).notifyDataSetChanged();
                if(resultCode==RESULT_OK && data!=null)
                    if(data.getBooleanExtra(ManageBillsFragment.EXTRA_IS_EDITED, false)){
                        syncDrawerHeader();
                        refreshFragment();
                        new SyncBills().execute();
                    }
                break;

            case REQUEST_PAY_BILLS_TOP:
                //Call currentFragment to Handle Payment Response
                //mCurrentFragment.onActivityResult(requestCode, resultCode, data);
                if (resultCode == Activity.RESULT_OK) {
                    if(data!=null && data.getStringExtra("RESULT")!=null && !TextUtils.isEmpty(data.getStringExtra("RESULT"))) {
                        String response = data.getStringExtra("RESULT");
                        String mTransactionMessage = "";
                        try {
                            //Get Response From Bank API
                            JSONObject responseJSON = new JSONObject(response);
                            byte Status = (byte) responseJSON.getInt(PaymentRecord.TAG_PAYMENT_Status);
                            mTransactionMessage = responseJSON.getString("Message");

                            //If Payment was Successful Save Payment
                            if(Status==0) {
                                //Get Result Attributes
                                JSONObject dataJSON = responseJSON.getJSONObject("Data");
                                long InvoiceNumber = dataJSON.getLong(PaymentRecord.TAG_PAYMENT_Invoice_Number);
                                int TraceNo = dataJSON.getInt(PaymentRecord.TAG_PAYMENT_Trace_Number);
                                int Score = dataJSON.getInt(PaymentRecord.TAG_PAYMENT_Score);
                                String payDate = new PersianCalendar().getPersianShortDate();

                                //Save and Register Payment Record
                                mCurrentPayment.setPaymentInfo(Status, InvoiceNumber, TraceNo, Score, payDate);
                                mCurrentPayment.save();
                                new RegisterPayments(mCurrentPayment).execute();

                                //Save Payment as a Temporary Billing Record
                                mCurrentPayment.toBillingHistory().save();

                                //Update UI
                                refreshFragment();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } finally {
                            ErrorHandler.showError(mParentView, mTransactionMessage);
                        }
                    }
                }

                //Close Current Payment if User Cancelled Transaction or Transaction Completed
                mCurrentPayment = null;
                break;

            case REQUEST_PAY_BILLS_MPL:
                Iterator<String> iterator = data.getExtras().keySet().iterator();
                while(iterator.hasNext()){
                    Log.d("MPL Result", iterator.next());
                }

                switch (resultCode){
                    case 1:
                    case 3:
                        Toast.makeText(this, "پرداخت با موفقیت انجام شد.", Toast.LENGTH_SHORT).show();
                        int status = data.getIntExtra("status", 0);
                        String message = data.getStringExtra("message");
                        String dataString = data.getStringExtra("enData");

                        if(!TextUtils.isEmpty(message)){
                            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                        }

                        //Register Payment With Server
                        registerMPLPayment(dataString);
                        break;

                    case 2:
                    case 4:
                        Toast.makeText(this, "مشکل در پرداخت", Toast.LENGTH_SHORT).show();;
                        data.getIntExtra("errorType", 0);
                        break;

                    case 5:
                    case 6:
                        Toast.makeText(this, "مشکل در کتابخانه پرداخت", Toast.LENGTH_SHORT).show();
                        data.getIntExtra("errorType", 0);
                        break;
                }

            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    public void registerMPLPayment(String dataString){

        OkHttpClient okHttpClient = new OkHttpClient();
        Request request = new Request.Builder()
                .url(WSHelper.getBaseUrl() + WSHelper.services_custom + WSHelper.method_payment_register_mpl)
                .addHeader("Token", SecurityHelper.getToken())
                .post(RequestBody.create(MediaType.parse("application/json"), dataString))
                .build();

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                //Show Error
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ErrorHandler.showError(mParentView, e.getMessage());
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(response.isSuccessful()){

                    try{
                        //Parse Response
                        String responseRaw = response.body().string();
                        JSONObject responseJSON = new JSONObject(responseRaw);
                        int TrcNo = responseJSON.getJSONObject("PayInfo").getInt("TrcNo");
                        String BillId   = responseJSON.getJSONObject("BillInfo").getString("BillId");
                        String PayId = responseJSON.getJSONObject("BillInfo").getString("PayId");

                        //Save and Register Payment Record
                        mCurrentPayment = new PaymentRecord();
                        mCurrentPayment.setBillInfo(BillId, PayId);
                        mCurrentPayment.setPaymentInfo((byte)0, 0, TrcNo, 0, new PersianCalendar().getPersianShortDate());
                        mCurrentPayment.save();

                        //Save Payment as a Temporary Billing Record
                        mCurrentPayment.toBillingHistory().save();

                        //Log
                      /*  Answers.getInstance().logPurchase(new PurchaseEvent()
                                .putItemId(mCurrentPayment.mBillId + "/" + mCurrentPayment.mBillId)
                        ); */
                    } catch (JSONException e){
                        e.printStackTrace();
                    }

                    //Update UI
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            refreshFragment();
                        }
                    });
                }
            }
        });

    }
    //endregion

}
