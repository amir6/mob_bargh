package com.andc.mobilebargh.Activities;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.andc.mobilebargh.Fragments.ContactUsFragment;
import com.andc.mobilebargh.Fragments.DeclareBlackoutFragment;
import com.andc.mobilebargh.Fragments.DeclareUsageFragment;
import com.andc.mobilebargh.Fragments.ManageBillsFragment;
import com.andc.mobilebargh.Fragments.PickLocationFragment;
import com.andc.mobilebargh.Fragments.RequestChangeName.IRequestChangeListener;
import com.andc.mobilebargh.Fragments.RequestFragments.ListCategoryFragment;
import com.andc.mobilebargh.Fragments.SendReportFragment;
import com.andc.mobilebargh.Fragments.SettingFragment;
import com.andc.mobilebargh.Models.CurrentUserInfo;
import com.andc.mobilebargh.Models.NewUserInfo;
import com.andc.mobilebargh.Models.PersonalDocumentInfo;
import com.andc.mobilebargh.R;
import com.google.android.gms.maps.model.LatLng;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.andc.mobilebargh.Utility.Util.Constants.REQUEST_CODE_WRITE_EXTERNAL_STORAGE;

/**
 * Created by Esbati on 4/23/2016.
 */
public class FragmentActivity extends AppCompatActivity implements IRequestChangeListener {

    public final static String EXTRA_FRAGMENT_TYPE = "extra_fragment_type";
    public final static String EXTRA_BILL_ID = "extra_bill_id";
    public final static String EXTRA_LOCATION = "extra_location";


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    //Global
    private int mFragmentType = 0;
    private String mBillId;
    private LatLng mLocation;
    private TextView mToolbarTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN)
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);

        setContentView(R.layout.activity_fragment);

        if (getIntent() != null) {
            mFragmentType = getIntent().getIntExtra(EXTRA_FRAGMENT_TYPE, 0);

            if (getIntent().hasExtra(EXTRA_BILL_ID))
                mBillId = getIntent().getStringExtra(EXTRA_BILL_ID);

            if (getIntent().hasExtra(EXTRA_LOCATION))
                mLocation = getIntent().getParcelableExtra(EXTRA_LOCATION);
        }

        Log.d("!Test!","Bill id is : " + mBillId);
        Log.d("!Test!","type fragment is : " + String.valueOf(mFragmentType));


        setupToolbar();
        setupFragment();

    }





    public void setupToolbar() {
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_right_white_24dp);
        setTitle();
    }

    public void setTitle() {
        int titleResId;
        switch (mFragmentType) {
            case R.id.send_report:
                titleResId = R.string.send_report;
                break;

            case R.id.declare_blackout:
                titleResId = R.string.declare_blackout;
                break;

            case R.id.declare_usage:
                titleResId = R.string.declare_usage;
                break;

            case R.id.fragment_pick_location:
                titleResId = R.string.pick_location;
                break;

            case R.id.manage_bills:
                titleResId = R.string.manage_bill_ids;
                break;

            case R.id.setting:
                titleResId = R.string.setting;
                break;

            case R.id.contact_us:
                IntroActivity.mBillIdCount++;
                titleResId = R.string.contact_us;
                break;

            case R.id.requests:
                titleResId = R.string.requests;
                break;

            default:
                titleResId = R.string.manage_bill_ids;
                break;
        }

        //mToolbarTitle.setText(title);
        setTitle(titleResId);
    }

    public void setupFragment() {
        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.fragment_container);

        if (fragment == null)
            switch (mFragmentType) {
                case R.id.send_report:
                    fragment = SendReportFragment.newInstance(mBillId);
                    break;

                case R.id.declare_blackout:
                    fragment = DeclareBlackoutFragment.newInstance(mBillId);
                    break;

                case R.id.declare_usage:
                    fragment = DeclareUsageFragment.newInstance(mBillId);
                    break;

                case R.id.fragment_pick_location:
                    fragment = PickLocationFragment.newInstance(mLocation);
                    break;

                case R.id.manage_bills:
                    fragment = new ManageBillsFragment();
                    break;

               case R.id.requests:
                  // getSupportActionBar().hide();
                   fragment = new ListCategoryFragment();
                 // fragment = new ChangeNameFragment();
                  //fragment = MainFragment.newInstance(mBillId);
                    break;

                case R.id.setting:
                    fragment = new SettingFragment();
                    break;

                case R.id.contact_us:
                    fragment = new ContactUsFragment();
                    break;

                default:
                    fragment = new ManageBillsFragment();
                    break;
            }
        if (fragment != null)
            fm.beginTransaction().replace(R.id.fragment_container, fragment).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        /*MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.request_item, menu);*/
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                break;
            case R.id.first:
              //  setFragment( new MainFragment());
               // setFragment(new ChangeNameFragment());
                break;
            case R.id.second:
                Toast.makeText(getApplicationContext(), "second", Toast.LENGTH_LONG).show();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }



    private void setFragment(Fragment frm) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction frmTrans = fragmentManager.beginTransaction();
        if (frm != null) {
            frmTrans.replace(R.id.fragment_container, frm);
            frmTrans.commit();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void ListenerButtonNextClicked() {

    }

    @Override
    public void ListenerButtonBackClicked() {

    }

    @Override
    public void setNewUserInfo(NewUserInfo newUserInfo) {

    }

    @Override
    public NewUserInfo getNewUserInfo() {
        return null;
    }

    @Override
    public void setCurrentUserInfo(CurrentUserInfo currentUserInfo) {

    }

    @Override
    public CurrentUserInfo getCurrentUserInfo() {
        return null;
    }

    @Override
    public void setPersonalDocumentInfo(PersonalDocumentInfo personalDocumentInfo) {

    }

    @Override
    public PersonalDocumentInfo getPersonalDocumentInfo() {
        return null;
    }

    @Override
    public void setFollowUpCode(String fCode) {

    }

    @Override
    public String getFollowUpCode() {
        return null;
    }
}
