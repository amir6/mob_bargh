package com.andc.mobilebargh.Activities

import android.graphics.Color
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.View
import com.andc.mobilebargh.Fragments.DialogFramgments.WaringDialog
import com.andc.mobilebargh.Fragments.RequestFragments.InvoiceFragment.*
import com.andc.mobilebargh.R
import com.andc.mobilebargh.Utility.Util.Constants.INTENT_DATA_POSITION
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_invoice.*


class InvoiceActivity : DaggerAppCompatActivity(), IRequestInvoice{

    private var selectFragement :Int = 1
    private  var mPosition: Int = 0






    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_invoice)
        window.decorView.layoutDirection = View.LAYOUT_DIRECTION_RTL
        mPosition = intent.getIntExtra(INTENT_DATA_POSITION, 0)
        setIcon(mPosition)
        prepareFrg(1)
        setUpToolbar()

    }
    fun setUpToolbar(){

       setSupportActionBar(toolbar)
        setToolbarTitle()

    }

    private fun setToolbarTitle() {
        var titleResId: Int = 0
        when(mPosition){
            0 -> titleResId = R.string.title_issue_bill
            1 -> titleResId = R.string.title_exmaine_bill
            2 -> titleResId = R.string.title_settlement_bill
            3 -> titleResId = R.string.title_calculation_bill

        }
        setTitle(titleResId)
    }

    override fun onBackPressed() {
        WaringDialog.exitDialog(this, resources.getString(R.string.dialog_exit_body))
    }

    private fun setIcon(position: Int) {
      when(position){
          0 -> {
              icon_header_1.visibility = View.VISIBLE
              icon_header_2.visibility = View.VISIBLE
              icon_header_3.visibility =View.VISIBLE
              img_icon_3.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_invoice_header))
              txt_img_icon_3.text = resources.getString(R.string.ivoice_header_text)

          }
          1 -> {
              icon_header_1.visibility = View.VISIBLE
              icon_header_2.visibility = View.VISIBLE
              icon_header_3.visibility =View.VISIBLE
              img_icon_3.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_calendar_grey600_24dp))
              txt_img_icon_3.text = resources.getString(R.string.declare_usage_declare_date)
              icon_header_4.visibility =View.VISIBLE
              icon_header_5.visibility =View.VISIBLE
          }
          2 -> {

              icon_header_1.visibility = View.VISIBLE
              icon_header_2.visibility = View.VISIBLE
              icon_header_3.visibility =View.VISIBLE
              img_icon_3.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_meter_reading))
              txt_img_icon_3.text = resources.getString(R.string.declare_usage)
              icon_header_4.visibility =View.VISIBLE
              icon_header_5.visibility =View.VISIBLE
          }
          3 -> {

              icon_header_1.visibility = View.VISIBLE
              icon_header_2.visibility = View.VISIBLE
              icon_header_3.visibility =View.VISIBLE
              icon_header_4.visibility = View.VISIBLE
              img_icon_3.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_meter_reading))
              img_icon_4.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_invoice_header))
              txt_img_icon_3.text = resources.getString(R.string.meter_reading_text)
              txt_img_icon_4.text = resources.getString(R.string.invoice_header_text)



          }


      }
    }

    private fun prepareFrg(pos: Int) {
        when (pos) {
            1 -> {
                setFragment(InvoiceReceiveInfoFragment.newInstance(mPosition))
                //                btnBack.setEnabled(false);

                icon_header_1.setBackgroundColor(Color.WHITE)

                selectFragement = 1
            }
            2 -> {
                setFragment(InvoiceBranchInfoFragment.newInstance(mPosition))
                //  btnBack.setEnabled(true);
                img_icon_1!!.setColorFilter(resources.getColor(R.color.green_3))
                icon_header_1.setBackgroundColor(resources.getColor(R.color.holo_primary))
                icon_header_2.setBackgroundColor(Color.WHITE)

                selectFragement = 2
            }
            3 -> {
                if (mPosition == 0)
                    setFragment(InvoiceIssueBillFragment.newInstance())
                else if (mPosition == 1)
                    setFragment(InvoiceDateFragment.newInstance())
                else if (mPosition == 2)
                    setFragment(InvoiceMeterFragment.newInstance())
                else if (mPosition == 3)
                    setFragment(InvoiceMeterFragment.newInstance())
                img_icon_1!!.setColorFilter(resources.getColor(R.color.green_3))
                img_icon_2!!.setColorFilter(resources.getColor(R.color.green_3))
                icon_header_2.setBackgroundColor(resources.getColor(R.color.holo_primary))
                icon_header_3.setBackgroundColor(Color.WHITE)

                selectFragement = 3
            }
            4 -> {
                if(mPosition == 1){
                    setFragment(InvoiceDocumentFragment.newInstance(mPosition))
                }else if (mPosition == 2)
                    setFragment(InvoiceDocumentFragment.newInstance(mPosition))
                else if(mPosition == 3)
                    setFragment(InvoiceIssueBillFragment.newInstance())

                img_icon_1!!.setColorFilter(resources.getColor(R.color.green_3))
                img_icon_2!!.setColorFilter(resources.getColor(R.color.green_3))
                img_icon_3!!.setColorFilter(resources.getColor(R.color.green_3))
                icon_header_3.setBackgroundColor(resources.getColor(R.color.holo_primary))
                icon_header_4.setBackgroundColor(Color.WHITE)
                selectFragement = 4
            }
            5 -> {
                if(mPosition == 1)
                    setFragment(InvoiceTrackingFragment.newInstance())
                else if (mPosition == 2)
                    setFragment(InvoiceTrackingFragment.newInstance())

                img_icon_1!!.setColorFilter(resources.getColor(R.color.green_3))
                img_icon_2!!.setColorFilter(resources.getColor(R.color.green_3))
                img_icon_3!!.setColorFilter(resources.getColor(R.color.green_3))
                img_icon_4!!.setColorFilter(resources.getColor(R.color.green_3))
                icon_header_4.setBackgroundColor(resources.getColor(R.color.holo_primary))
                icon_header_5.setBackgroundColor(Color.WHITE)
                selectFragement = 5
            }

        }
    }

    override fun OnNextClick() {
        when (selectFragement) {
            1 -> prepareFrg(2)
            2 -> prepareFrg(3)
            3 -> prepareFrg(4)
            4 -> prepareFrg(5)
            5 -> prepareFrg(6)
        }
    }

    private fun setFragment(frm: Fragment?) {
        val fragmentManager = supportFragmentManager
        val frmTrans = fragmentManager!!.beginTransaction()
        if (frm != null) {
            frmTrans.replace(R.id.invoice_content, frm)
            frmTrans.commit()
        }
    }
}

interface IRequestInvoice{

    fun OnNextClick()
}