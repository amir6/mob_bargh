package com.andc.mobilebargh.Activities;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageSwitcher;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.Toast;

import com.andc.mobilebargh.Controllers.BillCollection;
import com.andc.mobilebargh.Models.CustomError;
import com.andc.mobilebargh.Networking.Tasks.GetBills;
import com.andc.mobilebargh.R;
import com.andc.mobilebargh.Utility.ErrorHandler;

import java.util.ArrayList;

import dagger.android.support.DaggerAppCompatActivity;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Esbati on 12/13/2015.
 */
public class SplashActivity extends AppCompatActivity implements
        View.OnClickListener, Animation.AnimationListener,
        GetBills.OnAsyncRequestComplete{

    //Constants
    public final static String EXTRA_RESULT = "extra_result";

    //Global Views
    public View mParentView;
    private ImageSwitcher mSplashImage;
    private TextSwitcher mSplashTextWatcher;
    private TextView mSkipSplash;

    //Splash Attributes
    private ErrorHandler.ErrorState mSplashState = ErrorHandler.ErrorState.ConnectingToServer;
    private int mSplashCount;
    private float mSplashNegativeDurationRatio = 0.01f;
    private float mSplashNegativeAlpha = 0.01f;
    private Animation mInAnimation;
    private Animation mOutAnimation;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @TargetApi(17)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);

        //FetchBills mTask = new FetchBills(this, BillCollection.get(this).getBillIds()).execute();;
        new GetBills(this, BillCollection.get(this).getBillIds()).execute();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        setupView();




    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        mInAnimation = AnimationUtils.loadAnimation(SplashActivity.this, android.R.anim.slide_in_left);
        mInAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mSplashImage.startAnimation(mOutAnimation);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        mOutAnimation = AnimationUtils.loadAnimation(SplashActivity.this, android.R.anim.slide_out_right);
        mOutAnimation.setAnimationListener(this);
    }

    private void setupView(){
        mParentView = findViewById(android.R.id.content);

        mSplashImage = (ImageSwitcher)findViewById(R.id.splash_image);
        mSplashImage.setOnClickListener(this);

        mSplashTextWatcher = (TextSwitcher)findViewById(R.id.splash_description);
        mSplashTextWatcher.setInAnimation(this, android.R.anim.slide_in_left);
        mSplashTextWatcher.setOutAnimation(this, android.R.anim.slide_out_right);
        mSplashTextWatcher.setText(mSplashState.getMessage());

        mSkipSplash = (TextView)findViewById(R.id.splash_skip);
        mSkipSplash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moveToMain(ErrorHandler.ErrorState.UpdateProgressing);
            }
        });
    }

    public void updateSplashScreen(ErrorHandler.ErrorState splashState){
        mSplashState = splashState;

        if(mSplashTextWatcher != null)
            mSplashTextWatcher.setText(splashState.getMessage());
    }

    @Override
    public void asyncResponse(ArrayList<CustomError> errors) {
        //Set Proper Message and Move to Main
        mSplashState = ErrorHandler.ErrorState.UpdateSuccessful;
        if(errors==null | errors.size()==0)
            mSplashState = ErrorHandler.ErrorState.ConnectionFailed;
        else
            for (CustomError error:errors)
                if(error.mCode!=200)
                    mSplashState = ErrorHandler.ErrorState.UpdateFailed;

        updateSplashScreen(mSplashState);

        //Start the Main Activity
        android.os.Handler handler = new android.os.Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                moveToMain(mSplashState);
            }
        }, 1000);
    }

    public void moveToMain(ErrorHandler.ErrorState splashState){
        this.finish();
        Intent i = new Intent(this, MainActivity.class);
        i.putExtra(SplashActivity.EXTRA_RESULT, splashState);
        startActivity(i);
    }

    @Override
    public void onClick(View view) {
        //Setup ImageSwitcher Animation On First Click
        if(mSplashCount == 0){
            mSplashImage.setInAnimation(this, android.R.anim.slide_in_left);
            mSplashImage.setOutAnimation(this, android.R.anim.slide_out_right);
            mSplashImage.getInAnimation().setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    mSplashImage.setOnClickListener(null);
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                    Toast.makeText(SplashActivity.this, "Whoos!! Seems Something broke..", Toast.LENGTH_LONG).show();
                }
            });
            mSplashImage.getOutAnimation().setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    mSplashImage.setOnClickListener(SplashActivity.this);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                    Toast.makeText(SplashActivity.this, "Whoos!! Seems Something broke..", Toast.LENGTH_LONG).show();
                }
            });
        }

        if(mSplashCount < 3) {
            mSplashImage.setImageResource(R.drawable.ic_power);
        } else {
            mSplashImage.setOnClickListener(null);

            //Set RepeatAnimations
            mInAnimation.setDuration(400);
            mOutAnimation.setDuration(400);
            mSplashImage.startAnimation(mOutAnimation);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(SplashActivity.this, "Whoops!! Something seems broken..", Toast.LENGTH_SHORT).show();
                }
            }, 1000);
        }
        mSplashCount++;
    }

    @Override
    public void onAnimationStart(Animation animation) {
    }

    @Override
    public void onAnimationEnd(Animation animation) {

        //Increase Splash Animation Speed until it Reaches a Certain Speed
        long animationDuration = animation.getDuration();
        if(animationDuration > 40) {
            mSplashNegativeDurationRatio += new AccelerateInterpolator(0.5f).getInterpolation(mSplashNegativeDurationRatio);
            animationDuration -= mSplashNegativeDurationRatio * 400;
            if(animationDuration < 40)
                animationDuration = 40;

            mInAnimation.setDuration(animationDuration);
            mOutAnimation.setDuration(animationDuration);
        }

        //Set Splash Opacity
        float splashAlpha = mSplashImage.getAlpha();
        mSplashNegativeAlpha += new AccelerateInterpolator(0.8f).getInterpolation(mSplashNegativeAlpha);
        splashAlpha -= mSplashNegativeAlpha;
        if(splashAlpha < 0)
            splashAlpha = 0;

        mSplashImage.setAlpha(splashAlpha);

        //Show Animation Till Splash Image is No Longer Visible
        if(splashAlpha > 0) {
            //mSplashImage.setImageResource(R.drawable.ic_power);
            mSplashImage.startAnimation(mInAnimation);
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    //Show Splash Image and Restore its Value to Default
                    mSplashCount = 0;
                    mSplashNegativeAlpha = 0.01f;
                    mSplashNegativeDurationRatio = 0.01f;
                    mSplashImage.startAnimation(AnimationUtils.loadAnimation(SplashActivity.this, android.R.anim.fade_in));
                    mSplashImage.setAlpha(1);
                    mSplashImage.setOnClickListener(SplashActivity.this);
                    Toast.makeText(SplashActivity.this, "Just Kidding!", Toast.LENGTH_SHORT).show();
                }
            }, 1500);
        }
    }

    @Override
    public void onAnimationRepeat(Animation animation) {
    }
}
