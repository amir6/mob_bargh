package com.andc.mobilebargh.Activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.RadioGroup;

import com.andc.mobilebargh.Controllers.BillCollection;
import com.andc.mobilebargh.Controllers.PreferencesHelper;
import com.andc.mobilebargh.Controllers.SecurityHelper;
import com.andc.mobilebargh.Fragments.IntroFragments.AuthFragment;
import com.andc.mobilebargh.Fragments.IntroFragments.IntroFragment;
import com.andc.mobilebargh.Fragments.IntroFragments.LoginFragment;
import com.andc.mobilebargh.Models.IntroductionItems;
import com.andc.mobilebargh.Models.PaymentRecord;
import com.andc.mobilebargh.Models.UpdateFile;
import com.andc.mobilebargh.Networking.Tasks.CheckUpdate;
import com.andc.mobilebargh.Networking.Tasks.RegisterPayments;
import com.andc.mobilebargh.Networking.Tasks.SyncBills;
import com.andc.mobilebargh.R;
import com.andc.mobilebargh.Utility.CustomDialog;

import java.util.ArrayList;

import dagger.android.support.DaggerAppCompatActivity;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by win on 11/16/2015.
 */
public class IntroActivity extends AppCompatActivity implements CheckUpdate.OnAsyncRequestComplete{

    //Constants
    public static int mBillIdCount = 0;

    //View Pager
    public View mParentView;
    private AppBarLayout mAppbar;
    private Toolbar mToolbar;
    private ViewPager mPager;
    private RadioGroup mPageIndicator;
    private LoginPagerAdapter mPagerAdapter;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    public void asyncResponse(final UpdateFile updateFile) {
        if(updateFile==null)
            return;

        if(updateFile.mVersionCode>PreferencesHelper.getCurrentVersion())
            if(updateFile.mUpdateType==UpdateFile.UpdateType.Optional){
                //If Update is Optional, Promote User Only when Auto Update is Enabled
                if(PreferencesHelper.isOptionActive(PreferencesHelper.KEY_AUTO_UPDATE))
                    CheckUpdate.promoteUpdate(this, updateFile);
            } else {
                //If Update is Mandatory, Don't Let User Inside Before Updating
                AlertDialog dialog = new AlertDialog.Builder(this)
                        //.setView(mDialogView)
                        .setTitle("بروزرسانی برنامه")
                        .setMessage(updateFile.mUpdateMsg)
                        .setNegativeButton(getResources().getString(R.string.dialog_button_exit), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface arg0, int arg1) {
                                IntroActivity.this.finish();
                            }
                        }).setPositiveButton(getResources().getString(R.string.dialog_button_update), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface arg0, int arg1) {
                                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(updateFile.mUpdateUrl));
                                startActivity(i);
                            }
                        }).setCancelable(false).create();

                CustomDialog.show(dialog);
            }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        //If Token is Valid Skip Intro Activity
        if(SecurityHelper.isTokenValid()){

            //Sync Data If Needed
            if(PreferencesHelper.isOptionActive(PreferencesHelper.KEY_SYNC_NECESSARY))
                new SyncBills().execute();

            //Register any Unregistered Payment Record
            ArrayList<PaymentRecord> mUnregisteredPayments = (ArrayList)PaymentRecord.listAll(PaymentRecord.class);
            for(PaymentRecord mPaymentRecord:mUnregisteredPayments)
                if(!mPaymentRecord.isRegistered)
                    new RegisterPayments(mPaymentRecord).execute();

            enterApplication();
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        //new CheckUpdate(this).execute();

        mParentView = findViewById(R.id.parent_view);
        setupToolbar();
        setUpPager();

        //If User Registered Cellphone Move to Login Page and Show Auth Page
        if(!TextUtils.isEmpty(PreferencesHelper.load(PreferencesHelper.KEY_CELLPHONE))){
            mPager.setCurrentItem(mPagerAdapter.getCount() - 1);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, new AuthFragment())
                    .addToBackStack("auth_fragment")
                    .commit();
        }
    }

    private void setupToolbar(){
        mAppbar = (AppBarLayout)findViewById(R.id.appbar);
        mToolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                } else {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                }
            }
        });
    }

    private void setUpPager(){
        mPageIndicator = (RadioGroup)findViewById(R.id.navigation_indicator);
        mPagerAdapter = new LoginPagerAdapter(getSupportFragmentManager());
        mPager = (ViewPager)findViewById(R.id.pager);
        mPager.setAdapter(mPagerAdapter);
        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        mPageIndicator.check(R.id.indicator_intro);
                        toggleVisibility(mPageIndicator, View.VISIBLE);
                        toggleVisibility(mAppbar, View.INVISIBLE);
                        break;
                    case 1:
                        mPageIndicator.check(R.id.indicator_features1);
                        toggleVisibility(mPageIndicator, View.VISIBLE);
                        toggleVisibility(mAppbar, View.INVISIBLE);
                        break;
                    case 2:
                        mPageIndicator.check(R.id.indicator_features2);
                        toggleVisibility(mPageIndicator, View.VISIBLE);
                        toggleVisibility(mAppbar, View.INVISIBLE);
                        break;
                    case 3:
                        mPageIndicator.check(R.id.indicator_features3);
                        toggleVisibility(mPageIndicator, View.VISIBLE);
                        toggleVisibility(mAppbar, View.INVISIBLE);
                        break;
                    default:
                        toggleVisibility(mPageIndicator, View.INVISIBLE);
                        toggleVisibility(mAppbar, View.VISIBLE);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void toggleVisibility(View view, int visibility){
        if (view.getVisibility() != visibility){
            if(visibility==View.VISIBLE)
                view.startAnimation(AnimationUtils.loadAnimation(this, android.R.anim.fade_in));
            else
                view.startAnimation(AnimationUtils.loadAnimation(this, android.R.anim.fade_out));

            view.setVisibility(visibility);
        }
    }

    public void enterApplication(){
        //Move to Splash Screen if Requested and Any Bills Available to Load, Else Skip to Main Activity
        Intent i;

        if(PreferencesHelper.isOptionActive(PreferencesHelper.KEY_SPLASH_LOADING) && BillCollection.get(this).getBills()!= null && BillCollection.get(this).getBills().size()>0)
            i = new Intent(this, SplashActivity.class);
        else
            i = new Intent(this, MainActivity.class);

        startActivity(i);
        this.finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                this.onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public class LoginPagerAdapter extends FragmentPagerAdapter{

        public LoginPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return IntroductionItems.getSize() + 1;
        }

        @Override
        public Fragment getItem(int position){
            //Show Login Page If we are on the Last Page
            if(position == getCount()-1) {
                return new LoginFragment();
            } else{
                IntroductionItems.Items introItem = IntroductionItems.Items.values()[position];
                Fragment introFragment = new IntroFragment().newInstance(introItem.getImageResId(),
                        introItem.getTitleResId(),
                        introItem.getDescriptionResId());

                return introFragment;
            }
        }
    }
}
