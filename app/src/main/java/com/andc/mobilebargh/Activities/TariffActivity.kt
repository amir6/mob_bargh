package com.andc.mobilebargh.Activities

import android.graphics.Color
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.ContextMenu
import android.view.View
import com.andc.mobilebargh.Fragments.DialogFramgments.WaringDialog
import com.andc.mobilebargh.Fragments.RequestFragments.TariffChangeFragmemts.*
import com.andc.mobilebargh.R
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_tariff.*


class TarrifActivity: DaggerAppCompatActivity(), IRequestTariff  {

    private var selectFragement :Int = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tariff)
        window.decorView.layoutDirection = View.LAYOUT_DIRECTION_RTL
        setIcon()
        prepareFrg(1)
        setUpToolbar()
    }
    fun setUpToolbar(){

        setSupportActionBar(toolbar)
        setTitle(R.string.tariff_header_text)

    }

    private fun setIcon() {
        icon_header_1.visibility = View.VISIBLE
        icon_header_2.visibility = View.VISIBLE
        icon_header_3.visibility = View.VISIBLE
        icon_header_4.visibility = View.VISIBLE
        icon_header_5.visibility = View.VISIBLE
        img_icon_3.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.baseline_attach_money_black_36))
        txt_img_icon_3.text = resources.getString(R.string.tariff_header_text)
    }

    private fun setFragment(frm: Fragment?) {
        val fragmentManager = supportFragmentManager
        val frmTrans = fragmentManager!!.beginTransaction()
        if (frm != null) {
            frmTrans.replace(R.id.invoice_content, frm)
            frmTrans.commit()
        }
    }

    override fun onNextClick() {
        when (selectFragement) {
            1 -> prepareFrg(2)
            2 -> prepareFrg(3)
            3 -> prepareFrg(4)
            4 -> prepareFrg(5)
            5 -> prepareFrg(6)
        }
    }
    override fun onBackPressed() {
        WaringDialog.exitDialog(this, resources.getString(R.string.dialog_exit_body))
    }
   private fun prepareFrg(pos:Int){
        when(pos){
            1 -> {
                setFragment(TarrifReceiveInfoFragment.newInstance())
                icon_header_1.setBackgroundColor(Color.WHITE)
                selectFragement = 1
            }
            2 -> {
               setFragment( TariffBranchInfoFragments.newInstance())
                img_icon_1!!.setColorFilter(resources.getColor(R.color.green_3))
                icon_header_1.setBackgroundColor(resources.getColor(R.color.holo_primary))
                icon_header_2.setBackgroundColor(Color.WHITE)
                selectFragement = 2

            }
            3 -> {

               setFragment(TariffChangeInfoFragment.newInstance())
                img_icon_2!!.setColorFilter(resources.getColor(R.color.green_3))
                icon_header_2.setBackgroundColor(resources.getColor(R.color.holo_primary))
                icon_header_3.setBackgroundColor(Color.WHITE)
                selectFragement = 3
            }

            4 ->{

               setFragment( TariffDocumentFragment.newInstance())
                img_icon_3!!.setColorFilter(resources.getColor(R.color.green_3))
                icon_header_3.setBackgroundColor(resources.getColor(R.color.holo_primary))
                icon_header_4.setBackgroundColor(Color.WHITE)
                selectFragement = 4

            }
            5 -> {
                setFragment(TariffTrackingFragment.newInstance())
                img_icon_4!!.setColorFilter(resources.getColor(R.color.green_3))
                icon_header_4.setBackgroundColor(resources.getColor(R.color.holo_primary))
                icon_header_5.setBackgroundColor(Color.WHITE)
                selectFragement = 5
            }


        }


    }

}

interface  IRequestTariff{
    fun onNextClick()
}