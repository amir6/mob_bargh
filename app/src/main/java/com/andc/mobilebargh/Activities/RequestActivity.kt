package com.andc.mobilebargh.Activities


import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import com.andc.mobilebargh.Fragments.DialogFramgments.WaringDialog
import com.andc.mobilebargh.Fragments.RequestFragments.SupportRequestFragments.RequestReceiveInfoFragment
import com.andc.mobilebargh.Fragments.RequestFragments.SupportRequestFragments.SupportBranchInfoFragment
import com.andc.mobilebargh.Fragments.RequestFragments.SupportRequestFragments.SupportDocumentsFragment
import com.andc.mobilebargh.Fragments.RequestFragments.SupportRequestFragments.SupportTrackingFragment
import com.andc.mobilebargh.R
import com.andc.mobilebargh.Utility.Util.Constants.INTENT_DATA_POSITION
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_request.*


class RequestActivity: DaggerAppCompatActivity(), IRequest {


    private var selectFragement :Int = 1
    private  var mPposition: Int = 0



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_request)
        window.decorView.layoutDirection = View.LAYOUT_DIRECTION_RTL
        mPposition = intent.getIntExtra(INTENT_DATA_POSITION, 0)
        prepareFrg(1)
        setUpToolbar()


    }
    fun setUpToolbar(){

        setSupportActionBar(toolbar)
        setToolbarTitle()

    }

    private fun setToolbarTitle() {
        var titleResId: Int = 0
        when(mPposition){
            0 -> titleResId = R.string.disconnect_reconnect_temporary
            1 -> titleResId = R.string.collect_install_branch
            2 -> titleResId = R.string.collect_branch_permanent
            3 -> titleResId = R.string.move_meter_branch
            4 -> titleResId = R.string.meter_test_branch
            5 -> titleResId = R.string.correction_service_branch

        }
        setTitle(titleResId)
    }

    override fun onBackPressed() {
        WaringDialog.exitDialog(this, resources.getString(R.string.dialog_exit_body))
    }

    private fun setFragment(frm: Fragment?) {
        val fragmentManager = supportFragmentManager
        val frmTrans = fragmentManager!!.beginTransaction()
        if (frm != null) {
            frmTrans.replace(R.id.request_content, frm)
            frmTrans.commit()
        }
    }

    override fun OnNextClick() {
        when (selectFragement) {
            1 -> prepareFrg(2)
            2 -> prepareFrg(3)
            3 -> prepareFrg(4)
            4 -> prepareFrg(5)
            5 -> prepareFrg(6)
        }
    }

    private fun prepareFrg(pos: Int) {
        when (pos) {
            1 -> {
                setFragment(RequestReceiveInfoFragment.newInstance(mPposition))
                //                btnBack.setEnabled(false);
                lay_img_curr_user.setBackgroundColor(Color.WHITE)
                img_branch_info!!.setColorFilter(Color.BLACK)
                img_doc!!.setColorFilter(Color.BLACK)
                img_track_ign!!.setColorFilter(Color.BLACK)
                selectFragement = 1
            }
            2 -> {
                setFragment(SupportBranchInfoFragment.newInstance())
                //  btnBack.setEnabled(true);

                img_curr_user!!.setColorFilter(resources.getColor(R.color.green_3))
                lay_img_curr_user!!.setBackgroundColor(resources.getColor(R.color.holo_primary))
                lay_img_branch_info!!.setBackgroundColor(Color.WHITE)
                img_doc!!.setColorFilter(Color.BLACK)
                img_track_ign!!.setColorFilter(Color.BLACK)
                selectFragement = 2
            }
            3 -> {
                setFragment(SupportDocumentsFragment.newInstance())
                img_curr_user!!.setColorFilter(resources.getColor(R.color.green_3))
                img_branch_info!!.setColorFilter(resources.getColor(R.color.green_3))
                lay_img_branch_info.setBackgroundColor(resources.getColor(R.color.holo_primary))
                lay_img_doc!!.setBackgroundColor(Color.WHITE)
                img_track_ign!!.setColorFilter(Color.BLACK)
                selectFragement = 3
            }
            4 -> {
                setFragment(SupportTrackingFragment.newInstance())
                img_curr_user!!.setColorFilter(resources.getColor(R.color.green_3))
                img_branch_info!!.setColorFilter(resources.getColor(R.color.green_3))
                img_doc!!.setColorFilter(resources.getColor(R.color.green_3))
                lay_img_doc.setBackgroundColor(resources.getColor(R.color.holo_primary))
                lay_img_track_ign!!.setBackgroundColor(Color.WHITE)
                selectFragement = 4
            }

        }
    }



}
interface IRequest{

    fun OnNextClick()
}

