package com.andc.mobilebargh.Activities


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import com.andc.mobilebargh.Fragments.RequestFragments.EnquiryRequestFragment.EnquiryReceiveInfoFagment
import com.andc.mobilebargh.R
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_enquiry.*

class EnquiryActivity: DaggerAppCompatActivity() {





    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enquiry)
        window.decorView.layoutDirection = View.LAYOUT_DIRECTION_RTL
        setFragment(EnquiryReceiveInfoFagment.newInstance())
        setUptoolbar()

    }

    private fun setUptoolbar() {
        setSupportActionBar(toolbar)
        setTitle(R.string.enquiry_header_text)
    }

    private fun setFragment(frm: Fragment?) {
        val fragmentManager = supportFragmentManager
        val frmTrans = fragmentManager!!.beginTransaction()
        if (frm != null) {
            frmTrans.replace(R.id.enquiry_container, frm)
            frmTrans.commit()
        }
    }
}