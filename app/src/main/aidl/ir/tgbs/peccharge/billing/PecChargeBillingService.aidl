package ir.tgbs.peccharge.billing;

import android.os.Bundle;

/**
 * Created by <a href="http://about.me/kh.bakhtiari">Khaled Bakhtiari</a> on 1/3/2016.
 *
 * @author Khaled Bakhtiari
 */
interface PecChargeBillingService {
    int isBillingSupported(int apiVersion, String packageName, String type);
    Bundle getPurchaseIntent(int apiVersion, String packageName, String posPin, int price, in Bundle purchaseBundle);
    Bundle getChargeIntent(int apiVersion, String packageName, String cellNumber, int type, int price, in Bundle ChargeBundle);
    Bundle getBillPaymentIntent(int apiVersion, String packageName, String billId, String paymentId, in Bundle billBundle);
}
